package co.questin.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionMenu;

import co.questin.R;
import co.questin.activities.EmergencyContacts;
import co.questin.activities.NewsAnonocment;
import co.questin.settings.EmailUs;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;
import co.questin.weathersection.Function;

import static co.questin.fcm.MessagingService.college_news;

/**
 * Created by HP on 4/14/2018.
 */

public class ParentDashBoard extends BaseFragment {

    RelativeLayout layoutrelay;
    ImageView mainheader,imageView3,wetherIcon;
    ImageButton news,emergencyicon;
    Button shareapp,providefeedback;
    TextView badge_notification;
    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField;
    FloatingActionMenu materialDesignFAM;

    public static int[] CollageInformationImages = {
            R.mipmap.aboutusmain,
            //  R.mipmap.courses_programmes,
            R.mipmap.campus_map,
            R.mipmap.important_links,
            R.mipmap.campus_services,/*no new pic*/
            // R.mipmap.facilities,
            R.mipmap.academic_division,/*no new pic*/
            R.mipmap.accomodation,};



    public static int[] StudentCommunityImages = {
            R.mipmap.club_society,
            R.mipmap.media,
            R.mipmap.courses_modules,
            R.mipmap.college_events,
            R.mipmap.faculty,
            R.mipmap.alumni,
            R.mipmap.transport,
            R.mipmap.online_fee
    };



    public static int[] AcademicResourcesImages = {
            R.mipmap.disertation_projects,
            R.mipmap.study_notes,
            R.mipmap.old_question_paper,
            R.mipmap.schemes,

    };

    public static int[] moreResourcesImages = {
            R.mipmap.deals_discounts,
    };


    RecyclerView mRvCollegeInformation,mRvStudentInformation,mRvAcademicFinance,mmoreResource;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dashborad_home_activity, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initLayout(view);

    }

    private void initLayout(View view) {

        mRvCollegeInformation = view.findViewById(R.id.rv_college_information);
        mRvStudentInformation = view.findViewById(R.id.rv_student_community);
        mRvAcademicFinance = view.findViewById(R.id.rv_academic_finance);
        mmoreResource = view.findViewById(R.id.rv_more_resource);
        mainheader = view.findViewById(R.id.mainheader);
        layoutrelay = view.findViewById(R.id.layoutrelay);
        imageView3  = view.findViewById(R.id.imageView3);
        badge_notification =view.findViewById(R.id.badge_notification);
        news = view.findViewById(R.id.news);
        emergencyicon = view.findViewById(R.id.emergencyicon);
        shareapp = view.findViewById(R.id.inviteFriend);
        providefeedback = view.findViewById(R.id.provideFeeddback);
        cityField = view.findViewById(R.id.city_field);
        updatedField = view.findViewById(R.id.updated_field);
        currentTemperatureField = view.findViewById(R.id.current_temperature_field);
        humidity_field = view.findViewById(R.id.humidity_field);
        pressure_field = view.findViewById(R.id.pressure_field);
        wetherIcon = view.findViewById(R.id.wetherIcon);
        if(college_news >0){
            badge_notification.setVisibility(View.VISIBLE);
            badge_notification.setText(String.valueOf(college_news));

        }else if (college_news ==0){
            badge_notification.setVisibility(View.GONE);
        }

        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        materialDesignFAM.setVisibility(View.GONE);

        Function.placeIdTask asyncTask = new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise, String MainIcon) {
                String iconurl ="http://openweathermap.org/img/w/"+MainIcon+ ".png";

                cityField.setText(weather_city);
                updatedField.setText(weather_updatedOn);
                String numWihoutDecimal = String.valueOf(weather_temperature).split("\\.")[0];
                currentTemperatureField.setText(numWihoutDecimal +"°" +" "+ "C");
                humidity_field.setText("Humidity: " + weather_humidity);
                pressure_field.setText("Pressure: " + weather_pressure);

                try {

                    Glide.with(getActivity()).load(iconurl)
                            .placeholder(R.mipmap.clouds).dontAnimate()
                            .fitCenter().into(wetherIcon);
                }catch (Exception e){

                }
            }
        });


        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null) {
            String Lat = String.valueOf(Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat()));
            String   Long = String.valueOf(Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng()));

            asyncTask.execute(Lat, Long); //  asyncTask.execute("Latitude", "Longitude")
            Log.e("TAG", "LatLong " +Lat + Long);


        }
        else {
           // asyncTask.execute("28.7041", "77.1025");


        }


        if (SessionManager.getInstance(getActivity()).getCollage().getType()!=null){
            String Type = SessionManager.getInstance(getActivity()).getCollage().getType();
            if (Type.equals("Schools"))
            {


                CollageInformationImages = new int[]
                        {R.mipmap.aboutusmain,
                                //  R.mipmap.courses_programmes,
                                R.mipmap.campus_map,
                                R.mipmap.important_links,
                                R.mipmap.campus_services};


            }

        }




        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                badge_notification.setVisibility(View.GONE);
                college_news = 0;
                startActivity (new Intent (getActivity(), NewsAnonocment.class));

            }
        });

        emergencyicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent (getActivity(), EmergencyContacts.class));

            }
        });

        if (SessionManager.getInstance(getActivity()).getCollage().getField_group_image() !=null) {
            Log.e("TAG", "getField_group_image: " +SessionManager.getInstance(getActivity()).getCollage().getField_group_image());
            Glide.with(getActivity()).load(SessionManager.getInstance(getActivity()).getCollage().getField_group_image())
                    .placeholder(R.mipmap.header_image).dontAnimate()
                    .fitCenter().into(mainheader);



        }


        if (SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo() !=null) {
            Log.e("TAG", "logo2: " +SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo());
            Glide.with(getActivity()).load(SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo())
                    .placeholder(R.mipmap.appicon).dontAnimate()
                    .fitCenter().into(imageView3);



        }
        mRvCollegeInformation.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        mRvCollegeInformation.setLayoutManager(layoutManager);

        mRvCollegeInformation.setAdapter(new Adapterdashboard(getActivity(),"collegeInfo" ,CollageInformationImages));

        mRvStudentInformation.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutMana = new GridLayoutManager(getActivity(),2);
        mRvStudentInformation.setLayoutManager(layoutMana);
        mRvStudentInformation.setAdapter(new Adapterdashboard(getActivity(),"studentInfo", StudentCommunityImages));



        mRvAcademicFinance.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager2 = new GridLayoutManager(getActivity(),2);
        mRvAcademicFinance.setLayoutManager(layoutManager2);
        mRvAcademicFinance.setAdapter(new Adapterdashboard(getActivity(), "acFinance",AcademicResourcesImages));

        mmoreResource.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager3 = new GridLayoutManager(getActivity(),2);
        mmoreResource.setLayoutManager(layoutManager3);
        mmoreResource.setAdapter(new Adapterdashboard(getActivity(),"mResource",  moreResourcesImages));

        shareapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));
            }
        });

        providefeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(getActivity(), EmailUs.class)
                        .putExtra("TITLE","Provide Feedback");
                startActivity(backIntent);


            }
        });
    }


}
