package co.questin.parent;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.questin.R;
import co.questin.activities.AllAppNotification;
import co.questin.activities.ChangeCollage;
import co.questin.activities.ForceUpdateChecker;
import co.questin.activities.ProfileEdit;
import co.questin.activities.QuestinSite;
import co.questin.activities.StartupGuide;
import co.questin.activities.SwitchYourType;
import co.questin.activities.UserDisplayProfile;
import co.questin.adapters.NavigationItemsAdapter;
import co.questin.calendersection.CalenderMain;
import co.questin.campusfeedsection.AllCampusFeeds;
import co.questin.chat.ChatTabbedActivity;
import co.questin.chat.FriendsListService;
import co.questin.models.NavItems;
import co.questin.settings.AboutUs;
import co.questin.settings.EmailUs;
import co.questin.settings.FAQ;
import co.questin.settings.PrivacyPolicy;
import co.questin.settings.Settings;
import co.questin.settings.TermsAndConditions;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;

import static co.questin.fcm.MessagingService.All_App_Notification;
import static co.questin.fcm.MessagingService.college_classroom;
import static co.questin.fcm.MessagingService.college_feeds;
import static co.questin.fcm.MessagingService.singlechat;

public class ParentMain extends BaseAppCompactActivity implements  NavigationView.OnNavigationItemSelectedListener ,ForceUpdateChecker.OnUpdateNeededListener  {
    Toolbar toolbar;
    TextView mToolbarTitle,toolbar_title;
    ExpandableListView expandableListView;
    DrawerLayout mDrawerlayout;
    TextView mEmail;
    private NavigationItemsAdapter mNavigationItemsAdapter;
    private List<NavItems> listNavigationItem;
    private HashMap<String, List<NavItems>> listDataChild;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Fragment mFragment = null;
    private Class mFragmentClass = null;
    TextView tv_username, tv_useremail,tv_userbatch, tv_usernamechild,badge_notificationCalender,badge_notificationcampus,badge_notificationMsg;
    ImageView profileImage,imageProfilechild,mainheader;
    ImageButton imgbtn_edit_profile,imgbtn_account;
    Dialog imageDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    private TextView badge_notification_icon,bell_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_main);
        Utils.setBadge(this, 0);
        preferences = Utils.getSharedPreference(ParentMain.this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE);
        FirstOneThis();

       ForceUpdateChecker.with(this).onUpdateNeeded( this).check();

        pushFragment(new ParentDashBoard());

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);


        badge_notification_icon = findViewById(R.id.badge_notification_icon);
        bell_icon = findViewById(R.id.bell_icon);


        setSupportActionBar(toolbar);
        if(SessionManager.getInstance(getActivity()).getCollage().getTitle() !=null){

            toolbar_title.setText(SessionManager.getInstance(getApplicationContext()).getCollage().getTitle());
            toolbar_title.setSelected(true);
            getSupportActionBar().setTitle(null);



        }


        if(All_App_Notification >0){
            badge_notification_icon.setVisibility(View.VISIBLE);
            badge_notification_icon.setText(String.valueOf(All_App_Notification));

        }else if (All_App_Notification ==0){
            badge_notification_icon.setVisibility(View.GONE);
        }


        bell_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                All_App_Notification=0;
                badge_notification_icon.setVisibility(View.GONE);
                Intent backIntent = new Intent(ParentMain.this, AllAppNotification.class);
                startActivity(backIntent);


            }
        });

        badge_notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                badge_notification_icon.setVisibility(View.GONE);
                Intent backIntent = new Intent(ParentMain.this, AllAppNotification.class);
                startActivity(backIntent);
            }
        });




        mDrawerlayout = findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerlayout.setDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        expandableListView = findViewById(R.id.nav_expand_listview);

        prepareNavigationList ();
        Setuponbottombar();
       // setupNavigationView();

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.navigation_header_parent, null, false);
        expandableListView.addHeaderView(listHeaderView);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        imageProfilechild  = findViewById(R.id.imageProfilechild);
        tv_usernamechild =findViewById(R.id.tv_usernamechild);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        mainheader = findViewById(R.id.mainheader);
        imgbtn_account=findViewById(R.id.imgbtn_account);
        imgbtn_account.setVisibility(View.VISIBLE);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        displayUserData();
        Utils.init(ParentMain.this);

        if (Utils.getFCMId()==null){
            UpdateFCMTokenToServer();
        }

        imgbtn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(ParentMain.this,ProfileEdit.class);
                startActivity(n);
            }
        });

        imgbtn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(ParentMain.this,SwitchYourType.class);
                startActivity(n);
            }
        });

        imageProfilechild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(ParentMain.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID",SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());
                startActivity(backIntent);


            }
        });


        mNavigationItemsAdapter = new NavigationItemsAdapter (this,
                listNavigationItem,listDataChild);

        expandableListView.setAdapter (mNavigationItemsAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                int len = mNavigationItemsAdapter.getGroupCount();
                for (int i = 0; i < len; i++) {
                    if (i != groupPosition) {
                        expandableListView.collapseGroup(i);
                    }
                }


            }
        });
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener () {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long rowId) {
                selectFragments(groupPosition,10);
                return false;
            }
        });

        expandableListView.setOnChildClickListener (new ExpandableListView.OnChildClickListener () {
            @Override public boolean onChildClick (ExpandableListView parent, View v, int groupPosition,
                                                   int childPosition, long id) {
                selectFragments(groupPosition,childPosition);
                v.setSelected (true);
                return false;
            }
        });


        startService(new Intent(this, FriendsListService.class));

    }


    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private void FirstOneThis() {
        switch (Runfirst){
                    case Constants.ROLE_RUNNING_TRUE :
                        imageDialog = new Dialog(ParentMain.this);
                        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        imageDialog.getWindow().setBackgroundDrawable(
                                new ColorDrawable(Color.TRANSPARENT));
                        imageDialog.setContentView(R.layout.image_overlay_screen);
                        // dialogLogin.setCancelable(false);
                        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                                WindowManager.LayoutParams.MATCH_PARENT);
                        imageDialog.show();
                        ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                        showimage.setImageResource(R.mipmap.onboarding_dashboard);

                        showimage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                imageDialog.dismiss();

                                Utils.getSharedPreference(ParentMain.this).edit()
                                        .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                                Utils.removeStringPreferences(ParentMain.this,"0");

                            }
                        });



                        break;
                    case Constants.ROLE_RUNNING_FALSE :
                        break;


                }




    }





    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);

                Log.d("TAG", "Id's " + getUser().userprofile_id +"..." +getUser().getParentUid());



            }
            if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole())) {
                tv_useremail.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole());
            }
            if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageStudentNamev())) {
                tv_usernamechild.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageStudentNamev());
            }


            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(profileImage);

            }
            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(mainheader);


            }

            if (!TextUtils.isNullOrEmpty(getUser().childImage)) {
                Glide.with(this).load(getUser().childImage)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imageProfilechild);

            }

        }





    }


    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);
        badge_notificationCalender =findViewById(R.id.badge_notificationCalender);
        badge_notificationcampus =findViewById(R.id.badge_notificationcampus);
        badge_notificationMsg =findViewById(R.id.badge_notificationMsg);


        if(singlechat >0){
            badge_notificationMsg.setVisibility(View.VISIBLE);
            badge_notificationMsg.setText(String.valueOf(singlechat));

        }else if (singlechat ==0){
            badge_notificationMsg.setVisibility(View.GONE);
        }

        if(college_classroom >0){
            badge_notificationCalender.setVisibility(View.VISIBLE);
            badge_notificationCalender.setText(String.valueOf(college_classroom));

        }else if (college_classroom ==0){
            badge_notificationCalender.setVisibility(View.GONE);
        }

        if(college_feeds >0){
            badge_notificationcampus.setVisibility(View.VISIBLE);
            badge_notificationcampus.setText(String.valueOf(college_feeds));

        }else if (college_feeds ==0){
            badge_notificationcampus.setVisibility(View.GONE);
        }

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_classroom=0;
                badge_notificationCalender.setVisibility(View.GONE);
                Intent p = new Intent(ParentMain.this,CalenderMain.class);
                startActivity(p);
               /* finish();*/


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_feeds=0;
                badge_notificationcampus.setVisibility(View.GONE);
                Intent y = new Intent(ParentMain.this,AllCampusFeeds.class);
                startActivity(y);


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singlechat=0;
                badge_notificationMsg.setVisibility(View.GONE);
                Intent z = new Intent(ParentMain.this,ChatTabbedActivity.class);
                startActivity(z);



            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(ParentMain.this,ParentProfile.class);
                startActivity(j);


            }
        });


    }

    protected void pushFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.parent_content, fragment);
            ft.commit();
        }
    }



    private void prepareNavigationList () {

        listNavigationItem = new ArrayList<NavItems>();
        listDataChild = new HashMap<String, List<NavItems>> ();

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.community),
                getResources ().getString (R.string.questin)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.colleges),
                getResources ().getString (R.string.colleges)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.learning),
                getResources ().getString (R.string.learning)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.need_help),
                getResources ().getString (R.string.need_help)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.read_more)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.setting),
                getResources ().getString (R.string.setting)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.log_out),
                getResources ().getString (R.string.log_out)));

        List<NavItems> needHelp = new ArrayList<> ();
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.email_us)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq)));

        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq1)));

        List<NavItems> readMore = new ArrayList<> ();
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.about_us)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.term_condition)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.privacy_policy)));

        List<NavItems> empty = new ArrayList<NavItems> ();

        listDataChild.put (listNavigationItem.get (0).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (1).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (2).getNavItemName (), empty);

        listDataChild.put (listNavigationItem.get (3).getNavItemName (), needHelp);
        listDataChild.put (listNavigationItem.get (4).getNavItemName (), readMore);
        listDataChild.put (listNavigationItem.get (5).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (6).getNavItemName (), empty);
    }

    private void selectFragments(int groupPosition,int childPosition){


        switch (groupPosition){
            case 0:
                Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

              /*  Intent i = new Intent(ParentMain.this, QuestinSite.class);
                startActivity(i);*/
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 1:
               // Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                Intent j = new Intent(ParentMain.this, ChangeCollage.class);
                startActivity(j);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 2:
               // Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                Intent k = new Intent(ParentMain.this, QuestinSite.class);
                startActivity(k);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 3:

                switch (childPosition){
                    case 0:


                        Intent backIntent = new Intent(ParentMain.this, EmailUs.class)
                                .putExtra("TITLE"," Email Us");
                        startActivity(backIntent);

                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        startActivity (new Intent (ParentMain.this, FAQ.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, StartupGuide.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 4:
                switch (childPosition){
                    case 0:

                        startActivity (new Intent (ParentMain.this, AboutUs.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        Intent o = new Intent(ParentMain.this, TermsAndConditions.class);
                        startActivity(o);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (ParentMain.this, PrivacyPolicy.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 5:

                startActivity (new Intent (ParentMain.this, Settings.class));
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

            case 6:
                showNoticeDialog ();
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

        }


        try {
            mFragment = (Fragment) mFragmentClass.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager ();
            fragmentManager.beginTransaction ()
                    .replace (R.id.parent_content,mFragment)
                    .commit ();
        } catch (Exception e) {
            e.printStackTrace ();
        }

    }

    private void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ParentMain.this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Logout ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application


                deleteFireBaseToken=new DeleteFireBaseToken();
                deleteFireBaseToken.execute();



            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }


    @Override
    protected void onResume() {
        displayUserData();
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to close this application ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Utils.getSharedPreference(ParentMain.this).edit()
                        .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                Utils.removeStringPreferences(ParentMain.this,"0");

                //if user pressed "yes", then he is allowed to exit from application
                finishAffinity();

            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }




}


