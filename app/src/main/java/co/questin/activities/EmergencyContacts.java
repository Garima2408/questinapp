package co.questin.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.EmergencyAdapter;
import co.questin.models.EmergencyArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class EmergencyContacts extends BaseAppCompactActivity {
    RecyclerView EmerengyList;
    public ArrayList<EmergencyArray> memergencyList;
    private RecyclerView.LayoutManager layoutManager;
    private EmergencyAdapter emgerncyadapter;
    private CollageEmergencyContactDisplay emegrycontactlist = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contacts);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        memergencyList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        EmerengyList = findViewById(R.id.EmerengyList);
        EmerengyList.setHasFixedSize(true);
        EmerengyList.setLayoutManager(layoutManager);


        if(Utils.isInternetConnected(this)){
            emegrycontactlist = new CollageEmergencyContactDisplay();
            emegrycontactlist.execute();
        }else {
            showAlertDialog(getString(R.string.error_responce_internet));

        }





    }

    private class CollageEmergencyContactDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEMERGENCYNO+"?"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);



            } catch (JSONException | IOException e) {

                e.printStackTrace();
                EmergencyContacts.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            emegrycontactlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        for (int i = 0; i < jsonArrayData.length(); i++) {


                            EmergencyArray emeInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), EmergencyArray.class);

                            memergencyList.add(emeInfo);
                            emgerncyadapter = new EmergencyAdapter(EmergencyContacts.this, memergencyList);
                            EmerengyList.setAdapter(emgerncyadapter);


                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            emegrycontactlist = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
