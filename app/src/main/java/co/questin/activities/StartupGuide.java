package co.questin.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import co.questin.R;
import co.questin.utils.BaseAppCompactActivity;

public class StartupGuide extends BaseAppCompactActivity {
    private WebView wv1;
    private ProgressBar progressBar;
     private Button button2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup_guide);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        wv1= findViewById(R.id.webView);

        wv1.setWebViewClient(new MyBrowser());
        progressBar= findViewById(R.id.loading_spinner_t_n_c);
        button2= findViewById(R.id.button12);


        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        showLoading();
        wv1.loadUrl("http://www.questin.mobi/start-up-guide/");
        wv1.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideLoading();
            }
        });

            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    WebView webView=new WebView(StartupGuide.this);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setPluginState(WebSettings.PluginState.ON);

                    //---you need this to prevent the webview from
                    // launching another browser when a url
                    // redirection occurs---

                    wv1= findViewById(R.id.webView);
                    wv1.setWebViewClient(new MyBrowser());

                    wv1.getSettings().setLoadsImagesAutomatically(true);
                    wv1.getSettings().setJavaScriptEnabled(true);
                    wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                    wv1.loadUrl(" http://www.questin.mobi/start-up-guide/");
                    webView.setWebViewClient(new MyBrowser());

                    String pdfURL = "www.questin.co/quest-upload/Product_Description.pdf";
                    webView.loadUrl(
                            "http://docs.google.com/gview?embedded=true&url=" + pdfURL);

                    setContentView(webView);
                }
            });

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}