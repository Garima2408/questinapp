package co.questin.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class OnlyVisiter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_only_visiter);
    }
}
