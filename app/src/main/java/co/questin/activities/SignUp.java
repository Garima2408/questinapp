package co.questin.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class SignUp extends BaseAppCompactActivity implements  View.OnClickListener {
    EditText edt_first_name,edt_last_name,edt_email_address;
    Button btn_signup,btn_send;

    CheckBox chk_term_condition;
    private ProgressSignUp SignuptAuthTask = null;
    String  email,password,cpassword,firstName,LastName;
    boolean flag = true;
    TextView edt_password,edt_confirm_password;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_email_address = findViewById(R.id.edt_email_address);
        edt_password = findViewById(R.id.edt_password);
        edt_confirm_password = findViewById(R.id.edt_confirm_password);
        btn_signup = findViewById(R.id.btn_signup);
        btn_send =findViewById(R.id.btn_send);

        // chk_term_condition = (CheckBox) findViewById(co.questin.R.id.chk_term_condition);

        btn_signup.setOnClickListener(this);
        btn_send.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.btn_signup:
                hideKeyBoard(v);

                attemptToRegister();

                break;

            case R.id.btn_send:
                hideKeyBoard(v);

                Veryfiy();
                break;

        }
    }

    private void Veryfiy() {
        // Showing reCAPTCHA dialog
        SafetyNet.getClient(this).verifyWithRecaptcha("6LeBjz4UAAAAAL3qAhZWHCva8mhGRIUlFmyJup13")
                .addOnSuccessListener(this, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                        Log.d("TAG", "onSuccess");

                        if (!response.getTokenResult().isEmpty()) {

                            // Received captcha token
                            // This token still needs to be validated on the server
                            // using the SECRET key
                          //  verifyTokenOnServer(response.getTokenResult());
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.d("TAG", "Error message: " +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d("TAG", "Unknown type of error: " + e.getMessage());
                        }
                    }
                });
    }









    private void attemptToRegister() {

        // Reset errors.
        edt_first_name.setError(null);
        edt_last_name.setError(null);
        edt_email_address.setError(null);
        edt_password.setError(null);
        edt_confirm_password.setError(null);

        // Store values at the time of the login attempt.
        email = edt_email_address.getText().toString().trim();
        password = edt_password.getText().toString().trim();
        cpassword = edt_confirm_password.getText().toString().trim();
        firstName = edt_first_name.getText().toString().trim();
        LastName = edt_last_name.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isNullOrEmpty(email)) {
            focusView = edt_email_address;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = edt_email_address;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_invalid_email));
        } else if (TextUtils.isNullOrEmpty(password)) {
            // Check for a valid password, if the user entered one.
            focusView = edt_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (TextUtils.isNullOrEmpty(cpassword)) {
            // Check for a valid cpassword, if the user entered one.
            focusView = edt_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (!password.equalsIgnoreCase(cpassword)) {
            // Check for a valid cpassword, if the user entered one.
            focusView = edt_confirm_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_password_match));
        } else if (TextUtils.isNullOrEmpty(firstName)) {
            // check for First Name
            focusView = edt_first_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (TextUtils.isNullOrEmpty(LastName)) {
            // check for Contact No
            focusView = edt_last_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt Registration and focus the first
            // form field with an error.
            if (focusView != null) {
                focusView.requestFocus();
            }
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.

            // showLoading();
            SignuptAuthTask = new ProgressSignUp();
            SignuptAuthTask.execute();
        }
    }




    private class ProgressSignUp extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("name", email)
                    .add("pass", password)
                    .add("mail",email)
                    .add("field_first_name[und][0][value]",firstName)
                    .add("field_last_name[und][0][value]",LastName)
                    .build();




            try {
                String responseData = ApiCall.POST(client, URLS.URL_SINGUP,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                SignUp.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            SignuptAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Intent upanel = new Intent(SignUp.this, SignIn.class);
                        upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(upanel);
                        finish();





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            showAlertDialog(getString(R.string.error_responce));
            SignuptAuthTask = null;
            hideLoading();


        }
    }




}




