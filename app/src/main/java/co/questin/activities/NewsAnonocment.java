package co.questin.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.NewsAnounceAdapter;
import co.questin.models.AnouncementArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class NewsAnonocment extends BaseAppCompactActivity {
    static RecyclerView rv_NewsAndAnounce;
    public static ArrayList<AnouncementArray> mnewsList;
    private RecyclerView.LayoutManager layoutManager;
    private static NewsAnounceAdapter newsanounceadapter;
    private static CollageNewsAanounceDisplay anouncelist = null;
    FloatingActionButton fab;
    static Activity activity;
    private static ProgressBar spinner;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static SwipeRefreshLayout swipeContainer;
    static TextView  ErrorText;
     static ImageView ReloadProgress;
    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_anonocment);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        fab = findViewById(R.id.fab);
        activity = getActivity();
        mnewsList=new ArrayList<>();
        spinner= findViewById(R.id.progressBar);
        swipeContainer = findViewById(R.id.swipeContainer);
        ErrorText=findViewById(R.id.ErrorText);
        ReloadProgress = findViewById(R.id.ReloadProgress);
        layoutManager = new LinearLayoutManager(this);
        rv_NewsAndAnounce = findViewById(R.id.rv_NewsAndAnounce);
        rv_NewsAndAnounce.setHasFixedSize(true);
        rv_NewsAndAnounce.setLayoutManager(layoutManager);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        newsanounceadapter = new NewsAnounceAdapter(rv_NewsAndAnounce, mnewsList ,activity);
        rv_NewsAndAnounce.setAdapter(newsanounceadapter);

        anouncelist = new CollageNewsAanounceDisplay();
        anouncelist.execute();
        inItView();


        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {

            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity (new Intent (NewsAnonocment.this, CreateNewsAnnouncement.class));

                }
            });

            rv_NewsAndAnounce.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {
                        fab.hide();
                    } else if (dy < 0) {
                        fab.show();
                    }
                }
            });





        }else {
            fab.setVisibility(View.GONE);

        }

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FromNewsAndNotification();
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){
                            mShimmerViewContainer.startShimmerAnimation();
                            FromNewsAndNotification();

                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);

                        }

                    }
                }, 1000);
            }
        });




    }


    public static void FromNewsAndNotification() {
        PAGE_SIZE=0;
        isLoading=true;
        swipeContainer.setRefreshing(true);
        anouncelist = new CollageNewsAanounceDisplay();
        anouncelist.execute();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mShimmerViewContainer.stopShimmerAnimation();}

    @Override
    protected void onResume() {
        mShimmerViewContainer.startShimmerAnimation();
        super.onResume();



    }


    private void inItView(){


        swipeContainer.setRefreshing(false);

        rv_NewsAndAnounce.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    newsanounceadapter.addProgress();

                    anouncelist = new CollageNewsAanounceDisplay();
                    anouncelist.execute();


                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    private static class CollageNewsAanounceDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ErrorText.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEANNOUNCEMENTLIST+"?"+"cid="+ SessionManager.getInstance(activity).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });
                newsanounceadapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            anouncelist = null;

            try {
                if (responce != null) {
                    rv_NewsAndAnounce.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        ArrayList<AnouncementArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            AnouncementArray newsInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AnouncementArray.class);
                            arrayList.add(newsInfo);

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_NewsAndAnounce.setVisibility(View.VISIBLE);
                                newsanounceadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                newsanounceadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                newsanounceadapter.addData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                                }

                            isLoading = false;

                        }else{
                            if (PAGE_SIZE==0){
                                rv_NewsAndAnounce.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText(R.string.No_News);
                                swipeContainer.setVisibility(View.GONE);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                newsanounceadapter.removeProgress();

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {


                        Utils.showAlertFragmentDialog(activity,"Information", msg);
                        swipeContainer.setRefreshing(false);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            anouncelist = null;
            swipeContainer.setRefreshing(false);

        }
    }



    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
