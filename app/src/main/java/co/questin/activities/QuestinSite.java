package co.questin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;

import co.questin.R;
import co.questin.adapters.QuestinSiteAdapter;
import co.questin.questinsitecontent.QuestinsiteArticles;
import co.questin.questinsitecontent.QuestinsiteDissertation;
import co.questin.questinsitecontent.QuestinsiteDocuments;
import co.questin.questinsitecontent.QuestinsiteEvents;
import co.questin.questinsitecontent.QuestinsiteGroups;
import co.questin.questinsitecontent.QuestinsiteQnA;

public class QuestinSite extends AppCompatActivity {

    GridView GridView_questin;


    public static int[] QuestinResourcesImages = {
            R.mipmap.groups_community,
            R.mipmap.q_a,
            R.mipmap.events,
            R.mipmap.artclecommunity,
            R.mipmap.document_community,
            R.mipmap.disertation_projects,

    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questinsite);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        GridView_questin = findViewById(R.id.GridView_questin);



        GridView_questin.setAdapter(new QuestinSiteAdapter(QuestinSite.this, QuestinResourcesImages));


        GridView_questin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id){



                switch (position){
                    case 0:

                        startActivity (new Intent(QuestinSite.this, QuestinsiteGroups.class));

                        break;
                    case 1:
                        startActivity (new Intent (QuestinSite.this, QuestinsiteQnA.class));



                        break;
                    case 2:

                        startActivity (new Intent (QuestinSite.this, QuestinsiteEvents.class));


                        break;
                    case 3:
                        startActivity (new Intent (QuestinSite.this, QuestinsiteArticles.class));

                        break;
                    case 4:
                        startActivity (new Intent (QuestinSite.this, QuestinsiteDocuments.class));

                        break;
                    case 5:
                        startActivity (new Intent (QuestinSite.this, QuestinsiteDissertation.class));

                        break;

                }
            }
        });

    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
