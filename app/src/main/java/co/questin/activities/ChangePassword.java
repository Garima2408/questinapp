package co.questin.activities;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ChangePassword extends BaseAppCompactActivity implements View.OnClickListener{

    TextView updatepassword_save;
    String newpassword,oldpassword,reentered;
    private ProgressUpdateUserPassword updatepasswordAuthTask = null;
    TextView  password,con_password,re_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        password = findViewById(R.id.password);
        con_password = findViewById(R.id.con_password);
        re_password = findViewById(R.id.re_password);
        updatepassword_save = findViewById(R.id.updatepassword_save);
        updatepassword_save.setOnClickListener(this);
        updatepassword_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Utils.isInternetConnected(ChangePassword.this)){
                    SaveUserPassword();
                    hideKeyBoard(view);
                }else {
                    showAlertDialog(getString(R.string.error_responce_internet));

                }



            }
        });

    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.updatepassword_save:

                break;


        }
    }



    private void SaveUserPassword() {
        // Reset errors.
        password.setError(null);
        con_password.setError(null);
        re_password.setError(null);

        oldpassword  = password.getText().toString().trim();
        newpassword = con_password.getText().toString().trim();
        reentered  = re_password.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(oldpassword)) {
            focusView = con_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

        } else if (TextUtils.isEmpty(newpassword)) {
            focusView = password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));


        }  else if (TextUtils.isEmpty(reentered)) {
            focusView = re_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }


        if (cancel) {

            focusView.requestFocus();
        } else {

            if (reentered.matches(newpassword)) {

                updatepasswordAuthTask = new ProgressUpdateUserPassword();
                updatepasswordAuthTask.execute();


            }else {
                showSnackbarMessage(getString(R.string.password_dont));

            }






        }
    }








 /*update user password*/


    private class ProgressUpdateUserPassword extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("pass",newpassword)
                    .add("current_pass",oldpassword)
                    .build();




            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_UPDATEPASSWORD+"/"+ SessionManager.getInstance(getActivity()).getUser().getUserprofile_id(),body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                ChangePassword.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            updatepasswordAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject data = responce.getJSONObject("data");

                        AlertDialog.Builder builder = new AlertDialog.Builder(ChangePassword.this);

                        builder.setMessage(R.string.Password_Updated)
                                .setCancelable(false)
                                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            updatepasswordAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
