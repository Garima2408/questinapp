package co.questin.buttonanimation;

public interface OnAnimationEndListener {
    void onAnimationEnd(LikeButton likeButton);
}
