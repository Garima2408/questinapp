package co.questin.driver;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import co.questin.BuildConfig;
import co.questin.R;
import co.questin.Retrofit.ApiInterface;
import co.questin.Retrofit.ServiceGenerator;
import co.questin.models.chat.SendMessageGroupModel;
import co.questin.models.chat.SendMessageModel;
import co.questin.models.etaResponse.DistanceModel;
import co.questin.models.routedetailResponse.RouteDetailResponseModel;
import co.questin.models.routedetailResponse.Waypoint;
import co.questin.models.tracker.StopsModel;
import co.questin.models.tracker.WayPointResponse;
import co.questin.models.tracker.WayPointsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.tracker.GeofenceRegistrationService;
import co.questin.tracker.HttpConnection;
import co.questin.tracker.PathJSONParser;
import co.questin.tracker.TrackerService;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static co.questin.utils.Constants.GEOFENCE_ID_STAN_UNI;

public class TrackerActivity extends BaseAppCompactActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, SharedPreferences.OnSharedPreferenceChangeListener
{
    private GeofencingRequest geofencingRequest;
    private static PendingIntent pendingIntent;
    static Activity activity;
    private SendMessageModel sendMessageModel;
    private SendMessageGroupModel sendMessage;
    private Gson gson;
    static Boolean RemoveData;
    private Handler uiHandler;
    private StopsModel destStop;
    private StopsModel sourceStop;
    private boolean isMapInitialized = false;
    private boolean isRouteDetailsFetched = false;
    private ArrayList<StopsModel> mStopsList;
    private ArrayList<StopsModel> mMyMarkersArray = new ArrayList<StopsModel>();
    private HashMap<Marker, StopsModel> mMarkersHashMap;
    DistanceModel.Duration durationModel;
    WayPointResponse wayPickDropJson=new WayPointResponse();
    ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
    WayPointsArray wayPointsPick=new WayPointsArray();
    WayPointsArray wayPointsDrop=new WayPointsArray();
    private Polyline polyline;
    private PolylineOptions polylineOptions;
    private RouteDetailResponseModel routeDetailResponseModel;
    Boolean RemoveNotification;
    Double CurrentLat, CurrentLng;
    private GoogleMap mMap;
    Location location;
    private boolean isTrackingStarted = false;
    private boolean SendNotification;
    private static boolean isMonitoring = false;
    private String RouteTittle;
    private String AdminId;
    private static String Route_id;
    private String CollageId;
    ImageView traficEnable;
    private static GoogleApiClient googleApiClient;
    private boolean isLocationOk = false;
    private int RC_CHECK_LOCATION_SETTINGS = 0;
    private int RC_FINE_LOCATION_PERMISSION = 1;
    boolean isPressed = false;

    private static final String TAG = "Tracker_Activity";

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private MyReceiver myReceiver;

    // A reference to the service used to get location updates.
    private TrackerService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    // UI elements.
    private Button mRequestLocationUpdatesButton;
    private Button mRemoveLocationUpdatesButton;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TrackerService.LocalBinder binder = (TrackerService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };


    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {

            } else {

            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    private LocationManager mLocationManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myReceiver = new MyReceiver();
        setContentView(R.layout.activity_tracker);
        Intent intent = getIntent();
        Route_id = intent.getStringExtra("ROUTE_ID");
        RouteTittle = intent.getStringExtra("ROUTE_NAME");
        AdminId = intent.getStringExtra("ADMIN_ID");
        CollageId = intent.getStringExtra("COLLAGE_ID");
        gson = new Gson();
        activity = getActivity();

        uiHandler = new Handler();
        mStopsList = new ArrayList<>();
        sendMessageModel = new SendMessageModel();
        sendMessage =new SendMessageGroupModel();
        polylineOptions = new PolylineOptions().geodesic(true).width(5).color(Color.BLUE);
        mMarkersHashMap = new HashMap<>();
        RemoveNotification =true;

        traficEnable =findViewById(R.id.traficEnable);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();



        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            isLocationOk = false;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    RC_FINE_LOCATION_PERMISSION);
        } else {
            checkLocationSettings();
        }



        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Utils.requestingLocationUpdates(this)) {
            if (!checkPermissions()) {
                requestPermissions();
            }
        }
        if (Utils.isInternetConnected(this)) {
            getRouteDetailInfo();
            RemoveData = true;

        }else {
            Toast.makeText(TrackerActivity.this,
                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        getCurrentLocation();


        traficEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPressed){
                    mMap.setTrafficEnabled(true);

                }else if(!isPressed){

                    mMap.setTrafficEnabled(false);

                }

                isPressed = !isPressed; // reverse

            }
        });



    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        googleApiClient.connect();
        Log.d(TAG, "Google Api Client Connected");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        isMonitoring = true;
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (isRouteDetailsFetched && !isMapInitialized) {
            isMapInitialized = true;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(true);
            mMap.getUiSettings().setAllGesturesEnabled(true);
            mMap.getUiSettings().isCompassEnabled();

            initMap();
        }
        getCurrentLocation();


    }


    //Getting current location
    private void getCurrentLocation() {

        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location Curentlocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (Curentlocation != null) {
            //Getting longitude and latitude
            mMap.moveCamera(CameraUpdateFactory
                    .newLatLngZoom(new LatLng(Curentlocation.getLatitude(), Curentlocation.getLongitude()), 16));

        } else {

        }
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);

        mRequestLocationUpdatesButton = findViewById(R.id.StartTrack);
        mRemoveLocationUpdatesButton = findViewById(R.id.StopTrack);
        setButtonsState(Utils.requestingLocationUpdates(this));
        bindService(new Intent(this, TrackerService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);

        mRequestLocationUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.isInternetConnected(TrackerActivity.this)) {
                    checkLocationSettings();

                    if (isLocationOk) {
                        isTrackingStarted = true;
                        getRouteDetail();


                        mService.requestLocationUpdatesOnId(CollageId,RouteTittle, AdminId,Route_id,"TRUE");
                        mService.requestLocationUpdates();

                        if (SendNotification == true) {
                            Toast.makeText(TrackerActivity.this,
                                    "Tracking is already started", Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(TrackerActivity.this,
                                    "Tracking started", Toast.LENGTH_SHORT).show();

                            //NotifyToTheAdmin("has commenced at");
                            NotifyToTheParent("has commenced at","start");

                        }
                    } else {
                /*Toast.makeText(TrackerActivity.this,
                        "Location not enabled or permission not granted", Toast.LENGTH_SHORT).show();*/
                    }

                } else {
                    Toast.makeText(TrackerActivity.this,
                            "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }


            }
        });


        // Restore the state of the buttons when the activity (re)launches.
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.


        mRemoveLocationUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.isInternetConnected(TrackerActivity.this)) {
                    checkLocationSettings();

                    if (isLocationOk) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(TrackerActivity.this);
                        builder.setCancelable(false);
                        builder.setMessage("Do You Want To Stop Tracking");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    mService.removeLocationUpdates();

                                    isTrackingStarted = false;

                                    NotifyToTheAdmin("has stopped");
                                    NotificationManager notificationManager = (NotificationManager) activity.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                    notificationManager.cancel(400);

                                    // NotifyToTheParent("has stopped","end");


                                    Toast.makeText(getApplicationContext(),
                                            "Tracking Stopped", Toast.LENGTH_SHORT).show();

                                }catch (Exception e){

                                }

                                finish();


                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //if user select "No", just cancel this dialog and continue with app
                                dialog.cancel();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();



                    }
                }

                }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,
                new IntentFilter(TrackerService.ACTION_BROADCAST));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        return  PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(R.id.activity_driver),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(TrackerActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(TrackerActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                mService.requestLocationUpdates();
            } else {
                // Permission denied.
                setButtonsState(false);
                Snackbar.make(
                        findViewById(R.id.activity_driver),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }


    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            location = intent.getParcelableExtra(TrackerService.EXTRA_LOCATION);
            if (location != null) {
               /* Toast.makeText(TrackerActivity.this, Utils.getLocationText(location),
                        Toast.LENGTH_SHORT).show();
*/
                if (location !=null){

                    if (CurrentLat !=null){
                        getETA(new LatLng(location.getLatitude(), location.getLongitude()),
                                new LatLng(Double.valueOf(CurrentLat),
                                        Double.valueOf(CurrentLng))
                        );

                    }



                }
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        // Update the buttons state depending on whether location updates are being requested.
        if (s.equals(Utils.KEY_REQUESTING_LOCATION_UPDATES)) {
            setButtonsState(sharedPreferences.getBoolean(Utils.KEY_REQUESTING_LOCATION_UPDATES,
                    false));
        }
    }

    private void setButtonsState(boolean requestingLocationUpdates) {
        if (requestingLocationUpdates) {
            mRequestLocationUpdatesButton.setEnabled(false);
            mRemoveLocationUpdatesButton.setEnabled(true);
            mRequestLocationUpdatesButton.setBackgroundResource(R.drawable.tracking_start);
            mRemoveLocationUpdatesButton.setBackgroundResource(R.drawable.button_round_background);
        } else {
            mRequestLocationUpdatesButton.setEnabled(true);
            mRemoveLocationUpdatesButton.setEnabled(false);
            mRequestLocationUpdatesButton.setBackgroundResource(R.drawable.button_round_background);
            mRemoveLocationUpdatesButton.setBackgroundResource(R.drawable.tracking_stop);
        }
    }




    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do You Want To Stop Tracking?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try{
                    mService.removeLocationUpdates();
                    LocalBroadcastManager.getInstance(TrackerActivity.this).unregisterReceiver(myReceiver);
                    NotifyToTheParent("has stopped","end");

                }
                catch (Exception e){

                }


                Toast.makeText(getApplicationContext(),
                        "Tracking Stopped", Toast.LENGTH_SHORT).show();
                finish();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {



                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }





    private void getRouteDetail() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    ApiCall.setContext(TrackerActivity.this);
                    String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + Route_id);

                    if (response != null) {

                        routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);

                        sourceStop = new StopsModel(Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLat()),
                                Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLng()),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getField_place_name(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldStartTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldDepartureTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldPhoto());

                        destStop = new StopsModel(Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLat()),
                                Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLng()),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getField_place_name(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldStartTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldDepartureTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldPhoto());


                        for (Waypoint waypoint : routeDetailResponseModel.getData().getFieldCollectionWayPoints().getWaypoints()) {
               /*     mStopsList.add(new StopsModel(waypoint.getFieldStartTime(),
                            Double.valueOf(waypoint.getFieldWayPoints().getLat()), Double.valueOf(waypoint.getFieldWayPoints().getLng())));*/

                            mStopsList.add(new StopsModel(Double.valueOf(waypoint.getFieldWayPoints().getLat()),
                                    Double.valueOf(waypoint.getFieldWayPoints().getLng()),
                                    waypoint.getField_place_name(),
                                    waypoint.getFieldStartTime(),
                                    waypoint.getFieldDepartureTime(),
                                    waypoint.getFieldPhoto()));


                        }
                        isRouteDetailsFetched = true;
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (!isMapInitialized) {
                                    isMapInitialized = true;
                                    initMap();

                                }
                            }
                        });
                    }

                }

                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    private void getRouteDetailInfo(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + Route_id);

                    Log.e("response test", response);

                    if (response!=null){

                        routeDetailResponseModel=new RouteDetailResponseModel();
                        routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);

                        sourceStop = new StopsModel(Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLat()),
                                Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLng()),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getField_place_name(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldStartTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldDepartureTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldPhoto());

                        destStop = new StopsModel(Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLat()),
                                Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLng()),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getField_place_name(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldStartTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldDepartureTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldPhoto());





                        for(Waypoint waypoint : routeDetailResponseModel.getData().getFieldCollectionWayPoints().getWaypoints()){

                            mStopsList.add(new StopsModel(Double.valueOf(waypoint.getFieldWayPoints().getLat()),
                                    Double.valueOf(waypoint.getFieldWayPoints().getLng()),
                                    waypoint.getField_place_name(),
                                    waypoint.getFieldStartTime(),
                                    waypoint.getFieldDepartureTime(),
                                    waypoint.getFieldPhoto()));




                        }

                        for (int i = 0; i < mStopsList.size()-1; i++) {


                            CurrentLat=mStopsList.get(i).getLat();
                            CurrentLng=mStopsList.get(i).getLng();
                            String name =mStopsList.get(i).getPlanceName();

                            Log.d(TAG, "run: "+CurrentLat+".."+CurrentLng +".."+name);


                        }


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void initMap() {

        for (int i = 0; i < mStopsList.size(); i++) {
            if (i == mStopsList.size() - 1) {
                String url = getDirectionsUrl(mStopsList.get(i), destStop);
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);
                addStopMarker(mStopsList.get(i));


                continue;
            }
            if (i == 0) {
                String url = getDirectionsUrl(sourceStop, mStopsList.get(0));
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);
                addStopMarker(mStopsList.get(0));

            }
            String url = getDirectionsUrl(mStopsList.get(i), mStopsList.get(i + 1));
            ReadTask downloadTask = new ReadTask();
            downloadTask.execute(url);

            addStopMarker(mStopsList.get(i + 1));
        }

        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(sourceStop.getLat(), sourceStop.getLng()));
        Marker currentMarker = mMap.addMarker(markerOption);
        mMarkersHashMap.put(currentMarker, sourceStop);


        mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter(this, sourceStop));

        MarkerOptions markerOptDes = new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .position(new LatLng(destStop.getLat(), destStop.getLng()));


        Marker currentMarkerDes = mMap.addMarker(markerOptDes);
        mMarkersHashMap.put(currentMarkerDes, destStop);

        mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter(this, destStop));

        polyline = mMap.addPolyline(polylineOptions);

        Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(destStop.getLat(), destStop.getLng()))
                .radius(Constants.GEOFENCE_RADIUS_IN_METERS)
                .strokeColor(Color.RED)
                .strokeWidth(4f));


        Utils.clearPickDropData();

        wayPointsDrop.setLat(""+destStop.getLat());
        wayPointsDrop.setLng(""+destStop.getLng());
        wayPointsDrop.setField_place_name(destStop.getPlanceName());
        wayPointsDrop.setDrop(true);




        // wayPointsArrays.add(wayPointsPick);
        wayPointsArrays.add(wayPointsDrop);
        wayPickDropJson.setWayPointsArrays(wayPointsArrays);


        Utils.savePickAndDrop(TrackerActivity.this,wayPickDropJson);





    }

    private void addStopMarker(StopsModel stopModel) {

        MarkerOptions markerOptDes = new MarkerOptions().position(new LatLng(stopModel.getLat(), stopModel.getLng()))
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.stop));
        Marker currentMarker = mMap.addMarker(markerOptDes);
        mMarkersHashMap.put(currentMarker, stopModel);

        mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter(this, stopModel));

    }

    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        Activity activity;
        StopsModel stopsModel;

        public MarkerInfoWindowAdapter(Activity activity, StopsModel stopsModel) {
            this.activity = activity;
            this.stopsModel = stopsModel;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v = activity.getLayoutInflater().inflate(R.layout.googlemapinfo_window, null);


            StopsModel myMarker = mMarkersHashMap.get(marker);

            TextView tittle = v.findViewById(R.id.tittle);
            TextView pick = v.findViewById(R.id.Arivaltime);
            TextView titleArivaltime = v.findViewById(R.id.titleArivaltime);
            TextView titleDepatureTime = v.findViewById(R.id.titleDepatureTime);
            TextView drop = v.findViewById(R.id.DepatureTime);
            ImageView location = v.findViewById(R.id.pinlocation);
            // location.setImageResource(R.mipmap.stoplocation);
            if (myMarker != null) {
                if (myMarker.getPlanceName() != null) {
                    tittle.setText(myMarker.getPlanceName());
                    pick.setText(myMarker.getStartTime());
                    drop.setText(myMarker.getDepartTime());
                    titleArivaltime.setVisibility(View.VISIBLE);
                    titleDepatureTime.setVisibility(View.VISIBLE);


                } else {
                    tittle.setText("bus");
                    titleArivaltime.setVisibility(View.GONE);
                }


            }

            return v;
        }
    }

    class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", "test" + e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("onPostExecute Task", "test" + result);

            new ParserTask().execute(result);

        }
    }

    class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("parserTask fail", "test");
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            //  Log.e("onPostExecute success","test"+routes.size());

            for (int i = 0; i < routes.size(); i++) {

                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(5);
                polyLineOptions.color(Color.BLUE);
            }

            if (polyLineOptions != null)
                mMap.addPolyline(polyLineOptions);
        }
    }


    private String getDirectionsUrl(StopsModel origin, StopsModel dest) {

        // Origin of route
        String str_origin = "origin=" + origin.getLat() + "," + origin.getLng();

        // Destination of route
        String str_dest = "destination=" + dest.getLat() + "," + dest.getLng();

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + Constants.MAPS_API_KEY;


        return url;
    }


    private void NotifyToTheAdmin(String tracking_is_started) {

        Calendar mcalender = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        String formattedDate2 = sdf2.format(mcalender.getTime());


        sendMessage(sendMessageModel
                .setUids(AdminId)
                .setMessage("Bus for route" + " " + RouteTittle + " " + tracking_is_started + " " + formattedDate2 + " " + "and is being driven by" + " " + SessionManager.getInstance(this).getUser().getUsername()));


    }


    private void NotifyToTheParent(String tracking_is_started, String trigerValue) {

        Calendar mcalender = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        String formattedDate2 = sdf2.format(mcalender.getTime());



        sendMessageToAll(sendMessage
                .setgid(Route_id)
                .settrigger(trigerValue));
    }







    private void sendMessage(final SendMessageModel sendMessageModel) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = RequestBody.create(Constants.JSON,
                        gson.toJson(sendMessageModel, SendMessageModel.class));
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.SEND_MSG, requestBody);

                    if (response != null) {

                        SendNotification = true;
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }




    private void sendMessageToAll(final SendMessageGroupModel sendMessage) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = RequestBody.create(Constants.JSON,
                        gson.toJson(sendMessage, SendMessageGroupModel.class));
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.SEND_MSG_TO_ALL, requestBody);

                    if (response != null) {

                        SendNotification = true;
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void getETA(LatLng origin, LatLng dest) {
        Retrofit distance= ServiceGenerator.getDistance();
        ApiInterface service= distance.create(ApiInterface.class);


        Call<DistanceModel> call= service.distanceMatrix("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origin.latitude+","+origin.longitude+"&destinations="+dest.latitude+","+dest.longitude+
                //           "&departure_time=now&traffic_model=best_guess&key="+Constants.MAPS_API_KEY);
                "&departure_time=now&traffic_model=best_guess"+"&key="+Constants.MAPS_API_KEY);

        call.enqueue(new Callback<DistanceModel>() {
            @Override
            public void onResponse(Call<DistanceModel> call, retrofit2.Response<DistanceModel> response) {

                if(response!=null){
                    try {

                        if (response.body().getRows()!=null && response.body().getRows().size()>0){
                            String distance=response.body().getRows().get(0).getElements().get(0).getDistance().getText();



                            Log.e("distance",response.body().getDestinationAddresses()+""
                                    +distance+"time"
                                    +response.body().getRows().get(0).getElements().get(0).getDuration().getText());
                            durationModel= response.body().getRows().get(0).getElements().get(0).getDuration();


                            String str = distance;
                            String[] splited = str.split("\\s+");
                            String DistanceSplit =  splited[0];
                            Float CalculateRange= Float.valueOf(DistanceSplit);
                            Log.e("distanceSplit", "distance"  +CalculateRange);

                            if(CalculateRange >=0.00 && CalculateRange <= 0.500)//Check if it is in range
                            {
                                CallTheGeofencingMethod();
                                // Toast.makeText(activity, "Start moniter one time" + distance.toString() , Toast.LENGTH_SHORT).show();

                            }





                            if(CalculateRange >=0.00 && CalculateRange <= 0.100)//Check if it is in range
                            {
                                //  RemoveData=false;

                            }






                            if (distance!=null){

                            }
                        }

                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<DistanceModel> call, Throwable t) {

            }
        });
    }
    private void CallTheGeofencingMethod() {

        if (RemoveData == true){
            startGeofencing();




        }else {
            // stopGeoFencing();

        }
    }

    private void startGeofencing() {
        Log.d(TAG, "Start geofencing monitoring call");
        pendingIntent = getGeofencePendingIntent();
        RemoveData=false;
        geofencingRequest = new GeofencingRequest.Builder()
                .setInitialTrigger(Geofence.GEOFENCE_TRANSITION_ENTER)
                .addGeofence(getGeofence())
                .build();

        if (!googleApiClient.isConnected()) {
            Log.d(TAG, "Google API client not connected");
        } else {
            try {
                LocationServices.GeofencingApi.addGeofences(googleApiClient, geofencingRequest, pendingIntent).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                            Log.d(TAG, "Successfully Geofencing Connected");
                        } else {
                            Log.d(TAG, "Failed to add Geofencing " + status.getStatus());
                        }
                    }
                });
            } catch (SecurityException e) {
                Log.d(TAG, e.getMessage());
            }
        }
        isMonitoring = true;
    }

    @NonNull
    private Geofence getGeofence() {

        WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(TrackerActivity.this);

        Double latiii = Double.valueOf(wayPointResponse.getWayPointsArrays().get(0).getLat());
        Double longg = Double.valueOf(wayPointResponse.getWayPointsArrays().get(0).getLng());

        Log.d(TAG, "Google"+latiii+"..." +longg);

        return new Geofence.Builder()
                .setRequestId(GEOFENCE_ID_STAN_UNI)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setCircularRegion(latiii, longg, Constants.GEOFENCE_RADIUS_IN_METERS)
                .setNotificationResponsiveness(1000)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();


    }

    private static PendingIntent getGeofencePendingIntent() {
        if (pendingIntent != null) {
            return pendingIntent;
        }
        Intent intent = new Intent(activity, GeofenceRegistrationService.class);
        intent.putExtra("ROUTE_ID",Route_id);
        intent.putExtra("RemoveData",RemoveData);

        return PendingIntent.getService(activity, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
    }

    public static void stopGeoFencing() {
        pendingIntent = getGeofencePendingIntent();
        LocationServices.GeofencingApi.removeGeofences(googleApiClient, pendingIntent)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess())
                            Log.d(TAG, "Stop geofencing");
                        else
                            Log.d(TAG, "Not stop geofencing");
                    }
                });
        isMonitoring = false;
    }




    private void checkLocationSettings() {


        LocationRequest mLocationRequest = Utils.createLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        client.checkLocationSettings(builder.build())
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        isLocationOk = true;
                        Log.e(TAG, "onSuccess: " + locationSettingsResponse.toString());
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case CommonStatusCodes.RESOLUTION_REQUIRED:
                                ResolvableApiException resolvable = (ResolvableApiException) e;
                                try {
                                    resolvable.startResolutionForResult(TrackerActivity.this, RC_CHECK_LOCATION_SETTINGS);
                                } catch (IntentSender.SendIntentException e1) {
                                    e1.printStackTrace();
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "onFailure: can't change settings");
                                isLocationOk = false;
                                break;
                        }
                    }
                });
    }


}
