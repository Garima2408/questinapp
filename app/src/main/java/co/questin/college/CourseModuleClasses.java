package co.questin.college;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.StudentSubjectsClassesAdapters;
import co.questin.models.SubjectsClassesArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import okhttp3.OkHttpClient;

public class CourseModuleClasses extends BaseFragment {

    public ArrayList<SubjectsClassesArray> mClassesList;
    private StudentSubjectsClassesAdapters classesAdapters;
    RecyclerView rv_subject_classes;
    private RecyclerView.LayoutManager layoutManager;
    private CourseModuleClassessDisplay courseclassdisplay = null;
    String Subject_id,Adminuser;
    static Activity activity;
    private static ProgressBar spinner;
    TextView ErrorText;
    static ImageView ReloadProgress;
    static SwipeRefreshLayout swipeContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_course_module_classes, container, false);
        activity = (Activity) view.getContext();
        Subject_id = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        mClassesList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(activity);
        spinner= view.findViewById(R.id.progressBar);
        rv_subject_classes = view.findViewById(R.id.rv_subject_classes);
        rv_subject_classes.setHasFixedSize(true);
        rv_subject_classes.setLayoutManager(layoutManager);
        ErrorText =view.findViewById(R.id.ErrorText);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        swipeContainer =view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        if (Adminuser.equals("TRUE")) {
            courseclassdisplay = new CourseModuleClassessDisplay();
            courseclassdisplay.execute();



        } else if (Adminuser.equals("FALSE")) {
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");
        }


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClassesList.clear();
                courseclassdisplay = new CourseModuleClassessDisplay();
                courseclassdisplay.execute();


            }
        });
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        mClassesList.clear();
                        courseclassdisplay = new CourseModuleClassessDisplay();
                        courseclassdisplay.execute();


                    }
                }, 1000);
            }
        });

        return view;
    }

    private class CourseModuleClassessDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGESUBCLASSES+ "?"+"sid="+Subject_id+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            courseclassdisplay = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectsClassesArray CourseInfo = new SubjectsClassesArray();
                                String classid = jsonArrayData.getJSONObject(i).getString("tnid");
                                String classtittle = jsonArrayData.getJSONObject(i).getString("title");
                                String classlocation = jsonArrayData.getJSONObject(i).getString("location");
                                String classroom = jsonArrayData.getJSONObject(i).getString("room");

                                JSONArray classdates = jsonArrayData.getJSONObject(i).getJSONArray("date");
                                if (classdates != null && classdates.length() > 0) {

                                    for (int j = 0; j < classdates.length(); j++) {
                                        String startdate = classdates.getJSONObject(j).getString("start_date");
                                        String endate = classdates.getJSONObject(j).getString("end_date");
                                        String statrttime = classdates.getJSONObject(j).getString("start_time");
                                        String endtime = classdates.getJSONObject(j).getString("end_time");
                                        String week = classdates.getJSONObject(j).getString("week");


                                        CourseInfo.tnid = classid;
                                        CourseInfo.title = classtittle;
                                        CourseInfo.location = classlocation;
                                        CourseInfo.start_date = startdate;
                                        CourseInfo.end_date = endate;
                                        CourseInfo.start_time = statrttime;
                                        CourseInfo.end_time = endtime;
                                        CourseInfo.week = week;
                                        CourseInfo.room =classroom;
                                        mClassesList.add(CourseInfo);

                                        classesAdapters = new StudentSubjectsClassesAdapters(activity, mClassesList);
                                        rv_subject_classes.setAdapter(classesAdapters);
                                        ErrorText.setVisibility(View.INVISIBLE);
                                        ReloadProgress.setVisibility(View.GONE);
                                    }

                                }

                            }
                        }else {


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Classes Found");
                            ReloadProgress.setVisibility(View.GONE);

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
               // ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            courseclassdisplay = null;
            swipeContainer.setRefreshing(false);


        }
    }

}


