package co.questin.college;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.CampusServicesAdapter;
import co.questin.models.CampusServiceArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import okhttp3.OkHttpClient;

public class CampusServices extends BaseAppCompactActivity {


    public ArrayList<CampusServiceArray> mServiceList;
    private RecyclerView.LayoutManager layoutManager;
    private CampusServicesAdapter serviceadapter;
    RecyclerView rv_CampusServices;
    SwipeRefreshLayout swipeContainer;
    static TextView ErrorText;
    private  boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private static ShimmerFrameLayout mShimmerViewContainer;
    private CampusServicesDisplay campusservicesdisplay = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_campus_services);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        mServiceList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(CampusServices.this);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        rv_CampusServices = findViewById(co.questin.R.id.rv_CampusServices);
        ErrorText =findViewById(R.id.ErrorText);

        swipeContainer =findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        rv_CampusServices.setHasFixedSize(true);
        rv_CampusServices.setLayoutManager(layoutManager);
        serviceadapter = new CampusServicesAdapter(rv_CampusServices, mServiceList ,CampusServices.this);
        rv_CampusServices.setAdapter(serviceadapter);


        campusservicesdisplay = new CampusServicesDisplay();
        campusservicesdisplay.execute();

        inItView();





        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        campusservicesdisplay = new CampusServicesDisplay();
                        campusservicesdisplay.execute();

                    }
                }, 3000);
            }
        });






    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }



    private void inItView() {


        swipeContainer.setRefreshing(false);

        rv_CampusServices.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    serviceadapter.addProgress();

                    campusservicesdisplay = new CampusServicesDisplay();
                    campusservicesdisplay.execute();

                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }

    private class CampusServicesDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_CAMPUSSERVICES+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CampusServices.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

                serviceadapter.removeProgress();
                isLoading=false;


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {


            try {
                if (responce != null) {
                    rv_CampusServices.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<CampusServiceArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            CampusServiceArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusServiceArray.class);
                            arrayList.add(CourseInfo);

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_CampusServices.setVisibility(View.VISIBLE);
                                serviceadapter.setInitialData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            } else {
                                isLoading = false;
                                serviceadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                serviceadapter.addData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);





                            }

                        }else{
                            if (PAGE_SIZE==0){
                                rv_CampusServices.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                                ErrorText.setText("No Services");
                                swipeContainer.setVisibility(View.GONE);
                            }else
                                serviceadapter.removeProgress();

                        }
                        // view.setVisibility(View.GONE);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        // spinner.setVisibility(View.INVISIBLE);
                        showAlertDialog(msg);

                        swipeContainer.setRefreshing(false);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {
                // spinner.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            campusservicesdisplay = null;

            swipeContainer.setRefreshing(false);


        }
    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}


