package co.questin.college;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import co.questin.R;
import co.questin.models.ClusterOnMap;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

public class CollageMaps extends BaseAppCompactActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener {
    private ClusterManager<ClusterOnMap> mClusterManager;

    //Our Map
    private GoogleMap mMap;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;
    String longi;
    String lati;
    String CollageImageurl;
    Double Lat,Long;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                // Logger.d(String.format("%f, %f", location.getLatitude(), location.getLongitude()));
                // drawMarker(location);
                mLocationManager.removeUpdates(mLocationListener);
            } else {
                // Logger.d("Location is null");
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    private LocationManager mLocationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_collage_maps);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        initMap();
        getCurrentLocation();

    }

    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    private void initMap() {
        int googlePlayStatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (googlePlayStatus != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(googlePlayStatus, this, -1).show();
            finish();
        } else {
            if (mMap != null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
            }
        }
    }






    //Getting current location
    private void getCurrentLocation() {

        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


            //moving the map to location
            moveMap();
            onSearch();
        }
    }

    //Function to move the map
    private void moveMap() {
        mMap.clear();
        MakeClusters();


        //String to display current latitude and longitude
        String msg = latitude + ", " + longitude;
        Log.d("TAG", "msg: " + msg);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        //Creating a LatLng Object to store Coordinates


        if(SessionManager.getInstance(getActivity()).getCollage().getMultiple() != null && SessionManager.getInstance(getActivity()).getCollage().getMultiple().size() > 0) {

            for (int i = 0; i < SessionManager.getInstance(getActivity()).getCollage().getMultiple().size(); i++) {

                System.out.println(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLat());

                System.out.println(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLng());

                Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLat());

                Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLng());

                LatLng latLng = new LatLng(Lat, Long);


                //prints element i


            }
        }else {
            LatLng latLng = new LatLng(latitude, longitude);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title("My Location")); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            // mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 25));

        }






    }

    private void MakeClusters() {

        mClusterManager = new ClusterManager<>(this, mMap);

        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.cluster();

        addClusterItems();





    }

    private void addClusterItems() {
        if (SessionManager.getInstance(getActivity()).getCollage().getMultiple()!=null){
            for (int i = 0; i < SessionManager.getInstance(getActivity()).getCollage().getMultiple().size(); i++) {


                Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLat());

                Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLng());

                LatLng latLng = new LatLng(Lat, Long);

                mClusterManager.addItem(new ClusterOnMap(Lat, Long, SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getName(), ""));

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

            }


        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
       /* LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));*/
        //  mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        // mMap.setOnMapLongClickListener(this);
        getCurrentLocation();
        moveMap();
        onSearch();


    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Clearing all the markers
       /* mMap.clear();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));*/
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map
        moveMap();
        onSearch();

    }

    @Override
    public void onClick(View v) {

    }




    public void onSearch() {
        /*if(SessionManager.getInstance(getActivity()).getCollage().getMultiple() != null && SessionManager.getInstance(getActivity()).getCollage().getMultiple().size() > 0) {

            for (int i = 0; i < SessionManager.getInstance(getActivity()).getCollage().getMultiple().size(); i++) {

                System.out.println(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLat());

                System.out.println(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLng());

                Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLat());

                Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getMultiple().get(i).getLng());

                LatLng latLng = new LatLng(Lat, Long);

                mMap.addMarker(new MarkerOptions()
                        .position(latLng) //setting position
                        .draggable(true) //Making the marker draggable
                        .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title
                //Moving the camera
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                //Animating the camera
                // mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));


            }
        }else {
            LatLng latLng = new LatLng(latitude, longitude);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title("My Location")); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            // mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));




        }
*/


    }




    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private class RenderClusterInfoWindow extends DefaultClusterRenderer<ClusterOnMap> {

        RenderClusterInfoWindow(Context context, GoogleMap map, ClusterManager<ClusterOnMap> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onClusterRendered(Cluster<ClusterOnMap> cluster, Marker marker) {

            mMap.addMarker(new MarkerOptions()
                    .position(cluster.getPosition()) //setting position
                    .draggable(true) //Making the marker draggable
                    .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(cluster.getPosition()));

            //Animating the camera
            // mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(), 25));




            super.onClusterRendered(cluster, marker);
        }

        @Override
        protected void onBeforeClusterItemRendered(ClusterOnMap item, MarkerOptions markerOptions) {
            markerOptions.title(item.getName());

            super.onBeforeClusterItemRendered(item, markerOptions);
        }
    }

}
