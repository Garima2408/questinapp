package co.questin.college;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.OkHttpClient;

public class ClassesDetails extends BaseAppCompactActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener {
    TextView ClassTittle,Teacher,Details,Date,StartTime,EndTime,roomno,bulding,Classlocation, WeekText;
    String Classes_Id,tittle,ClassDetails,ClassTeacher,classDate,ClassEndDate,ClassStart,ClassEnd,Classroom,Buldingno,Location,weeknos;
    Double Lat,Long;
    private ProgressClassDetails classesdetailsAuthTask = null;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        Bundle b = getActivity().getIntent().getExtras();
        Classes_Id = b.getString("CLASSES_ID");
        ClassTittle = findViewById(R.id.ClassTittle);
        Teacher = findViewById(R.id.Teacher);
        Details = findViewById(R.id.Details);
        Date = findViewById(R.id.Date);
        StartTime = findViewById(R.id.StartTime);
        EndTime = findViewById(R.id.endtime);
        roomno = findViewById(R.id.roomno);
        bulding = findViewById(R.id.buldingno);
        Classlocation = findViewById(R.id.Classlocation);
        WeekText = findViewById(R.id.WeekText);

        GetAllServicesDetails();


    }

    private void GetAllServicesDetails() {

        classesdetailsAuthTask = new ProgressClassDetails();
        classesdetailsAuthTask.execute();


    }




    private class ProgressClassDetails extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTCLASSESDETAILS+ "/" +Classes_Id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                ClassesDetails.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            classesdetailsAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");


                        tittle =data.getString("title");
                        ClassTeacher =data.getString("field_instructor_class");
                        ClassDetails=data.getString("body");
                        Buldingno =data.getString("field_building");
                        Location =data.getString("field_dept_location");
                        Classroom =data.getString("field_grades");
                        JSONArray ClassdateArrays = data.getJSONArray("field_class_time");

                        JSONArray ar = data.getJSONArray("field_class_time");
                        JSONObject lastObj = ar.getJSONObject(ar.length()-1);
                        ClassEndDate = lastObj.getString("endDate");
                        Log.d("TAG", "ClassEndDatedddd: " + ClassEndDate);

                        if(ClassdateArrays != null && ClassdateArrays.length() > 0 ) {


                            for (int j = 0; j < ClassdateArrays.length(); j++) {
                                classDate = ClassdateArrays.getJSONObject(0).getString("startDate");
                                ClassStart = ClassdateArrays.getJSONObject(j).getString("startTime");
                              //  ClassEndDate  = ClassdateArrays.getJSONObject(j).getString("endDate");
                                ClassEnd = ClassdateArrays.getJSONObject(j).getString("endTime");
                                weeknos = ClassdateArrays.getJSONObject(j).getString("Week");

                                Log.d("TAG", "recource: " + classDate + ClassStart);
                              //  Log.d("TAG", "ClassEndDate: " + ClassEndDate);

                            }



                        }

                        JSONObject Local = data.getJSONObject("field_dept_location");
                        if (Local!=null && Local.length() >0){
                            Lat = Double.valueOf(Local.getString("lat"));
                            Long = Double.valueOf(Local.getString("lng"));
                            Log.d("TAG", "recource3: " + Lat + Long);

                        }else {
                            if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
                                Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
                                Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());

                            }else {
                                Lat = latitude;
                                Long = longitude;


                            }
                        }



                        ClassTittle.setText(tittle);
                        Teacher.setText(ClassTeacher);
                        Details.setText(Html.fromHtml(ClassDetails));
                        Date.setText(classDate+" "+"-"+" "+ClassEndDate);
                        StartTime.setText(ClassStart);
                        EndTime.setText(ClassEnd);
                        bulding.setText(Buldingno);
                        roomno.setText(Classroom);
                        WeekText.setText(weeknos);
                        Classlocation.setText("Location");
                        onSearch();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            classesdetailsAuthTask = null;
            hideLoading();


        }
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        getCurrentLocation();

    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @Override
    public void onClick(View v) {

    }

    public void onSearch() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        if (Classlocation != null) {
            Log.d("TAG", "recource3: " + Lat + Long);

            LatLng latLng = new LatLng(Lat, Long);
            mMap.addMarker(new MarkerOptions().position(latLng).title(SessionManager.getInstance(getActivity()).getCollage().getTitle()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

        }
    }
    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}


