package co.questin.college;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.AssignmentfileDisplayAdapter;
import co.questin.models.InfoArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.OkHttpClient;

public class SubjectsAssignmentsDetails extends BaseAppCompactActivity {
    TextView AssignTittle, Teacher, Details, Date, StartTime;
    String Assignment_Id, tittle, ClassDetails, ClassTeacher, classDate, ClassStart,assignBody;
    private ProgressAssignmentDetails assignmentdetailsAuthTask = null;
     RecyclerView resourcelist;
    public  ArrayList<InfoArray> mInfoList;
    private AssignmentfileDisplayAdapter infoadapter;
    private RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects_assignments_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getActivity().getIntent().getExtras();
        Assignment_Id = b.getString("ASSIGNMENT_ID");
        AssignTittle = findViewById(R.id.AssignTittle);
        Teacher = findViewById(R.id.Teacher);
        Details = findViewById(R.id.Details);
        Date = findViewById(R.id.Date);
        StartTime = findViewById(R.id.StartTime);
        layoutManager = new LinearLayoutManager(this);
        resourcelist = findViewById(R.id.resourceList);
        resourcelist.setHasFixedSize(true);
        resourcelist.setLayoutManager(layoutManager);


        GetAllServicesDetails();
        mInfoList = new ArrayList<>();

    }

    private void GetAllServicesDetails() {

        assignmentdetailsAuthTask = new ProgressAssignmentDetails();
        assignmentdetailsAuthTask.execute();


    }


    private class ProgressAssignmentDetails extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTSDETAILS +"/"+ Assignment_Id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                SubjectsAssignmentsDetails.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdetailsAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");


                        tittle = data.getString("title");
                        ClassTeacher = data.getString("field_instructor_assign");
                        assignBody = data.getString("body");


                        JSONArray ClassdateArrays = data.getJSONArray("field_date_of_birth");
                        if (ClassdateArrays != null && ClassdateArrays.length() > 0) {


                            for (int j = 0; j < ClassdateArrays.length(); j++) {
                                classDate = ClassdateArrays.getJSONObject(j).getString("startDate");
                                ClassStart = ClassdateArrays.getJSONObject(j).getString("startTime");
                                Log.d("TAG", "recource: " + classDate + ClassStart);

                            }
                        }
                        JSONArray resourceArrays = data.getJSONArray("field_attachments");
                        if (resourceArrays != null && resourceArrays.length() > 0) {



                            for (int j = 0; j < resourceArrays.length(); j++) {
                                String t = resourceArrays.getJSONObject(j).getString("value");
                                String p = resourceArrays.getJSONObject(j).getString("title");
                                Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = resourceArrays.getJSONObject(j).getString("title");
                                LinkInfo.value = resourceArrays.getJSONObject(j).getString("value");
                                LinkInfo.pos = resourceArrays.getJSONObject(j).getString("pos");
                                mInfoList.add(LinkInfo);


                            }


                        }
                        infoadapter = new AssignmentfileDisplayAdapter(SubjectsAssignmentsDetails.this, mInfoList,Assignment_Id);
                        resourcelist.setAdapter(infoadapter);



                        AssignTittle.setText(tittle);
                        Teacher.setText(ClassTeacher);
                        Details.setText(Html.fromHtml(assignBody));
                        Date.setText(classDate);
                        StartTime.setText(ClassStart);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            assignmentdetailsAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}