package co.questin.college;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.CollageFacultyAdapter;
import co.questin.models.FacultyArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.teacher.CreateStudentInClass;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;



public class Faculties extends BaseAppCompactActivity {

    private RecyclerView.LayoutManager layoutManager;
    private CollageFacultyAdapter collagefacultyadapter;
    RecyclerView rv_collagefaculty;
    static SwipeRefreshLayout swipeContainer;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4;
    public ArrayList<FacultyArray> mFacultyList;
    private CollageFacultiesList collagefacultylist = null;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_faculties);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);


        swipeContainer = findViewById(R.id.swipeContainer);
        ErrorText =findViewById(R.id.ErrorText);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mFacultyList = new ArrayList<>();
        collagefacultyadapter = new CollageFacultyAdapter(Faculties.this, mFacultyList);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        layoutManager = new LinearLayoutManager(this);

        rv_collagefaculty = findViewById(co.questin.R.id.rv_collagefaculty);
        rv_collagefaculty.setHasFixedSize(true);
        rv_collagefaculty.setLayoutManager(layoutManager);
        rv_collagefaculty.setAdapter(collagefacultyadapter);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = findViewById(R.id.material_design_floating_action_menu_item4);

        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {



            materialDesignFAM.setVisibility(View.VISIBLE);


        }else {
            materialDesignFAM.setVisibility(View.GONE);

        }



        /*Schools*/




        collagefacultylist = new CollageFacultiesList();
        collagefacultylist.execute();

        inItView();


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE = 0;
                        isLoading = false;
                        swipeContainer.setRefreshing(true);
                        rv_collagefaculty.setVisibility(View.GONE);

                        collagefacultylist = new CollageFacultiesList();
                        collagefacultylist.execute();

                    }
                }, 3000);
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
                    if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")){
                        Intent intent = new Intent(Faculties.this, CreateStudentInClass.class);
                        startActivity(intent);


                    }else {
                       /* Intent intent = new Intent(Faculties.this, CreateStudentInClass.class);
                        startActivity(intent);
*/

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                        String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                        intent.putExtra(Intent.EXTRA_TEXT,app_url);
                        startActivity(Intent.createChooser(intent,"share via"));
                    }
                }

                }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));
            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));

            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,"insert subject here");
                String app_url = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";
                intent.putExtra(Intent.EXTRA_TEXT,app_url);
                startActivity(Intent.createChooser(intent,"share via"));

            }
        });


    }


    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_collagefaculty.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    collagefacultyadapter.addProgress();

                    collagefacultylist = new CollageFacultiesList();
                    collagefacultylist.execute();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }


    private class CollageFacultiesList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEFACULTIES+"?"+"cid="+SessionManager.getInstance(Faculties.this).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Faculties.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        Utils.showAlertFragmentDialog(Faculties.this, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });
                collagefacultyadapter.removeProgress();
                isLoading=false;


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {

                    rv_collagefaculty.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);

                    //hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        ArrayList<FacultyArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            FacultyArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), FacultyArray.class);
                            arrayList.add(CourseInfo);

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_collagefaculty.setVisibility(View.VISIBLE);
                                collagefacultyadapter.setInitialData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                isLoading = false;
                                collagefacultyadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                collagefacultyadapter.addData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            }
                        }

                        else{
                            if (PAGE_SIZE==0){
                                rv_collagefaculty.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Faculty");
                                swipeContainer.setVisibility(View.GONE);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                collagefacultyadapter.removeProgress();

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertFragmentDialog(Faculties.this, "Error", "There seems to be some problem with the Server. Try again later.");
                        swipeContainer.setRefreshing(false);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);




                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);

            }
        }

        @Override
        protected void onCancelled() {
            collagefacultylist = null;
            swipeContainer.setRefreshing(false);


        }
    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}


