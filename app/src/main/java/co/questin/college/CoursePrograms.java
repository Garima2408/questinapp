package co.questin.college;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.CourseOfferedAdapter;
import co.questin.models.CourseOfferedList;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.OkHttpClient;

public class CoursePrograms extends BaseAppCompactActivity {
          GridView course_information;


    ArrayList<CourseOfferedList> CourseInformationList;

    private ProgressCourseOffredList progresssearchofcourselist = null;



    public static int[] CourseInformationImages = {
            R.mipmap.accomodation,
            R.mipmap.club_society,
            R.mipmap.mba,
            R.mipmap.bba,
            R.mipmap.bca,
            R.mipmap.mca,
            };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_programs);
        GetTheListOfCourse();
        CourseInformationList = new ArrayList<CourseOfferedList>();

        course_information = findViewById(R.id.course_information);
      //  course_information.setAdapter(new CourseOfferedAdapter(getActivity(),CourseInformationList, CourseInformationImages));
    }


    private void GetTheListOfCourse() {
        progresssearchofcourselist = new ProgressCourseOffredList();
        progresssearchofcourselist.execute();

    }


    private class ProgressCourseOffredList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEPROVIDED+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CoursePrograms.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofcourselist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {
                            CourseOfferedList GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), CourseOfferedList.class);
                            CourseInformationList.add(GoalInfo);
                            course_information.setAdapter(new CourseOfferedAdapter(getActivity(),CourseInformationList, CourseInformationImages));

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofcourselist = null;
            hideLoading();


        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}








