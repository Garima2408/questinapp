package co.questin.college;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.OkHttpClient;

public class ServicesDetailedPage extends BaseAppCompactActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener{

    Double Lat,Long;
    ImageView profileImage,backone;
    TextView Tittle,Address,email,phone,site,building_no,room_no;
    String Service_id,Serivetittle,ServiceAddress,ServiceEmail,ServicePhone,ServiceSite,ServiceImage,ServiceBuilding,Serviceroom;
    private ProgressServicesDetails servicedetailsAuthTask = null;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;

    private CollapsingToolbarLayout collapsingToolbarLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_detailed_page);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);



        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        collapsingToolbarLayout =  findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.CollapsingToolbarLayout);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsingToolbarLayout);


        Bundle b = getActivity().getIntent().getExtras();
        Service_id = b.getString("SERVICE_ID");
        Serivetittle= b.getString("SERVICE_TITLE");
        collapsingToolbarLayout.setTitle(Serivetittle);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();



        Address = findViewById(R.id.discription);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        site = findViewById(R.id.site);
        profileImage = findViewById(R.id.profileImage);
        building_no = findViewById(R.id.building_no);
        room_no = findViewById(R.id.room_no);


        GetAllServicesDetails();


    }

    private void GetAllServicesDetails() {
        servicedetailsAuthTask = new ProgressServicesDetails();
        servicedetailsAuthTask.execute();


    }

    private class ProgressServicesDetails extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SERVICEDETAILS+ "/" +Service_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                ServicesDetailedPage.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            servicedetailsAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");


                        Serivetittle =data.getString("title");
                        ServiceAddress =data.getString("body");
                        ServiceEmail =data.getString("field_team_email");
                        ServicePhone=data.getString("field_discipline");
                        ServiceSite =data.getString("field_url");
                        ServiceBuilding =data.getString("field_building");
                        Serviceroom  =data.getString("field_grades");


                        JSONArray picture = data.getJSONArray("field_groups_logo");
                        if(picture != null && picture.length() > 0 ) {
                            for (int j = 0; j < picture.length(); j++) {
                                Log.e("Media pictire", picture.getString(j));
                                ServiceImage =(picture.getString(0));

                            }
                        }

                      try {
                            if (data.has("field_dept_location")) {
                              JSONObject locate = data.getJSONObject("field_dept_location");
                              if (locate != null && locate.length() > 0 ){
                                  Lat = Double.valueOf(locate.getString("lat"));
                                  Long = Double.valueOf(locate.getString("lng"));

                                  Log.d("TAG", "location: " + Lat + Long );


                              }else {

                              }
                          }
                      }catch (JSONException e){

                      }


                        if(ServiceImage != null && ServiceImage.length() > 0 ) {


                            Glide.with(ServicesDetailedPage.this).load(ServiceImage)
                                    .placeholder(R.mipmap.service_display).dontAnimate()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .priority(Priority.HIGH)
                                    .fitCenter().into(profileImage);

                        }else {
                            profileImage.setImageResource(R.mipmap.service_display);

                        }

                        collapsingToolbarLayout.setTitle(Serivetittle);

                        Address.setText(Html.fromHtml("<p><b>"+Serivetittle+" "+ "</b><br/><br/>"+ServiceAddress));
                        email.setText(ServiceEmail);
                        phone.setText(ServicePhone);
                        site.setText(ServiceSite);
                        building_no.setText(ServiceBuilding);
                        room_no.setText(Serviceroom);
                        moveMap();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            servicedetailsAuthTask = null;
            hideLoading();


        }


    }


    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


            //moving the map to location
            moveMap();
        }
    }

    private void moveMap() {
        mMap.clear();
        //String to display current latitude and longitude
        String msg = latitude + ", " + longitude;
        Log.d("TAG", "msg: " + msg);


        /*  Log.d("TAG", "location: " + Lat + Long );*/


        if (Lat !=null  && Long  !=null){

            LatLng latLng = new LatLng(Lat, Long);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(false) //Making the marker draggable
                    .title(Serivetittle)); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

        }else if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
            LatLng latLng = new LatLng(Lat, Long);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(false) //Making the marker draggable
                    .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

            //Displaying current coordinates in toast
            //  Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(false);
        getCurrentLocation();
        moveMap();
    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }


    public void onSearch() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
              /*  if (Classlocation != null) {
                    Log.d("TAG", "recource3: " + Lat + Long);

                    LatLng latLng = new LatLng(Lat, Long);
                    mMap.addMarker(new MarkerOptions().position(latLng).title(SessionManager.getInstance(getActivity()).getCollage().getTitle()));
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                }*/
    }



    @Override
    public void onBackPressed() {

        ActivityCompat.finishAfterTransition(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                ActivityCompat.finishAfterTransition(this);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}