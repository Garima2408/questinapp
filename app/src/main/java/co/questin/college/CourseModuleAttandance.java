package co.questin.college;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.adapters.MyAttendanceAdapter;
import co.questin.models.AttendanceArray;
import co.questin.models.TaskChildListArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.studentprofile.DetailDisplayMyattandance;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CourseModuleAttandance extends BaseFragment {
    static Activity activity;
    String Subject_id,Adminuser;
    private ExpandableListView mExpandableListView;
    private MyAttendanceAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;
    private ArrayList<TaskChildListArray> goalList;
    private static ProgressBar spinner;
    TextView ErrorText;
    private List<AttendanceArray> expandableList;
    private ProgressoftaskList progressoftasklistAuthTask = null;
    static ImageView ReloadProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_course_module_attandance, container, false);
        activity = (Activity) view.getContext();
        Subject_id = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        spinner= view.findViewById(R.id.progressBar);
        mExpandableListView =  view.findViewById(co.questin.R.id.itemExpandableListView);
        ErrorText =view.findViewById(R.id.ErrorText);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        expandableList = new ArrayList<AttendanceArray>();
        ArrayList<TaskChildListArray> goalList = null;

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableList.clear();
                progressoftasklistAuthTask = new ProgressoftaskList();
                progressoftasklistAuthTask.execute();

            }
        });


        if (Adminuser.equals("TRUE")) {
            progressoftasklistAuthTask = new ProgressoftaskList();
            progressoftasklistAuthTask.execute();



        } else if (Adminuser.equals("FALSE")) {
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");

        }

        return view;
    }


    private class ProgressoftaskList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POST(client, URLS.URL_MYSUBJECTATTENDANCE +"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id+"/"+Subject_id , body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progressoftasklistAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                JSONObject perResult = jsonArrayData.getJSONObject(i);

                                JSONArray ClassArray = perResult.getJSONArray("class");
                                ArrayList<TaskChildListArray> goalList = new ArrayList<>();
                                for (int j = 0; j < ClassArray.length(); j++) {

                                    TaskChildListArray task = new Gson().fromJson(ClassArray.getJSONObject(j).toString(), TaskChildListArray.class);
                                    goalList.add(task);

                                }


                                AttendanceArray habitParentBean = new AttendanceArray();
                                habitParentBean.parentTitle = perResult.getString("subject");
                                habitParentBean.numbers = perResult.getString("total");
                                habitParentBean.childsTaskList.addAll(goalList);
                                expandableList.add(habitParentBean);


                                mExpandableListAdapter = new MyAttendanceAdapter(activity, expandableList);
                                mExpandableListView.setIndicatorBounds(0, 20);
                                mExpandableListView.setAdapter(mExpandableListAdapter);
                                mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                    @Override
                                    public void onGroupExpand(int groupPosition) {
                                        int len = mExpandableListAdapter.getGroupCount();
                                        for (int i = 0; i < len; i++) {
                                            if (i != groupPosition) {
                                                mExpandableListView.collapseGroup(i);
                                            }
                                        }
                                    }
                                });


                                mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                                    @Override
                                    public boolean onChildClick(ExpandableListView parent, View v,
                                                                int groupPosition, int childPosition, long id) {

                                        Intent intent=new Intent(activity,DetailDisplayMyattandance.class);
                                        Bundle bundle=new Bundle();
                                        bundle.putString("TITTLE",expandableList.get(groupPosition).childsTaskList.get(childPosition).getTitle());
                                        bundle.putString("PERCENT", expandableList.get(groupPosition).childsTaskList.get(childPosition).getPerc());
                                        bundle.putString("CLASS_ID", expandableList.get(groupPosition).childsTaskList.get(childPosition).getNid());

                                        intent.putExtras(bundle);
                                        startActivity(intent);

                                        return false;

                                    }
                                });


                            }
                        } else {

                            ErrorText.setText("No Attandance");
                            ErrorText.setVisibility(View.VISIBLE);



                        }
                    }
                    else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);



                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            progressoftasklistAuthTask = null;
            spinner.setVisibility(View.GONE);


        }
    }


}



