package co.questin.college;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.questin.R;

public class CreateMedia extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_media);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
