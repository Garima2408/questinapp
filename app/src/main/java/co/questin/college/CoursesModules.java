package co.questin.college;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.CourseModuleAdapter;
import co.questin.models.CoursemoduleArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;


public class CoursesModules extends BaseAppCompactActivity implements SearchView.OnQueryTextListener {

    String Offsetvalue;
    public ArrayList<CoursemoduleArray> mCouseList;
    private static CourseModuleAdapter coursemoduleadapter;
    static RecyclerView rv_coursemodule;
    static SwipeRefreshLayout swipeContainer;
    private RecyclerView.LayoutManager layoutManager;
    private static CourceModuleList courcemoduleList = null;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static TextView ErrorText;
    static Activity activity;

    private static ShimmerFrameLayout mShimmerViewContainer;

    private static CourceModuleSearchedList courcemodulesearchList = null;
    // private CourceModuleListRefreshed courcemoduleListRefresh = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses_modules);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        activity = getActivity();
        mCouseList=new ArrayList<>();

        ErrorText =findViewById(R.id.ErrorText);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        layoutManager = new LinearLayoutManager(this);

        rv_coursemodule = findViewById(R.id.rv_coursemodule);
        rv_coursemodule.setHasFixedSize(true);
        rv_coursemodule.setLayoutManager(layoutManager);



        coursemoduleadapter = new CourseModuleAdapter(rv_coursemodule, mCouseList ,CoursesModules.this);
        rv_coursemodule.setAdapter(coursemoduleadapter);
        swipeContainer =findViewById(R.id.swipeContainer);

        courcemoduleList = new CourceModuleList();
        courcemoduleList.execute();
        inItView();
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        CallFrromJoin();



/*
                        if (!isLoading){
                            CallFrromJoin();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }*/

                    }
                }, 1000);
            }
        });




    }


    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }



    public static  void CallFrromJoin(){
        PAGE_SIZE=0;
        isLoading=false;

        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);
       // rv_coursemodule.setVisibility(View.INVISIBLE);

        courcemoduleList = new CourceModuleList();
        courcemoduleList.execute();
    }



    private void inItView(){
        swipeContainer.setRefreshing(false);

        rv_coursemodule.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    coursemoduleadapter.addProgress();

                    courcemoduleList = new CourceModuleList();
                    courcemoduleList.execute();

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    private static class CourceModuleList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMODULE+"?"+"cid="+SessionManager.getInstance(activity).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20");
                Log.d("TAG", "responseData: " + responseData);
                jsonObject = new JSONObject(responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");
                    }
                });

                coursemoduleadapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
              courcemoduleList = null;

            try {
                if (responce != null) {
                    rv_coursemodule.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        ArrayList<CoursemoduleArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                            arrayList.add(CourseInfo);

                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_coursemodule.setVisibility(View.VISIBLE);
                                coursemoduleadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {

                                isLoading=false;
                                coursemoduleadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                coursemoduleadapter.addData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }
                           // isLoading = false;
                        }


                        else{
                            if (PAGE_SIZE==0){
                                rv_coursemodule.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Course");
                                swipeContainer.setVisibility(View.GONE);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                coursemoduleadapter.removeProgress();

                        }





                        // view.setVisibility(View.GONE);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        // spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", msg);

                        swipeContainer.setRefreshing(false);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
            }
        }
        @Override
        protected void onCancelled() {
            courcemoduleList = null;



        }
    }


    private class CourceModuleSearchedList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMODULE+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CoursesModules.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            courcemodulesearchList = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                mCouseList.add(CourseInfo);
                                coursemoduleadapter = new CourseModuleAdapter(rv_coursemodule, mCouseList ,CoursesModules.this);
                                rv_coursemodule.setAdapter(coursemoduleadapter);

                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(CoursesModules.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(R.string.No_course))
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            courcemodulesearchList = null;


        }
    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {

            PAGE_SIZE= 0;
            isLoading=false;
            finish();
            return true;
        }



        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        Log.d("TAG", "query:"+ query);
        mCouseList.clear();
        courcemodulesearchList = new CourceModuleSearchedList();
        courcemodulesearchList.execute(query);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {



        return true;
    }






    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


}


