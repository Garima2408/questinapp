package co.questin.alumni;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.CollageAluminiAdapter;
import co.questin.models.AluminiArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class Alumni extends AppCompatActivity {
    private RecyclerView.LayoutManager layoutManager;
    private CollageAluminiAdapter collageAluniadapter;
    RecyclerView rv_collagealumni;
    static SwipeRefreshLayout swipeContainer;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    public ArrayList<AluminiArray> mAluminiList;
    private CollageAluminiList collageAluminilist = null;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumni);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);


        swipeContainer = findViewById(R.id.swipeContainer);
        ErrorText =findViewById(R.id.ErrorText);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mAluminiList = new ArrayList<>();
        collageAluniadapter = new CollageAluminiAdapter(Alumni.this, mAluminiList);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        layoutManager = new LinearLayoutManager(this);

        rv_collagealumni = findViewById(co.questin.R.id.rv_collagealumni);
        rv_collagealumni.setHasFixedSize(true);
        rv_collagealumni.setLayoutManager(layoutManager);
        rv_collagealumni.setAdapter(collageAluniadapter);

        collageAluminilist = new CollageAluminiList();
        collageAluminilist.execute();

        inItView();


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE = 0;
                        isLoading = false;
                        swipeContainer.setRefreshing(true);
                        rv_collagealumni.setVisibility(View.GONE);

                        collageAluminilist = new CollageAluminiList();
                        collageAluminilist.execute();

                    }
                }, 3000);
            }
        });


        }


    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_collagealumni.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    collageAluniadapter.addProgress();

                    collageAluminilist = new CollageAluminiList();
                    collageAluminilist.execute();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }


    private class CollageAluminiList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEALUMINILIST+"?"+"cid="+SessionManager.getInstance(Alumni.this).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Alumni.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        Utils.showAlertFragmentDialog(Alumni.this, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });
                collageAluniadapter.removeProgress();
                isLoading=false;


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {

                    rv_collagealumni.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);

                    //hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        ArrayList<AluminiArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            AluminiArray AluniniInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AluminiArray.class);
                            arrayList.add(AluniniInfo);

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_collagealumni.setVisibility(View.VISIBLE);
                                collageAluniadapter.setInitialData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                isLoading = false;
                                collageAluniadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                collageAluniadapter.addData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            }
                        }

                        else{
                            if (PAGE_SIZE==0){
                                rv_collagealumni.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No ALumini");
                                swipeContainer.setVisibility(View.GONE);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                collageAluniadapter.removeProgress();

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertFragmentDialog(Alumni.this, "Error", "There seems to be some problem with the Server. Try again later.");
                        swipeContainer.setRefreshing(false);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);




                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);

            }
        }

        @Override
        protected void onCancelled() {
            collageAluminilist = null;
            swipeContainer.setRefreshing(false);


        }
    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}


