package co.questin.alumni;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.activities.UserProfileEdit;
import co.questin.calendersection.CalenderMain;
import co.questin.campusfeedsection.AllCampusFeeds;
import co.questin.campusfeedsection.MyCampusFeeds;
import co.questin.chat.ChatTabbedActivity;
import co.questin.models.UserDetail;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.paymentactivity.DuesDash;
import co.questin.studentprofile.FriendsSection;
import co.questin.studentprofile.MyAttendances;
import co.questin.studentprofile.MyCourses;
import co.questin.studentprofile.MyEvents;
import co.questin.tracker.AllRoutesDisplay;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.TextUtils;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class AluminiProfile extends BaseAppCompactActivity implements View.OnClickListener{
    LinearLayout ll_my_feeds,ll_my_courses, ll_my_group, ll_my_attendances, ll_my_results, ll_my_upcoming_events, ll_friends, ll_invite_friends,ll_mydues,ll_transport,ll_pendind;
    TextView tv_username, tv_useremail,tv_userbatch;
    ImageButton imgbtn_edit_profile;
    ImageView mainheader,profileImage;
    Activity activity;
    String imagePath;
    Bitmap thumbnail = null;
    private ProgressUpdateUserProfile updateProfileAuthTask = null;
    Uri imageUri;
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    String strFile = null;
    private int SELECT_FILE = 1;
    private static final int EXTERNAL_STORAGE_PERMISSION_REQUEST = 23;
    RequestBody body;
    String Fileimagename;
    private Fragment mFragment = null;
    private Class mFragmentClass = null;
    BottomNavigationView mBottomNavigationView;
    Dialog imageDialog,profileDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    public static final int RequestPermissionCode = 1;
    boolean ExternslWriteAccepted,ExternslreadAccepted,cameraAccepted;
    Boolean CallingCamera,CallingGallary;
    private String mImageFileLocation = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumini_profile);
        preferences = Utils.getSharedPreference(AluminiProfile.this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_FALSE_PROFILE);
        FirstOneThis();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Setuponbottombar();
        ll_my_feeds = findViewById(R.id.ll_my_feeds);
        ll_my_courses = findViewById(R.id.ll_my_courses);
        ll_my_group = findViewById(R.id.ll_my_group);
        ll_my_attendances = findViewById(R.id.ll_my_attendances);
        ll_my_results = findViewById(R.id.ll_my_results);
        ll_my_upcoming_events = findViewById(R.id.ll_my_upcoming_events);
        mainheader = findViewById(R.id.mainheader);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        //  ll_invite_friends = findViewById(R.id.ll_invite_friends);
        ll_mydues = findViewById(R.id.ll_mydues);
        ll_transport = findViewById(R.id.ll_transport);
        ll_friends = findViewById(R.id.ll_friends);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        // ll_pendind = findViewById(R.id.ll_pendind);



           imgbtn_edit_profile.setVisibility(View.VISIBLE);
            ll_mydues.setVisibility(View.GONE);
            ll_transport.setVisibility(View.GONE);
           ll_my_courses.setVisibility(View.GONE);




        ll_my_feeds.setOnClickListener(this);
        ll_my_courses.setOnClickListener(this);
        ll_my_group.setOnClickListener(this);
        ll_my_attendances.setOnClickListener(this);
        ll_my_results.setOnClickListener(this);
        ll_my_upcoming_events.setOnClickListener(this);
        //  ll_invite_friends.setOnClickListener(this);
        ll_friends.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        ll_transport.setOnClickListener(this);
        ll_mydues.setOnClickListener(this);
        imgbtn_edit_profile.setOnClickListener(this);
        // ll_pendind.setOnClickListener(this);
        displayUserData();


    }

    private void FirstOneThis() {

        switch (Runfirst){
                    case Constants.ROLE_RUNNING_TRUE :
                        imageDialog = new Dialog(AluminiProfile.this);
                        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        imageDialog.getWindow().setBackgroundDrawable(
                                new ColorDrawable(Color.TRANSPARENT));
                        imageDialog.setContentView(R.layout.image_overlay_screen);
                        // dialogLogin.setCancelable(false);
                        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                                WindowManager.LayoutParams.MATCH_PARENT);


                        imageDialog.show();
                        ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                        showimage.setImageResource(R.mipmap.onboarding_profile);

                        showimage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                imageDialog.dismiss();

                                Utils.getSharedPreference(AluminiProfile.this).edit()
                                        .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_FALSE_PROFILE).apply();
                                Utils.removeStringPreferences(AluminiProfile.this,"0");

                            }
                        });



                        break;
                    case Constants.ROLE_RUNNING_FALSE :
                        break;


                }




    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent y = new Intent(AluminiProfile.this,AluminiMain.class);
                startActivity(y);
                finish();


            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent y = new Intent(AluminiProfile.this,CalenderMain.class);
                startActivity(y);
                finish();

            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent y = new Intent(AluminiProfile.this,AllCampusFeeds.class);
                startActivity(y);
                finish();


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(AluminiProfile.this,ChatTabbedActivity.class);
                startActivity(z);
                finish();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_my_feeds:
                startActivity (new Intent(this, MyCampusFeeds.class));

                break;

            case R.id.ll_my_courses:
                startActivity (new Intent(this, MyCourses.class));

                break;

            case R.id.ll_my_group:
              // startActivity (new Intent (this, MyGroups.class));
                Toast.makeText(getActivity(), "Your Upcoming Feature", Toast.LENGTH_LONG).show();

                break;
            case R.id.ll_my_attendances:
                startActivity (new Intent (getActivity(), MyAttendances.class));

                break;

            case R.id.ll_my_results:
              //  startActivity (new Intent (this, MyResults.class));
                Toast.makeText(getActivity(), "Your Upcoming Feature", Toast.LENGTH_LONG).show();
                break;

            case R.id.ll_my_upcoming_events:
                startActivity (new Intent (this, MyEvents.class));
                // Toast.makeText(getActivity(), "Your Upcoming Feature", Toast.LENGTH_LONG).show();
                break;

            case R.id.ll_transport:
              startActivity(new Intent(this, AllRoutesDisplay.class));

                //  startActivity (new Intent (this, MyTransport.class));
              //  Toast.makeText(getActivity(), "Your Upcoming Feature", Toast.LENGTH_LONG).show();
                break;

            case R.id.ll_mydues:
              //  startActivity (new Intent (this, DuesDash.class));
                // startActivity (new Intent (this, DuesDash.class));
                // Toast.makeText(getActivity(), "Your Upcoming Feature", Toast.LENGTH_LONG).show();
                startActivity(new Intent( this, DuesDash.class));
                break;

            case R.id.ll_friends:
                startActivity (new Intent (this, FriendsSection.class));
                // Toast.makeText(getActivity(), "Your Upcoming Feature", Toast.LENGTH_LONG).show();
                break;
            case R.id.imageProfile:
                showImageDialog();

                break;
            case R.id.imgbtn_edit_profile:
                startActivity (new Intent (this , UserProfileEdit.class));
                break;

        }
    }




    private void showImageDialog() {
        final CharSequence[] items = { "View Profile","Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(AluminiProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("View Profile")){

                    showProfilepic(getUser().getPhoto());

                }

                else if (items[item].equals("Take Photo")) {
                    profileImage.setImageDrawable(null);

                    CallingCamera =true;
                    CallingGallary =false;
                    if(checkPermission()){
                        CallCamera();
                    }


                } else if (items[item].equals("Choose from Gallery")) {
                    profileImage.setImageDrawable(null);
                    CallingGallary =true;
                    CallingCamera =false;
                    if(checkPermission()){
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancel")) {
                    if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUser().photo)) {
                        Glide.with(AluminiProfile.this).load(getUser().photo)
                                .placeholder(R.mipmap.place_holder).dontAnimate()
                                .fitCenter().into(profileImage);
                    }

                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void CallCamera() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(AluminiProfile.this, "co.questin.fileprovider",photoFile);

        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }


    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }




    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }
            if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole())) {
                tv_useremail.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()+" "+SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBatch());
            }
            if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch())) {
                tv_userbatch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch());
            }

            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(profileImage);


            }

            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(mainheader);

            }
        }
    }






    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {

            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)


                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            if (!TextUtils.isNullOrEmpty(getUser().photo))
                Glide.with(AluminiProfile.this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(profileImage);

        }

    }


    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                profileImage.setImageBitmap(thumbnail);
                if (strFile !=null){
                    updateProfileAuthTask = new ProgressUpdateUserProfile();
                    updateProfileAuthTask.execute(strFile);


                }else {
                    showSnackbarMessage("There seems to be some problem.please select again");

                }





            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }




    /*New Galley Code*/



    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();
            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }

        profileImage.setImageBitmap(BitmapFactory.decodeFile(Fileimagename));//convert file to bitmap
        strFile = encodeImageTobase64(BitmapFactory.decodeFile(Fileimagename));//convert file to base64
        // Fileimagename ="GallaryImage.jpg";
        System.out.println("file in bitmap first method from cam " + strFile +"...");

        if (strFile !=null){
            updateProfileAuthTask = new ProgressUpdateUserProfile();
            updateProfileAuthTask.execute(strFile);


        }else {
            showSnackbarMessage("There seems to be some problem.please select again");

        }



    }



    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private void showProfilepic(final String photo){

        final ImageView message,info,profilepic;
        TextView senderName;

        profileDialog = new Dialog(AluminiProfile.this);
        profileDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.setContentView(R.layout.show_profilepic);
        profileDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        profileDialog.show();

        message = profileDialog.findViewById(R.id.messsage);
        profilepic = profileDialog.findViewById(R.id.profilepic);
        info = profileDialog.findViewById(R.id.info);
        senderName = profileDialog.findViewById(R.id.senderName);

        //senderName.setText(name);

        Glide.clear(profilepic);
        Glide.with(this).load(photo)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(profilepic);



        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ImageDialogOpen(photo);
                profileDialog.dismiss();

            }
        });
    }

    private void ImageDialogOpen(String photo) {

        final ImageView backone;
        TextView senderName;


        imageDialog = new Dialog(AluminiProfile.this);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.image_preview_show);
        // dialogLogin.setCancelable(false);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        ImageView showimage = imageDialog.findViewById(R.id.showimage);
        ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(activity);
        showimage.setOnTouchListener(imageMatrixTouchHandler);

        senderName = imageDialog.findViewById(R.id.senderName);
        backone = imageDialog.findViewById(R.id.backone);
        Glide.clear(showimage);

        Glide.with(this).load(photo)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(showimage);


        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });



    }





    private class ProgressUpdateUserProfile extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            if (strFile!=null){
                body = new FormBody.Builder()
                        .add("file", strFile)
                        .add("filename","profile")
                        .build();


            }



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_AVATAR+"/"+ getUser().userprofile_id,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AluminiProfile.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            updateProfileAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        String urlsofphoto = responce.getString("data");


                            UserDetail userDetail = new UserDetail();
                            userDetail.userprofile_id = SessionManager.getInstance(getActivity()).getUser().userprofile_id;
                            userDetail.username = SessionManager.getInstance(getActivity()).getUser().getFirstName() + " " + SessionManager.getInstance(getActivity()).getUser().getLastName();
                            userDetail.email = SessionManager.getInstance(getActivity()).getUser().email;
                            userDetail.FirstName = SessionManager.getInstance(getActivity()).getUser().getFirstName();
                            userDetail.LastName = SessionManager.getInstance(getActivity()).getUser().getLastName();
                            userDetail.photo = urlsofphoto;
                            userDetail.Userwallpic = SessionManager.getInstance(getActivity()).getUser().getUserwallpic();

                            SessionManager.getInstance(getActivity()).saveUser(userDetail);









                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            updateProfileAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        Utils.getSharedPreference(AluminiProfile.this).edit()
                .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_FALSE_PROFILE).apply();
        Utils.removeStringPreferences(AluminiProfile.this,"0");

        finish();
    }

    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}
