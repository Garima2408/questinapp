package co.questin.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.TeacherCourseModuleAdapter;
import co.questin.models.CoursemoduleArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class TeacherMyCource extends BaseAppCompactActivity {
    public static ArrayList<CoursemoduleArray> mCouseList;
    private static SwipeRefreshLayout swipeContainer;
    static FloatingActionButton fab;
    static FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1,fab2;
    static TextView text_view;
    static TextView text_view2;
    RelativeLayout relative;
    private static TeacherCourseModuleAdapter coursemoduleadapter;
    static RecyclerView rv_teachercoursemodule;
    private RecyclerView.LayoutManager layoutManager;
    private static MyCoursesListDisplay mycourseslistdisplay = null;
    static Activity activity;
    private static ProgressBar spinner;
    static TextView ErrorText;
    private static boolean isLoading= false;
    static RelativeLayout Displaytext;
    private static int PAGE_SIZE = 0;
    private static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_my_cource);
        activity = getActivity();
        swipeContainer = findViewById(R.id.swipeContainer);
        fab = findViewById(R.id.fab);
        fab2 =findViewById(R.id.fab2);
        spinner= findViewById(R.id.progressBar);
        mCouseList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);


        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        rv_teachercoursemodule = findViewById(R.id.rv_teachercoursemodule);
        rv_teachercoursemodule.setHasFixedSize(true);
        final LinearLayoutManager mLayoutMan= new LinearLayoutManager(this);
        rv_teachercoursemodule.setLayoutManager(layoutManager);
        coursemoduleadapter = new TeacherCourseModuleAdapter(rv_teachercoursemodule, mCouseList ,activity,"TeacherMyCource");
        rv_teachercoursemodule.setAdapter(coursemoduleadapter);
        Displaytext = findViewById(R.id.Displaytext);
        relative = findViewById(R.id.relative);
        text_view = findViewById(R.id.text_view);
        text_view2 = findViewById(R.id.text_view2);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        ErrorText = findViewById(R.id.ErrorText);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);


        mycourseslistdisplay = new MyCoursesListDisplay();
        mycourseslistdisplay.execute();

        inItView();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent backIntent = new Intent(activity, CreateCourse.class)
                        .putExtra("open", "CameFromMyCourseTeacher");
                activity.startActivity(backIntent);



            }
        });
        relative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(TeacherMyCource.this, TeacherCourseModule.class);
                startActivity(intent);


            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent backIntent = new Intent(activity, CreateCourse.class)
                        .putExtra("open", "CameFromMyCourseTeacher");
                activity.startActivity(backIntent);


            }
        });


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        if (!isLoading){
                            CalledFromAddCourse();

                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 3000);
            }
        });



    }



    public static void CalledFromAddCourse() {

        PAGE_SIZE=0;
        isLoading=true;
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);
        mycourseslistdisplay = new MyCoursesListDisplay();
        mycourseslistdisplay.execute();


    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.e("onPause","CampusNewFeeds");
        mShimmerViewContainer.stopShimmerAnimation();
    }


    private void inItView() {


        swipeContainer.setRefreshing(false);

        rv_teachercoursemodule.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    coursemoduleadapter.addProgress();

                    mycourseslistdisplay = new MyCoursesListDisplay();
                    mycourseslistdisplay.execute();
                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

    }


    public static void RefershMyCourseList() {
        mCouseList.clear();
        mycourseslistdisplay  = new MyCoursesListDisplay();
        mycourseslistdisplay.execute();
    }






    private static class MyCoursesListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYCOURSES+"?"+"uid"+"="+SessionManager.getInstance(activity).getUser().userprofile_id+"&"+"cid="+ SessionManager.getInstance(activity).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");
                    }
                });

                coursemoduleadapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            mycourseslistdisplay = null;
            try {
                if (responce != null) {

                    rv_teachercoursemodule.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);
                    Displaytext.setVisibility(View.GONE);
                    fab.setVisibility(View.VISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        ArrayList<CoursemoduleArray> arrayList = new ArrayList<>();

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CoursemoduleArray courseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                arrayList.add(courseInfo);


                            }


                        }


                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_teachercoursemodule.setVisibility(View.VISIBLE);
                                coursemoduleadapter.setInitialData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                coursemoduleadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                coursemoduleadapter.addData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }

                            isLoading = false;

                        }else{
                            if (PAGE_SIZE==0){


                                Displaytext.setVisibility(View.VISIBLE);
                                fab.setVisibility(View.GONE);
                                swipeContainer.setVisibility(View.GONE);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                coursemoduleadapter.removeProgress();

                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");



                    }
                }
            } catch (JSONException e) {
                isLoading=false;
                swipeContainer.setRefreshing(false);


            }
        }

        @Override
        protected void onCancelled() {
            mycourseslistdisplay = null;


        }
    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        PAGE_SIZE=0;
        isLoading=false;

    }

}
