package co.questin.teacher;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.StudentListInClassAdapter;
import co.questin.models.StudentListsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.OkHttpClient;

public class AddStudentInClass extends BaseAppCompactActivity {
    private RecyclerView.LayoutManager layoutManager;
    public ArrayList<StudentListsArray> mStudentList;
    private StudentListInClassAdapter studentadapter;
    RecyclerView rv_student_inClass;
    private StudentListDisplay studentlist = null;
    String Course_id,Class_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student_in_class);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getIntent().getExtras();

        Course_id = b.getString("COURSE_ID");
        Class_id = b.getString("CLASS_ID");
        mStudentList=new ArrayList<>();

        layoutManager = new LinearLayoutManager(this);
        rv_student_inClass = findViewById(R.id.rv_student_inClass);
        rv_student_inClass.setHasFixedSize(true);
        rv_student_inClass.setLayoutManager(layoutManager);


        studentlist = new StudentListDisplay();
        studentlist.execute();



    }

    private class StudentListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_STUDENTATTENDANCELIST+"/"+Course_id+"/"+"og");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AddStudentInClass.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                StudentListsArray StudentInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), StudentListsArray.class);
                                mStudentList.add(StudentInfo);
                                studentadapter = new StudentListInClassAdapter(AddStudentInClass.this, mStudentList,Class_id);
                                rv_student_inClass.setAdapter(studentadapter);


                            }






                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddStudentInClass.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(R.string.No_Student_subject))
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentlist = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



}




