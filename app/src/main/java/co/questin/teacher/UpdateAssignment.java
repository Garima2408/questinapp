package co.questin.teacher;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.models.SubjectFacultyArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class UpdateAssignment extends BaseAppCompactActivity {
    EditText SubjectTittle,Details;
    TextView StartDate;
    String Subject_id,Assignment_id,Assignmenttittle,AssignmentDetails,AssignmentTeacher,AssignmentDate,startfrom,TeacherEmail,TeacherUid, detailtittle,detailClassTeacher,detailclassDate,detailClassStart,assignBody;
    private AssignmentCreateTeacherAuthTask assignmentcreateteacherAuthTask = null;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;
    Spinner Teacher;
    private ProgressSearchOfTeacherList progresssearchofteacherlist = null;
    public ArrayList<SubjectFacultyArray> mfacultyList;
    private ArrayList<String> facultynamelist;
    ProgressAssignmentDetails assignmentdetailsAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_assignment);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getIntent().getExtras();
        Subject_id = b.getString("SUBJECT_ID");
        Assignment_id =b.getString("ASSIGNMENT_ID");

        SubjectTittle = findViewById(R.id.SubjectTittle);
        Teacher = findViewById(R.id.Teacher);
        Details = findViewById(R.id.Details);
        StartDate = findViewById(R.id.StartDate);

       /* Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String  formattedDate = sdf.format(c.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(c.getTime());

*/

      /*  StartDate.setText(formattedDate);
        startfrom =formattedDate2;
*/
        StartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartdateDialog();
            }
        });

        mfacultyList=new ArrayList<>();
        facultynamelist = new ArrayList<String>();
        progresssearchofteacherlist = new ProgressSearchOfTeacherList();
        progresssearchofteacherlist.execute();
        GetAllServicesDetails();

    }

    private void GetAllServicesDetails() {

        assignmentdetailsAuthTask = new ProgressAssignmentDetails();
        assignmentdetailsAuthTask.execute();




    }



    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), android.app.AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {

        startfrom = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        StartDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));

    }


    private class ProgressSearchOfTeacherList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDTEACHERINSUBJECTLIST+"/"+Subject_id+"/faculty");                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateAssignment.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofteacherlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {



                            SubjectFacultyArray TeacherInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectFacultyArray.class);
                            facultynamelist.add(TeacherInfo.getName());
                            mfacultyList.add(TeacherInfo);

                            Teacher.setAdapter(new ArrayAdapter<String>(UpdateAssignment.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    facultynamelist));


                            Teacher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    TeacherUid = mfacultyList.get(position).getUid();
                                    TeacherEmail = mfacultyList.get(position).getMail();

                                    Log.d("TAG", "edt_selectsub: " + TeacherUid +TeacherEmail);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofteacherlist = null;
            hideLoading();


        }
    }

    private class ProgressAssignmentDetails extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTSDETAILS +"/"+ Assignment_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateAssignment.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdetailsAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");


                        detailtittle = data.getString("title");
                        detailClassTeacher = data.getString("field_instructor_assign");
                        assignBody = data.getString("body");
                        JSONArray ClassdateArrays = data.getJSONArray("field_date_of_birth");
                        if (ClassdateArrays != null && ClassdateArrays.length() > 0) {


                            for (int j = 0; j < ClassdateArrays.length(); j++) {
                                detailclassDate = ClassdateArrays.getJSONObject(j).getString("startDate");
                                detailClassStart = ClassdateArrays.getJSONObject(j).getString("startTime");
                                Log.d("TAG", "recource: " + detailclassDate + detailClassStart);

                            }
                        }


                        SubjectTittle.setText(detailtittle);

                        Details.setText(assignBody);
                        StartDate.setText(detailclassDate);

                        DateFormat inputFormat = new SimpleDateFormat("MMM-dd-yyyy");
                        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");

                        Date datemain;
                        try {
                            datemain = inputFormat.parse(detailclassDate);
                            String outputDateStr = outputFormat.format(datemain);
                            startfrom =outputDateStr;

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            assignmentdetailsAuthTask = null;
            hideLoading();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:

                AddAssignmentInClassProcess();
                return true;
            case android.R.id.home:
                finish();

                return true;




            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void AddAssignmentInClassProcess() {


        SubjectTittle.setError(null);
        Details.setError(null);
        StartDate.setError(null);



        // Store values at the time of the login attempt.
        Assignmenttittle = SubjectTittle.getText().toString().trim();
        AssignmentDetails = Details.getText().toString().trim();
        AssignmentDate =startfrom;


        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(Assignmenttittle)) {
            focusView = SubjectTittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }

        else if (co.questin.utils.TextUtils.isNullOrEmpty(AssignmentDetails)) {
            // check for First Name
            focusView = Details;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.questin.utils.TextUtils.isNullOrEmpty(AssignmentDate)) {
            // check for First Name
            focusView = StartDate;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            assignmentcreateteacherAuthTask = new AssignmentCreateTeacherAuthTask();
            assignmentcreateteacherAuthTask.execute();



        }
    }


    private class AssignmentCreateTeacherAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            Log.d("TAG", "AssignmentDate: " + AssignmentDate);




            RequestBody body = new FormBody.Builder()
                    .add("title",Assignmenttittle)
                    .add("body",AssignmentDetails)
                    .add("field_date_of_birth[value]",AssignmentDate)
                    .add("field_og_subject",Subject_id)
                    .add("field_instructor_assign",TeacherEmail+" [uid:"+TeacherUid+"]")
                    .build();
            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_TEACHERUPDATEASSIGNMENT+"/"+Assignment_id,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateAssignment.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentcreateteacherAuthTask = null;
            try {
                if (responce != null) {

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                       // showAlertDialog(msg);
                        hideLoading();
                        TeacherAssignmentList.CalledFromAddAssignMent();
                        finish();






                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);
                        finish();


                    }
                }
            } catch (JSONException e) {
                hideLoading();


            }
        }

        @Override
        protected void onCancelled() {
            assignmentcreateteacherAuthTask = null;
            hideLoading();



        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}

