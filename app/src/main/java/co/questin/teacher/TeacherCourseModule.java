package co.questin.teacher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.TeacherCourseModuleAdapter;
import co.questin.models.CoursemoduleArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TeacherCourseModule extends BaseAppCompactActivity implements SearchView.OnQueryTextListener {

    public static ArrayList<CoursemoduleArray> mCouseList;
    FloatingActionButton fab;
    private static TeacherCourseModuleAdapter coursemoduleadapter;
    static RecyclerView rv_teachercoursemodule;
    private RecyclerView.LayoutManager layoutManager;
    public static CourceModuleList courcemoduleList = null;
    private static CourceModuleSearchedList courcemodulesearchList = null;
    static Activity activity;
    public static ProgressBar progressBar2;
     public static TextView ErrorText;
      public static SwipeRefreshLayout swipeContainer;
    public static boolean isLoading= false;
    public static int PAGE_SIZE = 0;
    public static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_course_module);
        activity = getActivity();
        fab = findViewById(R.id.fab);
        ErrorText =findViewById(R.id.ErrorText);

        mCouseList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        rv_teachercoursemodule = findViewById(R.id.rv_teachercoursemodule);
        rv_teachercoursemodule.setHasFixedSize(true);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        swipeContainer = findViewById(R.id.swipeContainer);
        coursemoduleadapter = new TeacherCourseModuleAdapter(rv_teachercoursemodule, mCouseList ,activity,"TeacherCource");
        rv_teachercoursemodule.setAdapter(coursemoduleadapter);

        rv_teachercoursemodule.setLayoutManager(layoutManager);
        courcemoduleList = new CourceModuleList();
        courcemoduleList.execute();
        inItView();



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(activity, CreateCourse.class)
                        .putExtra("open", "CameFromCourseModuleTeacher");
                startActivity(backIntent);


            }
        });

        Animation makeInAnimation = AnimationUtils.makeInAnimation(activity,false);
        makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) { }

            public void onAnimationRepeat(Animation animation) { }

            public void onAnimationStart(Animation animation) {
                fab.setVisibility(View.VISIBLE);
            }
        });

        Animation makeOutAnimation = AnimationUtils.makeOutAnimation(activity,true);
        makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                fab.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) { }

            @Override
            public void onAnimationStart(Animation animation) { }
        });


        if (fab.isShown()) {
            fab.startAnimation(makeOutAnimation);
        }

        if (!fab.isShown()) {
            fab.startAnimation(makeInAnimation);
        }

        rv_teachercoursemodule.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {

                    fab.show();
                }
            }
        });
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){

                            PAGE_SIZE=0;
                            isLoading=false;

                            swipeContainer.setRefreshing(true);

                            /* CALLING LISTS OF COMMENTS*/
                            courcemoduleList = new CourceModuleList();
                            courcemoduleList.execute();
                            //CalledFromAddCourse();

                        }else{
                            isLoading=true;
                            swipeContainer.setRefreshing(false);
                        }
                    }
                }, 3000);
            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();

    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");


        super.onPause();
    }



    public static  void CalledFromAddCourse() {
        PAGE_SIZE=0;
        isLoading=true;

        swipeContainer.setRefreshing(true);

        /* CALLING LISTS OF COMMENTS*/
        courcemoduleList = new CourceModuleList();
        courcemoduleList.execute();

    }


     public static void RefershCourseList() {
         mCouseList.clear();
        courcemoduleList = new CourceModuleList();
        courcemoduleList.execute();
     }

    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_teachercoursemodule.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    coursemoduleadapter.addProgress();

                    courcemoduleList = new CourceModuleList();
                    courcemoduleList.execute();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }




    private static class CourceModuleList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client,
                        URLS.URL_COLLAGECOURCEMODULE+
                                "?"+"cid="+SessionManager.getInstance(activity).getCollage().getTnid()
                                +"&offset="+PAGE_SIZE+"limit=20");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");
                    }
                });
                coursemoduleadapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {



            try {
                if (responce != null) {
                    rv_teachercoursemodule.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        ArrayList<CoursemoduleArray> arrayList = new ArrayList<>();
                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                            arrayList.add(CourseInfo);

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_teachercoursemodule.setVisibility(View.VISIBLE);
                                coursemoduleadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                coursemoduleadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                coursemoduleadapter.addData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);




                            }

                            isLoading = false;

                        }

                        else{
                            if (PAGE_SIZE==0){
                                rv_teachercoursemodule.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Course");
                                swipeContainer.setVisibility(View.GONE);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            }else
                                coursemoduleadapter.removeProgress();

                        }

                        } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertFragmentDialog(activity,"Information", msg);
                        swipeContainer.setRefreshing(false);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch ( Exception e) {

                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            courcemoduleList = null;


        }
    }



    private class CourceModuleSearchedList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMODULE+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherCourseModule.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            courcemodulesearchList = null;
            try {
                if (responce != null) {
                    hideLoading();

                    mCouseList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                mCouseList.add(CourseInfo);
                                coursemoduleadapter = new TeacherCourseModuleAdapter(rv_teachercoursemodule, mCouseList ,activity,"TeacherCource");
                                rv_teachercoursemodule.setAdapter(coursemoduleadapter);

                            }
                        } else {
                            showAlertDialog(getString(R.string.No_course));

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);




                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            courcemodulesearchList = null;

            hideLoading();

        }
    }







    @Override
    public void onBackPressed() {

        PAGE_SIZE=0;
        isLoading=false;
        finish();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {

            PAGE_SIZE= 0;
            isLoading=false;
            finish();
            return true;
        }




        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        Log.d("TAG", "query:"+ query);
        mCouseList.clear();
        courcemodulesearchList = new CourceModuleSearchedList();
        courcemodulesearchList.execute(query);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {



        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

