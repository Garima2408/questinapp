package co.questin.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.StudentBatchDetailsAdapter;
import co.questin.adapters.StudentDetailsClassAdapter;
import co.questin.models.AttendanceClassArray;
import co.questin.models.StudentBatchDetailArray;
import co.questin.models.StudentClassDetailArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class ClassStudentDetails extends Fragment {
    static ArrayList<AttendanceClassArray> listofcourse;
    static ArrayAdapter<String> adapter;
    private static ArrayList<String> courselist;
    private ArrayList<String> BtachInfoList;
    static TextView BatchText;
    static Activity activity;
    private static ProgressBar spinner;
    static TextView ErrorText;
    FrameLayout top;
    static View Display,viewDisplay;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3, floatingActionButton4, floatingActionButton5;
    static Spinner classess;
    static Spinner ClasstypeSpinner;
    static String Subject_idMain;
    static String Course_id;
    static String Adminuser;
    static String CourseTittle;
    static String ClasssId;
    static String ClassTpyeSelected;
    private static ProgressSearchOfClassList progresssearchofclasslist = null;
    private RecyclerView.LayoutManager layoutManager,mLayoutManager;
    static public ArrayList<StudentClassDetailArray> mStudentList;
    static public ArrayList<StudentBatchDetailArray> mStudentBatchList;
    static String uid,name,member_class,id;
    static StudentBatchDetailsAdapter studentBatchadapter;
    static private StudentDetailsClassAdapter studentadapter;
    static RecyclerView rv_student_inClass,rv_student_Batch;
    private static StudentListDisplay studentlist = null;
    public ClassStudentDetails() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
       if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();

        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_class_student_details, container, false);
        activity = (Activity) view.getContext();

        Course_id = getArguments().getString("COURSE_ID");
        Subject_idMain = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");

        final String[] classType = { "Practical", "Tutorial", "Others",  };

        courselist = new ArrayList<String>();
        listofcourse = new ArrayList<AttendanceClassArray>();
        BtachInfoList =new ArrayList<>();
        ErrorText =view.findViewById(R.id.ErrorText);
        spinner= view.findViewById(R.id.progressBar);
        classess =view.findViewById(R.id.classess);
        ClasstypeSpinner = view.findViewById(R.id.ClasstypeSpinner);
        Display =view.findViewById(R.id.Display);
        BatchText =view.findViewById(R.id.BatchText);
        top =view.findViewById(R.id.top);
        viewDisplay =view.findViewById(R.id.view);
        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);

        layoutManager = new LinearLayoutManager(activity);
        rv_student_inClass = view.findViewById(R.id.rv_student_inClass);
        rv_student_inClass.setHasFixedSize(true);
        rv_student_inClass.setLayoutManager(layoutManager);

        rv_student_Batch =view.findViewById(R.id.rv_student_Batch);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        rv_student_Batch.setLayoutManager(mLayoutManager);

        if (Adminuser.equals("TRUE")) {

            materialDesignFAM.setVisibility(View.VISIBLE);
            progresssearchofclasslist = new ProgressSearchOfClassList();
            progresssearchofclasslist.execute();


            rv_student_inClass.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {
                        materialDesignFAM.setVisibility(View.GONE);
                    } else if (dy < 0) {
                        materialDesignFAM.setVisibility(View.VISIBLE);
                    }
                }
            });





        } else if (Adminuser.equals("FALSE")) {
            materialDesignFAM.setVisibility(View.INVISIBLE);
            ErrorText.setVisibility(View.VISIBLE);
            top.setVisibility(View.GONE);
            viewDisplay.setVisibility(View.GONE);
            ErrorText.setText("You Are Not Authorised");

        }

        ArrayAdapter classadapter = new ArrayAdapter(activity,android.R.layout.simple_spinner_item,classType);
        classadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        ClasstypeSpinner.setAdapter(classadapter);


        ClasstypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ClassTpyeSelected = classType[position];

                if (courselist !=null &&courselist.size()>1){

                    studentlist = new StudentListDisplay();
                    studentlist.execute(ClasssId,ClassTpyeSelected);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                materialDesignFAM.close(true);
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                materialDesignFAM.close(true);
                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                materialDesignFAM.close(true);
                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                materialDesignFAM.close(true);
                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                materialDesignFAM.close(true);
                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);
                Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

            }
        });

        return view;
    }


    public static void  RefreshedThepagetheAdapter(){
        listofcourse.clear();
        courselist.clear();
        progresssearchofclasslist = new ProgressSearchOfClassList();
        progresssearchofclasslist.execute();


    }



    private static class ProgressSearchOfClassList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTTHRORYCLASS + "?sid=" +Course_id+"&offset="+"0"+"&limit=10");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });


            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");
                    listofcourse.clear();
                    courselist.clear();
                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if (data != null && data.length() > 1) {
                            for (int i = 0; i < data.length(); i++) {
                                AttendanceClassArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), AttendanceClassArray.class);
                                courselist.add(GoalInfo.getTitle());
                                listofcourse.add(GoalInfo);

                                classess.setAdapter(new ArrayAdapter<String>(activity,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        courselist));


                                classess.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        ClasssId = listofcourse.get(position).getTnid();
                                        studentlist = new StudentListDisplay();
                                        studentlist.execute(ClasssId,ClassTpyeSelected);

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }

                                });
                            }

                        } else {
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Class Added. Add Class");


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslist = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }


    private static class StudentListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_SUBJECTCLASSADDSTUDENT+"/"+args[0]+"?name="+args[1]+"&offset="+"0"+"&limit=150",body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");

                        if(data != null && data.length() > 0 ) {
                            for (int i = 0; i < data.length(); i++) {
                                JSONArray classes = data.getJSONArray("classes");
                                JSONArray students = data.getJSONArray("students");
                                mStudentList =new ArrayList<>();
                                mStudentBatchList =new ArrayList<>();
                                mStudentBatchList.clear();
                                mStudentList.clear();

                                if (classes != null && classes.length() > 0) {
                                    rv_student_Batch.setVisibility(View.VISIBLE);
                                    rv_student_inClass.setVisibility(View.VISIBLE);
                                    Display.setVisibility(View.VISIBLE);
                                    BatchText.setVisibility(View.VISIBLE);
                                    ErrorText.setVisibility(View.GONE);
                                    for (int j = 0; j < classes.length(); j++) {
                                        StudentBatchDetailArray BatchInfo = new StudentBatchDetailArray();

                                        String Batch = classes.getJSONObject(j).getString("batch");
                                        id = classes.getJSONObject(j).getString("id");
                                        BatchInfo.batch =Batch;
                                        BatchInfo.id =id;
                                        BatchInfo.uid =name;
                                        BatchInfo.StudentInfo =mStudentList;




                                        mStudentBatchList.add(BatchInfo);
                                    }

                                }else {

                                    rv_student_Batch.setVisibility(View.GONE);
                                    rv_student_inClass.setVisibility(View.GONE);
                                    Display.setVisibility(View.GONE);
                                    BatchText.setVisibility(View.GONE);
                                    ErrorText.setVisibility(View.VISIBLE);

                                }

                                if (students != null && students.length() > 0) {

                                    for (int k = 0; k < students.length(); k++) {
                                        StudentClassDetailArray ClassInfo = new StudentClassDetailArray();

                                        if (students != null && students.length() > 0) {

                                            uid =students.getJSONObject(k).getString("uid");
                                            name = students.getJSONObject(k).getString("name");
                                            member_class = students.getJSONObject(k).getString("member_class");

                                            ClassInfo.uid =uid;
                                            ClassInfo.name =name;
                                            ClassInfo.member_class =member_class;
                                            ClassInfo.BatchId =id;
                                            ClassInfo.BatchInfo =mStudentBatchList;
                                            mStudentList.add(ClassInfo);

                                        }


                                    }


                                    studentadapter = new StudentDetailsClassAdapter(activity, mStudentList,mStudentBatchList);
                                    rv_student_inClass.setAdapter(studentadapter);

                                }


                            }
                            if (mStudentBatchList !=null && mStudentBatchList.size() >0){

                                CallTheHeader(mStudentBatchList);

                            }


                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Information", msg);



                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            studentlist = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }







    private static void CallTheHeader(ArrayList<StudentBatchDetailArray> mStudentBatchList) {
        if (mStudentBatchList !=null && mStudentBatchList.size() >0){

            StudentBatchDetailArray BatchInfo = new StudentBatchDetailArray();


            BatchInfo.batch ="Remove";
            BatchInfo.id ="";
            BatchInfo.uid ="";
            mStudentBatchList.add(BatchInfo);

            Display.setVisibility(View.VISIBLE);
            studentBatchadapter = new StudentBatchDetailsAdapter(activity, mStudentBatchList);
            rv_student_Batch.setAdapter(studentBatchadapter);

        }



    }


}
