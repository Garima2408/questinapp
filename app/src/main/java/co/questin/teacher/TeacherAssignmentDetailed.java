package co.questin.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.AssignmentuplodedfileAdapter;
import co.questin.models.InfoArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class TeacherAssignmentDetailed extends BaseAppCompactActivity {
    static TextView AssignTittle, Teacher, Details, Date, StartTime;
    static String Assignment_Id;
    static String tittle,ClassDetails,ClassTeacher,classDate,ClassStart,Assignment_IdMain,assignBody;
    static Activity activity;
    private static ProgressBar spinner;
    private static ProgressAssignmentDetails assignmentdetailsAuthTask = null;
    TextView Addresource;
    static RecyclerView resourcelist;
    public static ArrayList<InfoArray> mInfoList;
    private static AssignmentuplodedfileAdapter infoadapter;
    private RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_assignment_detailed);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getActivity().getIntent().getExtras();
        Assignment_Id = b.getString("ASSIGNMENT_ID");
        Assignment_IdMain = b.getString("ASSIGNMENT_ID");
        activity = getActivity();
        AssignTittle = findViewById(R.id.AssignTittle);
        Teacher = findViewById(R.id.Teacher);
        Details = findViewById(R.id.Details);
        Date = findViewById(R.id.Date);
        StartTime = findViewById(R.id.StartTime);
        Addresource = findViewById(R.id.Addresource);
        spinner= findViewById(R.id.progressBar);
        layoutManager = new LinearLayoutManager(this);
        resourcelist = findViewById(R.id.resourceList);
        resourcelist.setHasFixedSize(true);
        resourcelist.setLayoutManager(layoutManager);
        GetAllServicesDetails();
        mInfoList = new ArrayList<>();
        Addresource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newIntent = new Intent(TeacherAssignmentDetailed.this,UploadFileInAssignment.class);
                Bundle bundle=new Bundle();
                bundle.putString("ASSIGNMENT_ID",Assignment_Id);
                newIntent.putExtras(bundle);
                startActivity(newIntent);

            }
        });

    }

    private void GetAllServicesDetails() {

        assignmentdetailsAuthTask = new ProgressAssignmentDetails();
        assignmentdetailsAuthTask.execute();


    }



    public static void RefreshWorkedFaculty() {

        mInfoList.clear();
        assignmentdetailsAuthTask = new ProgressAssignmentDetails();
        assignmentdetailsAuthTask.execute();

    }



    private static class ProgressAssignmentDetails extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTSDETAILS +"/"+ Assignment_Id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdetailsAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");


                        tittle = data.getString("title");
                        ClassTeacher = data.getString("field_instructor_assign");
                        assignBody = data.getString("body");


                        JSONArray ClassdateArrays = data.getJSONArray("field_date_of_birth");
                        if (ClassdateArrays != null && ClassdateArrays.length() > 0) {


                            for (int j = 0; j < ClassdateArrays.length(); j++) {
                                classDate = ClassdateArrays.getJSONObject(j).getString("startDate");
                                ClassStart = ClassdateArrays.getJSONObject(j).getString("startTime");
                                Log.d("TAG", "recource: " + classDate + ClassStart);

                            }


                        }



                        JSONArray resourceArrays = data.getJSONArray("field_attachments");
                        if (resourceArrays != null && resourceArrays.length() > 0) {



                            for (int j = 0; j < resourceArrays.length(); j++) {
                                String t = resourceArrays.getJSONObject(j).getString("value");
                                String p = resourceArrays.getJSONObject(j).getString("title");
                                Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = resourceArrays.getJSONObject(j).getString("title");
                                LinkInfo.value = resourceArrays.getJSONObject(j).getString("value");
                                LinkInfo.pos = resourceArrays.getJSONObject(j).getString("pos");
                                mInfoList.add(LinkInfo);


                            }


                        }
                        infoadapter = new AssignmentuplodedfileAdapter(activity, mInfoList,Assignment_Id);
                        resourcelist.setAdapter(infoadapter);






                        AssignTittle.setText(tittle);
                        Teacher.setText(ClassTeacher);
                        Details.setText(assignBody);
                        Date.setText(classDate);
                        StartTime.setText(ClassStart);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);

                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            assignmentdetailsAuthTask = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}