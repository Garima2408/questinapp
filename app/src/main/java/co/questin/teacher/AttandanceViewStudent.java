package co.questin.teacher;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import co.questin.R;
import co.questin.adapters.TeacherViewStudentAttandanceAdapter;
import co.questin.models.ShowAttandanceDates;
import co.questin.models.TeacherViewStudentArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class AttandanceViewStudent extends BaseAppCompactActivity {
    String Course_id, Classname, classId, weekSdate, weekLdate,CalenderMonth;
    public ArrayList<TeacherViewStudentArray> mviewattandanceList;
    public ArrayList<ShowAttandanceDates> mdateattandanceList;
    RelativeLayout MainHead;
    LinearLayout listlayout;
    String date0,attendance0,date1,attendance1,date2,attendance2,date3,attendance3,date4,attendance4,date5,attendance5,date6,attendance6,date7,attendance7;

    private RecyclerView.LayoutManager layoutManager,mLayoutManager;
    private TeacherViewStudentAttandanceAdapter viewStudentAdapter;
    RecyclerView rv_studentList,attandance_Lists;
    private ClassStudentAttandanceDisplay classstudentdisplay = null;
    TextView weekstart, weekend, ClassName, dayone, daytwo, daythree, dayfour, dayfive, daysix, dayseven, dayeight, weekeight, weekseven, weeksix, weekfive, weekfour, weekthree, weektwo, weekone,curentmonth,ErrorText;
    private int CalendarHour, CalendarMinute, CalendarSecon;
    private Calendar mcalender;
    private int startDay, startMonth, startYear;
    FrameLayout weekdate,top ;
    RelativeLayout displaydates;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attandance_view_student);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        layoutManager = new LinearLayoutManager(this);
        mLayoutManager = new LinearLayoutManager(this);
        // mLayoutManager = new LinearLayoutManager(AttandanceViewStudent.this, LinearLayoutManager.HORIZONTAL, false);
        mdateattandanceList = new ArrayList<>();
        ClassName = findViewById(R.id.ClassName);
        weekstart = findViewById(R.id.weekstart);
        weekend = findViewById(R.id.weekend);
        displaydates= findViewById(R.id.displaydates);
        dayone = findViewById(R.id.dayone);
        daytwo = findViewById(R.id.daytwo);
        daythree = findViewById(R.id.daythree);
        dayfour = findViewById(R.id.dayfour);
        dayfive = findViewById(R.id.dayfive);
        daysix = findViewById(R.id.daysix);
        dayseven = findViewById(R.id.dayseven);
        dayeight = findViewById(R.id.dayeight);
        MainHead = findViewById(R.id.MainHead);
        weekeight = findViewById(R.id.weekeight);
        weekseven = findViewById(R.id.weekseven);
        weeksix = findViewById(R.id.weeksix);
        weekfive = findViewById(R.id.weekfive);
        weekfour = findViewById(R.id.weekfour);
        weekthree = findViewById(R.id.weekthree);
        weektwo = findViewById(R.id.weektwo);
        weekone = findViewById(R.id.weekone);
        curentmonth = findViewById(R.id.curentmonth);
        ErrorText  = findViewById(R.id.ErrorText);
        weekdate =findViewById(R.id.weekdate);
        top =findViewById(R.id.top);
        listlayout =findViewById(R.id.listlayout);
        rv_studentList = findViewById(co.questin.R.id.rv_studentList);
        rv_studentList.setHasFixedSize(true);
        rv_studentList.setLayoutManager(layoutManager);
        rv_studentList.setAdapter(viewStudentAdapter);

        attandance_Lists = findViewById(co.questin.R.id.attandance_Lists);
        attandance_Lists.setHasFixedSize(true);
        attandance_Lists.setLayoutManager(mLayoutManager);
        // attandance_Lists.setAdapter(mDisplayDateAdapter);


        Bundle b = getIntent().getExtras();
        // Course_id = b.getString("COURSE_ID");
        Classname = b.getString("CLASSNAME");
        classId = b.getString("CLASSID");
        mcalender = Calendar.getInstance();
        CalendarHour = mcalender.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = mcalender.get(Calendar.MINUTE);


        final Calendar datepicker = Calendar.getInstance();
        mYear = datepicker.get(Calendar.YEAR);
        mMonth = datepicker.get(Calendar.MONTH);
        mDay = datepicker.get(Calendar.DAY_OF_MONTH);




        Calendar c = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = sdf.format(c.getTime());

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -6);
        System.out.println("Date = " + cal.getTime());

        SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDatee = sdf3.format(cal.getTime());


        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(mcalender.getTime());

        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate4 = sdf4.format(cal.getTime());


        weekstart.setText(formattedDate);
        weekend.setText(formattedDatee);
        //  CalenderMonth =formattedDate;

        weekSdate = formattedDate2;
        weekLdate = formattedDate4;


        if(weekSdate !=null &&  weekLdate!=null){

            classstudentdisplay = new ClassStudentAttandanceDisplay();
            classstudentdisplay.execute(weekLdate,weekSdate);



        }

        ClassName.setText(Classname);


        weekstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartdateDialog();

            }
        });
        weekend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   StartEnddateDialog();

            }
        });
    }


    private void StartdateDialog() {


        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {



                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, mYear, mMonth, mDay);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis() +1000);
        dpDialog.show();

    }


  /*  private void StartEnddateDialog() {
        DatePickerDialog.OnDateSetListener listenerend = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateEnd(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listenerend, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        // dpDialog.show();

        Calendar c = Calendar.getInstance();

        // Change date
        c.set(Calendar.YEAR, startYear);
        c.set(Calendar.MONTH, startMonth);
        c.set(Calendar.DATE, startDay);
        Date newDate = c.getTime();

        long now = newDate.getTime() - 1000;
        dpDialog.getDatePicker().setMinDate(now);
        dpDialog.getDatePicker().setMaxDate(now + (1000 * 60 * 60 * 24 * 7)); //After 7 Days from Now

        // dpDialog.getDatePicker().setMaxDate(newDate.getTime() - 1000);
        dpDialog.show();

    }*/

    private void showDateStart(int year, int month, int day) {
        Log.d("TAG", "Start Date: " + day + "-" + month + "-" + year);
        weekSdate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        weekstart.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));



        // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(weekSdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, -6);  // number of days to add
        String weekyyyy = sdf.format(c.getTime());
        weekLdate =weekyyyy;

        Log.d("TAG", "weekyyyy " +weekyyyy);

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;
        try {
            date = inputFormat.parse(weekyyyy);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);

        Log.d("Splited String ", "Splited String" + outputDateStr+outputDateStr);
        weekend.setText(outputDateStr);
        CalenderMonth =outputDateStr;

    }


   /* private void showDateEnd(int year, int month, int day) {
        Log.d("TAG", "End Date: " + day + "-" + month + "-" + year);
        weekLdate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        weekend.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));


    }
*/

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.Submit:
                mdateattandanceList.clear();
                classstudentdisplay = new ClassStudentAttandanceDisplay();
                classstudentdisplay.execute(weekLdate,weekSdate);

                return true;

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ClassStudentAttandanceDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("from_date", args[0])
                    .add("to_date", args[1])
                    .build();

            Log.d("TAG", "BODY: " + weekSdate + "-" + weekLdate );


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_TEACHERSTUDENTVIEWATTANDANCE+"/"+classId+"?offset=0&limit=130", body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AttandanceViewStudent.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            classstudentdisplay = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        mviewattandanceList = new ArrayList<>();
                        //   mviewattandanceList.clear();

                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {


                                TeacherViewStudentArray CourseInfo = new TeacherViewStudentArray();
                                String student = jsonArrayData.getJSONObject(i).getString("student");
                                String picture = jsonArrayData.getJSONObject(i).getString("picture");
                                String perc = jsonArrayData.getJSONObject(i).getString("perc");
                                String Enrollment =jsonArrayData.getJSONObject(i).getString("enrollment_id");

                                JSONArray jsonArrayDates = new JSONArray(jsonArrayData.getJSONObject(i).getString("dates"));



                                //  mdateattandanceList.clear();
                                for (int j = 0; j < jsonArrayDates.length(); j++) {

                                    ShowAttandanceDates attandancedate = new ShowAttandanceDates();

                                    String date = jsonArrayDates.getJSONObject(j).getString("date");
                                    String attendance = jsonArrayDates.getJSONObject(j).getString("attendance");


                                    date0 = jsonArrayDates.getJSONObject(0).getString("date");
                                    attendance0 = jsonArrayDates.getJSONObject(0).getString("attendance");
                                    date1 = jsonArrayDates.getJSONObject(1).getString("date");
                                    attendance1 = jsonArrayDates.getJSONObject(1).getString("attendance");
                                    date2 = jsonArrayDates.getJSONObject(2).getString("date");
                                    attendance2 = jsonArrayDates.getJSONObject(2).getString("attendance");
                                    date3 = jsonArrayDates.getJSONObject(3).getString("date");
                                    attendance3 = jsonArrayDates.getJSONObject(3).getString("attendance");
                                    date4 = jsonArrayDates.getJSONObject(4).getString("date");
                                    attendance4 = jsonArrayDates.getJSONObject(4).getString("attendance");
                                    date5 = jsonArrayDates.getJSONObject(5).getString("date");
                                    attendance5 = jsonArrayDates.getJSONObject(5).getString("attendance");
                                    date6 = jsonArrayDates.getJSONObject(6).getString("date");
                                    attendance6 = jsonArrayDates.getJSONObject(6).getString("attendance");
                                   /* date7 = jsonArrayDates.getJSONObject(7).getString("date");
                                    attendance7 = jsonArrayDates.getJSONObject(7).getString("attendance");
*/




                                    attandancedate.date = date;
                                    attandancedate.attendance = attendance;
                                    mdateattandanceList.add(attandancedate);



                                }

                                CourseInfo.student = student;
                                CourseInfo.picture = picture;
                                CourseInfo.Enrollment=Enrollment;
                                CourseInfo.perc = perc;
                                CourseInfo.date0 =date0;
                                CourseInfo.date1 =date1;
                                CourseInfo.date2 =date2;
                                CourseInfo.date3 =date3;
                                CourseInfo.date4 =date4;
                                CourseInfo.date5 =date5;
                                CourseInfo.date6 =date6;
                               // CourseInfo.date7 =date7;
                                CourseInfo.attendance0 =attendance0;
                                CourseInfo.attendance1 =attendance1;
                                CourseInfo.attendance2 =attendance2;
                                CourseInfo.attendance3 =attendance3;
                                CourseInfo.attendance4 =attendance4;
                                CourseInfo.attendance5 =attendance5;
                                CourseInfo.attendance6 =attendance6;
                               // CourseInfo.attendance7 =attendance7;




                                mviewattandanceList.add(CourseInfo);


                                AddAllValues(mdateattandanceList);

                                viewStudentAdapter = new TeacherViewStudentAttandanceAdapter(AttandanceViewStudent.this, mviewattandanceList, mdateattandanceList);
                                rv_studentList.setAdapter(viewStudentAdapter);

                            }




                        } else {

                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Attandance");
                            weekdate.setVisibility(View.GONE);
                            top.setVisibility(View.GONE);
                            listlayout.setVisibility(View.GONE);






                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialogFinish(AttandanceViewStudent.this,msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            classstudentdisplay = null;
            hideLoading();


        }
    }

    private void AddAllValues(ArrayList<ShowAttandanceDates> mdateattandanceList) {
        displaydates.setVisibility(View.VISIBLE);

        for (int i = 0; i < mdateattandanceList.size(); i++) {

            System.out.println(mdateattandanceList.get(i).getAttendance() + "....." + mdateattandanceList.get(i).getDate()); //prints element i

         /*   mDisplayDateAdapter = new DisplayDemoDatesAdapter(AttandanceViewStudent.this,  mdateattandanceList);
            attandance_Lists.setAdapter(mDisplayDateAdapter);*/
          /*  mDisplayDateAdapter = new DisplayAttandanceDateAdapter(AttandanceViewStudent.this, R.layout.list_ofdisplatdateattandance, mdateattandanceList);
            attandance_Lists.setAdapter(mDisplayDateAdapter);*/
        }



        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");

        Date date = null;
        try {
            date = inputFormat.parse(mdateattandanceList.get(0).getDate());
            String outputDateStr = outputFormat.format(date);
            String[] outputx = outputDateStr.split("-");
            /* String[] Datevaluex = outputx[0].split(" ");*/
            curentmonth.setText(outputx[1]);

        } catch (ParseException e) {
            e.printStackTrace();
        }








        String one = mdateattandanceList.get(0).getDate();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(one);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format2 = new SimpleDateFormat("EEE");
        String finalDay = format2.format(dt1);
        Log.d("TAG", "finalDay: " + finalDay);

        String[] output = mdateattandanceList.get(0).getDate().split("-");
        String[] Datevalue = output[2].split(" ");
        dayone.setText(Datevalue[0]);
        weekone.setText(finalDay);


        String two = mdateattandanceList.get(1).getDate();
        SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt2 = null;
        try {
            dt2 = format3.parse(two);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format4 = new SimpleDateFormat("EEE");
        String finalDay2 = format4.format(dt2);
        Log.d("TAG", "finalDay2: " + finalDay2);
        String[] output1 = mdateattandanceList.get(1).getDate().split("-");
        String[] Datevalue1 = output1[2].split(" ");
        daytwo.setText(Datevalue1[0]);
        weektwo.setText(finalDay2);


        String three = mdateattandanceList.get(2).getDate();
        SimpleDateFormat format5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt3 = null;
        try {
            dt3 = format5.parse(three);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format6 = new SimpleDateFormat("EEE");
        String finalDay3 = format6.format(dt3);
        Log.d("TAG", "finalDay3: " + finalDay3);
        String[] output2 = mdateattandanceList.get(2).getDate().split("-");
        String[] Datevalue2 = output2[2].split(" ");
        daythree.setText(Datevalue2[0]);
        weekthree.setText(finalDay3);


        String four = mdateattandanceList.get(3).getDate();
        SimpleDateFormat format7 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt4 = null;
        try {
            dt4 = format7.parse(four);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format8 = new SimpleDateFormat("EEE");
        String finalDay4 = format8.format(dt4);
        Log.d("TAG", "finalDay4: " + finalDay4);
        String[] output4 = mdateattandanceList.get(3).getDate().split("-");
        String[] Datevalue4 = output4[2].split(" ");
        dayfour.setText(Datevalue4[0]);
        weekfour.setText(finalDay4);


        String five = mdateattandanceList.get(4).getDate();
        SimpleDateFormat format9 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt5 = null;
        try {
            dt5 = format9.parse(five);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format10 = new SimpleDateFormat("EEE");
        String finalDay5 = format10.format(dt5);
        Log.d("TAG", "finalDay5: " + finalDay5);
        String[] output5 = mdateattandanceList.get(4).getDate().split("-");
        String[] Datevalu5e = output5[2].split(" ");
        dayfive.setText(Datevalu5e[0]);
        weekfive.setText(finalDay5);

        String six = mdateattandanceList.get(5).getDate();
        SimpleDateFormat format11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt6 = null;
        try {
            dt6 = format11.parse(six);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format12 = new SimpleDateFormat("EEE");
        String finalDay6 = format12.format(dt6);
        Log.d("TAG", "finalDay6: " + finalDay6);
        String[] output6 = mdateattandanceList.get(5).getDate().split("-");
        String[] Datevalue6 = output6[2].split(" ");
        daysix.setText(Datevalue6[0]);
        weeksix.setText(finalDay6);

        String seven = mdateattandanceList.get(6).getDate();
        SimpleDateFormat format13 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt7 = null;
        try {
            dt7 = format13.parse(seven);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format14 = new SimpleDateFormat("EEE");
        String finalDaydt7 = format14.format(dt7);
        Log.d("TAG", "finalDaydt7: " + finalDaydt7);
        String[] output7 = mdateattandanceList.get(6).getDate().split("-");
        String[] Datevalue7 = output7[2].split(" ");
        dayseven.setText(Datevalue7[0]);
        weekseven.setText(finalDaydt7);



       /* String eight = mdateattandanceList.get(7).getDate();
        SimpleDateFormat format15 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt8 = null;
        try {
            dt8 = format15.parse(eight);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format16 = new SimpleDateFormat("EEE");
        String finalDay8 = format16.format(dt8);
        Log.d("TAG", "finalDay: " + finalDay8);
        String[] output8 = mdateattandanceList.get(7).getDate().split("-");
        String[] Datevalue8 = output8[2].split(" ");
        dayeight.setText(Datevalue8[0]);
        weekeight.setText(finalDay8);
*/

    }
}




