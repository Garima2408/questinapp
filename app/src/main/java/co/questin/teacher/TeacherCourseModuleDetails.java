package co.questin.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.questin.R;

public class TeacherCourseModuleDetails extends AppCompatActivity {
    private TabLayout tabLayout;
    private static ViewPager viewPager;
    String Course_id,CourseTittle,CourseImage,Adminuser,Join_Value,CourseValue,is_Student;
    private Bundle bundle;
    TextView toolbar_title;
    ImageView backone;
    private static int currentPage =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_course_module_details);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras.containsKey("CHECKEDVALUETRUE")) {

            if (extras.getString("CHECKEDVALUETRUE").equals("TRUE")) {
                Course_id = intent.getStringExtra("COURSE_ID");
                CourseTittle = intent.getStringExtra("TITTLE");
                CourseImage =intent.getStringExtra("COURSE_IMAGE");
                CourseValue =intent.getStringExtra("VALUECHANGED");
                Adminuser = "TRUE";


            } else if (extras.getString("CHECKEDVALUETRUE").equals("FALSE")) {

                Course_id = intent.getStringExtra("COURSE_ID");
                CourseTittle = intent.getStringExtra("TITTLE");
                CourseImage =intent.getStringExtra("COURSE_IMAGE");
                CourseValue =intent.getStringExtra("VALUECHANGED");
                is_Student = intent.getStringExtra("ISStudent");
                Adminuser = "FALSE";
            }
        }
        if (extras.containsKey("JOINREQUEST")) {

            if (extras.getString("JOINREQUEST").equals("member")) {

                Join_Value = "member";


            } else if (extras.getString("JOINREQUEST").equals("pending")) {


                Join_Value = "pending";
            }
            else if (extras.getString("JOINREQUEST").equals("nomember")) {


                Join_Value = "nomember";
            }
        }
        actionBar.setTitle(Html.fromHtml(CourseTittle));
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(9);

    }

    public static void getCurrentPosition(int i) {
        viewPager.setCurrentItem(i);

    }


    private void setupViewPager(ViewPager viewPager) {
        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        bundle = new Bundle();
        bundle.putString("COURSE_ID", Course_id);
        bundle.putString("ADMIN", Adminuser);
        bundle.putString("TITTLE", CourseTittle);
        bundle.putString("JOINED", Join_Value);
        bundle.putString("COURSE_IMAGE", CourseImage);
        bundle.putString("VALUECHANGED",CourseValue);
        bundle.putString("ISStudent",is_Student);


        CourseModuleTeacherInfo info = new CourseModuleTeacherInfo();
        info.setArguments(bundle);

        TeacherCourceGroup group = new TeacherCourceGroup();
        group.setArguments(bundle);

        TeacherClassList classlist = new TeacherClassList();
        classlist.setArguments(bundle);

        ClassStudentDetails student_details = new ClassStudentDetails();
        student_details.setArguments(bundle);

        TeacherExamList exam = new TeacherExamList();
        exam.setArguments(bundle);

        TeacherAssignmentList assignment = new TeacherAssignmentList();
        assignment.setArguments(bundle);

        TeacherResultUpload result = new TeacherResultUpload();
        result.setArguments(bundle);

        TeacherSelectAttendance attendance = new TeacherSelectAttendance();
        attendance.setArguments(bundle);

        Invite_Students invite = new Invite_Students();
        invite.setArguments(bundle);

        SubjectMemberrequestList pending = new SubjectMemberrequestList();
        pending.setArguments(bundle);


        adapter.addFragment(info, "Info");
        adapter.addFragment(group, "Group");
        adapter.addFragment(classlist, "Class");
        adapter.addFragment(student_details,"Student Details");
        adapter.addFragment(exam, "Exam");
        adapter.addFragment(assignment, "Assignment");
        adapter.addFragment(result, "Result");
        adapter.addFragment(attendance, "Attendance");
        adapter.addFragment(invite, "Member List");
        adapter.addFragment(pending, "Pending Members");
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                adapter.getItem(position);
                Log.i("TAG", "page selected " + position);
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }





    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(TeacherCourseModuleDetails.this, TeacherCourseModule.class);
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }



}