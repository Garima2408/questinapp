package co.questin.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.CourseMembersAdapter;
import co.questin.adapters.TeacherSubjectsCommentsAdapter;
import co.questin.college.AddSubjectComment;
import co.questin.models.CourseMemberLists;
import co.questin.models.SubjectCommentArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class TeacherCourceGroup extends BaseFragment {
    String Course_id,comments,Adminuser;
    static String Course_idMain,Join_Value,is_Student;
    RecyclerView GroupMember_Lists;
    static RecyclerView rv_subject_comment;
    CardView card_view1;
    static TextView ErrorText;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    CourseMembersAdapter mGroupMemberAdapter;
    static TeacherSubjectsCommentsAdapter mcommentAdapter;
    public ArrayList<CourseMemberLists> memberList;
    public static ArrayList<SubjectCommentArray> commentsList;
    private GroupMemberList groupmemberList = null;

    private static SubjectsCommentsList subjectscommentsList = null;
    //  private static SubjectsCommentsListRefrsh subjectscommentsListrefresh = null;
    static String tittle,URLDoc;
    static Activity activity;
    private static ProgressBar spinner;
    static FloatingActionButton fab;
    static SwipeRefreshLayout swipeContainer;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private boolean _hasLoadedOnce= false; // your boolean field
    public static ShimmerFrameLayout mShimmerViewContainer;

    public TeacherCourceGroup() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

    }
//
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
//            PAGE_SIZE=0;
//        }
//    }


    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(isFragmentVisible_);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Log.e("view", "CollegeNewFeed");

                if (Adminuser.equals("TRUE")) {
                    PAGE_SIZE=0;
                    isLoading=false;

                    swipeContainer.setRefreshing(true);
                    subjectscommentsList = new SubjectsCommentsList();
                    subjectscommentsList.execute(Course_idMain);
                    _hasLoadedOnce = true;
                }else {
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    swipeContainer.setRefreshing(true);
                    subjectscommentsList = new SubjectsCommentsList();
                    subjectscommentsList.execute(Course_idMain);
                    swipeContainer.setVisibility(View.GONE);
                }


            }
            groupmemberList = new GroupMemberList();
            groupmemberList.execute(Course_idMain);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_teacher_cource_group, container, false);
        activity = (Activity) view.getContext();
        Course_id = getArguments().getString("COURSE_ID");
        Course_idMain= getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        Join_Value = getArguments().getString("JOINED");


        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        Log.d("TAG", "Course_idMain: " + Course_idMain);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        card_view1 = view.findViewById(R.id.card_view1);
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        spinner= view.findViewById(R.id.progressBar);
        rv_subject_comment = view.findViewById(R.id.rv_subject_comment);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);

        GroupMember_Lists.setLayoutManager(mLayoutManager);

        fab = view. findViewById(R.id.fab);
        ErrorText= view. findViewById(R.id.ErrorText);
        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<SubjectCommentArray>();

        /*CALLING LISTS OF STUDENTS*/
        groupmemberList = new GroupMemberList();
        groupmemberList.execute(Course_idMain);


        inItView();

        CallTheSpin();

        if (Adminuser.equals("TRUE")) {


            rv_subject_comment.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {
                        fab.hide();
                    } else if (dy < 0) {
                        fab.show();
                    }
                }
            });


            Animation makeInAnimation = AnimationUtils.makeInAnimation(activity,false);
            makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) { }

                public void onAnimationRepeat(Animation animation) { }

                public void onAnimationStart(Animation animation) {
                    fab.setVisibility(View.VISIBLE);
                }
            });

            Animation makeOutAnimation = AnimationUtils.makeOutAnimation(activity,true);
            makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationEnd(Animation animation) {
                    fab.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) { }

                @Override
                public void onAnimationStart(Animation animation) { }
            });


            if (fab.isShown()) {
                fab.startAnimation(makeOutAnimation);
            }

            if (!fab.isShown()) {
                fab.startAnimation(makeInAnimation);
            }





        } else if (Adminuser.equals("FALSE")) {
            fab.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);

        }

        if (Join_Value.equals("member")) {
            fab.setVisibility(View.VISIBLE);

        } else if (Join_Value.equals("pending")) {
            fab.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);

        }
        else if (Join_Value.equals("nomember")) {
            fab.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);

            }



        fab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent backIntent = new Intent(activity, AddSubjectComment.class)
                                .putExtra("COURSE_ID",Course_id)
                                .putExtra("SAME","1")
                                .putExtra("open","TeacherCourceGroup");

                        startActivity(backIntent);

                    }
                });

        return view;
    }





    private void inItView(){
        layoutManager = new LinearLayoutManager(activity);
        rv_subject_comment.setHasFixedSize(true);
        rv_subject_comment.setLayoutManager(layoutManager);
        mcommentAdapter = new TeacherSubjectsCommentsAdapter(rv_subject_comment, commentsList ,activity);
        rv_subject_comment.setAdapter(mcommentAdapter);
        swipeContainer.setVisibility(View.VISIBLE);

        rv_subject_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {


                if(!isLoading   ){
                    isLoading= true;
                    PAGE_SIZE+=20;
                    mcommentAdapter.addProgress();
                    subjectscommentsList = new SubjectsCommentsList();
                    subjectscommentsList.execute(Course_idMain);
                    //fetchData(fYear,,);
                }else{

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }








    public static void  RefreshWorked() {
        PAGE_SIZE=0;
        isLoading=true;

        swipeContainer.setRefreshing(true);
        subjectscommentsList = new SubjectsCommentsList();
        subjectscommentsList.execute(Course_idMain);


    }


    /*STUDENTS ON THE SUBJECTS*/



    private class GroupMemberList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          //  spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMEMBERLISTS+"/"+args[0]+"/"+"users"+"?"+"fields"+"="+"id, etid, gid, state&parameters=state=1");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    memberList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");
                    //inItView();
                    //   CallTheSpin();
                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);
                                if (CourseInfo.getEtid().contains(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                {
                                    Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());


                                    fab.setVisibility(View.VISIBLE);



                                }else {

                                    /*rv_subject_comment.setVisibility(View.GONE);
                                    fab.setVisibility(View.GONE);
                                    swipeContainer.setRefreshing(false);
                                    swipeContainer.setVisibility(View.GONE);*/
                                }


                            }

                            mGroupMemberAdapter = new CourseMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                            GroupMember_Lists.setAdapter(mGroupMemberAdapter);
/*
                            card_view1.setVisibility(View.VISIBLE);
*/

                        }else {

                           /* JoinGroup.setVisibility(View.VISIBLE);
                            rv_subject_comment.setVisibility(View.GONE);
                            fab.setVisibility(View.GONE);*/
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            spinner.setVisibility(View.INVISIBLE);



        }
    }



    private void CallTheSpin()
    {

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

//                        PAGE_SIZE=0;
//                        isLoading=false;
//
//                        swipeContainer.setRefreshing(true);
//                        subjectscommentsList = new SubjectsCommentsList();
//                        subjectscommentsList.execute(Course_idMain);


                        if (!isLoading){
                            RefreshWorked();
                        }else{

                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 3000);
            }
        });


    }

    @Override
    public void onResume() {
        PAGE_SIZE=0;
       // inItView();
        Log.e("onResume","onResume");
        mShimmerViewContainer.startShimmerAnimation();



        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
        mShimmerViewContainer.stopShimmerAnimation();
        isLoading=false;
        _hasLoadedOnce= false;
        PAGE_SIZE=0;
    }

    /*LISTS OF COMMENTS ON SUBJECTS*/

    private static class SubjectsCommentsList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ErrorText.setVisibility(View.GONE);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTSCOMMENTS+"?"+"sid"+"="+args[0]+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " +"subject "+ responseData);


            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);

                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

                mcommentAdapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            try {
                if (responce != null) {
                    rv_subject_comment.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);



                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        ArrayList<SubjectCommentArray> arrayList = new ArrayList<>();
                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {



                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectCommentArray.class);


                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                if (jsonObject.has("field_comment_attachments")){
                                    JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                    if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                        URLDoc = AttacmentsFeilds.getString("value");
                                        tittle = AttacmentsFeilds.getString("title");
                                        subjects.title =tittle;
                                        subjects.url =URLDoc;

                                    }else {

                                    }
                                }

                                arrayList.add(subjects);

                             /*   mcommentAdapter.notifyDataSetChanged();
                                rv_subject_comment.setVisibility(View.VISIBLE);
                                mcommentAdapter.setInitialData(commentsList);
                                ErrorText.setVisibility(View.INVISIBLE);
*/



                            }

                        }


                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                                rv_subject_comment.setVisibility(View.VISIBLE);
                                mcommentAdapter.setInitialData(arrayList);

                            } else {

                                mcommentAdapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                                mcommentAdapter.addData(arrayList);


                            }
                            isLoading = false;
                        }

                        else{
                            if (PAGE_SIZE==0){

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                                rv_subject_comment.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                                swipeContainer.setVisibility(View.GONE);
                            }else
                                mcommentAdapter.removeProgress();

                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);


                        swipeContainer.setRefreshing(false);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);


                    }
                }else {
                    swipeContainer.setRefreshing(false);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            subjectscommentsList = null;
            swipeContainer.setRefreshing(false);



        }
    }



    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        // EventBus.getDefault().unregister(this);
    }





    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        super.onDestroy();
    }

}

