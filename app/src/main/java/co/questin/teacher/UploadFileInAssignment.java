package co.questin.teacher;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class UploadFileInAssignment extends BaseAppCompactActivity {

    Uri imageUri;
    EditText Titttle, Value;
    ImageView imagepreview,cancelPreview;
    TextView Addresource;
    RelativeLayout previewlayout;
    private CourseFileCreateTeacherAuthTask resourceFileteacherAuthTask = null;
    static String strFile = null;
    private static final int REQUEST_PATH = 3;
    String versionstr, osstr, filepath, AssignmentId;
    Dialog AttachmentDialog;
    private File selectedFile;
    String curFileName;
    String Fileimagename,fullPath;
    Bitmap thumbnail = null;
    private static final int REQUEST_CODE = 2;
    /*new cam/gallary*/
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private String mImageFileLocation = "";
    private int SELECT_FILE = 1;
    public static final int RequestPermissionCode = 1;
    boolean ExternslWriteAccepted,ExternslreadAccepted,cameraAccepted;
    Boolean CallingCamera,CallingGallary,CallingAttachment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_file_in_assignment);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getActivity().getIntent().getExtras();
        AssignmentId = b.getString("ASSIGNMENT_ID");


        Addresource = findViewById(R.id.Addresource);
        Titttle = findViewById(R.id.Titttle);
        Value = findViewById(R.id.Value);
        imagepreview  =findViewById(R.id.imagepreview);
        cancelPreview  =findViewById(R.id.cancelPreview);
        previewlayout  =findViewById(R.id.previewlayout);

        Addresource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AttachmentDialogOpen();

            }
        });

        cancelPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                previewlayout.setVisibility(View.GONE);

                strFile ="";
                curFileName="";
                versionstr="";
                Addresource.setVisibility(View.VISIBLE);


            }
        });


    }

    private void AttachmentDialogOpen() {

        AttachmentDialog = new Dialog(this);
        AttachmentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        AttachmentDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.WHITE));
        AttachmentDialog.setContentView(R.layout.list_of_attactmentmenu);

        AttachmentDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        AttachmentDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        AttachmentDialog.show();


        TextView File = AttachmentDialog.findViewById(R.id.File);
        TextView Photo = AttachmentDialog.findViewById(R.id.Photo);
        TextView Camera = AttachmentDialog.findViewById(R.id.Camera);


        File.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CallingAttachment =true;
                CallingCamera =false;
                CallingGallary =false;


                if(checkPermission()){
                    AddValueFromTheBrows();
                }
                //  Addresource.setVisibility(View.INVISIBLE);
                AttachmentDialog.dismiss();

            }
        });


        Photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagepreview.setImageDrawable(null);
                CallingGallary =true;
                CallingAttachment =false;
                CallingCamera =false;

                if(checkPermission()){
                    galleryIntent();
                }


                AttachmentDialog.dismiss();
            }
        });
        Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CallingCamera =true;
                CallingGallary =false;
                if(checkPermission()){
                    CallCamera();
                }



            }
        });


    }


    private void CallCamera() {
        imagepreview.setImageDrawable(null);
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(UploadFileInAssignment.this, "co.questin.fileprovider",photoFile);

        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
        AttachmentDialog.dismiss();
    }

    private void AddValueFromTheBrows() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimeTypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                "text/plain",
                "application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, REQUEST_CODE);

        AttachmentDialog.dismiss();


    }


    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }
    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {
            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }

        if (requestCode == REQUEST_CODE &&resultCode == RESULT_OK) {
            Addresource.setVisibility(View.INVISIBLE);

            Uri FileURI = data.getData();
            String uriString = FileURI.toString();
            selectedFile = new File(uriString);
            try {
                fullPath = Utils.getPath(this,FileURI);
            }catch (NumberFormatException e){

                System.out.println("not a number");

            } catch (Exception e){

                System.out.println(e);
            }


            String FileName = Utils.getDataColumn(this,FileURI,null,null);

            Log.d("TAG", "onActivityResult: " + FileName + FileName );

            String path = selectedFile.toString();

            String filepath = path;

            String displayName = null;

            if (uriString.startsWith("content://")) {

                Cursor cursor = null;

                try {

                    cursor = getContentResolver().query(FileURI, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {

                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

                        Fileimagename = cursor.getString(nameIndex);

                        Log.d("TAG", "onActivityResult: " + Fileimagename + filepath + fullPath);

                        if (fullPath != null) {

                            if (filepath.matches("(.*)providers(.*)")) {

                                String fullfilePath = fullPath + "/" + Fileimagename;

                                convertFileToString(fullfilePath, Fileimagename);

                                Log.d("TAG", "onActivityResult3: " + fullfilePath);
                            } else if (filepath.matches("(.*)externalstorage(.*)")) {

                                fullPath = fullPath;
                                convertFileToString(fullPath, Fileimagename);
                                Log.d("TAG", "onActivityResult4: " + fullPath);
                            }


                        } else {

                            Toast.makeText(this, "File not found !! Get it from internal/external storage", Toast.LENGTH_SHORT).show();
                            Addresource.setVisibility(View.VISIBLE);
                        }


                    }





                }catch (Exception e )
                {
                    e.printStackTrace();
                }

            }




        }



    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setImageBitmap(thumbnail);
                Addresource.setVisibility(View.GONE);




            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }



    public String convertFileToString(String pathOnSdCard, String curFileName){

        File file = new File(pathOnSdCard);

        try {

            byte[] data = FileUtils.readFileToByteArray(file);//Convert any file, image or video into byte array


            // byte[] data = FileUtils.readFileToByteArray(file); //Convert any file, image or video into byte array

            strFile = Base64.encodeToString(data, Base64.NO_WRAP);

            //Convert byte array into string

            System.out.println("file in bitmap first method " + strFile);

            Fileimagename =curFileName;

            Log.d("TAG", "onActivityResult2: " + strFile);

            System.out.println("filename in bitmap first method " + Fileimagename);

            // CreateFileMsg(strFile,curFileName);

            String Extension = Fileimagename.substring(Fileimagename.lastIndexOf("."));

            if (Extension.matches(".jpeg") || Extension.matches(".jpg"))
            {
                imagepreview.setImageBitmap(BitmapFactory.decodeFile(fullPath));//convert file to bitmap

                previewlayout.setVisibility(View.VISIBLE);
                Addresource.setVisibility(View.GONE);

            }else if (Extension.matches(".png") )
            {
                imagepreview.setImageBitmap(BitmapFactory.decodeFile(fullPath));//convert file to bitmap

                previewlayout.setVisibility(View.VISIBLE);
                Addresource.setVisibility(View.GONE);

            }else{

                imagepreview.setBackgroundResource(R.mipmap.file);
                Addresource.setVisibility(View.GONE);
                previewlayout.setVisibility(View.VISIBLE);

            }

            /*imagepreview.setBackgroundResource(R.mipmap.file);

            previewlayout.setVisibility(View.VISIBLE);*/

        } catch (Exception e) {

            e.printStackTrace();

        }

        return strFile;

    }




    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {

            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();

            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }

    }



    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:


                AddValueToList();


                return true;
            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void AddValueToList() {


        Titttle.setError(null);
        Value.setError(null);

        if(strFile ==null){

            showSnackbarMessage(getString(R.string.error_field_attac));
        }




        // Store values at the time of the login attempt.
        versionstr = Titttle.getText().toString().trim();
        osstr = Value.getText().toString().trim();


        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(versionstr)) {
            focusView = Titttle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        else  if (android.text.TextUtils.isEmpty(strFile)) {
            focusView = Titttle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_attac));

            // Check for a valid email address.
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            AddFileInSubjectProcess();

        }
    }


    private void AddFileInSubjectProcess() {

        resourceFileteacherAuthTask = new CourseFileCreateTeacherAuthTask();
        resourceFileteacherAuthTask.execute();

    }


    @Override
    public void onBackPressed() {
        finish();
    }


    private class CourseFileCreateTeacherAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()

                    .add("file", strFile)
                    .add("filename", Fileimagename)
                    .add("description", versionstr)
                    .build();


            try {
                String responseData = ApiCall.POSTHEADERCREATE(client, URLS.URL_UPLOADFILETOASSIGNMENT +"/"+AssignmentId, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UploadFileInAssignment.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            resourceFileteacherAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        //  showAlertDialog(msg);
                        TeacherAssignmentDetailed.RefreshWorkedFaculty();

                        finish();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            resourceFileteacherAuthTask = null;
            hideLoading();


        }
    }


    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }else if (CallingAttachment==true){
                            AddValueFromTheBrows();

                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}








