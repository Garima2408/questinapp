package co.questin.teacher;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.questin.R;
import co.questin.models.SubjectFacultyArray;
import co.questin.models.SubjectsClassesArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class TeacherAddClasses extends BaseAppCompactActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener ,View.OnClickListener {
    EditText ClassTittle, Details, roomno, bulding ;
    TextView StartDate, DateEnds, StartTime, EndTime,location;
    String Subject_id, tittle, classDetails, classTeacher, classDatestart, classDatesend, classtimestart, classtimesend,
            roomi, Buldingno, Location, format, weekmon, weektues, weekwedns, weekthur, weekfri, weeksat, weeksun,
            classrepeat,time24formatreapet,time24formarend,TeacherEmail,TeacherUid,ClassTpyeSelected,TypeSelected,ClassTypeModified,ClassTittleModified,BatchTpyeSelected,TypeSelectedBatch,TypeSelectedBatchModified,
            ClassId,ClassTittleSelected,classIDCreated;
    private int CalendarHour, CalendarMinute ,CalendarSecon;
    CheckBox repeat,mon, tue, wed, thur, fri, sat, sun;
    LinearLayout Weeklayoutlayout;
    boolean isPressed = false;
    StringBuilder Weekselect;
    Dialog addStudentDialog;
    TimePickerDialog timepickerdialog;
    private Calendar mcalender;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;
    private int startDay1, startMonth1, startYear1;
    private int year1, month1, day1, week1;
    String Lat, Long;
    Double SelectedLat, SelectedLong;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;
    private ClassCreateTeacherAuthTask classcreateteacherAuthTask = null;
    private ProgressSearchOfTeacherList progresssearchofteacherlist = null;
    private ProgressSearchOfClassList progresssearchofclasslist = null;
    public ArrayList<SubjectsClassesArray> mClassesList;
    public ArrayList<SubjectFacultyArray> mfacultyList;
    private ArrayList<String> facultynamelist;
    //To store longitude and latitude from map
    private double longitude;
    private double latitude;
    JSONObject ClassDetailsJson;
    private ArrayList<String> selectedclassdetails;
    Spinner Teacher,ClasstypeSpinner,BatchSpinner,ClassSpinner;
    FrameLayout top2,top3;
    int FragmentPosition;
    /*PlaceApi*/

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    // Used for selecting the current place.
    private static final int PLACE_PICKER_REQUEST = 1000;
    RelativeLayout MapLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_add_classes);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        SupportMapFragment  mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);



        Bundle b = getIntent().getExtras();
        Subject_id = b.getString("COURSE_ID");
        FragmentPosition = Integer.parseInt(b.getString("SAME"));
        final String[] classType = { "Theory", "Practical", "Tutorial", "Others",  };
        final String[] BatchType = { "A", "B", "C", "D", "E",  };


        MapLayout  = findViewById(R.id.MapLayout);
        ClassTittle = findViewById(R.id.ClassTittle);
        Teacher = findViewById(R.id.Teacher);
        Details = findViewById(R.id.Details);
        roomno = findViewById(R.id.roomno);
        bulding = findViewById(R.id.bulding);
        location = findViewById(R.id.location);
        StartDate = findViewById(R.id.StartDate);
        DateEnds = findViewById(R.id.DateEnds);
        StartTime = findViewById(R.id.StartTime);
        EndTime = findViewById(R.id.EndTime);
        mon = findViewById(R.id.mon);
        tue = findViewById(R.id.tue);
        wed = findViewById(R.id.wed);
        thur = findViewById(R.id.thu);
        fri = findViewById(R.id.fri);
        sat = findViewById(R.id.sat);
        sun = findViewById(R.id.sun);
        repeat = findViewById(R.id.repeat);
        top2 =findViewById(R.id.top2);
        top3 = findViewById(R.id.top3);
        BatchSpinner = findViewById(R.id.BatchSpinner);
        Weeklayoutlayout = findViewById(R.id.Weeklayoutlayout);
        ClasstypeSpinner = findViewById(R.id.ClasstypeSpinner);
        ClassSpinner =findViewById(R.id.ClassSpinner);
        mcalender = Calendar.getInstance();
        CalendarHour = mcalender.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = mcalender.get(Calendar.MINUTE);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = sdf.format(c.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(mcalender.getTime());


        StartDate.setText(formattedDate);
        DateEnds.setText(formattedDate);
        classDatestart = formattedDate2;
        classDatesend = formattedDate2;

        String time = new SimpleDateFormat("hh:mm a").format(new java.util.Date().getTime());

       /* SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
        String time = simpleDateFormat.format(mcalender.getTime());
*/
        StartTime.setText(time);
        EndTime.setText(time);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(time);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        String time24format = displayFormat.format(date);

        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));




        classtimestart =time24format;
        classtimesend =time24format;
        classrepeat ="0";
        StartDate.setOnClickListener(this);
        DateEnds.setOnClickListener(this);
        StartTime.setOnClickListener(this);
        EndTime.setOnClickListener(this);
        repeat.setOnClickListener(this);
        mon.setOnClickListener(this);
        tue.setOnClickListener(this);
        wed.setOnClickListener(this);
        thur.setOnClickListener(this);
        fri.setOnClickListener(this);
        sat.setOnClickListener(this);
        sun.setOnClickListener(this);
        Weekselect =new StringBuilder();
        mfacultyList=new ArrayList<>();
        mClassesList=new ArrayList<>();
        facultynamelist = new ArrayList<String>();
        selectedclassdetails = new ArrayList<String>();

        progresssearchofteacherlist = new ProgressSearchOfTeacherList();
        progresssearchofteacherlist.execute();

        ArrayAdapter classadapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,classType);
        classadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        ClasstypeSpinner.setAdapter(classadapter);


        ArrayAdapter batchadapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,BatchType);
        batchadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        BatchSpinner.setAdapter(batchadapter);

        ClasstypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ClassTpyeSelected = classType[position];

                if(ClassTpyeSelected.contains("Theory")){
                    top2.setVisibility(View.GONE);
                    top3.setVisibility(View.GONE);
                    ClassTittle.setEnabled(true);
                    ClassTittle.setVisibility(View.VISIBLE);
                    TypeSelected="theory";
                    ClassTypeModified="";

                }else if(ClassTpyeSelected.contains("Practical")){
                    top2.setVisibility(View.VISIBLE);
                    top3.setVisibility(View.VISIBLE);
                    ClassTittle.setVisibility(View.GONE);
                    ClassTittle.setEnabled(false);
                    TypeSelected ="practical";
                    ClassTypeModified= ClassTpyeSelected.substring(0,4);
                    mClassesList.clear();
                    selectedclassdetails.clear();
                    progresssearchofclasslist = new ProgressSearchOfClassList();
                    progresssearchofclasslist.execute();




                }else if(ClassTpyeSelected.contains("Tutorial")){
                    top2.setVisibility(View.VISIBLE);
                    top3.setVisibility(View.VISIBLE);
                    ClassTittle.setVisibility(View.GONE);
                    ClassTittle.setEnabled(false);
                    TypeSelected ="tutorial";
                    ClassTypeModified= ClassTpyeSelected.substring(0,3);
                    mClassesList.clear();
                    selectedclassdetails.clear();
                    progresssearchofclasslist = new ProgressSearchOfClassList();
                    progresssearchofclasslist.execute();


                }else if(ClassTpyeSelected.contains("Others")){
                    top2.setVisibility(View.VISIBLE);
                    top3.setVisibility(View.VISIBLE);
                    ClassTittle.setVisibility(View.GONE);
                    ClassTittle.setEnabled(false);
                    TypeSelected ="others";
                    ClassTypeModified= ClassTpyeSelected.substring(0,3);
                    mClassesList.clear();
                    selectedclassdetails.clear();
                    progresssearchofclasslist = new ProgressSearchOfClassList();
                    progresssearchofclasslist.execute();
                }

                //   Toast.makeText(getApplicationContext(),collageType[position] , Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
                    SelectedLat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
                    SelectedLong = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());

                    }else {
                    SelectedLat = latitude;
                    SelectedLong = longitude;


                }


                LatLngBounds latLngBounds = new LatLngBounds(new LatLng(SelectedLat, SelectedLong),
                        new LatLng(SelectedLat, SelectedLong));
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                builder.setLatLngBounds(latLngBounds);

                try {
                    startActivityForResult(builder.build(TeacherAddClasses.this), PLACE_PICKER_REQUEST);
                } catch (Exception e) {
                    Log.e("TAG", e.getStackTrace().toString());
                }


            
            }
        });




        BatchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                BatchTpyeSelected = BatchType[position];

                if(BatchTpyeSelected.contains("A")){

                    TypeSelectedBatch ="a";
                    TypeSelectedBatchModified = "A";

                }
                else if(BatchTpyeSelected.contains("B")){

                    TypeSelectedBatch ="b";
                    TypeSelectedBatchModified ="B";


                }else if(BatchTpyeSelected.contains("C")){

                    TypeSelectedBatch ="c";
                    TypeSelectedBatchModified ="C";


                }else if(BatchTpyeSelected.contains("D")){

                    TypeSelectedBatch ="d";
                    TypeSelectedBatchModified ="D";


                }
                else if(BatchTpyeSelected.contains("E")){

                    TypeSelectedBatch ="e";
                    TypeSelectedBatchModified ="E";


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.StartDate:
                hideKeyBoard(v);

                StartdateDialog();
                break;

            case R.id.DateEnds:
                hideKeyBoard(v);

                StartEnddateDialog();
                break;

            case R.id.repeat:
                hideKeyBoard(v);


                if (((CheckBox) v).isChecked()) {

                    classrepeat = "1";
                    Weeklayoutlayout.setVisibility(View.VISIBLE);

                }else {
                    classrepeat = "0";
                    Weeklayoutlayout.setVisibility(View.GONE);

                }
                break;


            case R.id.StartTime:
                hideKeyBoard(v);


                timepickerdialog = new TimePickerDialog(TeacherAddClasses.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }

                                String Slow =(hourOfDay + ":" + minute + format);
                                StartTime.setText(hourOfDay + ":" + minute + format);



                                DateFormat f1 = new SimpleDateFormat("h:mma", Locale.US);

                                try {
                                    Date d = f1.parse(Slow);
                                    DateFormat f2 = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                    String FF = f2.format(d).toLowerCase();
                                    Log.d("TAG", "f2 " + FF);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }




                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String Time = null;
                                try {
                                    Time = outFormatx.format(inFormatx.parse(Slow));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatS : " + Time);

                                classtimestart=Time;





                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();
                break;
            case R.id.EndTime:
                hideKeyBoard(v);
                timepickerdialog = new TimePickerDialog(TeacherAddClasses.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }

                                String SlowEnd =(hourOfDay + ":" + minute + format);
                                EndTime.setText(hourOfDay + ":" + minute + format);

                                Log.d("TAG", "EndTime " + EndTime);


                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String time24 = null;
                                try {
                                    time24 = outFormatx.format(inFormatx.parse(SlowEnd));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatE : " + time24);

                                classtimesend=time24;





                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();

                break;



        }


    }

    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }






    private void StartEnddateDialog() {
        DatePickerDialog.OnDateSetListener listenerend = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateEnd(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listenerend, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        // dpDialog.show();

        Calendar c = Calendar.getInstance();

        // Change date
        c.set(Calendar.YEAR, startYear);
        c.set(Calendar.MONTH, startMonth);
        c.set(Calendar.DATE, startDay);
        Date newDate = c.getTime();

        //  dpDialog.getDatePicker().setMinDate(newDate.getTime() - 1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {
        Log.d("TAG", "Start Date: " + day + "-" + month + "-" + year);
        classDatestart = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        StartDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));

    }

    private void showDateEnd(int year, int month, int day) {
        Log.d("TAG", "End Date: " + day + "-" + month + "-" + year);
        classDatesend = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        DateEnds.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));


    }


    /*list of Teacher*/

    private class ProgressSearchOfTeacherList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDTEACHERINSUBJECTLIST+"/"+Subject_id+"/faculty");                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherAddClasses.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofteacherlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {



                            SubjectFacultyArray TeacherInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectFacultyArray.class);
                            facultynamelist.add(TeacherInfo.getName());
                            mfacultyList.add(TeacherInfo);

                            Teacher.setAdapter(new ArrayAdapter<String>(TeacherAddClasses.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    facultynamelist));


                            Teacher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    TeacherUid = mfacultyList.get(position).getUid();
                                    TeacherEmail = mfacultyList.get(position).getMail();

                                    Log.d("TAG", "edt_selectsub: " + TeacherUid +TeacherEmail);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofteacherlist = null;
            hideLoading();


        }
    }


    /*List of Class*/




    private class ProgressSearchOfClassList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECLASSMODULE+ "?"+"sid="+Subject_id+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherAddClasses.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");
                    mClassesList.clear();
                    selectedclassdetails.clear();
                    if (errorCode.equalsIgnoreCase("1")) {
                        JSONArray data = responce.getJSONArray("data");
                        for (int i=0; i<data.length()-1;i++) {

                            SubjectsClassesArray CourseInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectsClassesArray.class);
                            mClassesList.add(CourseInfo);
                            selectedclassdetails.add(CourseInfo.getTitle());



                            ClassSpinner.setAdapter(new ArrayAdapter<String>(TeacherAddClasses.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    selectedclassdetails));


                            ClassSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    ClassId = mClassesList.get(position).getTnid();
                                    ClassTittleSelected = mClassesList.get(position).getTitle();
                                    try {
                                        ClassTittleModified=  ClassTittleSelected.substring(0,5);

                                    }catch (IndexOutOfBoundsException i){

                                    }
                                    ClassTittle.setText(ClassTittleModified);
                                    Log.d("TAG", "edt_selectsub: " + ClassId +ClassTittleSelected);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });


                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslist = null;
            hideLoading();


        }
    }








    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:

                if (mClassesList.size() == 0) {

                    if (TypeSelected == "theory") {
                        AddClassesIncourceProcess();
                    } else if (TypeSelected == "practical") {
                        showSnackbarMessage(getString(R.string.Create_ThClass));

                    } else if (TypeSelected == "tutorial") {
                        showSnackbarMessage(getString(R.string.Create_ThClass));
                    }
                    else if (TypeSelected == "others") {
                        showSnackbarMessage(getString(R.string.Create_ThClass));

                    }

                }else if(mClassesList.size()>0) {

                    if (TypeSelected == "theory") {
                        AddClassesIncourceProcess();
                    }

                    else if (TypeSelected == "practical") {
                        AddClassesIncourceProcess();

                    } else if (TypeSelected == "tutorial") {
                        AddClassesIncourceProcess();
                    }
                    else if(TypeSelected == "others") {
                        AddClassesIncourceProcess();


                    }

                }

                return true;
            case android.R.id.home:
                finish();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void selectdays() {

        int totalselect = 0;

        if (mon.isChecked()) {
            weekmon ="MO";
            Weekselect.append(weekmon+","+" " );
            totalselect += 1;
        }

        if (tue.isChecked()) {
            weektues ="TU";
            Weekselect.append(weektues+","+" " );
            totalselect += 1;
        }
        if (wed.isChecked()) {
            weekwedns ="WE";
            Weekselect.append(weekwedns+","+" " );
            totalselect += 1;
        }

        if (thur.isChecked()) {
            weekthur ="TH";
            Weekselect.append(weekthur+","+" ");
            totalselect += 1;
        }
        if (fri.isChecked()) {
            weekfri ="FR";
            Weekselect.append(weekfri+","+" ");
            totalselect += 1;
        }

        if (sat.isChecked()) {
            weeksat ="SA";
            Weekselect.append(weeksat+","+" ");
            totalselect += 1;
        }
        if (sun.isChecked()) {
            weeksun ="SU";
            Weekselect.append(weeksun+","+" ");
            totalselect += 1;
        }

        // Weekselect.append("\nTotal: " + totalselect);
        // Toast.makeText(getApplicationContext(), Weekselect.toString(), Toast.LENGTH_LONG).show();

    }



    private void AddClassesIncourceProcess() {
        selectdays();
        ClassTittle.setError(null);
        Details.setError(null);
        roomno.setError(null);
        bulding.setError(null);
        location.setError(null);

        if (Lat !=null && Lat.length() >0){

        }else {
            if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
                Lat = SessionManager.getInstance(getActivity()).getCollage().getLat();
                Lat = SessionManager.getInstance(getActivity()).getCollage().getLng();

            }else {
                Lat = String.valueOf(latitude);
                Lat = String.valueOf(longitude);


            }
        }

        if (ClassId == null){
            ClassId ="";
        }

        if (TypeSelected == null){
            TypeSelected ="";
        }


        if (TypeSelectedBatch == null){
            TypeSelectedBatch ="";
        }

        if (ClassTypeModified == null){
            ClassTypeModified =" ";
        }

        if (TypeSelectedBatchModified == null){
            TypeSelectedBatchModified =" ";
        }

        // Store values at the time of the login attempt.

        if (TypeSelected.matches("theory")){
            tittle = ClassTittle.getText().toString().trim();
        }else {
            tittle = ClassTittle.getText().toString().trim()+" "+ClassTypeModified+" "+TypeSelectedBatchModified;


        }
        classDetails = Details.getText().toString().trim();
        Buldingno = bulding.getText().toString().trim();
        Location = location.getText().toString().trim();
        roomi = roomno.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(tittle)) {
            focusView = ClassTittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
       /* } else if (co.questin.utils.TextUtils.isNullOrEmpty(classDetails)) {
            // check for First Name
            focusView = Details;
            cancel = true;
            showToast(getString(R.string.error_field_required));*/
        } else if (co.questin.utils.TextUtils.isNullOrEmpty(Buldingno)) {
            // check for First Name
            focusView = bulding;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
       /* } else if (co.questin.utils.TextUtils.isNullOrEmpty(Location)) {
            // check for First Name
            focusView = location;
            cancel = true;
            showToast(getString(R.string.error_field_required));*/
        } else if (co.questin.utils.TextUtils.isNullOrEmpty(roomi)) {
            // check for First Name
            focusView = roomno;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }else if (co.questin.utils.TextUtils.isNullOrEmpty(Location)) {
            // check for First Name
            focusView = location;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            /*  */

            ClassDetailsJson = new JSONObject();

            try {
                ClassDetailsJson.put("start_date", classDatestart);
                ClassDetailsJson.put("end_date", classDatesend);
                ClassDetailsJson.put("start_time", classtimestart);
                ClassDetailsJson.put("end_time", classtimesend);
                ClassDetailsJson.put("week", Weekselect);
                ClassDetailsJson.put("repeat", classrepeat);


                Log.d("TAG", "classdetails: " + ClassDetailsJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            classcreateteacherAuthTask = new ClassCreateTeacherAuthTask();
            classcreateteacherAuthTask.execute();

        }

    }






    private class ClassCreateTeacherAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            RequestBody body = new FormBody.Builder()
                    .add("type","classes")
                    .add("title",tittle)
                    .add("body",classDetails)
                    .add("field_og_subject",Subject_id)
                    .add("field_class_type",TypeSelected)
                    .add("field_theory_class",ClassId)
                    .add("field_batch",TypeSelectedBatch)
                    .add("field_instructor_class",TeacherEmail+" [uid:"+TeacherUid+"]")
                    .add("field_dept_location[lat]", String.valueOf(Lat))
                    .add("field_dept_location[lng]", String.valueOf(Long))
                    .add("field_building",Buldingno)
                    .add("field_grades",roomi)
                    .add("field_class_time",ClassDetailsJson.toString())
                    .build();
            try {
                String responseData = ApiCall.POSTHEADERCREATE(client, URLS.URL_TEACHERCREATECLASS,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherAddClasses.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            classcreateteacherAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        if (FragmentPosition ==2){
                            TeacherCourseModuleDetails.getCurrentPosition(FragmentPosition);
                            TeacherClassList.CalledFromAddClass();
                            finish();

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();


            }
        }

        @Override
        protected void onCancelled() {
            classcreateteacherAuthTask = null;
            hideLoading();



        }
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }


    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }




    //Getting current location
    private void getCurrentLocation() {
        // mMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);



        }
    }

    //Function to move the map
    private void moveMap(double latitude, double longitude, String placename) {
        LatLng latLng = new LatLng(latitude, longitude);
        MapLayout.setVisibility(View.VISIBLE);
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title(placename)); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(25));



    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.getUiSettings().setAllGesturesEnabled(false);

        // Use a custom info window adapter to handle multiple lines of text in the
        // info window contents.
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.class_location,
                        (FrameLayout) findViewById(R.id.map), false);

                TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                title.setText(marker.getTitle());

                TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                snippet.setText(marker.getSnippet());

                return infoWindow;
            }
        });


        getCurrentLocation();
        getLocationPermission();

    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                location.setText(placename);
                  Lat=latitude;
                  Long =longitude;
                moveMap(place.getLatLng().latitude,place.getLatLng().longitude,placename);
                Log.d("ClassBuilding", "onActivityResult: "+stBuilder.toString());

            }
        }
    }




    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
