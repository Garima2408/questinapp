package co.questin.teacher;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import co.questin.R;
import co.questin.adapters.ExamResultUploadAdapter;
import co.questin.models.ResultUploadArray;
import co.questin.models.SubjectExamsArray;
import co.questin.models.SubjectsAssignmentsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.adapters.ExamResultUploadAdapter.lastselectedAnswers;
import static co.questin.adapters.ExamResultUploadAdapter.selectedAnswers;

public class TeacherExamResultUpload extends BaseAppCompactActivity {
    Spinner ExamsSpin,TypeList;
    String Course_id,Module,CourseTittle,StudentId,StudentMarks,Studentpresent,resultuploaded;
    ArrayList<SubjectExamsArray> listofexams;
    private ProgressSearchOfExamList progresssearchofexamlist = null;
    private  SubjectsAssignmentsDisplay assignmentdisplay = null;
    private StudentListForResultUpload studentlistresultupload = null;
    ListView list_of_Students;
    private ExamResultUploadAdapter addattendanceadapter;
    public ArrayList<ResultUploadArray> mAttendanceList;
    public  ArrayList<SubjectsAssignmentsArray> mAssignmentList;
    private ArrayList<String> examlist;
    private ArrayList<String> assignmentlist;
    int FragmentPosition;
    JSONArray studentselected;
    JSONObject sharefriendJson;
    private AddAttendenceAsync attendenceAsync = null;
    String ExamId;
    String AddedValue;
    FrameLayout top;
    String ResultTpyeSelected,ResultSelectType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_exam_result_upload);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        try{

            Bundle b = getIntent().getExtras();
            Course_id = b.getString("COURSE_ID");
            CourseTittle = b.getString("TITTLE");
            FragmentPosition = Integer.parseInt(b.getString("SAME"));
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        ExamsSpin =findViewById(R.id.ExamsSpin);
        list_of_Students =findViewById(R.id.list_of_Students);
        TypeList =findViewById(R.id.typelist);
        top =findViewById(R.id.top);
        examlist = new ArrayList<String>();
        listofexams = new ArrayList<SubjectExamsArray>();
        mAttendanceList=new ArrayList<ResultUploadArray>();
        assignmentlist=new ArrayList<String>();
        mAssignmentList =new ArrayList<SubjectsAssignmentsArray>();

        final String[] ResultType = { "Exam", "Assignment",  };
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(c.getTime());
        resultuploaded = formattedDate2+" 00:00:00";


        ArrayAdapter classadapter = new ArrayAdapter(TeacherExamResultUpload.this,android.R.layout.simple_spinner_item,ResultType);
        classadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        TypeList.setAdapter(classadapter);


        TypeList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ResultTpyeSelected = ResultType[position];


                if (ResultType !=null){


                    if (ResultTpyeSelected.matches("Exam")){
                        examlist.clear();
                        listofexams.clear();
                        ResultSelectType ="exam";
                        progresssearchofexamlist = new ProgressSearchOfExamList();
                        progresssearchofexamlist.execute(Course_id);

                    }else if (ResultTpyeSelected.matches("Assignment")){
                        mAssignmentList.clear();
                        assignmentlist.clear();
                        ResultSelectType ="assignment";
                        assignmentdisplay = new SubjectsAssignmentsDisplay();
                        assignmentdisplay.execute(Course_id);

                    }


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }



/*list of exams*/

    private class ProgressSearchOfExamList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTEXAMS+"?"+"sid="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherExamResultUpload.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofexamlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if (data != null && data.length() > 0) {
                            for (int i = 0; i < data.length(); i++) {
                                SubjectExamsArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectExamsArray.class);
                                examlist.add(GoalInfo.getTitle());
                                listofexams.add(GoalInfo);

                                ExamsSpin.setAdapter(new ArrayAdapter<String>(TeacherExamResultUpload.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        examlist));


                                ExamsSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        ExamId = listofexams.get(position).getTnid();
                                        mAttendanceList.clear();
                                        studentlistresultupload = new StudentListForResultUpload();
                                        studentlistresultupload.execute(Course_id);

                                        Log.d("TAG", "edt_selectsub: " + ExamId);

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }

                                });


                            }
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherExamResultUpload.this);
                            builder.setTitle("Info");
                            builder.setMessage("No Exam Added. Add Exam")
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                            finish();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofexamlist = null;
            hideLoading();


        }
    }

    /*LIST OF ASSIGNMENTS*/


    private  class SubjectsAssignmentsDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTS+"?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                TeacherExamResultUpload.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdisplay = null;
            try {
                String errorCode = null;
                if (responce != null) {
                    hideLoading();
                    errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                SubjectsAssignmentsArray assignmentInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectsAssignmentsArray.class);
                                mAssignmentList.add(assignmentInfo);
                                assignmentlist.add(assignmentInfo.getTitle());

                            }


                            ExamsSpin.setAdapter(new ArrayAdapter<String>(TeacherExamResultUpload.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    assignmentlist));


                            ExamsSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    ExamId = mAssignmentList.get(position).getTnid();
                                    mAttendanceList.clear();
                                    studentlistresultupload = new StudentListForResultUpload();
                                    studentlistresultupload.execute(Course_id);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }

                            });


                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherExamResultUpload.this);
                            builder.setTitle("Info");
                            builder.setMessage("No Exam Added. Add Exam")
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }

                }
            }catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            assignmentdisplay = null;
            hideLoading();

        }
    }









    private class StudentListForResultUpload extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDSTUDENTRESULTLIST+"/"+args[0]+"/og");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherExamResultUpload.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlistresultupload = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        for (int i = 0; i <= jsonArrayData.length(); i++) {



                            ResultUploadArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ResultUploadArray.class);
                            mAttendanceList.add(CourseInfo);


                            addattendanceadapter = new ExamResultUploadAdapter(TeacherExamResultUpload.this, mAttendanceList);
                            list_of_Students.setAdapter(addattendanceadapter);


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentlistresultupload = null;
            hideLoading();


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;

            case R.id.Submit:
                String message = "";

                studentselected = new JSONArray();

            // get the value of selected answers from custom adapter

                for (int j=0; j< lastselectedAnswers.size(); j++){

                    Log.d("TAG", "message: " + lastselectedAnswers.get(0));

                    selectedAnswers.add(lastselectedAnswers.get(0));

                }



                for (int i = 0; i < selectedAnswers.size(); i++) {
                    message = selectedAnswers.get(i);

                    Log.d("TAG", "messageprintdata: " + message);

                    String[] splited = message.split(",");
                    String uid = splited[0];
                    StudentMarks = splited[1];
                    StudentId= splited[2];

                    if (StudentMarks.isEmpty()) {

                        StudentMarks ="0";
                        Studentpresent="2";
                        Log.d("TAG", "arryform: " + StudentMarks +","+Studentpresent);

                    }
                    else {
                        Log.d("TAG", "arryform2: " + StudentMarks +","+StudentId+","+Studentpresent);
                        Studentpresent="1";
                        sharefriendJson = new JSONObject();
                        try {
                            sharefriendJson.put("uid", StudentId);
                            sharefriendJson.put("present", Studentpresent);
                            sharefriendJson.put("subject", Course_id);
                            sharefriendJson.put("nid", ExamId);
                            sharefriendJson.put("marks", StudentMarks);
                            sharefriendJson.put("type", ResultSelectType);
                            sharefriendJson.put("date", resultuploaded);
                            studentselected.put(sharefriendJson);
                            Log.d("TAG", "studentselected: " + studentselected);
                            Log.d("TAG", "sharefriendJson: " + sharefriendJson);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                attendenceAsync = new AddAttendenceAsync();
                attendenceAsync.execute();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private class AddAttendenceAsync extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("records",studentselected.toString())
                    .build();





            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDSTUDENTRESULTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherExamResultUpload.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            attendenceAsync = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(TeacherExamResultUpload.this);
                        builder.setMessage(msg)
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                                        if (FragmentPosition ==6){
                                            TeacherCourseModuleDetails.getCurrentPosition(FragmentPosition);
                                            TeacherResultUpload.CalledFromAddResult();
                                            finish();

                                        }


                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            attendenceAsync = null;
            hideLoading();


        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }



}
