package co.questin.network;

/**
 * Created by Dell on 19-07-2017
 */

public interface URLS {

    String QUESTIN_BASE_URL = "https://www.questin.co/";
    String QUESTIN_API ="api/";
    String QUESTIN_VERSION ="v1/";



    String FINAL_QUESTIN_URL = QUESTIN_BASE_URL+QUESTIN_API+QUESTIN_VERSION;



    /*Setting*/

    String PRIVACY_POLICY = QUESTIN_BASE_URL+"privacy-policy";
    String TERMS_OF_SERVICE = QUESTIN_BASE_URL+"terms-conditions";





    String URL_LOGIN = FINAL_QUESTIN_URL+"people/login";
    String URL_LOGOUT = FINAL_QUESTIN_URL+"people/logout";
    String URL_SINGUP = FINAL_QUESTIN_URL+"people";
    String URL_FORGETPASSWORD = FINAL_QUESTIN_URL+"people/request_new_password";
    String URL_MYGROUP = FINAL_QUESTIN_URL+"groups";
    String URL_MYCOURSES = FINAL_QUESTIN_URL+"subjects";
    String URL_EMAIL = FINAL_QUESTIN_URL+"submission";
    String URL_AVATAR = FINAL_QUESTIN_URL+"people/avatar";
    String URL_UPDATENAME = FINAL_QUESTIN_URL+"people";
    String URL_UPDATEPASSWORD = FINAL_QUESTIN_URL+"people";
    String URL_USERPROFILEDISPLAY = FINAL_QUESTIN_URL+"college_user_profile";
    String URL_MYEVENTS = FINAL_QUESTIN_URL+"people/mycollege_events";





    /*collage urls*/

    String URL_COLLAGELISTBYTYPE = FINAL_QUESTIN_URL;
    String URL_COLLAGELIST = FINAL_QUESTIN_URL+"collegebyname";
    String URL_STUDENETREGISTRATION = FINAL_QUESTIN_URL+"og_membership";
    String URL_COURCELIST = FINAL_QUESTIN_URL+"college_courses";
    String URL_IMPORTANTLINKS = FINAL_QUESTIN_URL+"college_links";
    String URL_CAMPUSSERVICES = FINAL_QUESTIN_URL+"college_service";
    String URL_SERVICEDETAILS = FINAL_QUESTIN_URL+"content";
    String URL_COLLAGECLUBS = FINAL_QUESTIN_URL+"college_subgroup";
    String URL_COLLAGEGROUPINFO= FINAL_QUESTIN_URL+"content";
    String URL_COLLAGEEVENT = FINAL_QUESTIN_URL+"college_events";
    String URL_COLLAGEEVENTDETAILS = FINAL_QUESTIN_URL+"content";
    String URL_COLLAGEEVENTDETAILSSUBSCRIBE = FINAL_QUESTIN_URL+"flag/flag";
    String URL_COLLAGMEDIA = FINAL_QUESTIN_URL+"college_media";
    String URL_COLLAGMEDIADETAILS = FINAL_QUESTIN_URL+"content";
    String URL_COLLAGEFACILITIES = FINAL_QUESTIN_URL+"college_facilities";
    String URL_COLLAGEFACULTIES = FINAL_QUESTIN_URL+"college_faculty_list";
    String URL_COLLAGESUBJECTINFO= FINAL_QUESTIN_URL+"content";
    String URL_COLLAGEANNOUNCEMENTLIST= FINAL_QUESTIN_URL+"college_news";
    String URL_COLLAGEMERGENCYNO= FINAL_QUESTIN_URL+"college_emergency_contacts";
    String URL_COLLAGEPROVIDED = FINAL_QUESTIN_URL+"college_courses";
    String URL_COLLAGEALUMINILIST = FINAL_QUESTIN_URL+"college_alumni_list";






    /*MY FRIENDS WORK*/

    String URL_REMOVE_MYFRIEND = FINAL_QUESTIN_URL+"relationships/delete";
    String URL_MYFRIENDS = FINAL_QUESTIN_URL+"myfriends";
    String URL_ALLUSERS = FINAL_QUESTIN_URL+"userslistbymail";
    String URL_SENDINFITE = FINAL_QUESTIN_URL+"people";
    String URL_AXCCEPTREQUEST = FINAL_QUESTIN_URL+"relationships/approve";
    String URL_REJECTREQUEST = FINAL_QUESTIN_URL+"relationships/delete";
    String URL_SEARCHBYEMAIL = FINAL_QUESTIN_URL+"userslistbymail?mail=";
    String URL_GETPENDINGREQUEST = FINAL_QUESTIN_URL+"receivedpendingfriendrequest";



    /*CAMPUS FEEDS*/

    String URL_CAMPUSFEED = FINAL_QUESTIN_URL+"college_post";
    String URL_FILTERLIST = FINAL_QUESTIN_URL+"taxonomy_vocabulary/getTree";
    String URL_CAMPUSFEEDADD = FINAL_QUESTIN_URL+"comment";
    String URL_MYCAMPUSFEED = FINAL_QUESTIN_URL+"my_college_post";
    String URL_UPDATEMYCAMPUSFEED = FINAL_QUESTIN_URL+"comment";
    String URL_DELETECOMMENTS = FINAL_QUESTIN_URL+"comment";
    String URL_SHORTCOMMENTS = FINAL_QUESTIN_URL+"college_post_by_term";
    String URL_LIKEDCOMMENTS = FINAL_QUESTIN_URL+"flag/flag";
    String URL_MARKEDINAPPROPRIATECOMMENT = FINAL_QUESTIN_URL+"flag/flag";



    /*SUBJECTS COMMENTS*/

    String URL_COLLAGECOURCEMODULE = FINAL_QUESTIN_URL+"college_subjects";
    String URL_COLLAGESUBCLASSES = FINAL_QUESTIN_URL+"subject_classes";
    String URL_SUBJECTCLASSESDETAILS = FINAL_QUESTIN_URL+"content";
    String URL_SUBJECTEXAMS = FINAL_QUESTIN_URL+"subject_exam";
    String URL_SUBJECTASSIGNMENTS = FINAL_QUESTIN_URL+"subject_assignment";
    String URL_SUBJECTASSIGNMENTSDETAILS = FINAL_QUESTIN_URL+"content";
    String URL_SUBJECTEXAMSDETAILS = FINAL_QUESTIN_URL+"content";
    String URL_DELETESUBJECTCOMMENTS = FINAL_QUESTIN_URL+"comment";
    String URL_UPDATEMYSUBJECTCOMMENT = FINAL_QUESTIN_URL+"comment";
    String URL_TEACHERMYSUBJECTS = FINAL_QUESTIN_URL+"people";
    String URL_COLLAGECOURCESEARCH = FINAL_QUESTIN_URL;
    String URL_COLLAGECLASSMODULE = FINAL_QUESTIN_URL+"subject_theory_classes";




    /*FAQ*/

    String URL_FAQ = FINAL_QUESTIN_URL+"faq_service";


    /*DUES URLS*/
    String URL_MYCOURCEINCOMPLETEDUESLIST= FINAL_QUESTIN_URL+"commerce/user_fee";

    String URL_GENERATEMYORDERID= FINAL_QUESTIN_URL+"commerce/create_order";

    String URL_STATUSSENDTOSERVER= FINAL_QUESTIN_URL+"order";

    String URL_STUDENTDUESURL= FINAL_QUESTIN_URL+"attendance";







    /*
     */

    /*TEACHER SUBJECTS LIST*/

    String URL_TEACHERMYCOURCE = FINAL_QUESTIN_URL+"subjects";


    /*my profiles*/



    String URL_MYATTENDANCE = FINAL_QUESTIN_URL+"attendance/retrieve";
    String URL_MYRESULT = FINAL_QUESTIN_URL+"results";
    String URL_MYWALLLPIC = FINAL_QUESTIN_URL+"people/upload_wall_picture";
    String URL_EXITGROUP = FINAL_QUESTIN_URL+"og/leave";




    /*Course and subjects*/


    String URL_COLLAGECOURCEMEMBERLISTS = FINAL_QUESTIN_URL+"og";
    String URL_JOINSUBJECTS = FINAL_QUESTIN_URL+"og/join";
    String URL_SUBJECTSCOMMENTS = FINAL_QUESTIN_URL+"subject_post";
    String URL_SUBJECTSADDCOMMENTS = FINAL_QUESTIN_URL+"comment";
    String URL_SUBJECTSREPLYCOMMENTS = FINAL_QUESTIN_URL+"subject_child_comments";
    String URL_MYSUBJECTATTENDANCE = FINAL_QUESTIN_URL+"attendance/student_attendance_by_subject";
    String URL_MYCALENDERDISPLAY = FINAL_QUESTIN_URL+"attendance/user_monthly_attendance";
    String URL_JOININSUBJECTS = FINAL_QUESTIN_URL+"og_membership";
    String URL_SUBJECTSRESULT = FINAL_QUESTIN_URL+"results";



    /*GROUP AND SOCITY*/


    String URL_GROUPMEMBERLISTS = FINAL_QUESTIN_URL+"og";
    String URL_JOINGROUP = FINAL_QUESTIN_URL+"og/join";
    String URL_GROUPCOMMENTS = FINAL_QUESTIN_URL+"club_post";
    String URL_REPLYCOMMENTS = FINAL_QUESTIN_URL+"college_post_comments";
    String URL_ADDCOMMENTS = FINAL_QUESTIN_URL+"comment";


    /*TEACHER SECTION*/

    String URL_STUDENTATTENDANCELIST = FINAL_QUESTIN_URL+"attendance";
    String URL_ADDSTUDENTATTENDANCE = FINAL_QUESTIN_URL+"attendance";
    String URL_ATTENDANCECLASSES = FINAL_QUESTIN_URL+"attendance_subject_classes";
    String URL_TEACHERCREATEEXAM = FINAL_QUESTIN_URL+"content";
    String URL_TEACHERCREATEASSIGNMENT = FINAL_QUESTIN_URL+"content";
    String URL_TEACHERCREATECOURSE = FINAL_QUESTIN_URL+"content";
    String URL_STUDENTADDATTANDANCE = FINAL_QUESTIN_URL+"attendance/student_list_attendance";
    String URL_TEACHERCREATECLASS = FINAL_QUESTIN_URL+"content";
    String URL_TEACHERSTUDENTVIEWATTANDANCE = FINAL_QUESTIN_URL+"attendance/faculty_student_attendance";
    String URL_ADDSTUDENTINSUBJECTBYEMAIL = FINAL_QUESTIN_URL+"college_student_list";
    String URL_ADDSTUDENTINSUBJECT = FINAL_QUESTIN_URL+"og/join";
    String URL_DELETESUBJECTS = FINAL_QUESTIN_URL+"content";
    String URL_UPDATESUBJECTS = FINAL_QUESTIN_URL+"content/";
    String URL_ADDSTUDENTRESULTLIST = FINAL_QUESTIN_URL+"attendance";
    String URL_ADDSTUDENTRESULTS = FINAL_QUESTIN_URL+"results";
    String URL_STUDENTSUBJECTREQUESTED = FINAL_QUESTIN_URL+"og";
    String URL_STUDENTSUBJECTREQUESTEDAPPROVED = FINAL_QUESTIN_URL+"og_membership";
    String URL_STUDENTSUBJECTREQUESTEDREJECT = FINAL_QUESTIN_URL+"og_membership";
    String URL_SEMSTERID = FINAL_QUESTIN_URL+"taxonomy_vocabulary/getTree";
    String URL_UPLOADFILETOSERVER= FINAL_QUESTIN_URL+"file/upload_subject_documents";
    String URL_COLLAGEFACULTYLIST= FINAL_QUESTIN_URL+"college_faculty_list";
    String URL_ADDTEACHERINSUBJECT = FINAL_QUESTIN_URL+"og/join";
    String URL_ADDTEACHERINBUSROUTE = FINAL_QUESTIN_URL+"og/join";
    String URL_ADDTEACHERINSUBJECTLIST = FINAL_QUESTIN_URL+"subject";
    String URL_TEACHERUPDATEEXAM = FINAL_QUESTIN_URL+"content";
    String URL_TEACHERREMOVETEACHER = FINAL_QUESTIN_URL+"og/leave";
    String URL_TEACHERADDLOGO = FINAL_QUESTIN_URL+"file/upload_subject_icon";
    String URL_TEACHERREMOVEDOCUMENTS = FINAL_QUESTIN_URL+"file/remove_subject_documents";
    String URL_TEACHERREMOVELINK = FINAL_QUESTIN_URL+"content";
    String URL_TEACHERUPDATEASSIGNMENT = FINAL_QUESTIN_URL+"content";
    String URL_DELETEASSIGNMENT = FINAL_QUESTIN_URL+"content";
    String URL_UPLOADFILETOASSIGNMENT= FINAL_QUESTIN_URL+"file/upload_subject_assignment_documents";
    String URL_UPLOADFILETOASSIGNMENTREMOVE= FINAL_QUESTIN_URL+"file/remove_subject_assignment_documents";
    String URL_COLLAGEFACULTYSUBJECT= FINAL_QUESTIN_URL+"people";
    String URL_CREATENEWSANDANNOUNCE= FINAL_QUESTIN_URL+"content";
    String URL_UPDATECOLLAGENEWSLIST = FINAL_QUESTIN_URL+"content";
    String URL_DELETECOLLAGENEWSLIST = FINAL_QUESTIN_URL+"content";
    String URL_CREATE_STUDENTINCOLLAGE = FINAL_QUESTIN_URL+"people/create_student";
    String URL_SUBJECTTHRORYCLASS = FINAL_QUESTIN_URL+"subject_theory_classes";
    String URL_SUBJECTCLASSADDSTUDENT = FINAL_QUESTIN_URL+"subject/class_flagged_members";
    String URL_STUDENTDETAILRESULTDISPLAY = FINAL_QUESTIN_URL+"results"+"/faculty_student_results";


    /*CALENDER APIS*/

    String URL_CALENDERMONTLYDATES = FINAL_QUESTIN_URL+"people/timetable";
    String URL_MYCALENDERMONTLYDATES = FINAL_QUESTIN_URL+"people/my_timetable";
    String URL_MYCLASSESDETAILS = FINAL_QUESTIN_URL+"content";




    /*CHAT GROUP NAME*/
    String URL_CREATEGROUP = FINAL_QUESTIN_URL+"content";
    String URL_UPDATEGROUPNAME = FINAL_QUESTIN_URL+"content";
    String URL_CREATEGROUPMEMBER = FINAL_QUESTIN_URL+"og/join";
    String URL_CHATGROUPMEMBER = FINAL_QUESTIN_URL+"og";
    String URL_MYGROUPSMEMBER = FINAL_QUESTIN_URL+"my_chat_groups";
    String URL_DELETEMYGROUPS = FINAL_QUESTIN_URL+"content";
    String URL_UPDATEGROUPPHOTO = FINAL_QUESTIN_URL+"file/upload_chat_group_icon";
    String URL_CHATGROUPMEMBEREXIXT = FINAL_QUESTIN_URL+"og/leave";
    String URL_DELETE_SERVER_CHAT = FINAL_QUESTIN_URL+"messages";




    // User APIs
    String USER_DETAILS = FINAL_QUESTIN_URL + "people/" /* {uid} user id */;


    //    Google maps web services
    String MAPS_BASE_URL = "https://maps.googleapis.com/maps/api/";
    String MAPS_DIRECTIONS = MAPS_BASE_URL + "directions/json?";
    String MAPS_DIST_MATRIX = MAPS_BASE_URL + "distancematrix/json?";




    // Push Notifications
    String REG_TOKEN = FINAL_QUESTIN_URL + "push_notifications"; // add token for delete request
    String SEND_MSG = FINAL_QUESTIN_URL + "push_notifications/send_message";
    String GROUPMSG = FINAL_QUESTIN_URL + "push_notifications/send_group_message";
    String SEND_MESSAGE_TO_SERVER =FINAL_QUESTIN_URL+"messages";
    String GET_LIST_FROM_SERVER =FINAL_QUESTIN_URL+"messages?type=inbox";
    String GET_CHAT_FROM_SERVER =FINAL_QUESTIN_URL+"messages/user_messages";
    String SEND_MSG_TO_ALL = FINAL_QUESTIN_URL + "push_notifications/send_bus_route_notifications";




    /*WeatherUrl*/



    /*TRANSPORT WORK*/
    // College Routes
    String ROUTES_LIST = FINAL_QUESTIN_URL + "college_bus_routes"; // add college ID
    String ROUTE_DETAILS = FINAL_QUESTIN_URL + "content/"; // add route ID
    String ROUTE_JOIN = FINAL_QUESTIN_URL + "og/join"; // add route ID
    String URL_ROUTEINFO= FINAL_QUESTIN_URL+"content";
    String URL_ROUTEJOINPENDINGREQUEST= FINAL_QUESTIN_URL+"og";
    String URL_ROUTEJOINAXCCEPTREQUEST = FINAL_QUESTIN_URL+"og_membership";
    String URL_ROUTEJOINREJECTREQUEST = FINAL_QUESTIN_URL+"og_membership";
    String URL_ROUTESTUDENTLIST = FINAL_QUESTIN_URL+"bus-route";
    String URL_ROUTESTUDENTBUSATTANDANCE = FINAL_QUESTIN_URL+"attendance/add_bus_attendance";
    String URL_ROUTECOMMENTSLIST = FINAL_QUESTIN_URL+"bus_route_comments";
    String URL_ROUTEATTANDANCEDISPLAY = FINAL_QUESTIN_URL+"bus-route/faculty_student_attendance";
    String URL_ROUTEMEMBERLIST = FINAL_QUESTIN_URL+"og";
    String URL_ROUTEREPLYCOMMENTS = FINAL_QUESTIN_URL+"bus_route_child_comments";
    String URL_ROUTEMYATTANDANCESTUDENT = FINAL_QUESTIN_URL+"bus-route/user_monthly_attendance";
    String URL_ROUTEFACULTYLISTING = FINAL_QUESTIN_URL+"bus-route";
    String URL_ROUTEFACULTYREMOVE = FINAL_QUESTIN_URL+"og/leave";
    String URL_ROUTESTUDENTJOINT = FINAL_QUESTIN_URL+"og/join";
    String URL_ROUTEREMOVETEACHER = FINAL_QUESTIN_URL+"og/leave";
    String ROUTE_JOINBUS = FINAL_QUESTIN_URL+"og_membership"; // add route ID


    /*ADMIN URLS*/

    String URL_COLLAGEMEMBERLIST = FINAL_QUESTIN_URL;





}
