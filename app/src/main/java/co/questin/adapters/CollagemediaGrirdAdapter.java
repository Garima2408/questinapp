package co.questin.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.questin.R;

/**
 * Created by AKASH on 26-Feb-18.
 */

public class CollagemediaGrirdAdapter extends BaseAdapter {
    ImageView imageViewAndroid;
    private Context mContext;
    int i=0;
    List<String> list;
    ViewPager pagerImage;
    PagerAdapter Imageadapter;
    Dialog imageDialog;

    public CollagemediaGrirdAdapter(Context context,  List<String> list) {
        mContext = context;
        this.list = list;

    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            gridViewAndroid = new View(mContext);

            gridViewAndroid = inflater.inflate(R.layout.dashbord_grids, null);

             imageViewAndroid = gridViewAndroid.findViewById(R.id.android_gridview_image);



             if(list.get(i) !=null &&list.get(i).length() > 0 ){

                 Glide.with(mContext).load(list.get(i))
                         .placeholder(R.mipmap.media_placeholder).dontAnimate()
                         .fitCenter().into(imageViewAndroid);
             }

            imageViewAndroid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageDialogmultiple(list,i);

                }
            });



        } else {
            gridViewAndroid = convertView;
        }

        return gridViewAndroid;
    }

    private void imageDialogmultiple(List<String> s, int i) {

            imageDialog = new Dialog(mContext);
            imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            imageDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.BLACK));
            imageDialog.setContentView(R.layout.multiple_images_display);
            // dialogLogin.setCancelable(false);
            imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            imageDialog.show();
            pagerImage = imageDialog. findViewById(R.id.pager);

            Imageadapter = new MediaDetailsAdapter(mContext, s);
            pagerImage.setAdapter(Imageadapter);
                   pagerImage.setCurrentItem(i);
    }

    private View.OnClickListener onChagePageClickListener(final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageDialogmultiple(list,i);

            }
        };
    }


    public class EnableDisableViewPager extends ViewPager {

        private boolean enabled = true;

        public EnableDisableViewPager(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent arg0) {
            if(enabled)
                return super.onInterceptTouchEvent(arg0);

            return false;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
    }



}

