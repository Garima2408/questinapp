package co.questin.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.models.SubjectFacultyArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.teacher.CourseModuleTeacherInfo;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 14-01-2018.
 */

public class FacultyListInSubjectAdapter  extends RecyclerView.Adapter<FacultyListInSubjectAdapter.CustomVholder> {
    private DeleteMemberFromList deletememberfromlist = null;

    private ArrayList<SubjectFacultyArray> lists;
    String course_id,Adminuser;
    private Activity activity;


    public FacultyListInSubjectAdapter( Activity activity, ArrayList<SubjectFacultyArray> lists , String course_id ,String Adminuser) {
        this.lists = lists;
        this.activity = activity;
        this.course_id =course_id;
        this.Adminuser =Adminuser;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_teacherinsubject, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.Teacher.setText(lists.get(position).getName());

            holder.intro4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID", lists.get(position).getUid());
                    Pair<ImageView, String> p1 = Pair.create(holder.friendIcon, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(holder.Teacher, "publisherName");

                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());



                }
            });





            holder.Department.setText(lists.get(position).getDepartment());
            //  holder.tv_department.setText(lists.get(position).getDepartment());


            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(holder.friendIcon);
                Glide.with(activity).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.friendIcon);
            }


            if (Adminuser.equals("TRUE")) {


                if(lists.get(position).getUid().equals(SessionManager.getInstance(activity).getUser().getUserprofile_id())){
                    holder.options.setVisibility(View.INVISIBLE);

                }else {
                    holder.options.setVisibility(View.VISIBLE);

                }




            } else if (Adminuser.equals("FALSE")) {
                holder.options.setVisibility(View.INVISIBLE);

            }



            holder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenu();


                }

                private void showPopupMenu() {


                    PopupMenu popup = new PopupMenu(activity, holder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.removepopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {

                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Remove")
                                            .setMessage(R.string.Remove)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    deletememberfromlist = new DeleteMemberFromList();
                                                    deletememberfromlist.execute(course_id,lists.get(position).getUid());

                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();



                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();




                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView friendIcon,options;
        private TextView Teacher, tv_email,Department;
        RelativeLayout intro4;

        public CustomVholder(View itemView) {
            super(itemView);

            Teacher = itemView.findViewById(R.id.Teacher);
            friendIcon = itemView.findViewById(R.id.friendIcon);
            Department =itemView.findViewById(R.id.Department);
            options =itemView.findViewById(R.id.options);
            intro4 =itemView.findViewById(R.id.intro4);

        }


    }


    /*LISTS OF DELETE COMMENTS ON SUBJECTS*/


    private class DeleteMemberFromList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(activity);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_TEACHERREMOVETEACHER +"/"+args[0]+"/"+args[1],body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletememberfromlist = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(activity);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.p_dialog_dismiss(activity);
                       // Utils.showAlertDialog(mcontext, "Delete", msg);
                        CourseModuleTeacherInfo.RefreshWorkedFaculty();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(activity);
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                } else {
                    Utils.p_dialog_dismiss(activity);
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deletememberfromlist = null;
            Utils.p_dialog_dismiss(activity);


        }
    }


}





