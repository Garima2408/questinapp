package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.models.MyduesArray;
import co.questin.paymentactivity.InvoiceDetails;

/**
 * Created by Dell on 09-12-2017.
 */

public class MyPaidDuesAdapter extends RecyclerView.Adapter<MyPaidDuesAdapter.CustomVholder> {


    private ArrayList<MyduesArray> lists;
    private Context mcontext;
    boolean isPressed = false;

    public MyPaidDuesAdapter(Context mcontext, ArrayList<MyduesArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_mycompletedues, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {
        try {
            holder.Fees.setText(lists.get(position).getFee());
            holder.duefees.setText(lists.get(position).getLate_fee());
            holder.totalPay.setText(lists.get(position).getTotal_fee());
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = inputFormat.parse(lists.get(position).getTransaction_date());
            String outputDateStr = outputFormat.format(date);
            holder.Deposit_Date.setText(outputDateStr);
            holder.Sports_fee.setText(lists.get(position).getSports_fee());
            holder.Education_Fee.setText(lists.get(position).getEducation_Fee());
            holder.Exam_fee.setText(lists.get(position).getExam_fee());
            holder.Hostel_Fee.setText(lists.get(position).getHostel_Fee());
            holder.Others.setText(lists.get(position).getOthers());
            holder.Late_Fee.setText(lists.get(position).getLate_fee());
            holder.Transaction_Id.setText(lists.get(position).getTransaction_id());


            holder.paylayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mcontext,InvoiceDetails.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("Order_ID",lists.get(position).getId());
                    bundle.putString("Uid",lists.get(position).getUid());
                    bundle.putString("Enrolment_ID",lists.get(position).getEnrollmentId());
                    bundle.putString("DUE_Date",lists.get(position).getTransaction_date());
                    bundle.putString("Total_FEE",lists.get(position).getTotal_fee());
                    bundle.putString("Transaction_id",lists.get(position).getTransaction_id());
                    bundle.putString("Sports_fee",lists.get(position).getSports_fee());
                    bundle.putString("Education_Fee",lists.get(position).getEducation_Fee());
                    bundle.putString("Exam_fee",lists.get(position).getExam_fee());
                    bundle.putString("Hostel_Fee",lists.get(position).getHostel_Fee());
                    bundle.putString("Others",lists.get(position).getOthers());
                    bundle.putString("Late_Fee",lists.get(position).getLate_fee());








                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);





                }
            });

            holder.TextStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(isPressed) {
                        holder.ComponentList.setVisibility(View.VISIBLE);
                        holder.componetexpnd.setImageResource(R.mipmap.expanminus);

                    }
                    else {
                        holder.ComponentList.setVisibility(View.GONE);
                        holder.componetexpnd.setImageResource(R.mipmap.expanplus);


                    }


                    isPressed = !isPressed; // reverse


                }



            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {

        private TextView Fees,duefees,Deposit_Date,TextStatus,totalPay,Transaction_Id;
        LinearLayout link,ComponentList;
        RelativeLayout paylayout,Layexpand;
        ImageView componetexpnd;
        private TextView Sports_fee,Education_Fee,Exam_fee,Hostel_Fee,Others,Late_Fee;

        public CustomVholder(View itemView) {
            super(itemView);

            Fees = itemView.findViewById(R.id.Fees);
            duefees = itemView.findViewById(R.id.duefees);
            Deposit_Date = itemView.findViewById(R.id.Deposit_Date);
            TextStatus = itemView.findViewById(R.id.TextStatus);
            totalPay =itemView.findViewById(R.id.totalPay);
            link = itemView.findViewById(R.id.link);
            paylayout =itemView.findViewById(R.id.paylayout);
            componetexpnd =itemView.findViewById(R.id.componetexpnd);
            ComponentList =itemView.findViewById(R.id.ComponentList);
            Layexpand =itemView.findViewById(R.id.Layexpand);
            Sports_fee = itemView.findViewById(R.id.Sports_fee);
            Education_Fee = itemView.findViewById(R.id.Education_Fee);
            Exam_fee = itemView.findViewById(R.id.Exam_fee);
            Hostel_Fee = itemView.findViewById(R.id.Hostel_Fee);
            Others = itemView.findViewById(R.id.Others);
            Late_Fee =itemView.findViewById(R.id.Late_Fee);
            Transaction_Id =itemView.findViewById(R.id.Transaction_Id);

        }

    }
}
