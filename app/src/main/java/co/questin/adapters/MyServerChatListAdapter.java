package co.questin.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import co.questin.R;
import co.questin.models.chat.ServerChatListArray;
import co.questin.utils.SessionManager;

/**
 * Created by AKASH on 11-Mar-18.
 */

public class MyServerChatListAdapter extends ArrayAdapter<ServerChatListArray> {

    private Activity activity;
    private List<ServerChatListArray> messages;

    public MyServerChatListAdapter(Activity context, int resource, List<ServerChatListArray> messages) {
        super(context, resource, messages);
        this.activity = context;
        this.messages = messages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        int layoutResource = 0; // determined by view type
        try {
            ServerChatListArray ChatBubble = getItem(position);
            int viewType = getItemViewType(position);

        }catch (IndexOutOfBoundsException o){


        }

        if (SessionManager.getInstance(activity).getUser().getParentUid()!=null){
            if (messages.get(position).getRecipient().equals(SessionManager.getInstance(activity).getUser().getParentUid())) {
                layoutResource = R.layout.chat_text_left;
            } else {
                layoutResource = R.layout.chat_text_right;
            }
        }else{
            if (messages.get(position).getRecipient().equals(SessionManager.getInstance(activity).getUser().getUserprofile_id())) {
                layoutResource = R.layout.chat_text_left;
            } else {
                layoutResource = R.layout.chat_text_right;
            }
        }


        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        //set message content
        holder.msg.setText(messages.get(position).getBody());
        holder.text_time.setText(messages.get(position).getTimestamp().substring(12,17));

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        // return the total number of view types. this value should never change
        // at runtime. Value 2 is returned because of left and right views.
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        // return a value between 0 and (getViewTypeCount - 1)
        return position % 2;
    }

    private class ViewHolder {
        private TextView msg,text_time;


        public ViewHolder(View v) {
           msg = v.findViewById(R.id.text_message);
            text_time = v.findViewById(R.id.text_time);
        }
    }
}
