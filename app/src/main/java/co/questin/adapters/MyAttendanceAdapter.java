package co.questin.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import co.questin.R;
import co.questin.models.AttendanceArray;
import co.questin.models.TaskChildListArray;

/**
 * Created by Dell on 25-08-2017.
 */

public class MyAttendanceAdapter extends BaseExpandableListAdapter {

    private List<AttendanceArray> catList;
    private int itemLayoutId;
    private int groupLayoutId;
    private Context mContext;

    public MyAttendanceAdapter(Context ctx, List<AttendanceArray> catList) {

        this.itemLayoutId = R.layout.list_of_childgroup;
        this.groupLayoutId = R.layout.list_of_parentgroup;
        this.catList = catList;
        this.mContext = ctx;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return catList.get(groupPosition).childsTaskList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return catList.get(groupPosition).childsTaskList.get(childPosition).hashCode();
    }


    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_of_childgroup, parent, false);
        }

        TextView tittleChild = v.findViewById(R.id.tittlechild);
        TextView noChild = v.findViewById(R.id.nochild);

        TaskChildListArray task = catList.get(groupPosition).childsTaskList.get(childPosition);
        tittleChild.setText(task.getTitle());
        noChild.setText(task.getPerc());

        return v;

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int size = catList.get(groupPosition).childsTaskList.size();
        System.out.println("Child for group [" + groupPosition + "] is [" + size + "]");
        return size;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return catList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return catList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return catList.get(groupPosition).hashCode();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_of_parentgroup, parent, false);
        }

        TextView groupName = v.findViewById(R.id.tittle);
        TextView groupNumber = v.findViewById(R.id.no);


        if (isExpanded) {
            groupName.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.mipmap.faq_expanminus), null, null, null);
            groupName.setCompoundDrawablePadding(10);
        } else {
            groupName.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.mipmap.faq_expanplus), null, null, null);
            groupName.setCompoundDrawablePadding(10);
        }
        AttendanceArray cat = catList.get(groupPosition);

        groupName.setText(cat.parentTitle);
        groupNumber.setText(cat.numbers);
        return v;

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
