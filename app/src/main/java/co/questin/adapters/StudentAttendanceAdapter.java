package co.questin.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

import co.questin.R;
import co.questin.models.StudentListsArray;


/**
 * Created by Dell on 28-09-2017.
 */

public class StudentAttendanceAdapter extends RecyclerView.Adapter<StudentAttendanceAdapter.CustomVholder> {

    private ArrayList<StudentListsArray> lists;
    private Context mcontext;
    String Group_id,GroupTittle,groupImage;


    public StudentAttendanceAdapter(Context mcontext, ArrayList<StudentListsArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_markattendance, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.student_name.setText(lists.get(position).getName());

            if(lists.get(position).getPicture().isEmpty()){
                holder.circleView.setImageResource(R.mipmap.place_holder);

            }else {

                new DownloadImageTask(holder.circleView).execute(lists.get(position).getPicture());

            }

            holder.toplayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {







                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
       ImageView circleView;
        private TextView student_name;
        ImageButton leave,absent,present;
        RelativeLayout toplayout;


        public CustomVholder(View itemView) {
            super(itemView);


            student_name = itemView.findViewById(R.id.student_name);
            circleView = itemView.findViewById(R.id.circleView);

          //  present = (ImageButton) itemView.findViewById(R.id.present);
            toplayout = itemView.findViewById(R.id.toplayout);



        }

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
