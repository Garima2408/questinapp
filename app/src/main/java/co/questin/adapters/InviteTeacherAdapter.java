package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.models.InviteTeacherArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.teacher.InviteTeacher;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 14-01-2018.
 */

public class InviteTeacherAdapter extends RecyclerView.Adapter<InviteTeacherAdapter.CustomVholder> {


    private ArrayList<InviteTeacherArray> lists;
    private SearchedInviteTeacher sendinvitetoteacher = null;
    String course_id;
    private Context mcontext;


    public InviteTeacherAdapter(Context mcontext, ArrayList<InviteTeacherArray> lists, String course_id) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.course_id = course_id;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_inviteteacher, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getFirst_name() + " " + lists.get(position).getLast_name());
            Log.d("TAG", "Course_ID_adapter: " + course_id);

            holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID", lists.get(position).getUid());
                    mcontext.startActivity(backIntent);


                }
            });

             holder.tv_email.setText(lists.get(position).getEmail());
            holder.tv_department.setText(lists.get(position).getDepartment());


            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(holder.friendIcon);
                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.friendIcon);
            }
            holder.InviteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SendInviteToTeacher(course_id, lists.get(position).getUid());

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SendInviteToTeacher(String courseid, String Studentuid) {

        sendinvitetoteacher = new SearchedInviteTeacher();
        sendinvitetoteacher.execute(courseid, Studentuid);


    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView friendIcon;

        private TextView tv_friendname, tv_email,tv_department,InviteIcon;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_email = itemView.findViewById(R.id.tv_email);
            friendIcon = itemView.findViewById(R.id.friendIcon);
            InviteIcon = itemView.findViewById(R.id.InviteIcon);
            tv_department =itemView.findViewById(R.id.tv_department);


        }


    }
    /*
    SEND  REQUEST TO Teacher TO ADD IN SUBJECT*/

    private class SearchedInviteTeacher extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type", "subject")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDTEACHERINSUBJECT + "/" + args[0] + "/" + args[1], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData + args[1]);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetoteacher = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                      //  Utils.showAlertDialog(mcontext, "Response", msg);
                        InviteTeacher.RefreshedThepage();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);
                        InviteTeacher.RefreshedThepage();


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetoteacher = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }
}