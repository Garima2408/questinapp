package co.questin.adapters;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.questin.R;
import co.questin.models.AllFriends;


/**
 * Created by Dell on 03-11-2017.
 */

public class ChatMemeberSelectAdapter extends ArrayAdapter<AllFriends> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<AllFriends> friendslist;
    private SparseBooleanArray mSelectedItemsIds;

    public ChatMemeberSelectAdapter(Context context, int resourceId,
                                List<AllFriends> friendslist) {
        super(context, resourceId, friendslist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.friendslist = friendslist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {

        TextView friend_name;
        ImageView circleView;

    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_groupchat_member, null);
            // Locate the TextViews in listview_item.xml
            holder.friend_name = view.findViewById(R.id.friend_name);
            holder.circleView = view.findViewById(R.id.circleView);



            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.friend_name.setText(friendslist.get(position).field_firstname+" "+friendslist.get(position).getField_lastname());

        if(friendslist.get(position).getPicture() != null && friendslist.get(position).getPicture().length() > 0 ) {
            Glide.clear(holder.circleView);
            Glide.with(context).load(friendslist.get(position).getPicture())
                    .placeholder(R.mipmap.single).dontAnimate()
                    .fitCenter().into(holder.circleView);

        }else {
            holder.circleView.setImageResource(R.mipmap.single);
            Glide.clear(holder.circleView);
        }

        if(friendslist.get(position).equals(getSelectedIds().get(position)))

        {
            // holder.present.setVisibility(View.VISIBLE);
            //  holder.present.setImageDrawable(context.getResources().getDrawable(R.mipmap.presernt));
        }
        else
        {
            //  holder.present.setVisibility(View.INVISIBLE);
            //  holder.present.setImageDrawable(context.getResources().getDrawable(R.mipmap.absent));
        }

       /* if(attendancelist.get(position).equals(mSelectedItemsIds)){
            holder.present.setVisibility(View.VISIBLE);
        }*/



        return view;
    }

    @Override
    public void remove(AllFriends object) {
        friendslist.remove(object);
        notifyDataSetChanged();
    }

    public List<AllFriends> getattendancelist() {
        return friendslist;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));

    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);



        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }



}