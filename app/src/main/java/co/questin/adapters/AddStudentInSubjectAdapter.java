package co.questin.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.models.CollageStudentArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.teacher.ClassStudentDetails;
import co.questin.teacher.Invite_Students;
import co.questin.teacher.TeacherCourseModuleDetails;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 29-12-2017.
 */

public class AddStudentInSubjectAdapter extends RecyclerView.Adapter<AddStudentInSubjectAdapter.CustomVholder> {


    private ArrayList<CollageStudentArray> lists;
    private SearchedInviteStudent sendinvitetostudent = null;
    String course_id;
    private Activity activity;


    public AddStudentInSubjectAdapter(Activity activity, ArrayList<CollageStudentArray> lists, String course_id) {
        this.lists = lists;
        this.activity = activity;
        this.course_id = course_id;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_addstudentinsubject, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getFirst_name()+" "+lists.get(position).getLast_name());

            Log.d("TAG", "Course_ID_adapter: " + course_id);


            holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid());
                    Pair<ImageView, String> p1 = Pair.create(holder.friendIcon, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(holder.tv_friendname, "publisherName");

                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());

                }
            });

            holder.tv_email.setText(lists.get(position).getEmail());



            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(holder.friendIcon);
                Glide.with(activity).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.friendIcon);
            }
            holder.InviteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SendInviteToStudent(course_id,lists.get(position).getUid());

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SendInviteToStudent(String courseid,String Studentuid) {

        sendinvitetostudent = new SearchedInviteStudent();
        sendinvitetostudent.execute(courseid,Studentuid);



    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {

        ImageView friendIcon;

        private TextView tv_friendname, tv_email,InviteIcon;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_email = itemView.findViewById(R.id.tv_email);
            friendIcon =  itemView.findViewById(R.id.friendIcon);
            InviteIcon = itemView.findViewById(R.id.InviteIcon);


        }



    }
    /*
    SEND  REQUEST TO STUDENT TO ADD IN SUBJECT*/

    private class SearchedInviteStudent extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(activity);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type","subject")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDSTUDENTINSUBJECT+"/"+args[0]+"/"+args[1], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData +args[1]);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetostudent = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(activity);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        // Utils.showAlertDialog(mcontext, "Response", "Member is added successfully");
                        Invite_Students.RefreshedThepage();
                        ClassStudentDetails.RefreshedThepagetheAdapter();
                        TeacherCourseModuleDetails.getCurrentPosition(Integer.parseInt("8"));

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(activity);
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetostudent = null;
            Utils.p_dialog_dismiss(activity);


        }
    }


}


