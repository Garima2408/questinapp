package co.questin.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.college.SubjectExamDetails;
import co.questin.models.SubjectExamsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.teacher.UpdateExams;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

/**
 * Created by Dell on 22-09-2017.
 */

public class SubjectExamAdapter extends RecyclerView.Adapter<SubjectExamAdapter.CustomVholder> {


    private ArrayList<SubjectExamsArray> lists;
    String Service_id;
    private Context mcontext;
    String mainSubject_id;
    private DeleteSubjectExams deletesubjectExams = null;

    public SubjectExamAdapter(Context mcontext, ArrayList<SubjectExamsArray> lists, String mainSubject_id) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.mainSubject_id =mainSubject_id;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_subjectexams, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_title.setText(lists.get(position).getTitle());

            String str = lists.get(position).getDate();
            String[] splited = str.split("\\s+");

            String split_one=splited[0];
            String split_second=splited[1];


            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

            Date date = inputFormat.parse(split_one);
            String outputDateStr = outputFormat.format(date);

            Log.d("Splited String ", "Splited String" + split_one+split_second);

            SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
            Date date1 = null;
            try {
                date1 = parseFormat.parse(split_second);


            } catch (ParseException e) {
                e.printStackTrace();
            }
            String time24format = displayFormat.format(date1);
            System.out.println(parseFormat.format(date1) + " = " + displayFormat.format(date1));



            holder.tv_date.setText(outputDateStr+" "+time24format);
            holder.tv_room_no.setText(lists.get(position).getRoom());
            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(mcontext,SubjectExamDetails.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("CLASSES_ID",lists.get(position).getTnid());
                    // bundle.putString("DATES_IDS", lists.get(position).getDate().toString());


                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);
                }
            });

            holder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenu();


                }

                private void showPopupMenu() {


                    PopupMenu popup = new PopupMenu(mcontext, holder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.Editoption:


                                    Intent intent=new Intent(mcontext,UpdateExams.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("CLASSES_ID",lists.get(position).getTnid());
                                    bundle.putString("SUBJECT_ID",mainSubject_id);
                                    intent.putExtras(bundle);
                                    mcontext.startActivity(intent);




                                    return true;
                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Delete Exam")
                                            .setMessage(R.string.Delete)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    deletesubjectExams = new DeleteSubjectExams();
                                                    deletesubjectExams.execute(lists.get(position).getTnid());
                                                    lists.remove(position);
                                                    notifyDataSetChanged();

                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();


                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView tv_title, tv_room_no,tv_date;
        LinearLayout link;
        ImageView options;

        public CustomVholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_room_no = itemView.findViewById(R.id.tv_room_no);
            tv_date = itemView.findViewById(R.id.tv_date);
            link = itemView.findViewById(R.id.link);
            options =itemView.findViewById(R.id.options);

        }

    }


     /*DELETE  SUBJECTS*/

    private class DeleteSubjectExams extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETESUBJECTS+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletesubjectExams = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(mcontext, "Error", msg);



                    }
                }else {
                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deletesubjectExams = null;


        }
    }



}
