package co.questin.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.models.StudentBatchDetailArray;
import co.questin.models.StudentClassDetailArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class StudentDetailsClassAdapter extends RecyclerView.Adapter<StudentDetailsClassAdapter.CustomVholder> {

    private ArrayList<StudentClassDetailArray> lists;
    private ArrayList<StudentBatchDetailArray> mStudentBatchList;
    private Context mcontext;
    RecyclerView.LayoutManager mLayoutManager;
    private AddStudentInClass addstudentinclass = null;
    private RemoveStudentInClass removestudentinclass = null;
    public String RadioA,RadioB,RadioC,RadioD,RadioE,RemoveRadio;
    int position;

    public StudentDetailsClassAdapter(Context mcontext, ArrayList<StudentClassDetailArray> lists, ArrayList<StudentBatchDetailArray> mStudentBatchList) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.mStudentBatchList =mStudentBatchList;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_details_student, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.student_name.setText(lists.get(position).getName());

            if (lists.get(position).getMember_class()!=null){

                for (int i = 0; i < mStudentBatchList.size(); i++) {

                    if (mStudentBatchList.get(i).getId().matches(lists.get(position).getMember_class())){

                        String Display=  mStudentBatchList.get(i).getBatch();


                        if(Display.matches("A")){

                            holder.radiobtnA.setChecked(true);
                            lists.get(position).setBatchId(mStudentBatchList.get(i).getId());
                            lists.get(position).setMember_class(mStudentBatchList.get(i).getId());


                        }else if (Display.matches("B")){

                            holder.radiobtnB.setChecked(true);
                            lists.get(position).setBatchId(mStudentBatchList.get(i).getId());
                            lists.get(position).setMember_class(mStudentBatchList.get(i).getId());


                        }else if (Display.matches("C")){

                            holder.radiobtnC.setChecked(true);
                            lists.get(position).setBatchId(mStudentBatchList.get(i).getId());
                            lists.get(position).setMember_class(mStudentBatchList.get(i).getId());


                        }else if (Display.matches("D")){

                            holder.radiobtnD.setChecked(true);
                            lists.get(position).setBatchId(mStudentBatchList.get(i).getId());
                            lists.get(position).setMember_class(mStudentBatchList.get(i).getId());


                        }else if (Display.matches("E")){

                            holder.radiobtnE.setChecked(true);
                            lists.get(position).setBatchId(mStudentBatchList.get(i).getId());
                            lists.get(position).setMember_class(mStudentBatchList.get(i).getId());


                        }

                    }
                }
            }








            if (mStudentBatchList !=null &&mStudentBatchList.size() >0){
                holder.radiobtnRemove.setVisibility(View.VISIBLE);
                for (int k = 0; k < mStudentBatchList.size(); k++) {

                    Log.d("Batchstudent", "Batchstudent:" + mStudentBatchList.get(k).getId() +  mStudentBatchList.get(k).getBatch());

                    if(mStudentBatchList.get(k).getBatch().matches("A")){

                        RadioA =  mStudentBatchList.get(k).getId();
                        holder.radiobtnA.setVisibility(View.VISIBLE);

                    }else if (mStudentBatchList.get(k).getBatch().matches("B")){

                        RadioB =  mStudentBatchList.get(k).getId();
                        holder.radiobtnB.setVisibility(View.VISIBLE);


                    }else if (mStudentBatchList.get(k).getBatch().matches("C")){

                        RadioC =  mStudentBatchList.get(k).getId();
                        holder.radiobtnC.setVisibility(View.VISIBLE);


                    }else if (mStudentBatchList.get(k).getBatch().matches("D")){

                        RadioD =  mStudentBatchList.get(k).getId();
                        holder.radiobtnD.setVisibility(View.VISIBLE);


                    }else if (mStudentBatchList.get(k).getBatch().matches("E")){

                        RadioE =  mStudentBatchList.get(k).getId();
                        holder.radiobtnE.setVisibility(View.VISIBLE);


                    }


                }





            }

            holder.radiobtnA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.radiobtnA.setChecked(true);
                    holder.radiobtnB.setChecked(false);
                    holder.radiobtnC.setChecked(false);
                    holder.radiobtnD.setChecked(false);
                    holder.radiobtnE.setChecked(false);
                    holder.radiobtnRemove.setChecked(false);

                    addstudentinclass = new AddStudentInClass();
                    addstudentinclass.execute(lists.get(position).getUid(),RadioA,"flag");

                }
            });

            holder.radiobtnB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.radiobtnA.setChecked(false);
                    holder.radiobtnB.setChecked(true);
                    holder.radiobtnC.setChecked(false);
                    holder.radiobtnD.setChecked(false);
                    holder.radiobtnE.setChecked(false);
                    holder.radiobtnRemove.setChecked(false);

                    addstudentinclass = new AddStudentInClass();
                    addstudentinclass.execute(lists.get(position).getUid(),RadioB,"flag");
                }
            });


            holder.radiobtnC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.radiobtnA.setChecked(false);
                    holder.radiobtnB.setChecked(false);
                    holder.radiobtnC.setChecked(true);
                    holder.radiobtnD.setChecked(false);
                    holder.radiobtnE.setChecked(false);
                    holder.radiobtnRemove.setChecked(false);

                    addstudentinclass = new AddStudentInClass();
                    addstudentinclass.execute(lists.get(position).getUid(),RadioC,"flag");

                }
            });


            holder.radiobtnD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.radiobtnA.setChecked(false);
                    holder.radiobtnB.setChecked(false);
                    holder.radiobtnC.setChecked(false);
                    holder.radiobtnD.setChecked(true);
                    holder.radiobtnE.setChecked(false);
                    holder.radiobtnRemove.setChecked(false);

                    addstudentinclass = new AddStudentInClass();
                    addstudentinclass.execute(lists.get(position).getUid(),RadioD,"flag");

                }
            });



            holder.radiobtnE.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.radiobtnA.setChecked(false);
                    holder.radiobtnB.setChecked(false);
                    holder.radiobtnC.setChecked(false);
                    holder.radiobtnD.setChecked(false);
                    holder.radiobtnE.setChecked(true);
                    holder.radiobtnRemove.setChecked(false);

                    addstudentinclass = new AddStudentInClass();
                    addstudentinclass.execute(lists.get(position).getUid(),RadioE,"flag");

                }
            });


            holder.radiobtnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lists.get(position).getMember_class()!=null&&lists.get(position).getMember_class().length()>0) {
                        holder.radiobtnRemove.setChecked(true);
                        holder.radiobtnA.setChecked(false);
                        holder.radiobtnB.setChecked(false);
                        holder.radiobtnC.setChecked(false);
                        holder.radiobtnD.setChecked(false);
                        holder.radiobtnE.setChecked(false);
                        removestudentinclass = new RemoveStudentInClass();
                        removestudentinclass.execute(lists.get(position).getUid(),lists.get(position).getMember_class(), "unflag");
                    }else {
                        holder.radiobtnRemove.setChecked(false);
                        holder.radiobtnA.setChecked(false);
                        holder.radiobtnB.setChecked(false);
                        holder.radiobtnC.setChecked(false);
                        holder.radiobtnD.setChecked(false);
                        holder.radiobtnE.setChecked(false);

                    }
                }
            });




        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView circleView;
        private TextView student_name;
        RelativeLayout toplayout;
        RadioButton radiobtnA,radiobtnB,radiobtnC,radiobtnD,radiobtnE,radiobtnRemove;
        public CustomVholder(View itemView) {
            super(itemView);


            student_name = itemView.findViewById(R.id.student_name);
            circleView = itemView.findViewById(R.id.circleView);
            toplayout = itemView.findViewById(R.id.toplayout);
            radiobtnA =itemView.findViewById(R.id.radiobtnA);
            radiobtnB =itemView.findViewById(R.id.radiobtnB);
            radiobtnC =itemView.findViewById(R.id.radiobtnC);
            radiobtnD =itemView.findViewById(R.id.radiobtnD);
            radiobtnE =itemView.findViewById(R.id.radiobtnE);
            radiobtnRemove =itemView.findViewById(R.id.radiobtnRemove);




        }
    }





    private class AddStudentInClass extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","member")
                    .add("entity_id",args[1])
                    .add("action",args[2])
                    .add("uid",args[0])
                    .build();






            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LIKEDCOMMENTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Reload the page.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            addstudentinclass = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        // Utils.p_dialog_dismiss(mcontext);
                        //  CampusNewFeeds.RefreshWorked();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        //  Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);



                    }
                }else {

                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            addstudentinclass = null;


        }
    }


    private class RemoveStudentInClass extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","member")
                    .add("entity_id",args[1])
                    .add("action",args[2])
                    .add("uid",args[0])
                    .build();






            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LIKEDCOMMENTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Reload the page.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            removestudentinclass = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        //  Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);



                    }
                }else {
                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            removestudentinclass = null;


        }
    }

}








