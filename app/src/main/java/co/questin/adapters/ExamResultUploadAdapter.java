package co.questin.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.ResultUploadArray;

/**
 * Created by Dell on 05-01-2018.
 */

public class ExamResultUploadAdapter extends BaseAdapter {

    private Context context;
    ArrayList<ResultUploadArray> questionsList;
    public static ArrayList<String> selectedAnswers;
    public static ArrayList<String> lastselectedAnswers;
    String marks;
    String FocusId,SelectedId;


    public ExamResultUploadAdapter(Context context, ArrayList<ResultUploadArray> questionsList) {
        this.context = context;
        this.questionsList = questionsList;
        selectedAnswers = new ArrayList<>();
        lastselectedAnswers =new ArrayList<>();

    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }


    @Override
    public int getCount() {
        return questionsList.size();
    }

    @Override
    public ResultUploadArray getItem(int position) {
        return questionsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView
            , ViewGroup parent) {
        View row;
        final ListViewHolder listViewHolder;
        if(convertView == null)
        {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.list_of_resultstudent,parent,false);
            listViewHolder = new ListViewHolder();
            listViewHolder.student_name = row.findViewById(R.id.student_name);
            listViewHolder.marksfeild = row.findViewById(R.id.marksfeild);
            listViewHolder.display = row.findViewById(R.id.display);
            listViewHolder.circleView = row.findViewById(R.id.circleView);


            row.setTag(listViewHolder);

        }
        else
        {
            row=convertView;
            listViewHolder= (ListViewHolder) row.getTag();
        }
        final ResultUploadArray products = getItem(position);

        listViewHolder.student_name.setText(products.name);
        listViewHolder.id = position;

        if(questionsList.get(position).getPicture() != null && questionsList.get(position).getPicture().length() > 0 ) {
            Glide.with(context).load(questionsList.get(position).getPicture())
                    .placeholder(R.mipmap.place_holder).dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter().into(listViewHolder.circleView);

        }else {
            listViewHolder.circleView.setImageResource(R.mipmap.place_holder);
        }

        FocusId= String.valueOf(questionsList.get(questionsList.size()-1).getUid());





                          // Add listener for edit text
        listViewHolder.marksfeild
                .setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {

                        if(!hasFocus) {
                            int itemIndex = v.getId();
                            marks = ((EditText) v).getText()
                                    .toString();
                            Log.d("TAG", "enteredPrice: " + marks);
                            listViewHolder.display.setText(marks);
                            selectedAnswers.add(position,","+marks+","+questionsList.get(position).getUid());
                            Log.d("TAG", "selectedAnswers: " + selectedAnswers);



                        }else if (hasFocus){

                            SelectedId= questionsList.get(position).getUid();
                            Log.d("TAG", "SelectedId: " + SelectedId);



                        }
                    }
                });

        listViewHolder.marksfeild.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (questionsList !=null) {

                    if (FocusId.matches(SelectedId)) {
                        listViewHolder.display.setText(s);
                        String Main = listViewHolder.display.getText().toString();
                        lastselectedAnswers.add(0, "," + Main + "," + FocusId);
                        Log.d("TAG", "lastselectedAnswers: " + lastselectedAnswers);
                    }

                }
                    }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return row;
    }
}



