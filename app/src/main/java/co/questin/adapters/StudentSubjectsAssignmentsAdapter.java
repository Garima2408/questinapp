package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.college.SubjectsAssignmentsDetails;
import co.questin.models.SubjectsAssignmentsArray;

/**
 * Created by Dell on 22-09-2017.
 */

public class StudentSubjectsAssignmentsAdapter extends RecyclerView.Adapter<StudentSubjectsAssignmentsAdapter.CustomVholder> {


    private ArrayList<SubjectsAssignmentsArray> lists;
    String Service_id;
    private Context mcontext;


    public StudentSubjectsAssignmentsAdapter(Context mcontext, ArrayList<SubjectsAssignmentsArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_subjectassinments, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_title.setText(lists.get(position).getTitle());
            holder.tv_teacher_name.setText(lists.get(position).getTeacher());
            holder.options.setVisibility(View.GONE);
            String str = lists.get(position).getDate();
            String[] splited = str.split("\\s+");

            String split_one=splited[0];
            String split_second=splited[1];


            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

            Date date = inputFormat.parse(split_one);
            String outputDateStr = outputFormat.format(date);

            Log.d("Splited String ", "Splited String" + split_one+split_second);

            SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
            Date date1 = null;
            try {
                date1 = parseFormat.parse(split_second);


            } catch (ParseException e) {
                e.printStackTrace();
            }
            String time24format = displayFormat.format(date1);
            System.out.println(parseFormat.format(date1) + " = " + displayFormat.format(date1));



            holder.tv_date.setText(outputDateStr);




            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(mcontext,SubjectsAssignmentsDetails.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("ASSIGNMENT_ID",lists.get(position).getTnid());
                    // bundle.putString("DATES_IDS", lists.get(position).getDate().toString());


                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);
                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView tv_title, tv_teacher_name,tv_date;
        LinearLayout link;
        ImageView options;

        public CustomVholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_teacher_name = itemView.findViewById(R.id.tv_teacher_name);
            tv_date = itemView.findViewById(R.id.tv_date);
            link = itemView.findViewById(R.id.link);
            options =itemView.findViewById(R.id.options);

        }

    }
}
