package co.questin.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.buttonanimation.LikeButton;
import co.questin.buttonanimation.OnAnimationEndListener;
import co.questin.buttonanimation.OnLikeListener;
import co.questin.college.SubjectCommentsReply;
import co.questin.college.UpdateSubjectComment;
import co.questin.models.SubjectCommentArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.models.SubjectCommentArray.COARSE_TYPE;
import static co.questin.models.SubjectCommentArray.PROGRESS_;

/**
 * Created by Dell on 27-09-2017.
 */

public class TeacherSubjectsCommentsAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<SubjectCommentArray> lists;
    Dialog comentDialog;
    TextView commnetrepy;
    EditText editText_Commment;
    ImageButton Send;
    String comments,Subject_id,Comment_id;
    private DeleteSubjectComments deletesubjectcomments = null;
    private LikeFeedComments likefeedcomments = null;
    private InappropriateFeedComments inappropriatefeedcomments = null;
    private boolean isLoading;
    private Activity activity;
    Dialog imageDialog;
    Bitmap thumbnail = null;
    String Extension;

    public TeacherSubjectsCommentsAdapter(RecyclerView recyclerView, ArrayList<SubjectCommentArray> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;


    }

    public void addData(List<SubjectCommentArray> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<SubjectCommentArray> list){
        lists.clear();
        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                if(lists!=null && lists.size()>0){
                    lists.add(new SubjectCommentArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_subjectscomments, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final SubjectCommentArray course = lists.get(position);
            final UserViewHolder userViewHolder = (UserViewHolder) holder;

            userViewHolder.publisher_name.setText(lists.get(position).getUserName());
            String s =lists.get(position).getComment();
            userViewHolder.blog_content.setText(Html.fromHtml(s));
            userViewHolder.dateTime.setText(lists.get(position).getDate());
            userViewHolder.commentNo.setText(lists.get(position).getReplies_count());
            userViewHolder.likeNo.setText(lists.get(position).getLikes());




            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {

                Glide.with(activity).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(userViewHolder.circleView);

            }else {
                userViewHolder.circleView.setImageResource(R.mipmap.place_holder);

            }

            if(lists.get(position).getUrl() != null && lists.get(position).getUrl().length() > 0 ) {
                Extension = lists.get(position).getUrl().substring(lists.get(position).getUrl().lastIndexOf("."));
                Log.d("TAG", "Extension: " + Extension);

                userViewHolder.previewlayout.setVisibility(View.VISIBLE);

                if (Extension.matches(".jpg")) {
                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getUrl())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);


                } else if (Extension.matches(".png")) {

                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getUrl())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);

                }  else if (Extension.matches(".jpeg")) {

                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getUrl())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);

                }else if (Extension.matches(".docx")) {

                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_doc_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);




                } else if (Extension.matches(".pdf")) {

                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_pdf_download);

                    userViewHolder.imagePost.setVisibility(View.GONE);




                } else if (Extension.matches(".txt")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_text_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".doc")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_doc_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".xls")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_xls_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".xlsx")) {
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_xls_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);


                }

            }else {
                userViewHolder.previewlayout.setVisibility(View.GONE);
                userViewHolder.imagePost.setImageDrawable(null);
                Glide.clear(userViewHolder.imagePost);
            }


            userViewHolder.imagepreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Utils.downloadFile(activity,lists.get(position).getUrl());

                }
            });

            if(lists.get(position).getUid().equals(SessionManager.getInstance(activity).getUser().getUserprofile_id())){
                userViewHolder.options.setVisibility(View.VISIBLE);
                userViewHolder.inaprooptions.setVisibility(View.INVISIBLE);
            }else {
                userViewHolder.options.setVisibility(View.INVISIBLE);
                userViewHolder.inaprooptions.setVisibility(View.VISIBLE);
            }

            if(lists.get(position).getIs_liked().contains("False")){
                userViewHolder.like.setLiked(false);

            }else if(lists.get(position).getIs_liked().contains("True"))  {
                userViewHolder.like.setLiked(true);
            }


            userViewHolder.like.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    if (!lists.get(position).getIs_liked().isEmpty()) {
                        if (lists.get(position).getIs_liked().contains("False")) {
                            // userViewHolder.like.setBackgroundResource(R.mipmap.likeblue);
                            userViewHolder.like.setLiked(true);
                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) + 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();

                            likefeedcomments = new LikeFeedComments();
                            likefeedcomments.execute(lists.get(position).getId(),"flag");
                            lists.get(position).setIs_liked("True");
                            lists.get(position).setLikes(NewLikeNo);


                        } else if (lists.get(position).getIs_liked().contains("True")) {
                            //  userViewHolder.like.setBackgroundResource(R.mipmap.like);
                            userViewHolder.like.setLiked(false);
                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) - 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();
                            likefeedcomments = new LikeFeedComments();
                            likefeedcomments.execute(lists.get(position).getId(),"unflag");
                            lists.get(position).setIs_liked("False");
                            lists.get(position).setLikes(NewLikeNo);
                        }
                    }
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    if (!lists.get(position).getIs_liked().isEmpty()) {
                        if (lists.get(position).getIs_liked().contains("False")) {
                            // userViewHolder.like.setBackgroundResource(R.mipmap.likeblue);
                            userViewHolder.like.setLiked(true);

                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) + 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();

                            likefeedcomments = new LikeFeedComments();
                            likefeedcomments.execute(lists.get(position).getId(),"flag");
                            lists.get(position).setIs_liked("True");

                            lists.get(position).setLikes(NewLikeNo);


                        } else if (lists.get(position).getIs_liked().contains("True")) {
                            //  userViewHolder.like.setBackgroundResource(R.mipmap.like);
                            userViewHolder.like.setLiked(false);
                            userViewHolder.likeNo.setText(Integer.toString(Integer.parseInt(lists.get(position).getLikes()) - 1));
                            String NewLikeNo = userViewHolder.likeNo.getText().toString();
                            likefeedcomments = new LikeFeedComments();
                            likefeedcomments.execute(lists.get(position).getId(),"unflag");
                            lists.get(position).setIs_liked("False");
                            lists.get(position).setLikes(NewLikeNo);
                        }
                    }
                }
            });


            userViewHolder.like.setOnAnimationEndListener(new OnAnimationEndListener() {
                @Override
                public void onAnimationEnd(LikeButton likeButton) {

                }
            });



            userViewHolder.imagePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Utils.ImageDialogOpen(activity,lists.get(position).getUrl());

                }
            });

            userViewHolder.publisher_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid());
                    Pair<ImageView, String> p1 = Pair.create(userViewHolder.circleView, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(userViewHolder.publisher_name, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });


            userViewHolder.circleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid());
                    Pair<ImageView, String> p1 = Pair.create(userViewHolder.circleView, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(userViewHolder.publisher_name, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());




                }
            });


            userViewHolder.box1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {




                    Intent backIntent = new Intent(activity, SubjectCommentsReply.class)
                            .putExtra("SUBJECT_ID",lists.get(position).getSid())
                            .putExtra("PARENT_ID", lists.get(position).getId())
                            .putExtra("USERSELECTED_ID", lists.get(position).getUid())
                            .putExtra("NAME", lists.get(position).getUserName())
                            .putExtra("BLOG_CONTENT", lists.get(position).getComment())
                            .putExtra("DATE", lists.get(position).getDate())
                            .putExtra("IMAGE_URL", lists.get(position).getPicture())
                            .putExtra("IS_LIKED", lists.get(position).getIs_liked())
                            .putExtra("NOLIKED", lists.get(position).getLikes())
                            .putExtra("NOCOMMENT", lists.get(position).getReplies_count())
                            .putExtra("POSTCONENT", lists.get(position).getUrl())
                            .putExtra("open", "CameFromTeacherAdapter");

                    Pair<ImageView, String> p1 = Pair.create(userViewHolder.circleView, "transitionProfile");
                    Pair <TextView, String> p2 = Pair.create(userViewHolder.publisher_name, "publisherName");


                    ActivityOptionsCompat transitionActivityOptions = null;

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2});
                    }

                    activity.startActivity(backIntent,transitionActivityOptions.toBundle());



                }
            });
            userViewHolder.box3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String shareBody = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";

                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Questin (Open it in Google Play Store to Download the Application)");

                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody+lists.get(position).getSubject());
                    activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));




                }
            });

            userViewHolder.box1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, SubjectCommentsReply.class)
                            .putExtra("SUBJECT_ID",lists.get(position).getSid())
                            .putExtra("PARENT_ID", lists.get(position).getId())
                            .putExtra("USERSELECTED_ID", lists.get(position).getUid())
                            .putExtra("NAME", lists.get(position).getUserName())
                            .putExtra("BLOG_CONTENT", lists.get(position).getComment())
                            .putExtra("DATE", lists.get(position).getDate())
                            .putExtra("IMAGE_URL", lists.get(position).getPicture())
                            .putExtra("IS_LIKED", lists.get(position).getIs_liked())
                            .putExtra("NOLIKED", lists.get(position).getLikes())
                            .putExtra("NOCOMMENT", lists.get(position).getReplies_count())
                            .putExtra("POSTCONENT", lists.get(position).getUrl())
                            .putExtra("open", "CameFromTeacherAdapter");
                    activity.startActivity(backIntent);


                }
            });


            userViewHolder.inaprooptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.app.AlertDialog.THEME_HOLO_DARK)
                            .setTitle("Inappropriate")
                            .setMessage(R.string.Inappropriate)
                            .setCancelable(false)
                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                                  /*  DELETE COMMENTS ON POST*/
                                    inappropriatefeedcomments = new InappropriateFeedComments();
                                    inappropriatefeedcomments.execute(lists.get(position).getId());
                                    lists.remove(position);
                                    notifyDataSetChanged();


                                }
                            })
                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    builder.create().show();



                }
            });


            userViewHolder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenu();


                }

                private void showPopupMenu() {


                    PopupMenu popup = new PopupMenu(activity, userViewHolder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.Editoption:

                                    Intent backIntent = new Intent(activity, UpdateSubjectComment.class)
                                            .putExtra("PARENT_ID", lists.get(position).getId())
                                            .putExtra("NAME", lists.get(position).getUserName())
                                            .putExtra("BLOG_CONTENT", lists.get(position).getComment())
                                            .putExtra("DATE", lists.get(position).getDate())
                                            .putExtra("IMAGE_URL", lists.get(position).getPicture())
                                            .putExtra("USERSELECTED_ID", lists.get(position).getUid())
                                            .putExtra("POSTCONENT", lists.get(position).getUrl())
                                            .putExtra("open", "CameFromTeacherAdapter");
                                    activity.startActivity(backIntent);




                                    return true;
                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Delete My Feed")
                                            .setMessage(R.string.delete)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    /*  DELETE COMMENTS ON POST*/

                                                    deletesubjectcomments = new DeleteSubjectComments();
                                                    deletesubjectcomments.execute(lists.get(position).getId());
                                                    lists.remove(position);
                                                    notifyDataSetChanged();


                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();



                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();




                }
            });


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }
    private class UserViewHolder extends RecyclerView.ViewHolder {
        ImageView Imagecomment, options, share, comment, inaprooptions,imagePost,imagepreview;
        ImageView circleView;
        private TextView commentNo, likeNo, blog_content, publisher_name, dateTime;
        RelativeLayout previewlayout;
        LikeButton like;
        LinearLayout box1,box3;



        public UserViewHolder(View view) {
            super(view);
            commentNo = itemView.findViewById(R.id.commentNo);
            likeNo = itemView.findViewById(R.id.likeNo);
            blog_content = itemView.findViewById(R.id.blog_content);
            dateTime = itemView.findViewById(R.id.dateTime);
            publisher_name = itemView.findViewById(R.id.publisher_name);
            circleView = itemView.findViewById(R.id.circleView);
            options = itemView.findViewById(R.id.options);
            Imagecomment = itemView.findViewById(R.id.comment);
            share = itemView.findViewById(R.id.share);
            like  = itemView.findViewById(R.id.like);
            inaprooptions = itemView.findViewById(R.id.inaprooptions);
            imagePost =itemView.findViewById(R.id.imagePost);
            imagepreview =itemView.findViewById(R.id.imagepreview);
            previewlayout =itemView.findViewById(R.id.previewlayout);
            box1=itemView.findViewById(R.id.box1);
            box3=itemView.findViewById(R.id.box3);
        }
    }



    /*LISTS OF DELETE COMMENTS ON SUBJECTS*/


    private class DeleteSubjectComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETESUBJECTCOMMENTS + "/" + args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletesubjectcomments = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                } else {
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deletesubjectcomments = null;


        }
    }

         /*ADD LIKES ON COMMENTS */


    private class LikeFeedComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","comments_like")
                    .add("entity_id",args[0])
                    .add("action",args[1])
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LIKEDCOMMENTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            likefeedcomments = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(activity, "Error", msg);



                    }
                }else {
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            likefeedcomments = null;


        }
    }



    /*MARKED IN APPROPRIATE ON COMMENTS */


    private class InappropriateFeedComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","comments_inappropriate")
                    .add("entity_id",args[0])
                    .add("action","flag")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MARKEDINAPPROPRIATECOMMENT,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            inappropriatefeedcomments = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(activity, "Error", msg);



                    }
                }else {
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            inappropriatefeedcomments = null;


        }
    }


}
