package co.questin.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.college.CollageEventDetails;
import co.questin.models.CollageEventArray;

import static co.questin.models.CollageEventArray.COARSE_TYPE;
import static co.questin.models.CollageEventArray.PROGRESS_;

/**
 * Created by Dell on 17-08-2017.
 */

public class CollageEventAdapters  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private boolean isLoading;
    private Activity activity;
    private ArrayList<CollageEventArray> lists;

    String Event_id;


    public CollageEventAdapters(RecyclerView recyclerView, ArrayList<CollageEventArray> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;


    }


    public void addData(ArrayList<CollageEventArray> lists){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ lists.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(lists);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<CollageEventArray> list){


        this.lists = list;
        notifyDataSetChanged();

    }

    public void addProgress(){

        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new CollageEventArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };
        handler.post(r);

/*
        if(lists!=null && lists.size()>0){
            lists.add(new CollageEventArray());
            notifyItemInserted(lists.size()-1);
        }*/
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_collage_events, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final CollageEventArray course = lists.get(position);
            final UserViewHolder userViewHolder = (UserViewHolder) holder;

            try {
                userViewHolder.tv_title.setText(Html.fromHtml(lists.get(position).getTitle()));
                //  userViewHolder.tv_body.setText(lists.get(position).getLocation());

                String str = lists.get(position).getPeriod();
                String[] splited = str.split("\\s+");

                String split_one=splited[0];
                String split_second=splited[1];

                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

                Date date = inputFormat.parse(split_one);
                String outputDateStr = outputFormat.format(date);

                Log.d("Splited String ", "Splited String" + split_one+split_second);

                SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
                Date date1 = null;
                try {
                    date1 = parseFormat.parse(split_second);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String time24format = displayFormat.format(date1);
                System.out.println(parseFormat.format(date1) + " = " + displayFormat.format(date1));

                userViewHolder.tv_date.setText(outputDateStr +" "+ time24format);

                if(lists.get(position).getLogo() != null && lists.get(position).getLogo().length() > 0 ) {

                    Glide.with(activity).load(lists.get(position).getLogo())
                            .placeholder(R.mipmap.eventback).dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .fitCenter().into(userViewHolder.collageeventIcon);

                }else {
                    userViewHolder.collageeventIcon.setImageResource(R.mipmap.eventback);

                }

                userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent=new Intent(activity,CollageEventDetails.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("EVENT_ID",lists.get(position).getTnid());
                        bundle.putString("EVENT_LOGO",lists.get(position).getLogo());
                        bundle.putString("EVENT_TITLE",lists.get(position).getTitle());
                        intent.putExtras(bundle);

                        Pair<ImageView, String> p1 = Pair.create(userViewHolder.collageeventIcon, "transitionProfile");
                        Pair <TextView, String> p2 = Pair.create(userViewHolder.tv_title, "Eventtittle");
                        Pair <TextView, String> p3 = Pair.create(userViewHolder.tv_date, "eventDetail");

                        ActivityOptionsCompat transitionActivityOptions = null;

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2,p3});
                        }

                        activity.startActivity(intent,transitionActivityOptions.toBundle());




                    }
                });

                userViewHolder.mainlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent=new Intent(activity,CollageEventDetails.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("EVENT_ID",lists.get(position).getTnid());
                        bundle.putString("EVENT_LOGO",lists.get(position).getLogo());
                        bundle.putString("EVENT_TITLE",lists.get(position).getTitle());
                        intent.putExtras(bundle);

                        Pair<ImageView, String> p1 = Pair.create(userViewHolder.collageeventIcon, "transitionProfile");
                        Pair <TextView, String> p2 = Pair.create(userViewHolder.tv_title, "Eventtittle");
                        Pair <TextView, String> p3 = Pair.create(userViewHolder.tv_date, "eventDetail");

                        ActivityOptionsCompat transitionActivityOptions = null;

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, new Pair[]{p1,p2,p3});
                        }

                        activity.startActivity(intent,transitionActivityOptions.toBundle());

                    }
                });



            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_body, tv_date;
        ImageView collageeventIcon;
        LinearLayout link;
        RelativeLayout mainlay;

        public UserViewHolder(View view) {
            super(view);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_body = itemView.findViewById(R.id.tv_body);
            tv_date = itemView.findViewById(R.id.tv_date);
            collageeventIcon = itemView.findViewById(R.id.collageeventIcon);
            link = itemView.findViewById(R.id.link);
            mainlay = itemView.findViewById(R.id.mainlay);
        }
    }
}