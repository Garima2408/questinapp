package co.questin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.models.ShowAttandanceDates;
import co.questin.models.ShowAttandanceOnlyDates;
import co.questin.models.TeacherViewStudentArray;

/**
 * Created by Dell on 22-12-2017.
 */

public class TeacherViewStudentAttandanceAdapter extends RecyclerView.Adapter<TeacherViewStudentAttandanceAdapter.CustomVholder> {
    RecyclerView attandance_Lists;

    private ArrayList<TeacherViewStudentArray> lists;
    public ArrayList<ShowAttandanceDates> mList;
    public ArrayList<ShowAttandanceOnlyDates> mListttt;


    RecyclerView.LayoutManager mLayoutManager, layoutManager;


    String Service_id;
    private Context mcontext;


    public TeacherViewStudentAttandanceAdapter(Context mcontext, ArrayList<TeacherViewStudentArray> lists, ArrayList<ShowAttandanceDates> mList) {
        this.lists = lists;
        this.mList = mList;
        this.mcontext = mcontext;
        mListttt = new ArrayList<>();



    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_teacherviewattandance, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.name.setText(lists.get(position).getStudent());
            holder.Enrollment.setText(lists.get(position).getEnrollment());
            holder.percentattandance.setText(lists.get(position).getPerc());
            if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {

                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.student_holder).dontAnimate()
                        .fitCenter().into(holder.circleView);

            } else {
                holder.circleView.setImageResource(R.mipmap.student_holder);

            }


            if (lists.get(position).getDate0()!= null && lists.get(position).getDate0().length() > 0){

                String Convertdate = lists.get(position).getDate0();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate0().split("-");
                String[] Datevalue = output[2].split(" ");



                holder.dayone.setText(Datevalue[0]);
                holder.weekone.setText(lists.get(position).getAttendance0());

            }

            if (lists.get(position).getDate1()!= null && lists.get(position).getDate1().length() > 0){


                String Convertdate = lists.get(position).getDate1();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate1().split("-");
                String[] Datevalue = output[2].split(" ");




                holder.daytwo.setText(Datevalue[0]);
                holder.weektwo.setText(lists.get(position).getAttendance1());




            }
            if (lists.get(position).getDate2()!= null && lists.get(position).getDate2().length() > 0){
                String Convertdate = lists.get(position).getDate2();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate2().split("-");
                String[] Datevalue = output[2].split(" ");


                holder.daythree.setText(Datevalue[0]);
                holder.weekthree.setText(lists.get(position).getAttendance2());



            }
            if (lists.get(position).getDate3()!= null && lists.get(position).getDate3().length() > 0){
                String Convertdate = lists.get(position).getDate3();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate3().split("-");
                String[] Datevalue = output[2].split(" ");


                holder.dayfour.setText(Datevalue[0]);
                holder.weekfour.setText(lists.get(position).getAttendance3());



            }
            if (lists.get(position).getDate4()!= null && lists.get(position).getDate4().length() > 0){
                String Convertdate = lists.get(position).getDate4();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate4().split("-");
                String[] Datevalue = output[2].split(" ");

                holder.dayfive.setText(Datevalue[0]);
                holder.weekfive.setText(lists.get(position).getAttendance4());



            }
            if (lists.get(position).getDate5()!= null && lists.get(position).getDate5().length() > 0){
                String Convertdate = lists.get(position).getDate5();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate5().split("-");
                String[] Datevalue = output[2].split(" ");

                holder.daysix.setText(Datevalue[0]);
                holder.weeksix.setText(lists.get(position).getAttendance5());



            }
            if (lists.get(position).getDate6()!= null && lists.get(position).getDate6().length() > 0){
                String Convertdate = lists.get(position).getDate6();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate6().split("-");
                String[] Datevalue = output[2].split(" ");

                holder.dayseven.setText(Datevalue[0]);
                holder.weekseven.setText(lists.get(position).getAttendance6());



            }
            /*if (lists.get(position).getDate7()!= null && lists.get(position).getDate7().length() > 0){
                String Convertdate = lists.get(position).getDate7();

                SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dt1= null;
                try {
                    dt1 = format1.parse(Convertdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat format2=new SimpleDateFormat("EEEE");
                String finalDay=format2.format(dt1);
                Log.d("TAG", "no need: " + finalDay);



                String[] output = lists.get(position).getDate7().split("-");
                String[] Datevalue = output[2].split(" ");


                holder.dayeight.setText(Datevalue[0]);
                holder.weekeight.setText(lists.get(position).getAttendance7());


            }*/





            if(lists.get(position).getAttendance0().matches("P")){

                holder.weekone.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance0().matches("A")) {

                holder.weekone.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance0().matches("")) {
                holder.weekone.setBackgroundResource(R.drawable.round_circle_gray);

            }

            if(lists.get(position).getAttendance1().matches("P")){

                holder.weektwo.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance1().matches("A")) {

                holder.weektwo.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance1().matches("")) {
                holder.weektwo.setBackgroundResource(R.drawable.round_circle_gray);

            }

            if(lists.get(position).getAttendance2().matches("P")){

                holder.weekthree.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance2().matches("A")) {

                holder.weekthree.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance2().matches("")){
                holder.weekthree.setBackgroundResource(R.drawable.round_circle_gray);

            }

            if(lists.get(position).getAttendance3().matches("P")){

                holder.weekfour.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance3().matches("A")) {

                holder.weekfour.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance3().matches("")) {
                holder.weekfour.setBackgroundResource(R.drawable.round_circle_gray);

            }

            if(lists.get(position).getAttendance4().matches("P")){

                holder.weekfive.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance4().matches("A")) {

                holder.weekfive.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance4().matches("")) {
                holder.weekfive.setBackgroundResource(R.drawable.round_circle_gray);

            }

            if(lists.get(position).getAttendance5().matches("P")){

                holder.weeksix.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance5().matches("A")) {

                holder.weeksix.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance5().matches("")) {
                holder.weeksix.setBackgroundResource(R.drawable.round_circle_gray);

            }

            if(lists.get(position).getAttendance6().matches("P")){

                holder.weekseven.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance6().matches("A")) {

                holder.weekseven.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance6().matches("")){
                holder.weekseven.setBackgroundResource(R.drawable.round_circle_gray);

            }

          /*  if(lists.get(position).getAttendance7().matches("P")){

                holder.weekeight.setBackgroundResource(R.drawable.round_circle_p);

            }else if(lists.get(position).getAttendance7().matches("A")) {

                holder.weekeight.setBackgroundResource(R.drawable.round_circle_a);

            }else if(lists.get(position).getAttendance7().matches("")) {
                holder.weekeight.setBackgroundResource(R.drawable.round_circle_gray);

            }
*/

            System.out.println(lists.get(position).getStudent()+lists.get(position).getPerc()+"*****"+lists.get(position).getDate0()+lists.get(position).getAttendance0()+lists.get(position).getDate1()+lists.get(position).getAttendance1()+"***"+lists.get(position).getDate2()+lists.get(position).getAttendance2()+"*****"+lists.get(position).getDate3()+lists.get(position).getAttendance3()+"*****"+lists.get(position).getDate4()+lists.get(position).getAttendance4()+"*****"+lists.get(position).getDate5()+lists.get(position).getAttendance5()+"*****"+lists.get(position).getDate6()+lists.get(position).getAttendance6()+"*****");






        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        TextView Enrollment, weekstart, weekend, ClassName, dayone, daytwo, daythree, dayfour, dayfive, daysix, dayseven, dayeight, weekeight, weekseven, weeksix, weekfive, weekfour, weekthree, weektwo, weekone,curentmonth,ErrorText;

        private TextView name, monthview, percentattandance;
        ImageView circleView;
        RecyclerView attandance_Lists, attandance_datelist;


        public CustomVholder(View itemView) {
            super(itemView);

            circleView = itemView.findViewById(R.id.circleView);
            name = itemView.findViewById(R.id.name);
            Enrollment =itemView.findViewById(R.id.Enrollment);
            monthview = itemView.findViewById(R.id.monthview);
            percentattandance = itemView.findViewById(R.id.percent);
            dayone = itemView.findViewById(R.id.dayone);
            daytwo = itemView.findViewById(R.id.daytwo);
            daythree = itemView.findViewById(R.id.daythree);
            dayfour = itemView.findViewById(R.id.dayfour);
            dayfive = itemView.findViewById(R.id.dayfive);
            daysix = itemView.findViewById(R.id.daysix);
            dayseven =itemView. findViewById(R.id.dayseven);
            dayeight =itemView. findViewById(R.id.dayeight);

            weekeight = itemView.findViewById(R.id.weekeight);
            weekseven =itemView. findViewById(R.id.weekseven);
            weeksix = itemView.findViewById(R.id.weeksix);
            weekfive = itemView.findViewById(R.id.weekfive);
            weekfour = itemView.findViewById(R.id.weekfour);
            weekthree =itemView. findViewById(R.id.weekthree);
            weektwo = itemView.findViewById(R.id.weektwo);
            weekone = itemView.findViewById(R.id.weekone);


        }


    }







}
