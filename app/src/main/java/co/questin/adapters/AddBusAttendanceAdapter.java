package co.questin.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.models.tracker.BusAttandanceArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 03-10-2017.
 */

public class AddBusAttendanceAdapter extends RecyclerView.Adapter<AddBusAttendanceAdapter.CustomVholder> {


    private ArrayList<BusAttandanceArray> lists;
    private Context mcontext;
    String ShiftId;
    private ProcessPresentRequest presentrequest = null;
    private ProcessAbsentRequest absentrequest = null;
    JSONArray studentselected;
    JSONObject shareAttandanceJson;
    String Route_id;
    String startfrom;

    public AddBusAttendanceAdapter(Context mcontext, ArrayList<BusAttandanceArray> lists ,String ShiftId ,String Route_id ,String startfrom) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.ShiftId =ShiftId;
        this.Route_id =Route_id;
        this.startfrom =startfrom;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_aadbus_attandance, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.student_name.setText(lists.get(position).getName());

            holder.student_id.setText(lists.get(position).getEnrollment_id());

            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.student_holder).dontAnimate()
                        .fitCenter().into(holder.profileImage);

            }else {
                holder.profileImage.setImageResource(R.mipmap.student_holder);

            }



            if(ShiftId.contains("morning")){
                holder.EveningStatus.setOnClickListener(null);

                holder.morningStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!lists.get(position).getMorningStatus().isEmpty()) {
                            if (lists.get(position).getMorningStatus().contains("2")) {
                                holder.morningStatus.setBackgroundResource(R.mipmap.presernt);
                                studentselected = new JSONArray();

                                shareAttandanceJson = new JSONObject();
                                try {
                                    shareAttandanceJson.put("uid", lists.get(position).getUid());
                                    shareAttandanceJson.put("present", "1");
                                    shareAttandanceJson.put("bus_route", Route_id);
                                    shareAttandanceJson.put("type", ShiftId);
                                    shareAttandanceJson.put("date", startfrom);
                                    studentselected.put(shareAttandanceJson);


                                    Log.d("TAG", "studentselected: " + studentselected);
                                    Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                presentrequest = new ProcessPresentRequest();
                                presentrequest.execute(studentselected.toString());
                                lists.get(position).setMorningStatus("1");



                            } else if (lists.get(position).getMorningStatus().contains("1")) {
                                holder.morningStatus.setBackgroundResource(R.mipmap.absent);

                                studentselected = new JSONArray();

                                shareAttandanceJson = new JSONObject();
                                try {
                                    shareAttandanceJson.put("uid", lists.get(position).getUid());
                                    shareAttandanceJson.put("present", "2");
                                    shareAttandanceJson.put("bus_route", Route_id);
                                    shareAttandanceJson.put("type", ShiftId);
                                    shareAttandanceJson.put("date", startfrom);
                                    studentselected.put(shareAttandanceJson);


                                    Log.d("TAG", "studentselected: " + studentselected);
                                    Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                                absentrequest = new ProcessAbsentRequest();
                                absentrequest.execute(studentselected.toString());
                                lists.get(position).setMorningStatus("2");

                            } if (lists.get(position).getMorningStatus().contains("3")) {
                                holder.morningStatus.setBackgroundResource(R.mipmap.presernt);
                                studentselected = new JSONArray();

                                shareAttandanceJson = new JSONObject();
                                try {
                                    shareAttandanceJson.put("uid", lists.get(position).getUid());
                                    shareAttandanceJson.put("present", "1");
                                    shareAttandanceJson.put("bus_route", Route_id);
                                    shareAttandanceJson.put("type", ShiftId);
                                    shareAttandanceJson.put("date", startfrom);
                                    studentselected.put(shareAttandanceJson);


                                    Log.d("TAG", "studentselected: " + studentselected);
                                    Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                presentrequest = new ProcessPresentRequest();
                                presentrequest.execute(studentselected.toString());
                                lists.get(position).setMorningStatus("1");



                            }
                        }
                    }

                });



            }else if(ShiftId.contains("evening")){

                holder.morningStatus.setOnClickListener(null);
                holder.EveningStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!lists.get(position).getEveningStatus().isEmpty()) {
                            if (lists.get(position).getEveningStatus().contains("2")) {
                                holder.EveningStatus.setBackgroundResource(R.mipmap.presernt);
                                studentselected = new JSONArray();

                                shareAttandanceJson = new JSONObject();
                                try {
                                    shareAttandanceJson.put("uid", lists.get(position).getUid());
                                    shareAttandanceJson.put("present", "1");
                                    shareAttandanceJson.put("bus_route", Route_id);
                                    shareAttandanceJson.put("type", ShiftId);
                                    shareAttandanceJson.put("date", startfrom);
                                    studentselected.put(shareAttandanceJson);


                                    Log.d("TAG", "studentselected: " + studentselected);
                                    Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                presentrequest = new ProcessPresentRequest();
                                presentrequest.execute(studentselected.toString());
                                lists.get(position).setEveningStatus("1");



                            } else if (lists.get(position).getEveningStatus().contains("1")) {
                                holder.EveningStatus.setBackgroundResource(R.mipmap.absent);

                                studentselected = new JSONArray();

                                shareAttandanceJson = new JSONObject();
                                try {
                                    shareAttandanceJson.put("uid", lists.get(position).getUid());
                                    shareAttandanceJson.put("present", "2");
                                    shareAttandanceJson.put("bus_route", Route_id);
                                    shareAttandanceJson.put("type", ShiftId);
                                    shareAttandanceJson.put("date", startfrom);
                                    studentselected.put(shareAttandanceJson);


                                    Log.d("TAG", "studentselected: " + studentselected);
                                    Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }




                                absentrequest = new ProcessAbsentRequest();
                                absentrequest.execute(studentselected.toString());
                                lists.get(position).setEveningStatus("2");

                            } if (lists.get(position).getEveningStatus().contains("3")) {
                                holder.EveningStatus.setBackgroundResource(R.mipmap.presernt);
                                studentselected = new JSONArray();

                                shareAttandanceJson = new JSONObject();
                                try {
                                    shareAttandanceJson.put("uid", lists.get(position).getUid());
                                    shareAttandanceJson.put("present", "1");
                                    shareAttandanceJson.put("bus_route", Route_id);
                                    shareAttandanceJson.put("type", ShiftId);
                                    shareAttandanceJson.put("date", startfrom);
                                    studentselected.put(shareAttandanceJson);


                                    Log.d("TAG", "studentselected: " + studentselected);
                                    Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                presentrequest = new ProcessPresentRequest();
                                presentrequest.execute(studentselected.toString());
                                lists.get(position).setEveningStatus("1");



                            }
                        }
                    }

                });

            }




            if(lists.get(position).getMorningStatus() != null && lists.get(position).getMorningStatus().length() > 0 ) {

                if (lists.get(position).getMorningStatus().contains("1"))
                {
                    holder.morningStatus.setBackgroundResource(R.mipmap.presernt);

                } else if (lists.get(position).getMorningStatus().contains("2")) {
                    holder.morningStatus.setBackgroundResource(R.mipmap.absent);
                }
                else if (lists.get(position).getMorningStatus().contains("3")){

                    holder.morningStatus.setBackgroundResource(R.drawable.round_circle_gray);

                }


            }

            if(lists.get(position).getEveningStatus() != null && lists.get(position).getEveningStatus().length() > 0 ) {

                if (lists.get(position).getEveningStatus().contains("1"))
                {
                    holder.EveningStatus.setBackgroundResource(R.mipmap.presernt);

                } else if (lists.get(position).getEveningStatus().contains("2")) {
                    holder.EveningStatus.setBackgroundResource(R.mipmap.absent);
                }
                else if (lists.get(position).getEveningStatus().contains("3")){

                    holder.EveningStatus.setBackgroundResource(R.drawable.round_circle_gray);

                }

            }








        } catch (Exception e) {
            e.printStackTrace();
        }
    }








    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        ImageView profileImage;
        private TextView student_name,student_id;
        ImageButton morningStatus,EveningStatus;

        public CustomVholder(View itemView) {
            super(itemView);

            student_name = itemView.findViewById(R.id.student_name);
            morningStatus = itemView.findViewById(R.id.morningStatus);
            EveningStatus = itemView.findViewById(R.id.EveningStatus);
            profileImage =  itemView.findViewById(R.id.profileImage);
            student_id =itemView.findViewById(R.id.student_id);


        }



    }


    //* *//**//*METHOD MARK PRESENT*//**//*

    private class ProcessPresentRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            RequestBody body = new FormBody.Builder()
                    .add("records", args[0])
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ROUTESTUDENTBUSATTANDANCE, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            presentrequest = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            presentrequest = null;


        }
    }



    /* METHOD MARK ABSENT*/

    private class ProcessAbsentRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("records", args[0])
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ROUTESTUDENTBUSATTANDANCE, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            absentrequest = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            absentrequest = null;


        }
    }

}
