package co.questin.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.InfoArray;
import co.questin.utils.Utils;

/**
 * Created by Dell on 31-10-2017.
 */
public class StudentSubjectInfoResourceAdapter extends RecyclerView.Adapter<StudentSubjectInfoResourceAdapter.CustomVholder> {


    private ArrayList<InfoArray> lists;
    private Activity mcontext;
    String course_idMain;
    String Extension;
    Bitmap thumbnail = null;
    Dialog imageDialog;


    public StudentSubjectInfoResourceAdapter(Activity mcontext, ArrayList<InfoArray> lists, String course_idMain) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.course_idMain =course_idMain;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_subjectresourceinfo, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {

            if(lists.get(position).getTitle() != null && lists.get(position).getTitle().length() > 0 ) {
                holder.Tittle.setText(lists.get(position).getTitle());
            }else {

                holder.Tittle.setText("Resource Value");


            }



            String extension = lists.get(position).getValue().substring(lists.get(position).getValue().lastIndexOf("."));
            Log.d("TAG", "extension: " + extension);


            if(lists.get(position).getValue() != null && lists.get(position).getValue().length() > 0 ) {
                Extension = lists.get(position).getValue().substring(lists.get(position).getValue().lastIndexOf("."));

                Log.d("TAG", "Extension: " + Extension);
                if (Extension.matches(".jpg")) {


                    Glide.clear(holder.extentionImage);
                    Glide.with(mcontext).load(lists.get(position).getValue())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(holder.extentionImage);


                }
                if (Extension.matches(".jpeg")) {


                    Glide.clear(holder.extentionImage);
                    Glide.with(mcontext).load(lists.get(position).getValue())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(holder.extentionImage);


                } if (Extension.matches(".png")) {


                    Glide.clear(holder.extentionImage);
                    Glide.with(mcontext).load(lists.get(position).getValue())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(holder.extentionImage);


                }

                if (Extension.matches(".docx")) {

                    holder.extentionImage.setImageResource(R.mipmap.ic_doc_download);



                } else if (Extension.matches(".pdf")) {

                    holder.extentionImage.setImageResource(R.mipmap.ic_pdf_download);



                } else if (Extension.matches(".txt")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_text_download);



                } else if (Extension.matches(".zip")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_zip_download);



                } else if (Extension.matches(".doc")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_doc_download);



                } else if (Extension.matches(".xls")) {
                      holder.extentionImage.setImageResource(R.mipmap.ic_xls_download);



                } else if (Extension.matches(".xlsx")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_xls_download);



                }


            }else {
                holder.extentionImage.setImageDrawable(null);
            }



            holder.extentionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.downloadFile(mcontext,lists.get(position).getValue());

                }
            });



            holder.Values.setText(lists.get(position).getValue());





            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        Utils.downloadFile(mcontext,lists.get(position).getValue());
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            });



            holder.Values.setText(lists.get(position).getValue());
            holder.options.setVisibility(View.GONE);

            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Utils.downloadFile(mcontext,lists.get(position).getValue());

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {


        private TextView Tittle,Values;
        LinearLayout link;

        ImageView options,extentionImage;

        public CustomVholder(View itemView) {
            super(itemView);

            Tittle = itemView.findViewById(R.id.Tittle);
            Values = itemView.findViewById(R.id.Values);
            link = itemView.findViewById(R.id.link);
            options = itemView.findViewById(R.id.options);
            extentionImage = itemView.findViewById(R.id.extentionImage);



        }

    }




}


