package co.questin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import co.questin.models.NavItems;

public class NavigationItemsAdapter extends BaseExpandableListAdapter {

  private Context mContext;
  private List<NavItems> mListMainList;
  private HashMap<String, List<NavItems>> mListDataChild;

  public NavigationItemsAdapter (Context context, List<NavItems> listMainList,
      HashMap<String, List<NavItems>> listDataChild) {
    mContext = context;
    mListMainList = listMainList;
    mListDataChild = listDataChild;
  }

  @Override public int getGroupCount () {
    return this.mListMainList.size ();
  }

  @Override public int getChildrenCount (int groupPosition) {
    return this.mListDataChild.get (this.mListMainList.get (groupPosition)
        .getNavItemName ()).size ();

  }

  @Override public Object getGroup (int groupPosition) {
    return this.mListMainList.get (groupPosition);
  }

  @Override public Object getChild (int groupPosition, int childPosition) {
    return this.mListDataChild.get (this.mListMainList.
        get (groupPosition).getNavItemName ()).get (childPosition);
  }

  @Override public long getGroupId (int groupPosition) {
    return groupPosition;
  }

  @Override public long getChildId (int groupPosition, int childPosition) {
    return childPosition;
  }

  @Override public boolean hasStableIds () {
    return false;
  }

  @Override public View getGroupView (int groupPosition, boolean isExpanded, View convertView,
      ViewGroup parent) {


    if(convertView==null){
      LayoutInflater layoutInflater = (LayoutInflater) this.mContext.
          getSystemService (Context.LAYOUT_INFLATER_SERVICE);
      convertView = layoutInflater.inflate (co.questin.R.layout.nav_item_layout,null);

    }

    final RelativeLayout layout = convertView.findViewById (co.questin.R.id.rl_nav_layout);



    TextView navItem = convertView.findViewById (co.questin.R.id.tv_nav_item_title);
    ImageView navIcons = convertView.findViewById (co.questin.R.id.img_nav_icons);
    ImageView expandIcon = convertView.findViewById (co.questin.R.id.img_expand);

    navItem.setText (this.mListMainList.get (groupPosition).getNavItemName ());
    navIcons.setImageDrawable (this.mListMainList.get (groupPosition).getIcon ());

    if(getChildrenCount (groupPosition)==0){
      expandIcon.setVisibility (View.GONE);
    } else {
      expandIcon.setVisibility (View.VISIBLE);
      if (isExpanded) {
        expandIcon.setImageResource (co.questin.R.mipmap.up_arrow);
      } else {
        expandIcon.invalidate ();
        expandIcon.setImageResource (co.questin.R.mipmap.down_arrow);
      }
    }
    return convertView;
  }

  @Override public View getChildView (int groupPosition, int childPosition, boolean isLastChild,
      View convertView, ViewGroup parent) {
    LayoutInflater layoutInflater = (LayoutInflater) this.mContext.
        getSystemService (Context.LAYOUT_INFLATER_SERVICE);

      if (convertView == null) {
        convertView = layoutInflater.inflate (co.questin.R.layout.sub_nav_item_layout, null);
      }
      TextView subItem = convertView.findViewById (co.questin.R.id.tv_sub_nav_item_title);
      subItem.setText (this.mListDataChild.get (this.mListMainList.
          get (groupPosition).getNavItemName ()).get (childPosition).getNavItemName ());

    return convertView;
  }

  @Override public boolean isChildSelectable (int groupPosition, int childPosition) {
    return true;
  }
  @Override
  public boolean areAllItemsEnabled() {
    return false;
  }





}
