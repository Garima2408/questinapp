package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.models.AllUser;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.studentprofile.AnotherUserDisplay;
import co.questin.studentprofile.InviteFriends;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 21-08-2017.
 */

public class SearchedInviteFirendsAdapter extends RecyclerView.Adapter<SearchedInviteFirendsAdapter.CustomVholder> {


    private ArrayList<AllUser> lists;
    private SendInviteToFriends sendinvitetofriends = null;

    private Context mcontext;


    public SearchedInviteFirendsAdapter(Context mcontext, ArrayList<AllUser> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_invitefriends, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getName());
            holder.tv_email.setText(lists.get(position).getCollageName());

            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(holder.friendIcon);
                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.friendIcon);
            }

            if (lists.get(position).getMainRole() != null &&lists.get(position).getMainRole().length()> 0){

                if (lists.get(position).getMainRole().matches("13")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageBranch);


                } else if (lists.get(position).getMainRole().matches("14")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).getCollagedepartment());


                } else if (lists.get(position).getMainRole().matches("15")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageBranch);


                } else if (lists.get(position).getMainRole().matches("16")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).CollageStudentName);

                }else if (lists.get(position).getMainRole().matches("3")) {
                    holder.tv_departmentrole.setText(lists.get(position).CollageRole);
                    holder.tv_department.setText(lists.get(position).getCollagedepartment());


                }


            }else {

            }


            if(lists.get(position).getIs_friend_request_sent().matches("1")){
                holder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                holder.AlreadyFriendIcon.setVisibility(View.GONE);
                holder.InviteIcon.setVisibility(View.GONE);


            }else if (lists.get(position).getIs_friend_request_sent().matches("")){

            }


            if(lists.get(position).getIs_friend().matches("1")){
                holder.AlreadyFriendIcon.setVisibility(View.VISIBLE);
                holder.AlreadyInviteIcon.setVisibility(View.GONE);
                holder.InviteIcon.setVisibility(View.GONE);
            }else if (lists.get(position).getIs_friend().matches("")){


            }





            if (lists.get(position).getMainRole().matches("16")) {

                if(lists.get(position).getUid().equals(SessionManager.getInstance(mcontext).getUser().getParentUid())){
                    holder.AlreadyFriendIcon.setVisibility(View.GONE);
                    holder.AlreadyInviteIcon.setVisibility(View.GONE);
                    holder.InviteIcon.setVisibility(View.GONE);
                }else {

                }

            }else {
                if(lists.get(position).getUid().equals(SessionManager.getInstance(mcontext).getUser().getUserprofile_id())){
                    holder.AlreadyFriendIcon.setVisibility(View.GONE);
                    holder.AlreadyInviteIcon.setVisibility(View.GONE);
                    holder.InviteIcon.setVisibility(View.GONE);
                }else {

                }
            }


            holder.InviteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    lists.get(position).setIs_friend_request_sent("1");
                    holder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                    holder.AlreadyFriendIcon.setVisibility(View.GONE);
                    holder.InviteIcon.setVisibility(View.GONE);
                    sendinvitetofriends = new SendInviteToFriends();
                    sendinvitetofriends.execute(lists.get(position).getUid());


                }
            });


            holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    mcontext.startActivity(backIntent);

                }
            });

            holder.friendIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, AnotherUserDisplay.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                            .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                    mcontext.startActivity(backIntent);

                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
       ImageView friendIcon;

        private TextView tv_friendname, tv_email,InviteIcon,tv_department,tv_departmentrole,AlreadyFriendIcon,AlreadyInviteIcon;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_email = itemView.findViewById(R.id.tv_email);
            friendIcon =  itemView.findViewById(R.id.friendIcon);
            InviteIcon = itemView.findViewById(R.id.InviteIcon);
            tv_department = itemView.findViewById(R.id.tv_department);
            tv_departmentrole = itemView.findViewById(R.id.tv_departmentrole);
            AlreadyFriendIcon = itemView.findViewById(R.id.AlreadyFriendIcon);
            AlreadyInviteIcon = itemView.findViewById(R.id.AlreadyInviteIcon);

        }



    }
    /*
    SEND FRIEND REQUEST*/

    private class SendInviteToFriends extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()

                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_SENDINFITE+"/"+args[0]+"/"+"relationship/friends", body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();


                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetofriends = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject picture = responce.getJSONObject("data");



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        InviteFriends.RefreshWorked();

                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetofriends = null;


        }
    }


}


