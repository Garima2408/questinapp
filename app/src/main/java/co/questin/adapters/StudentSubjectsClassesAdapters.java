package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.college.ClassesDetails;
import co.questin.models.SubjectsClassesArray;

/**
 * Created by Dell on 21-09-2017.
 */

public class StudentSubjectsClassesAdapters extends RecyclerView.Adapter<StudentSubjectsClassesAdapters.CustomVholder> {


    private ArrayList<SubjectsClassesArray> lists;
    String Service_id,time24format,time24format1;
    private Context mcontext;



    public StudentSubjectsClassesAdapters(Context mcontext, ArrayList<SubjectsClassesArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_subjectsclass, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_title.setText(lists.get(position).getTitle());
            holder.tv_location.setText(lists.get(position).getRoom());

            if(lists.get(position).getStart_time() != null && lists.get(position).getStart_time().length() > 0 ) {
                SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
                Date date;
                try {
                    date = parseFormat.parse(lists.get(position).getStart_time());

                    time24format = displayFormat.format(date);
                    System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));


                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }else
            {

            }

            if (lists.get(position).getEnd_time() != null && lists.get(position).getEnd_time().length() > 0 ) {
                SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
                Date date1;
                try {

                    date1 = parseFormat.parse(lists.get(position).getEnd_time());
                    time24format1 = displayFormat.format(date1);



                } catch (ParseException e) {
                    e.printStackTrace();
                }




            }else {

            }



            holder.tv_date.setText(time24format+"-"+time24format1+" "+lists.get(position).getWeek() );



            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(mcontext,ClassesDetails.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("CLASSES_ID",lists.get(position).getTnid());
                   // bundle.putString("DATES_IDS", lists.get(position).getDate().toString());
                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);
                }
            });

            holder.options.setVisibility(View.GONE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView tv_title, tv_location,tv_date;
        LinearLayout link;
        ImageView options;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_date =itemView.findViewById(R.id.tv_date);
            link = itemView.findViewById(R.id.link);
            options =itemView.findViewById(R.id.options);



        }

    }





}


