package co.questin.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.AddAttendanceArray;


/**
 * Created by Dell on 03-10-2017.
 */

public class AddAttendanceAdapter extends BaseAdapter {


    private Context context;
    ArrayList<AddAttendanceArray> attendancelist;
    public static ArrayList<String> selectedAnswers;
    boolean isPressed = false;
    int StatusChecked;


    public AddAttendanceAdapter(Context context, ArrayList<AddAttendanceArray> attendancelist) {
        this.context = context;
        this.attendancelist = attendancelist;
        selectedAnswers = new ArrayList<>();

    }





    @Override
    public int getCount() {
        return attendancelist.size();
    }

    @Override
    public AddAttendanceArray getItem(int position) {
        return attendancelist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView
            , ViewGroup parent) {
        View row;
        final ListViewHolder listViewHolder;
        if(convertView == null)
        {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.list_of_markattendance,parent,false);
            listViewHolder = new ListViewHolder();
            listViewHolder.student_name = row.findViewById(R.id.student_name);
            listViewHolder.circleView = row.findViewById(R.id.circleView);
            listViewHolder.studentstatus = row.findViewById(R.id.studentstatus);
            listViewHolder.Enrollment =row.findViewById(R.id.Enrollment);


            row.setTag(listViewHolder);
        }
        else
        {
            row=convertView;
            listViewHolder= (ListViewHolder) row.getTag();
        }




        listViewHolder.id = position;


        listViewHolder.student_name.setText(attendancelist.get(position).getName());
        listViewHolder.Enrollment.setText(attendancelist.get(position).getEnrollment_id());


        if (attendancelist.get(position).getPicture() != null && attendancelist.get(position).getPicture().length() > 0) {
            Glide.clear(listViewHolder.circleView);
            Glide.with(context).load(attendancelist.get(position).getPicture())
                    .placeholder(R.mipmap.student_holder).dontAnimate()
                    .fitCenter().into(listViewHolder.circleView);

        } else {
            listViewHolder.circleView.setImageResource(R.mipmap.student_holder);
            Glide.clear(listViewHolder.circleView);
        }


        if (attendancelist.get(position).getAttendance().contains("1"))
        {
            listViewHolder.studentstatus.setBackgroundResource(R.mipmap.presernt);

        } else if (attendancelist.get(position).getAttendance().contains("2")) {
            listViewHolder.studentstatus.setBackgroundResource(R.mipmap.absent);
        } else if (attendancelist.get(position).getAttendance().contains("3")){

            listViewHolder.studentstatus.setBackgroundResource(R.drawable.round_circle_gray);

        }







        listViewHolder.studentstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPressed){

                    listViewHolder.studentstatus.setBackgroundResource(R.mipmap.presernt);
                    StatusChecked =1;
                    attendancelist.get(position).setAttendance("1");


                }else if(!isPressed){
                    listViewHolder.studentstatus.setBackgroundResource(R.mipmap.absent);
                    StatusChecked =2;
                    attendancelist.get(position).setAttendance("2");





                }
                selectedAnswers.add(attendancelist.get(position).getAttendance()+","+attendancelist.get(position).getUid());
                Log.d("TAG", "selectedAnswers2: " +selectedAnswers );

                isPressed = !isPressed; // reverse


            }


        });







        return row;
    }




}


