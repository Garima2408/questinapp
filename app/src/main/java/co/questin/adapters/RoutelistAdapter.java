package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import co.questin.R;
import co.questin.admin.AdminRouteActivity;
import co.questin.driver.TrackerActivity;
import co.questin.models.tracker.RoutelistArray;
import co.questin.tracker.TransportDetails;
import co.questin.tracker.TransportDetailsStudent;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;

public class RoutelistAdapter extends RecyclerView.Adapter<RoutelistAdapter.CustomVholder> {

    private SparseIntArray sparseIntArray;
    private ArrayList<RoutelistArray> lists;
    String Service_id;
    private Context mcontext;



    public RoutelistAdapter(Context mcontext, ArrayList<RoutelistArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
        sparseIntArray = new SparseIntArray(5);
        sparseIntArray.put(0, R.mipmap.transa);
        sparseIntArray.put(1, R.mipmap.transb);
        sparseIntArray.put(2, R.mipmap.transc);
        sparseIntArray.put(3, R.mipmap.transd);
        sparseIntArray.put(4, R.mipmap.transe);
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_routes, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.routename.setText(lists.get(position).getTitle());
            holder.transportImages.setBackground(ContextCompat.getDrawable(mcontext, sparseIntArray.get(position%sparseIntArray.size())));

            final String image = String.valueOf(sparseIntArray.get(position%sparseIntArray.size()));
            Log.d("TAG", "imagearray: "+image);

                holder.mainheader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    if (SessionManager.getInstance(mcontext).getUserClgRole().getRole().matches("13")) {
                        Intent intent=new Intent(mcontext,TransportDetailsStudent.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("ROUTE_ID",lists.get(position).getTnid());
                        bundle.putString("ROUTE_NAME",lists.get(position).getTitle());
                        bundle.putString("IS_STUDENT",lists.get(position).getIs_student());
                        bundle.putString("IS_CREATER",lists.get(position).getIs_creator());
                        bundle.putString("IS_MEMBER",lists.get(position).getIs_member());
                        bundle.putString("sponsered",lists.get(position).getSponsered());
                        bundle.putString("TRANSPORT_IMAGE",  String.valueOf(sparseIntArray.get(position%sparseIntArray.size())));
                        intent.putExtras(bundle);
                        mcontext.startActivity(intent);



                    } else if (SessionManager.getInstance(mcontext).getUserClgRole().getRole().matches("14")) {

                        if (lists.get(position).getIs_creator().contains("TRUE") && lists.get(position).getIs_faculty().contains("TRUE")) {

                            Intent intent=new Intent(mcontext,AdminRouteActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putString("ROUTE_ID",lists.get(position).getTnid());
                            bundle.putString("ROUTE_NAME",lists.get(position).getTitle());
                            bundle.putString("IS_FACULTY",lists.get(position).getIs_faculty());
                            bundle.putString("IS_CREATER",lists.get(position).getIs_creator());
                            bundle.putString("IS_MEMBER",lists.get(position).getIs_member());
                            bundle.putString("sponsered",lists.get(position).getSponsered());
                            bundle.putString("TRANSPORT_IMAGE", String.valueOf(sparseIntArray.get(position%sparseIntArray.size())));
                            intent.putExtras(bundle);
                            mcontext.startActivity(intent);

                        }else {
                              Intent intent=new Intent(mcontext,TransportDetails.class);
                              Bundle bundle=new Bundle();
                              bundle.putString("ROUTE_ID",lists.get(position).getTnid());
                              bundle.putString("ROUTE_NAME",lists.get(position).getTitle());
                              bundle.putString("IS_FACULTY",lists.get(position).getIs_faculty());
                              bundle.putString("IS_CREATER",lists.get(position).getIs_creator());
                              bundle.putString("IS_MEMBER",lists.get(position).getIs_member());
                              bundle.putString("sponsered",lists.get(position).getSponsered());
                              bundle.putString("TRANSPORT_IMAGE", String.valueOf(sparseIntArray.get(position%sparseIntArray.size())));
                              intent.putExtras(bundle);
                              mcontext.startActivity(intent);

                          }




                    } else if (SessionManager.getInstance(mcontext).getUserClgRole().getRole().matches("15")) {
                        Intent intent=new Intent(mcontext,TransportDetailsStudent.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("ROUTE_ID",lists.get(position).getTnid());
                        bundle.putString("ROUTE_NAME",lists.get(position).getTitle());
                        bundle.putString("IS_STUDENT",lists.get(position).getIs_student());
                        bundle.putString("IS_CREATER",lists.get(position).getIs_creator());
                        bundle.putString("IS_MEMBER",lists.get(position).getIs_member());
                        bundle.putString("sponsered",lists.get(position).getSponsered());
                        bundle.putString("TRANSPORT_IMAGE", String.valueOf(sparseIntArray.get(position%sparseIntArray.size())));
                        intent.putExtras(bundle);
                        mcontext.startActivity(intent);



                    } else if (SessionManager.getInstance(mcontext).getUserClgRole().getRole().matches("16")) {
                        Intent intent=new Intent(mcontext,TransportDetailsStudent.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("ROUTE_ID",lists.get(position).getTnid());
                        bundle.putString("ROUTE_NAME",lists.get(position).getTitle());
                        bundle.putString("IS_STUDENT",lists.get(position).getIs_student());
                        bundle.putString("IS_CREATER",lists.get(position).getIs_creator());
                        bundle.putString("IS_MEMBER",lists.get(position).getIs_member());
                        bundle.putString("sponsered",lists.get(position).getSponsered());
                        bundle.putString("TRANSPORT_IMAGE", String.valueOf(sparseIntArray.get(position%sparseIntArray.size())));
                        intent.putExtras(bundle);
                        mcontext.startActivity(intent);


                    }else if (SessionManager.getInstance(mcontext).getUserClgRole().getRole().matches("27")) {

                        if (lists.get(position).getIs_driver().contains("TRUE")){
                            Intent intent=new Intent(mcontext,TrackerActivity.class);
                            Bundle bundle=new Bundle();
                            bundle.putString("ROUTE_ID",lists.get(position).getTnid());
                            bundle.putString("ROUTE_NAME",lists.get(position).getTitle());
                            bundle.putString("COLLAGE_ID",SessionManager.getInstance(mcontext).getCollage().getTnid());

                            bundle.putString(Constants.EXTRA_ROUTE_ID,lists.get(position).getTnid());
                            bundle.putString(Constants.EXTRA_ROUTE_NAME,lists.get(position).getTitle());
                            bundle.putString("ADMIN_ID",lists.get(position).getNode_uid());
                            bundle.putString("TRANSPORT_IMAGE", String.valueOf(sparseIntArray.get(position%sparseIntArray.size())));
                            intent.putExtras(bundle);
                            mcontext.startActivity(intent);
                        }else {

                            Toast.makeText(mcontext, "You are not authorized to track this route", Toast.LENGTH_SHORT).show();


                        }



                    }



                }







            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }





    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView routename;
        RelativeLayout mainheader;
        ImageView transportImages;



        public CustomVholder(View itemView) {
            super(itemView);

            routename = itemView.findViewById(R.id.routename);
            mainheader = itemView.findViewById(R.id.mainheader);
            transportImages =itemView.findViewById(R.id.transportImages);


        }

    }
}