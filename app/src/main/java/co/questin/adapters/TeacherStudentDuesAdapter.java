package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.StudentListsArray;
import co.questin.paymentactivity.DetailsStudentDues;

public class TeacherStudentDuesAdapter extends RecyclerView.Adapter<TeacherStudentDuesAdapter.CustomVholder> {

    private ArrayList<StudentListsArray> lists;
    private Context mcontext;
    String class_id;



    public TeacherStudentDuesAdapter(Context mcontext, ArrayList<StudentListsArray> lists, String class_id) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.class_id =class_id;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_teacher_dues, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.student_name.setText(lists.get(position).getName());

            if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {
                Glide.clear(holder.circleView);
                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.circleView);

            } else {
                holder.circleView.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.circleView);
            }



               holder.toplayout.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       Intent intent=new Intent(mcontext,DetailsStudentDues.class);
                       Bundle bundle=new Bundle();
                       bundle.putString("STUDENTID",lists.get(position).getUid());
                       intent.putExtras(bundle);
                       mcontext.startActivity(intent);
                   }
               });


            } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        TextView student_name;
        ImageView circleView;
        RelativeLayout toplayout;


        public CustomVholder(View itemView) {
            super(itemView);


            student_name = itemView.findViewById(R.id.friend_name);
            circleView = itemView.findViewById(R.id.circleView);
            toplayout = itemView.findViewById(R.id.toplayout);



        }

    }
}
