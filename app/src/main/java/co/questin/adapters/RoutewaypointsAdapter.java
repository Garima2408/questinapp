package co.questin.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.tracker.WayPointsArray;
import co.questin.tracker.StopLocationDisplay;

public class RoutewaypointsAdapter extends RecyclerView.Adapter<RoutewaypointsAdapter.CustomVholder> {


    private ArrayList<WayPointsArray> lists;
    String SourceStartLat,SourceStartLng,DestinationLat,DestinationLng;
    private Context mcontext;


    public RoutewaypointsAdapter(Context mcontext, ArrayList<WayPointsArray> lists, String SourceStartLat, String SourceStartLng, String DestinationLat, String DestinationLng) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.SourceStartLat = SourceStartLat;
        this.SourceStartLng = SourceStartLng;
        this.DestinationLat = DestinationLat;
        this.DestinationLng = DestinationLng;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_allwaypoints, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tittle.setText(lists.get(position).getField_place_name());



           /* if(lists.get(position).getField_photo() != null && lists.get(position).getField_photo().length() > 0 ) {
                Glide.clear(holder.pinlocation);
                Glide.with(mcontext).load(lists.get(position).getField_photo())
                        .placeholder(R.mipmap.stoplocation).dontAnimate()
                        .fitCenter().into(holder.pinlocation);

            }else {
                holder.pinlocation.setImageResource(R.mipmap.stoplocation);
                Glide.clear(holder.pinlocation);
            }*/

            //  holder.tv_discription.setText(lists.get(position).getDescription());
            holder.Arivaltime.setText(lists.get(position).getField_start_time());
            holder.DepatureTime.setText(lists.get(position).getField_departure_time());

            holder.pinlocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(mcontext,StopLocationDisplay.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("ROUTE_LAT",lists.get(position).getLat());
                    bundle.putString("ROUTE_LONG",lists.get(position).getLng());
                    bundle.putString("ROUTE_LOGO",lists.get(position).getField_photo());
                    bundle.putString("ROUTE_NAME",lists.get(position).getField_place_name());
                    bundle.putString("ROUTE_PICK",lists.get(position).getField_start_time());
                    bundle.putString("ROUTE_DROP",lists.get(position).getField_departure_time());
                    bundle.putString("START_LAT",SourceStartLat);
                    bundle.putString("START_LNG",SourceStartLng);
                    bundle.putString("STOP_LAT",DestinationLat);
                    bundle.putString("STOP_LNG",DestinationLng);
                    bundle.putSerializable("AllPoints",  lists);

                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView tittle, Arivaltime,DepatureTime;
        ImageView pinlocation;

        public CustomVholder(View itemView) {
            super(itemView);

            tittle = itemView.findViewById(co.questin.R.id.tittle);
            Arivaltime = itemView.findViewById(co.questin.R.id.Arivaltime);
            DepatureTime = itemView.findViewById(co.questin.R.id.DepatureTime);
            pinlocation = itemView.findViewById(co.questin.R.id.pinlocation);

        }

    }
}

