package co.questin.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.questin.R;
import co.questin.models.AllUser;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.studentprofile.AnotherUserDisplay;
import co.questin.studentprofile.InviteFriends;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.models.AllUser.COARSE_TYPE;
import static co.questin.models.AllUser.PROGRESS_;

/**
 * Created by Dell on 21-08-2017.
 */

public class InviteFirendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<AllUser> lists;
    private SendInviteToFriends sendinvitetofriends = null;
    Activity activity;



    public InviteFirendsAdapter( RecyclerView recyclerView,Activity activity, ArrayList<AllUser> lists) {
        this.lists = lists;
        this.activity = activity;
    }

    public void addData(List<AllUser> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<AllUser> list){

        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new AllUser(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);

    }





    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_invitefriends, parent, false);
            return new CustomVholder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CustomVholder) {
            final CustomVholder userViewHolder = (CustomVholder) holder;

            try {
                userViewHolder.tv_friendname.setText(lists.get(position).getName());
                userViewHolder.tv_email.setText(lists.get(position).getCollageName());

                if (lists.get(position).getMainRole() != null && lists.get(position).getMainRole().length() > 0) {

                    if (lists.get(position).getMainRole().matches("13")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageBranch);


                    } else if (lists.get(position).getMainRole().matches("14")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).getCollagedepartment());


                    } else if (lists.get(position).getMainRole().matches("15")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageBranch);


                    } else if (lists.get(position).getMainRole().matches("16")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageStudentName);

                    }
                    else if (lists.get(position).getMainRole().matches("3")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).getCollagedepartment());


                    }


                } else {

                }


                if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {
                    Glide.clear(userViewHolder.friendIcon);
                    Glide.with(activity).load(lists.get(position).getPicture())
                            .placeholder(R.mipmap.place_holder).dontAnimate()
                            .fitCenter().into(userViewHolder.friendIcon);

                } else {
                    userViewHolder.friendIcon.setImageResource(R.mipmap.place_holder);
                    Glide.clear(userViewHolder.friendIcon);
                }
                userViewHolder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                                .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                                .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                        activity.startActivity(backIntent);

                    }
                });

                userViewHolder.friendIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                                .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                                .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                        activity.startActivity(backIntent);

                    }
                });


                if(lists.get(position).getIs_friend_request_sent().matches("1")){
                    userViewHolder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                    userViewHolder.AlreadyFriendIcon.setVisibility(View.GONE);
                    userViewHolder.InviteIcon.setVisibility(View.GONE);

                }else if (lists.get(position).getIs_friend_request_sent().matches("")){

                }


                if(lists.get(position).getIs_friend().matches("1")){
                    userViewHolder.AlreadyFriendIcon.setVisibility(View.VISIBLE);
                    userViewHolder.AlreadyInviteIcon.setVisibility(View.GONE);
                    userViewHolder.InviteIcon.setVisibility(View.GONE);
                }else if (lists.get(position).getIs_friend().matches("")){


                }

                userViewHolder.InviteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        lists.get(position).setIs_friend_request_sent("1");
                        userViewHolder.AlreadyInviteIcon.setVisibility(View.VISIBLE);
                        userViewHolder.AlreadyFriendIcon.setVisibility(View.GONE);
                        userViewHolder.InviteIcon.setVisibility(View.GONE);
                        sendinvitetofriends = new SendInviteToFriends();
                        sendinvitetofriends.execute(lists.get(position).getUid());


                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }



    @Override
    public int getItemCount() {
        return lists.size();
    }






    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    /**
     * Viewholder for Adapter
     */
    class CustomVholder extends RecyclerView.ViewHolder {

        ImageView friendIcon;

        private TextView tv_friendname, tv_email,InviteIcon,tv_department,tv_departmentrole,AlreadyFriendIcon,AlreadyInviteIcon;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_email = itemView.findViewById(R.id.tv_email);
            friendIcon =  itemView.findViewById(R.id.friendIcon);
            InviteIcon = itemView.findViewById(R.id.InviteIcon);
            tv_department = itemView.findViewById(R.id.tv_department);
            tv_departmentrole = itemView.findViewById(R.id.tv_departmentrole);
            AlreadyFriendIcon = itemView.findViewById(R.id.AlreadyFriendIcon);
            AlreadyInviteIcon = itemView.findViewById(R.id.AlreadyInviteIcon);

        }



    }
    /*
    SEND FRIEND REQUEST*/

    private class SendInviteToFriends extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_SENDINFITE+"/"+args[0]+"/"+"relationship/friends", body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetofriends = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                      //  InviteFriends.RefreshWorked();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        InviteFriends.RefreshWorked();
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetofriends = null;


        }
    }


}


