package co.questin.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.tracker.WayPointsArray;


/**
 * Created by HP on 5/31/2018.
 */

public class AdapterSelectInvitestudentPick extends RecyclerView.Adapter<AdapterSelectInvitestudentPick.ViewHolder> {
    private ArrayList<WayPointsArray> list;
    Activity activity;
    private boolean isPick;
    public AdapterSelectInvitestudentPick(Activity activity, ArrayList<WayPointsArray> wayPointsArrayList, boolean changePick) {
        this.list=wayPointsArrayList;
        this.activity=activity;
        this.isPick=changePick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_select_pick, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        viewHolder.txt_location.setText(""+list.get(i).field_place_name);


        if(list.get(i).isPick()){
            viewHolder.check_pick.setChecked(true);
        }else
            viewHolder.check_pick.setChecked(false);



        if(list.get(i).isDrop()){
            viewHolder.check_drop.setChecked(true);
        }else
            viewHolder.check_drop.setChecked(false);




        viewHolder.check_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isPick){
                    if(list.get(i).isPick()){
                        list.get(i).setPick(false);
                    }else{
                        list.get(i).setPick(true);
                        AddStudentInRouteAdapter.pickPos=i;

                    }
                    updateViewPick(i);
                }else{

                    if ( !list.get(i).isPick())
                        list.get(i).setPick(false);

                    notifyDataSetChanged();
                    Toast.makeText(activity,"Can't Change Pick and Drop Contact Admin", Toast.LENGTH_SHORT).show();
                }


            }
        });


        viewHolder.check_drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isPick){
                    if(list.get(i).isDrop()){

                        list.get(i).setDrop(false);

                    }else{
                        AddStudentInRouteAdapter.dropPos=i;

                        list.get(i).setDrop(true);

                    }

                    updateViewDrop(i);
                }else{
                    if ( !list.get(i).isDrop())
                        list.get(i).setDrop(false);
                    notifyDataSetChanged();

                    Toast.makeText(activity,"Can't Change Pick and Drop Contact Admin", Toast.LENGTH_SHORT).show();
                }


            }

        });

    }

    @Override
    public int getItemCount() {

        return list.size();

    }

    public void updateViewDrop(int pos){
        for(int i=0;i<list.size();i++){
            if(i!=pos){
                list.get(i).setDrop(false);

            }
        }
        notifyDataSetChanged();
    }
    public void updateViewPick(int pos){
        for(int i=0;i<list.size();i++){
            if(i!=pos){
                list.get(i).setPick(false);

            }
        }
        notifyDataSetChanged();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_location;
        CheckBox check_pick,check_drop;
        private LinearLayout linear_;
        public ViewHolder(View view) {
            super(view);

            check_drop=view.findViewById(R.id.check_drop);
            check_pick=view.findViewById(R.id.check_pick);
            txt_location=view.findViewById(R.id.txt_location);
            linear_= view.findViewById(R.id.linear_);
        }
    }
}