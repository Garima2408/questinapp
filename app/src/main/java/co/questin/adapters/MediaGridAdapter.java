package co.questin.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import co.questin.R;

public class MediaGridAdapter extends RecyclerView.Adapter<MediaGridAdapter.ViewHolder> {
    private Activity context;
    List<String> list;
    int i ;
    String typeDash;
    Dialog imageDialog;
    ViewPager pagerImage;
    PagerAdapter Imageadapter;

   public MediaGridAdapter(Activity context, List<String> list)
   {
       this.context = context;
       this.list = list;
   }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dashboard, parent, false);
        return new ViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


       if(list.get(position) !=null &&list.get(position).length() > 0 ){

                Glide.with(context).load(list.get(position))
                        .placeholder(R.mipmap.media_placeholder).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .fitCenter().into(holder.img_android);

            }



        holder.img_android.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                imageDialogmultiple(list,position);

                Log.d("TAG", "onClickitemposition: "+ list.get(position));



            }
        });

        }

    @Override
    public int getItemCount() {
       return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_android;
        public LinearLayout linear_;
        public ViewHolder(View view) {
            super(view);

            img_android = view.findViewById(R.id.img_android);

            linear_= view.findViewById(R.id.linear_);
        }
    }


    private void imageDialogmultiple(List<String> s, int i) {

        imageDialog = new Dialog(context);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.multiple_images_display);
        // dialogLogin.setCancelable(false);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        pagerImage = imageDialog.findViewById(R.id.pager);

        Imageadapter = new MediaDetailsAdapter(context, s);
        pagerImage.setAdapter(Imageadapter);
        pagerImage.setCurrentItem(i);
        pagerImage.setOnClickListener(onChagePageClickListener(String.valueOf(i)));



    }

    private View.OnClickListener onChagePageClickListener(final String i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagerImage.setCurrentItem(Integer.parseInt(i));
            }
        };
    }








}
