package co.questin.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.questin.R;
import co.questin.models.SubjectsAssignmentsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.teacher.TeacherAssignmentDetailed;
import co.questin.teacher.UpdateAssignment;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

/**
 * Created by Dell on 22-09-2017.
 */

public class SubjectsAssignmentsAdapter extends RecyclerView.Adapter<SubjectsAssignmentsAdapter.CustomVholder> {


    private ArrayList<SubjectsAssignmentsArray> lists;

    String Subject_id;
    private Context mcontext;
    private DeleteSubjectAssignment deletesubjectassignment = null;

    public SubjectsAssignmentsAdapter(Context mcontext, ArrayList<SubjectsAssignmentsArray> lists, String Subject_id) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.Subject_id =Subject_id;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_subjectassinments, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            String listposition =lists.get(position).toString();
            Log.d("listposition", "listposition" + listposition);

            holder.tv_title.setText(lists.get(position).getTitle());
            holder.tv_teacher_name.setText(lists.get(position).getTeacher());

            String str = lists.get(position).getDate();
            String[] splited = str.split("\\s+");

            String split_one=splited[0];
            String split_second=splited[1];


            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

            Date date = inputFormat.parse(split_one);
            String outputDateStr = outputFormat.format(date);

            Log.d("Splited String ", "Splited String" + split_one+split_second);

            SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
            Date date1 = null;
            try {
                date1 = parseFormat.parse(split_second);


            } catch (ParseException e) {
                e.printStackTrace();
            }
            String time24format = displayFormat.format(date1);
            System.out.println(parseFormat.format(date1) + " = " + displayFormat.format(date1));



            holder.tv_date.setText(outputDateStr);





            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(mcontext,TeacherAssignmentDetailed.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("ASSIGNMENT_ID",lists.get(position).getTnid());

                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);
                }
            });

            holder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenu();


                }

                private void showPopupMenu() {


                    PopupMenu popup = new PopupMenu(mcontext, holder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.Editoption:



                                    Intent i = new Intent(mcontext, UpdateAssignment.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("ASSIGNMENT_ID", lists.get(position).getTnid());
                                    bundle.putString("SUBJECT_ID", Subject_id);

                                    i.putExtras(bundle);
                                    mcontext.startActivity(i);

                                    return true;
                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Delete Attachment")
                                            .setMessage(R.string.Delete)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    deletesubjectassignment = new DeleteSubjectAssignment();
                                                    deletesubjectassignment.execute(lists.get(position).getTnid());
                                                    lists.remove(position);
                                                    notifyDataSetChanged();



                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();


                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView tv_title, tv_teacher_name,tv_date;
        LinearLayout link;
        ImageView options;

        public CustomVholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_teacher_name = itemView.findViewById(R.id.tv_teacher_name);
            tv_date = itemView.findViewById(R.id.tv_date);
            link = itemView.findViewById(R.id.link);
            options =itemView.findViewById(R.id.options);


        }

    }


     /*DELETE  SUBJECTS*/

    private class DeleteSubjectAssignment extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         //   Utils.p_dialog(mcontext);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETEASSIGNMENT+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
               // Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletesubjectassignment = null;
            try {
                if (responce != null) {
                 //   Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // Utils.p_dialog_dismiss(mcontext);



                    } else if (errorCode.equalsIgnoreCase("0")) {
                       // Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);



                    }
                }else {
                    //Utils.p_dialog_dismiss(mcontext);
                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
               // Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deletesubjectassignment = null;
          //  Utils.p_dialog_dismiss(mcontext);


        }
    }


}