package co.questin.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import co.questin.R;


public class CampusFeedsFilterAdapters extends ArrayAdapter<String> {

    private final Activity context;
    ArrayList<String> lists;

    private final Integer[] images;

    public CampusFeedsFilterAdapters(Activity context, ArrayList<String> lists, Integer[] images) {
        super(context, R.layout.list_of_categoryfeed, lists);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.lists=lists;
        this.images=images;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_of_categoryfeed, null,true);

        TextView  category_name = rowView.findViewById(R.id.category_name);
        ImageView   category_Image =rowView.findViewById(R.id.category_Image);


        category_name.setText(lists.get(position));
        category_Image.setImageResource(images[position]);

        return rowView;

    }
}


