package co.questin.admin;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import co.questin.R;
import co.questin.Retrofit.ApiInterface;
import co.questin.Retrofit.ServiceGenerator;
import co.questin.models.chat.SendMessageModel;
import co.questin.models.etaResponse.DistanceModel;
import co.questin.models.routedetailResponse.RouteDetailResponseModel;
import co.questin.models.routedetailResponse.Waypoint;
import co.questin.models.routelistResponse.RouteListResponseModel;
import co.questin.models.tracker.LocationModel;
import co.questin.models.tracker.RouteListAdminModel;
import co.questin.models.tracker.StopsModel;
import co.questin.models.tracker.WayPointResponse;
import co.questin.models.tracker.WayPointsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.tracker.HttpConnection;
import co.questin.tracker.PathJSONParser;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;



public class AdminRouteActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Gson gson;
    private Context mContext;
    private Handler uiHandler;
    LinearLayout NotiBAr, bottom_sheet;
    private Polyline polyline;
    TextView FirstNotification;
    private PolylineOptions polylineOptions;
    private ArrayList<RouteListAdminModel> currentRoutesList;
    private DatabaseReference locationDatabase;
    private DatabaseReference etaDatabase;
    private DatabaseReference geofenceDatabase;
    private ValueEventListener etaEventListener;
    private ValueEventListener geofenceEventListener;
    private ValueEventListener locationEventListener;
    DistanceModel.Duration durationModel;
    int speed;
    int PolyColour;
    private String collegeId;
    private ArrayList<RouteListAdminModel> dataReload;
    private HashMap<String, Marker> destMarkersMap;
    private HashMap<String, Marker> sourceMarkersMap;
    private HashMap<String, Marker> vehicleMarkersMap;
    private HashMap<String, StopsModel> destStopsMap;
    private HashMap<String, StopsModel> sourceStopsMap;
    private HashMap<String, ArrayList<Marker>> stopMarkersMap;
    private HashMap<String, ArrayList<StopsModel>> stopsListMap;
    private HashMap<String, ValueEventListener> locationListenersMap;
    private HashMap<String, Polyline> polylineHashMap;
    private HashMap<String, PolylineOptions> polylineOptionsHashMap;
    // private String collegeId,Route_id,routeName;
    private StopsModel sourceStop;
    private StopsModel destStop;
    private boolean isMapInitialized = false;
    private boolean isRouteDetailsFetched = false;
    private ArrayList<StopsModel> mStopsList;
    private RoutesListAdapter routesListAdapter;
    private RouteDetailResponseModel routeDetailResponseModel;
    BottomSheetBehavior sheetBehavior;
    private static final String TAG = "AdminMapsActivity";
    Handler delayhandler = new Handler();
    SessionManager sharedPreference;
    private int RC_TRACKER_INTENT = 0;
    ListView listofnotifications;
    List<SendMessageModel> adminNotificationList;
    private ArrayList<String> notificationlist;
    private LocationModel locationModel;
    private Marker vehicleMarker;
    ImageView imageListTogggle;
    RecyclerView recyclerRoutes;
    WayPointResponse wayPickDropJson=new WayPointResponse();
    ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
    WayPointsArray wayPointsPick=new WayPointsArray();
    WayPointsArray wayPointsDrop;
        String PollylineRouteName;
         ImageView traficEnable;
    boolean isPressed = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_route);

        uiHandler = new Handler();
        gson = new Gson();
        mContext = this;
        currentRoutesList = new ArrayList<>();
        mStopsList = new ArrayList<>();
        stopsListMap = new HashMap<>();
        destMarkersMap = new HashMap<>();
        sourceMarkersMap = new HashMap<>();
        destStopsMap = new HashMap<>();
        sourceStopsMap = new HashMap<>();
        stopMarkersMap = new HashMap<>();
        vehicleMarkersMap = new HashMap<>();
        polylineHashMap = new HashMap<>();
        polylineOptionsHashMap = new HashMap<>();
        locationListenersMap = new HashMap<>();
        sharedPreference = new SessionManager(this);
        adminNotificationList = new ArrayList<>();
        notificationlist = new ArrayList<>();
        sharedPreference = new SessionManager(this);
        adminNotificationList = new ArrayList<>();
        notificationlist = new ArrayList<>();
        dataReload =new ArrayList<>();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        collegeId = SessionManager.getInstance(this).getCollage().getTnid();


        NotiBAr = findViewById(R.id.NotiBAr);
        FirstNotification = findViewById(R.id.FirstNotification);
        FirstNotification.setSelected(true);
        traficEnable =findViewById(R.id.traficEnable);
        recyclerRoutes =findViewById(R.id.recycler_routes);
        imageListTogggle =findViewById(R.id.image_list_toggle);


        routesListAdapter = new RoutesListAdapter(new ArrayList<RouteListAdminModel>(), this);
        recyclerRoutes.setAdapter(routesListAdapter);

        recyclerRoutes.setLayoutManager(new LinearLayoutManager(this));
        imageListTogggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recyclerRoutes.getVisibility() == View.VISIBLE) {
                    recyclerRoutes.setVisibility(View.GONE);
                } else {
                    recyclerRoutes.setVisibility(View.VISIBLE);
                }
            }
        });


        traficEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPressed){
                    mMap.setTrafficEnabled(true);

                }else if(!isPressed){

                    mMap.setTrafficEnabled(false);

                }

                isPressed = !isPressed; // reverse





            }
        });


        getRoutesList();
        getNotificationLog();


        NotiBAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AdminRouteActivity.this, AdminNotificationLog.class);
                startActivity(i);

            }
        });


    }

    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


    }

    private void init(String id, String title) {
        gson = new Gson();


        etaDatabase = FirebaseDatabase.getInstance().getReference()
                .child( collegeId +"/routes/"+ id + "/routes_name/" + title + "/eta");
        locationDatabase = FirebaseDatabase.getInstance().getReference()
                .child( collegeId +"/routes/"+ id + "/routes_name/" + title + "/location");
        geofenceDatabase = FirebaseDatabase.getInstance().getReference()
                .child( collegeId +"/routes/"+ id + "/routes_name/" + title + "/geofence");

        locationEventListener = getLocationEventListener(title);
        mStopsList = new ArrayList<>();

    }


    private void getNotificationLog() {

        mUpdateTimeTask.run();


    }


    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {   // Todo
            long NOTIFY_INTERVAL = 10;
            long   millis =System.currentTimeMillis();
            int seconds = (int) (millis / 1000);
            seconds = seconds % 11;
           // Log.e(TAG, "Time:"+seconds);

            // This line is necessary for the next call
            delayhandler.postDelayed(this, NOTIFY_INTERVAL);

            if (seconds==NOTIFY_INTERVAL) {
                adminNotificationList = sharedPreference.getAdminNotification(AdminRouteActivity.this);
                FirstNotification.setText("");
                if ((adminNotificationList != null)) {

                    for (int i = 0; i < adminNotificationList.size(); i++) {
                        FirstNotification.setText(adminNotificationList.get(i).getMessage() + ".....");
                       // Log.d(TAG, "run: " + "next msg" +"" +adminNotificationList.get(i).getMessage());
                    }

                } else {
                    FirstNotification.setText("No New Notification");
                 //   Log.d(TAG, "run: " + "next msg" +"" +"No New Notification");


                }
            }

            }

    };


    @Override
    protected void onStop() {

        super.onStop();

    }



    private void getRoutesList() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String response =
                            ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                                    URLS.ROUTES_LIST + "?" + "cid=" + SessionManager.getInstance(AdminRouteActivity.this).getCollage().getTnid());


                    final RouteListResponseModel model = gson.fromJson(response, RouteListResponseModel.class);
                    Log.e(TAG, "run: " + URLS.ROUTES_LIST + SessionManager.getInstance(mContext).getCollage().getTnid());
                    Log.e(TAG, "run: " + response);

                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (model.getStatus() == 0) {
                                Log.e(TAG, "run: " + model.getMessage());
                                return;
                            }
                            for (int i = 0; i < model.getData().size(); i++) {
                                routesListAdapter.addData(new RouteListAdminModel(
                                        model.getData().get(i).getTitle(), model.getData().get(i).getTnid(), false, model.getData().get(i).getIs_faculty(), model.getData().get(i).getIs_student()
                                        , model.getData().get(i).getIs_member(), model.getData().get(i).getIs_driver(), model.getData().get(i).getNode_uid(), model.getData().get(i).getIs_creator()
                                        , model.getData().get(i).getSponsered()));
                            }

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void getRouteDetail(final RouteListAdminModel routeModel) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isRouteDetailsFetched = false;
                    final String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + routeModel.getId());

                    //htis is use full for regarding the show stop
                    routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);
                    sourceStopsMap.put(routeModel.getTitle(), new StopsModel(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldStartTime(),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLat()),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLng())));
                    destStopsMap.put(routeModel.getTitle(), new StopsModel(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldStartTime(),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLat()),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLng())));


                    //this use for show wy point on tghe map
                    ArrayList<StopsModel> stopsList = new ArrayList<>();
                    for (Waypoint waypoint : routeDetailResponseModel.getData().getFieldCollectionWayPoints().getWaypoints()) {
                        stopsList.add(new StopsModel(waypoint.getFieldStartTime(),
                                Double.valueOf(waypoint.getFieldWayPoints().getLat()), Double.valueOf(waypoint.getFieldWayPoints().getLng())));
                    }


                    stopsListMap.put(routeModel.getTitle(), stopsList);
                    isRouteDetailsFetched = true;


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            initRoute(routeModel.getTitle());


                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    private void getRouteAgain(final String RouteId, final String RouteTittle) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isRouteDetailsFetched = false;
                    final String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + RouteId);
                    Log.d(TAG, "run: "+response+".."+ RouteId);

                    //htis is use full for regarding the show stop
                    routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);

                    sourceStopsMap.put(RouteTittle, new StopsModel(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldStartTime(),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLat()),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLng())));
                    destStopsMap.put(RouteTittle, new StopsModel(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldStartTime(),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLat()),
                            Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLng())));


                    //this use for show wy point on tghe map
                    ArrayList<StopsModel> stopsList = new ArrayList<>();

                    for (Waypoint waypoint : routeDetailResponseModel.getData().getFieldCollectionWayPoints().getWaypoints()) {
                        stopsList.add(new StopsModel(waypoint.getFieldStartTime(),
                                Double.valueOf(waypoint.getFieldWayPoints().getLat()), Double.valueOf(waypoint.getFieldWayPoints().getLng())));
                    }


                    stopsListMap.put(RouteTittle, stopsList);
                    isRouteDetailsFetched = true;


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            init(RouteId,RouteTittle);

                            initRoute(RouteTittle);


                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }



    private ValueEventListener getLocationEventListener(final String title) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.getValue() == null){
                    Log.e(TAG, "onDataChange: null location object");
                    Toast.makeText(mContext, title+" "+"Location is null plz refresh", Toast.LENGTH_SHORT).show();



                    return;


                }else {
                    Log.e(TAG, "onDataChange AdminpageSpeed: " + dataSnapshot.toString());


                    if(vehicleMarkersMap.containsKey(title)){
                        LocationModel locationModel =
                                new LocationModel(dataSnapshot.getValue(LocationModel.class));
                       // locationModel.updateModel(dataSnapshot.getValue(LocationModel.class));
                        Marker marker = vehicleMarkersMap.get(title);
                        marker.setPosition(new LatLng(locationModel.getLat(), locationModel.getLng()));
                        marker.setRotation(locationModel.getBearing());
                        Double floatspeed =locationModel.getSpeed()*3.6;
                        speed = Integer.valueOf(floatspeed.intValue()) ;
                        marker.setSnippet("Speed: " +speed +"Km/h");
                        Log.e(TAG, "Admin page Speed: " + speed);
                        animateMarker(locationModel, vehicleMarker);

                        }
                    }















            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        };
    }



    private void addRoute(RouteListAdminModel routeListAdminModel) {
        getRouteDetail(routeListAdminModel);
    }




    private void removeRoute(RouteListAdminModel routeListAdminModel) {
        String id = routeListAdminModel.getId();
        String key = routeListAdminModel.getTitle();
       /* if (sourceMarkersMap.containsKey(key)) {
            sourceMarkersMap.get(key).remove();
            sourceMarkersMap.remove(key);
        }
        if (destMarkersMap.containsKey(key)) {
            destMarkersMap.get(key).remove();
            destMarkersMap.remove(key);
        }
        if (stopMarkersMap.containsKey(key)) {
            for (Marker marker : stopMarkersMap.get(key)) {
                marker.remove();
            }
            stopMarkersMap.remove(key);
        }*/




        if (vehicleMarkersMap.containsKey(key)) {
            vehicleMarkersMap.get(key).remove();
            vehicleMarkersMap.remove(key);
            vehicleMarkersMap.clear();



        }


        if (polylineHashMap.containsKey(key)) {
            polylineHashMap.get(key).remove();
            polylineHashMap.remove(key);
            polylineHashMap.clear();




        }

        if (locationListenersMap.containsKey(key)) {
            locationDatabase.child(collegeId +"/routes/" +id + "/routes_name/" + key + "/location")
                    .removeEventListener(locationListenersMap.get(key));
            locationListenersMap.remove(key);

        }


       /* if (sourceStopsMap.containsKey(key)) sourceStopsMap.remove(key);
        if (destStopsMap.containsKey(key)) destStopsMap.remove(key);
        if (stopsListMap.containsKey(key)) stopsListMap.remove(key);
*/
        polylineOptionsHashMap.remove(key);
           polylineOptionsHashMap.clear();

        mMap.clear();
        CallTheCompleteMapAgain(dataReload,key);



    }

    private void CallTheCompleteMapAgain(ArrayList<RouteListAdminModel> dataReload, String key) {

        for (int i = 0; i < dataReload.size(); i++) {

            if (dataReload.get(i).isSelected() == true){

                Log.d(TAG, "CallTheCompleteMapAgain: " +dataReload.get(i).getTitle() +dataReload.size());
                getRouteAgain(dataReload.get(i).getId(),dataReload.get(i).getTitle());
               // init(dataReload.get(i).getId(),dataReload.get(i).getTitle());

            }else if(dataReload.get(i).isSelected() == false) {


            }



        }



    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                recyclerRoutes.setVisibility(View.GONE);
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

    }

    private void initRoute(String routeName){
        PolyColour = getRandomColor();
        PollylineRouteName =routeName;
        ArrayList<StopsModel> stopsList = stopsListMap.get(routeName);
        stopMarkersMap.put(routeName, new ArrayList<Marker>());

        if (stopsList.size()>0){
            for(int i=0; i<stopsList.size(); i++){
                if(i == stopsList.size()-1){
                  String url =   getDirectionsUrl(stopsList.get(i),destStopsMap.get(routeName));
                    ReadTask downloadTask = new ReadTask();
                    downloadTask.execute(url);
                   // stopMarkersMap.get(routeName).add(addStopMarker(stopsList.get(i)));
                    continue;
                }
                if(i==0){

                    String url =   getDirectionsUrl(sourceStopsMap.get(routeName),stopsList.get(0));
                    ReadTask downloadTask = new ReadTask();
                    downloadTask.execute(url);
                }


                String url =   getDirectionsUrl(stopsList.get(i),stopsList.get(i+1));
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);


               // stopMarkersMap.get(routeName).add(addStopMarker(stopsList.get(i)));
            }
        }else{
            String url =   getDirectionsUrl(sourceStopsMap.get(routeName),destStopsMap.get(routeName));
            ReadTask downloadTask = new ReadTask();
            downloadTask.execute(url);
        }


        Utils.clearPickDropData();

        WayPointsArray wayPointsDrop=new WayPointsArray();

        wayPointsDrop.setLat(""+destStopsMap.get(routeName).getLat());
        wayPointsDrop.setLng(""+destStopsMap.get(routeName).getLng());
        wayPointsDrop.setField_place_name(destStopsMap.get(routeName).getName());
        wayPointsDrop.setDrop(true);


        wayPointsArrays.add(wayPointsDrop);
        wayPickDropJson.setWayPointsArrays(wayPointsArrays);


        Utils.savePickAndDrop(this,wayPickDropJson);






        // destMarkersMap.put(routeName, addDestMarker(destStopsMap.get(routeName)));
        //  sourceMarkersMap.put(routeName, addSourceMarker(sourceStopsMap.get(routeName)));

     /*   vehicleMarker = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bus))
                .title(routeName)
                .flat(true).position(new LatLng(sourceStopsMap.get(routeName).getLat(), sourceStopsMap.get(routeName).getLng())));
*/
        vehicleMarkersMap.put(routeName, addVehicleMarker(routeName));
        locationDatabase.addValueEventListener(locationEventListener);

        mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(sourceStopsMap.get(routeName).getLat(),
                        sourceStopsMap.get(routeName).getLng()), 15));


    }

    class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            // PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polylineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);


                }

                polylineOptions.addAll(points);
                polylineOptions.width(6);
                polylineOptions.color(PolyColour);
            }

            if (polylineOptions!=null)
                mMap.addPolyline(polylineOptions);

            polylineOptionsHashMap.put(PollylineRouteName, polylineOptions);
            polylineHashMap.put(PollylineRouteName, mMap.addPolyline(polylineOptions));


        }
    }
    private String getDirectionsUrl(StopsModel origin, StopsModel dest) {

        // Origin of route
        String str_origin = "origin=" + origin.getLat() + "," + origin.getLng();

        // Destination of route
        String str_dest = "destination=" + dest.getLat() + "," + dest.getLng()
                ;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+"&key="+Constants.MAPS_API_KEY;


        return url;
    }


    private Marker addSourceMarker(StopsModel sourceStop){
      /*  return mMap.addMarker(new MarkerOptions()
                .title(sourceStop.getName())
                .snippet("Source")
                .position(new LatLng(sourceStop.getLat(), sourceStop.getLng())));*/
        return null;
    }

    private Marker addDestMarker(StopsModel destStop){
       /* return mMap.addMarker(new MarkerOptions()
                .title(destStop.getName())
                .snippet("Destination")
                .position(new LatLng(destStop.getLat(), destStop.getLng())));*/
        return null;
    }

    private Marker addStopMarker(StopsModel stopModel){
       /* return mMap.addMarker(new MarkerOptions()
                .title(stopModel.getName())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.stop))
                .position(new LatLng(stopModel.getLat(), stopModel.getLng())));*/
        return null;
    }


    private Marker addVehicleMarker(String routeName){
        return   mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.bus))
                .position(new LatLng(sourceStopsMap.get(routeName).getLat(), sourceStopsMap.get(routeName).getLng()))
                .title(routeName).flat(true));






    }





    class RoutesListAdapter extends RecyclerView.Adapter<RoutesListAdapter.RoutesListViewHolder>{

        private Context mContext;
        private LayoutInflater inflater;
        private ArrayList<RouteListAdminModel> data;
        private SparseIntArray sparseIntArray;

        RoutesListAdapter(ArrayList<RouteListAdminModel> data, Context context){
            this.data = data;
            this.mContext = context;
            inflater = LayoutInflater.from(context);
            sparseIntArray = new SparseIntArray(5);
            sparseIntArray.put(0, R.mipmap.transa);
            sparseIntArray.put(1, R.mipmap.transb);
            sparseIntArray.put(2, R.mipmap.transc);
            sparseIntArray.put(3, R.mipmap.transd);
            sparseIntArray.put(4, R.mipmap.transe);
        }

        @Override
        public RoutesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new RoutesListViewHolder(inflater.inflate(R.layout.admin_routes_list_adapter, parent, false));
        }

        @Override
        public void onBindViewHolder(final RoutesListViewHolder holder, final int position) {
//            holder.checkSelected.setChecked(data.get(position).isSelected());
            if(data.get(position).isSelected()){
                holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            }
            else {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            }
            holder.textRouteName.setText(data.get(position).getTitle());
            holder.imageBus.setBackground(ContextCompat
                    .getDrawable(mContext, sparseIntArray.get(position%sparseIntArray.size())));


            final String image = String.valueOf(sparseIntArray.get(position%sparseIntArray.size()));
            Log.d("TAG", "imagearray: "+image);


            holder.checkSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                    if(mMap == null){
//                        compoundButton.toggle();
//                        Toast.makeText(mContext, "Map not initialized yet", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                    data.get(holder.getAdapterPosition()).setSelected(b);
//                    if(b) addRoute(data.get(holder.getAdapterPosition()));
//                    else removeRoute(data.get(holder.getAdapterPosition()));
                }
            });


            holder.txt_route_viewmore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(AdminRouteActivity.this,TransportDetailsAdmin.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("ROUTE_ID",data.get(position).getId());
                    bundle.putString("ROUTE_NAME",data.get(position).getTitle());
                    bundle.putString("IS_FACULTY",data.get(position).getIs_faculty());
                    bundle.putString("IS_CREATER",data.get(position).getIs_creator());
                    bundle.putString("IS_MEMBER",data.get(position).getIs_member());
                    bundle.putString("sponsered",data.get(position).getSponsered());
                    intent.putExtras(bundle);
                    startActivity(intent);



                }
            });



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(mMap == null){
                        Toast.makeText(mContext, "Map not initialized yet", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    boolean isSelected = data.get(holder.getAdapterPosition()).isSelected();


                    if(isSelected){
                        data.get(holder.getAdapterPosition()).setSelected(false);
                        removeRoute(data.get(holder.getAdapterPosition()));
                        dataReload.remove(data.get(holder.getAdapterPosition()));
                        view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                        recyclerRoutes.setVisibility(View.GONE);

                    }
                    else {
                        data.get(holder.getAdapterPosition()).setSelected(true);
                        addRoute(data.get(holder.getAdapterPosition()));
                        dataReload.add(data.get(holder.getAdapterPosition()));
                        init(data.get(position).getId(),data.get(position).getTitle());
                        view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                        recyclerRoutes.setVisibility(View.GONE);

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        void addData(RouteListAdminModel routeModel){
            data.add(routeModel);
            notifyItemInserted(data.size() - 1);
        }





        class RoutesListViewHolder extends RecyclerView.ViewHolder{

            View itemView;
            ImageView imageBus;
            CheckBox checkSelected;
            TextView textRouteName;
            ImageView txt_route_viewmore;



            RoutesListViewHolder(View itemView) {
                super(itemView);
                this.itemView = itemView;

                imageBus =itemView.findViewById(R.id.image_bus);
                checkSelected =itemView.findViewById(R.id.check_selected);
                textRouteName =itemView.findViewById(R.id.txt_route_name);
                txt_route_viewmore =itemView.findViewById(R.id.txt_route_viewmore);



            }
        }
    }


    private void animateMarker(final LocationModel locationModel, final Marker vehicleMarker) {

        if (this.vehicleMarker != null) {
            final LatLng startPosition = this.vehicleMarker.getPosition();
            final LatLng endPosition = new LatLng(locationModel.getLat(), locationModel.getLng());
            // Location  mLocation =lat+lng;


            final float startRotation = this.vehicleMarker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(2000); // duration 1 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        vehicleMarker.setPosition(newPosition);
                        vehicleMarker.setRotation(computeRotation(v, startRotation,locationModel.getBearing()));
                    } catch (Exception ex) {
                        // I don't care atm..
                    }
                }
            });

            valueAnimator.start();
        }


    }



    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }
    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    private void getETA(LatLng origin, LatLng dest,String placeName) {
        Retrofit distance= ServiceGenerator.getDistance();
        ApiInterface service= distance.create(ApiInterface.class);


        Call<DistanceModel> call= service.distanceMatrix("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origin.latitude+","+origin.longitude+"&destinations="+dest.latitude+","+dest.longitude+
                //           "&departure_time=now&traffic_model=best_guess&key="+Constants.MAPS_API_KEY);
                "&key="+Constants.MAPS_API_KEY);

        call.enqueue(new Callback<DistanceModel>() {
            @Override
            public void onResponse(Call<DistanceModel> call, retrofit2.Response<DistanceModel> response) {

                if(response!=null){
                    try {
                        Utils.init(AdminRouteActivity.this);

                        WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(AdminRouteActivity.this);
                        if (response.body().getRows()!=null && response.body().getRows().size()>0){
                            String distance=response.body().getRows().get(0).getElements().get(0).getDistance().getText();

                            Log.e("distance",response.body().getDestinationAddresses()+""
                                    +distance+"time"
                                    +response.body().getRows().get(0).getElements().get(0).getDuration().getText());
                            durationModel= response.body().getRows().get(0).getElements().get(0).getDuration();

                            String str = distance;
                            String[] splited = str.split("\\s+");
                            String DistanceSplit =  splited[0];
                            int CalculateRange= new Double(DistanceSplit).intValue();
                            Log.e("distance Split", String.valueOf(CalculateRange) +""+ splited[0]);
                            //  Toast.makeText(activity, "Main Eta Distance " + distance.toString() + response.body().getRows().get(0).getElements().get(0).getDuration().getText(), Toast.LENGTH_SHORT).show();
                            String title="Speed: " + speed +" "+"Km/h " +" "+"ETA: " + distance +" " +response.body().getRows().get(0).getElements().get(0).getDuration().getText();
                            vehicleMarker.setSnippet(title);




                           if (distance!=null){

                            }
                        }

                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<DistanceModel> call, Throwable t) {

            }
        });
    }






}