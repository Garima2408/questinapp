package co.questin.admin;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import co.questin.R;
import co.questin.models.chat.SendMessageModel;
import co.questin.utils.SessionManager;

public class AdminNotificationLog extends AppCompatActivity {
    SessionManager sharedPreference;
    ListView listofnotifications;
    List<SendMessageModel> adminNotificationList;
    private ArrayList<String> notificationlist;
    TextView ErrorText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_notification_log);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        sharedPreference = new SessionManager(this);
        adminNotificationList =new ArrayList<>();
        notificationlist = new ArrayList<>();
        listofnotifications = findViewById(R.id.listofnotifications);
        ErrorText =findViewById(R.id.ErrorText);
        adminNotificationList = sharedPreference.getAdminNotification(this);
        getNotifications();



        }

    private void getNotifications() {


        if ((adminNotificationList !=null)){

            Collections.reverse(adminNotificationList);

            for (int i=0; i<adminNotificationList.size();i++) {

                notificationlist.add(adminNotificationList.get(i).getMessage());


            }


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1,
                    notificationlist);
            listofnotifications.setAdapter(adapter);


        }else {
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("No New Notification");
            listofnotifications.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {

        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            case R.id.Clear:

                sharedPreference.removenotification(this);
                if(adminNotificationList != null){
                    notificationlist.clear();
                    adminNotificationList.clear();

                }else {

                }
                getNotifications();


                return true;




            default:
                return super.onOptionsItemSelected(item);
        }

    }




}
