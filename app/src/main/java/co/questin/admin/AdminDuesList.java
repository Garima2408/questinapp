package co.questin.admin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.TeacherStudentDuesAdapter;
import co.questin.models.StudentListsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.OkHttpClient;

public class AdminDuesList extends BaseAppCompactActivity {
  RecyclerView rv_memberInCollege;
    private RecyclerView.LayoutManager layoutManager;
    private TeacherStudentDuesAdapter studentadapter;
    private StudentListDisplay studentlist = null;
    public ArrayList<StudentListsArray> mStudentList;
    String SlectedType,COLLEGEMEMBER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dues_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        SlectedType = intent.getStringExtra("SELECTED");
        if (SlectedType.matches("Student")){
            COLLEGEMEMBER="college_student_list";
        }else if (SlectedType.matches("Teacher")){

           COLLEGEMEMBER="college_parent_list";

        }


        /* Student Teacher*/
        layoutManager = new LinearLayoutManager(this);
        rv_memberInCollege = findViewById(R.id.rv_memberInCollege);
        rv_memberInCollege.setHasFixedSize(true);
        rv_memberInCollege.setLayoutManager(layoutManager);

         mStudentList=new ArrayList<>();

        studentlist = new StudentListDisplay();
        studentlist.execute();


    }


    private class StudentListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEMEMBERLIST+COLLEGEMEMBER+"?"+"cid="+SessionManager.getInstance(AdminDuesList.this).getCollage().getTnid() +"&offset="+"0"+"&limit=120");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AdminDuesList.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                StudentListsArray StudentInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), StudentListsArray.class);
                                mStudentList.add(StudentInfo);
                                studentadapter = new TeacherStudentDuesAdapter(AdminDuesList.this, mStudentList, "");
                                rv_memberInCollege.setAdapter(studentadapter);

                            }

                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(AdminDuesList.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(R.string.No_Student_subject))
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentlist = null;
            hideLoading();


        }
    }



    @Override
    public void onBackPressed() {
        finish();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
