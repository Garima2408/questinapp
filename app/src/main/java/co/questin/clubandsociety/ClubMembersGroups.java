package co.questin.clubandsociety;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.ClubCommentsAdapter;
import co.questin.adapters.GroupMembersAdapter;
import co.questin.models.ClubCommentArray;
import co.questin.models.CourseMemberLists;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ClubMembersGroups extends BaseFragment {
    static String Group_id;
    static RecyclerView GroupMember_Lists,rv_club_comment;
    Button JoinGroup;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    GroupMembersAdapter mGroupMemberAdapter;
    static ClubCommentsAdapter mcommentAdapter;
    public ArrayList<CourseMemberLists> memberList;
    public ArrayList<ClubCommentArray> commentsList;
    private GroupMemberList groupmemberList = null;
    private ProgressJoinTheGroupRequest joingrouprequestList = null;
    private static GroupsCommentsList listcommentingroups = null;
    static Activity activity;
    private static ProgressBar spinner;
    static TextView ErrorText;
    static String tittle,URLDoc;
    static SwipeRefreshLayout swipeContainer;
    static FloatingActionButton fab;
    private boolean _hasLoadedOnce= false; // your boolean field
    private static ShimmerFrameLayout mShimmerViewContainer;
    private static int PAGE_SIZE = 0;
    private static boolean isLoading= true;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(isFragmentVisible_);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Log.e("view", "CollegeNewFeed");
                PAGE_SIZE = 0;
                isLoading = false;
                swipeContainer.setRefreshing(true);
                listcommentingroups = new GroupsCommentsList();
                listcommentingroups.execute(Group_id);

                _hasLoadedOnce = true;
            }
            groupmemberList = new GroupMemberList();
            groupmemberList.execute(Group_id);

        }
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_clubmember_group, container, false);
        activity = (Activity) view.getContext();
        Group_id = getArguments().getString("GROUP_ID");
        spinner= view.findViewById(R.id.progressBar);
        JoinGroup = view.findViewById(R.id.JoinGroup);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        rv_club_comment = view.findViewById(R.id.rv_club_comment);
        layoutManager = new LinearLayoutManager(activity);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        GroupMember_Lists.setLayoutManager(mLayoutManager);
        rv_club_comment.setHasFixedSize(true);
        rv_club_comment.setLayoutManager(layoutManager);
        fab = view. findViewById(R.id.fab);
        ErrorText= view. findViewById(R.id.ErrorText);
        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<ClubCommentArray>();

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        mcommentAdapter = new ClubCommentsAdapter(rv_club_comment, commentsList ,activity);
        rv_club_comment.setAdapter(mcommentAdapter);

        /*GROUP MEMBER LIST CALLING*/
        groupmemberList = new GroupMemberList();
        groupmemberList.execute();

        fab.setVisibility(View.VISIBLE);



        JoinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*JOIN ANY GROUP AND SOCITY CALLING*/

                joingrouprequestList = new ProgressJoinTheGroupRequest();
                joingrouprequestList.execute();
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent(activity, AddCommentClub.class)
                        .putExtra("GROUP_ID",Group_id)
                        .putExtra("SAME","1")
                        .putExtra("open","ClubAndSocityGroup");
                startActivity(backIntent);




            }
        });



        return view;
    }


    private void inItView(){
        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(false);
        CallTheSpin();
        rv_club_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();

                    listcommentingroups = new GroupsCommentsList();
                    listcommentingroups.execute(Group_id);

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void CallTheSpin()
    { swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {

            // implement Handler to wait for 3 seconds and then update UI means update value of TextView
            new Handler().postDelayed(new Runnable() {


                @Override
                public void run() {
                    if (!isLoading){
                        RefreshWorkedClub();
                    }else{
                        isLoading=false;
                        swipeContainer.setRefreshing(false);
                    }




                }
            }, 3000);
        }
    });


    }

    public static void RefreshWorkedClub() {
        PAGE_SIZE=0;
        isLoading=true;
        /* CALLING LISTS OF COMMENTS*/
        listcommentingroups = new GroupsCommentsList();
        listcommentingroups.execute(Group_id);


    }



    /*GROUP MEMBER LIST CALLING METHODS*/

    private class GroupMemberList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_GROUPMEMBERLISTS+"/"+Group_id+"/"+"users"+"?"+"fields"+"="+"id,etid,gid");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        GroupMember_Lists.setVisibility(View.VISIBLE);
                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);
                                if (CourseInfo.getGid().contains(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                {
                                    Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

                                    JoinGroup.setVisibility(View.GONE);
                                    fab.setVisibility(View.VISIBLE);
                                    inItView();


                                }
                                }
                            mGroupMemberAdapter = new GroupMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                            GroupMember_Lists.setAdapter(mGroupMemberAdapter);
                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    showAlertDialog(getString(R.string.error_responce));

                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }


      /*JOIN ANY GROUP AND SOCITY CALLING METHODS*/

    private class ProgressJoinTheGroupRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type","subgroup")
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_JOINGROUP+"/"+Group_id+"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            joingrouprequestList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // showAlertFragmentDialog(activity,"Information", msg);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            joingrouprequestList = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        setUserVisibleHint(true);
    }

    /*LISTS OF COMMENTS ON Club*/

    private static class GroupsCommentsList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ErrorText.setVisibility(View.GONE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_GROUPCOMMENTS+"?"+"sid"+"="+args[0]+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        //  Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

                mcommentAdapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            try {
                if (responce != null) {
                    rv_club_comment.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        ArrayList<ClubCommentArray> arrayList = new ArrayList<>();
                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {



                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                ClubCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ClubCommentArray.class);

                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                               /* if (jsonObject.has("field_comment_attachments")){
                                    JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                    if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                        URLDoc = AttacmentsFeilds.getString("value");
                                        tittle = AttacmentsFeilds.getString("title");
                                        subjects.title =tittle;
                                        subjects.url =URLDoc;

                                    }else {

                                    }
                                }*/
                                arrayList.add(subjects);



                            }

                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_club_comment.setVisibility(View.VISIBLE);
                                mcommentAdapter.setInitialData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                mcommentAdapter.removeProgress();
                                swipeContainer.setRefreshing(false);
                                mcommentAdapter.addData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }

                            isLoading = false;

                        } else{
                            if (PAGE_SIZE==0){
                                rv_club_comment.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                                swipeContainer.setVisibility(View.GONE);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                mcommentAdapter.removeProgress();

                        }









                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        //  Utils.showAlertFragmentDialog(activity,"Information", msg);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);






                    }
                }else {
                    swipeContainer.setRefreshing(false);
                    // Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                // Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            listcommentingroups = null;
            swipeContainer.setRefreshing(false);



        }
    }


}

