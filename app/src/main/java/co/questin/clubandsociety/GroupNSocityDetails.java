package co.questin.clubandsociety;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TabHost;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class GroupNSocityDetails extends AppCompatActivity {

    private TabLayout tabLayout;
    private static ViewPager viewPager;
    private Bundle bundle;
    ImageView ClubImage;
    String Group_id, GroupTittle, groupImage,groupdetails,groupemail;
    private TabHost tabHost;
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    private ProgressGroupInfo groupinfoAuthTask = null;
    Double Lat,Long;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_nsocity_details);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        Bundle b = getIntent().getExtras();
        Group_id = b.getString("GROUP_ID");
        GroupTittle = b.getString("TITTLE");

        GetAllEventDetails();
        collapsingToolbarLayout =  findViewById(R.id.collapsing_toolbar);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        collapsingToolbarLayout.setTitle(GroupTittle);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.CollapsingToolbarLayout);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsingToolbarLayout);

        ClubImage = findViewById(R.id.ClubImage);




    }



    private void setupViewPager(ViewPager viewPager) {


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        bundle = new Bundle();
        bundle.putString("GROUP_ID", Group_id);
        bundle.putString("TITTLE",GroupTittle);
        bundle.putString("GROUP_DETAILS",groupdetails);
        bundle.putString("GROUP_EMAIL",groupemail);
        bundle.putString("LAT", String.valueOf(Lat));
        bundle.putString("LNG", String.valueOf(Long));



        GroupInformationDetails info = new GroupInformationDetails();
        info.setArguments(bundle);

        ClubMembersGroups group = new ClubMembersGroups();
        group.setArguments(bundle);

        GroupMediaDetails classlist = new GroupMediaDetails();
        classlist.setArguments(bundle);


        adapter.addFragment(info, "Info");
        adapter.addFragment(group, "Group");
        adapter.addFragment(classlist, "Media");


        viewPager.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewPager.setNestedScrollingEnabled(false);
        }

    }

    public static void getCurrentPosition(int i) {
        viewPager.setCurrentItem(i);

    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    /*GROUPDETAILS*/

    private void GetAllEventDetails() {
        groupinfoAuthTask = new ProgressGroupInfo();
        groupinfoAuthTask.execute();


    }

    private class ProgressGroupInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEGROUPINFO+"/"+Group_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.showAlertFragmentDialog(GroupNSocityDetails.this, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupinfoAuthTask = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");

                        groupdetails =data.getString("body");
                        groupemail =data.getString("field_team_email");


                        JSONArray picture = data.getJSONArray("field_group_image");
                        if(picture != null && picture.length() > 0 ) {
                            for (int j = 0; j < picture.length(); j++) {
                                Log.e("group pictire", picture.getString(j));
                                groupImage =(picture.getString(0));

                            }
                        }


                        if(groupImage != null && groupImage.length() > 0 ) {
                            try {
                                Glide.with(GroupNSocityDetails.this).load(groupImage)
                                        .placeholder(R.mipmap.club).dontAnimate()
                                        .skipMemoryCache(true)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .fitCenter().into(ClubImage);




                            } catch (Exception e) {

                            }
                        }else {
                            ClubImage.setImageResource(R.mipmap.club);

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertFragmentDialog(GroupNSocityDetails.this,"Information", msg);


                    }
                }
            } catch (JSONException e) {

            }
        }

        @Override
        protected void onCancelled() {
            groupinfoAuthTask = null;



        }
    }


    @Override
    public void onBackPressed() {
        ActivityCompat.finishAfterTransition(this);
        Intent i = new Intent(GroupNSocityDetails.this, GroupNSocieties.class);
        finish();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                ActivityCompat.finishAfterTransition(this);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }



}