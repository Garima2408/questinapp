package co.questin.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import co.questin.R;
import co.questin.activities.SignIn;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.models.UserDetail;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.fcm.MessagingService.All_App_Notification;
import static co.questin.fcm.MessagingService.college_Allfeeds;
import static co.questin.fcm.MessagingService.college_assignment;
import static co.questin.fcm.MessagingService.college_class;
import static co.questin.fcm.MessagingService.college_classroom;
import static co.questin.fcm.MessagingService.college_exam;
import static co.questin.fcm.MessagingService.college_feeds;
import static co.questin.fcm.MessagingService.college_news;
import static co.questin.fcm.MessagingService.counter;
import static co.questin.fcm.MessagingService.singlechat;


public class BaseAppCompactActivity extends BaseActivity {
    RelativeLayout loading_view;
    private ProgressDialog progressDialog;
    // Progress Dialog
    private static ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    public QuestinSQLiteHelper questinSQLiteHelper;
    public DeleteFireBaseToken deleteFireBaseToken = null;
    public UpdateFirbaseID updateFirbaseID = null;
    public ProgressLogout logouttAuthTask = null;
    SessionManager sharedPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);
        ViewGroup contentView = findViewById(R.id.content_view);
        contentView.addView(getLayoutInflater().inflate(layoutResID, null));
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        loading_view = findViewById(R.id.loading_view);
        sharedPreference = new SessionManager(this);
        progressDialog = new ProgressDialog(BaseAppCompactActivity.this, R.style.MyTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_style));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
    }

    public void showLoading() {
        if (progressDialog != null)
            progressDialog.show();
    }

    public void hideLoading() {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyBoard(View v) {
        Utils.hideKeyBoard(BaseAppCompactActivity.this, v);
    }

    public Boolean isInternetConnected() {
        return Utils.isInternetConnected(BaseAppCompactActivity.this);
    }

    public void showAlertDialog(String message) {
        showAlertDialog(null, message);
    }




    public void showAlertDialog(String title, String message) {
        Utils.showAlertDialog(BaseAppCompactActivity.this, title, message);
    }

   /* public void showToast(String message) {
        Utils.showDataToast(message, BaseAppCompactActivity.this);
    }*/

    public UserDetail getUser() {
        return SessionManager.getInstance(getActivity()).getUser();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    public void showSnackbarUnautorized(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(this.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                        startActivity(new Intent(BaseAppCompactActivity.this, SignIn.class));
                    }
                });
        alertDialog.show();
    }



    public  void showAlertDialogFinish(Context context,String title) {
        try{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage(title);
            alertDialog.setPositiveButton("Proceed",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            alertDialog.show();

        }catch (Exception e){

        }


    }







    /*
     *   Show Snackbar
     */
    public void showSnackbarMessage(String message) {

        final Snackbar snackBar = Snackbar.make(loading_view, message, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();

            }
        });
        snackBar.show();
    }



    // method for encode bitmap image to string
    public static String encodeImageTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        //  Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    // method for decode String to bitmap image
    public Bitmap decodeImageBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }


    /*UPDATE FCM TOKEN*/

    public void UpdateFCMTokenToServer() {
        updateFirbaseID=new UpdateFirbaseID();
        updateFirbaseID.execute();

    }


    public class UpdateFirbaseID extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type", "android")
                    .add("token", FirebaseInstanceId.getInstance().getToken())
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.REG_TOKEN,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                BaseAppCompactActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(BaseAppCompactActivity.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            updateFirbaseID = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.init(BaseAppCompactActivity.this);
                        Utils.saveFCMId(BaseAppCompactActivity.this,FirebaseInstanceId.getInstance().getToken());
                        Log.e("FCMtoken",Utils.getFCMId());

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(BaseAppCompactActivity.this,"Error Info",msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            updateFirbaseID = null;
            hideLoading();

        }
    }


    /*DELETE FCM TOKEN*/


    public class DeleteFireBaseToken extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }
        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {


                String responseData = ApiCall.DELETE_HEADER(client, URLS.REG_TOKEN+"/"+ Utils.getFCMId());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                BaseAppCompactActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            deleteFireBaseToken = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        logouttAuthTask = new ProgressLogout();
                        logouttAuthTask.execute();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            deleteFireBaseToken = null;
            hideLoading();


        }
    }





    /*LOGOUT*/

    public class ProgressLogout extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LOGOUT,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                BaseAppCompactActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(BaseAppCompactActivity.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            logouttAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.clearData();


                        try{
                            //user chat delete
                            Utils.deleteUserChatAndTable(BaseAppCompactActivity.this);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        //use for clear snappy db
                        DB snappyDB = null;
                        try {
                            snappyDB = DBFactory.open(BaseAppCompactActivity.this);
                            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {

                                snappyDB.destroy();

                            }
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }
                        sharedPreference.removeAllAppnotification(BaseAppCompactActivity.this);
                        sharedPreference.removenotification(BaseAppCompactActivity.this);
                        sharedPreference.removeCampusnotification(BaseAppCompactActivity.this);
                        SessionManager.getInstance(getApplicationContext()).deleteUser();
                        Utils.getSharedPreference(BaseAppCompactActivity.this).edit()
                                .putInt(Constants.USER_ROLE, Constants.ROLE_NULL).apply();
                        Utils.removeStringPreferences(BaseAppCompactActivity.this,"0");
                        counter = 0;
                        college_news = 0;
                        singlechat = 0;
                        college_class = 0;
                        college_exam = 0;
                        college_assignment = 0;
                        college_feeds = 0;
                        college_Allfeeds = 0;
                        college_classroom = 0;
                        All_App_Notification = 0;


                        Intent upanel = new Intent(BaseAppCompactActivity.this,SignIn.class);
                        upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(upanel);
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(BaseAppCompactActivity.this,"Error Info",msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            logouttAuthTask = null;
            hideLoading();

        }
    }
}






















