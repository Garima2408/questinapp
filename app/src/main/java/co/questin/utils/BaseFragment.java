package co.questin.utils;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import co.questin.activities.SignIn;
import co.questin.models.UserDetail;


public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void showLoading() {
        ((BaseAppCompactActivity) getActivity()).showLoading();
    }

   /* public void showToast(String message) {
        ((BaseAppCompactActivity) getActivity()).showToast(message);
    }*/

    public void hideLoading() {
        ((BaseAppCompactActivity) getActivity()).hideLoading();
    }

    public void hideKeyBoard(View v) {
        ((BaseAppCompactActivity) getActivity()).hideKeyBoard(v);
    }

    public Boolean isInternetConnected() {
        return ((BaseAppCompactActivity) getActivity()).isInternetConnected();
    }

    public void showAlertDialog(String message) {
        showAlertDialog(null, message);
    }

    public void showAlertDialog(String title, String message) {
        ((BaseAppCompactActivity) getActivity()).showAlertDialog(title, message);
    }

    public UserDetail getUser() {
        return ((BaseAppCompactActivity) getActivity()).getUser();
    }



    public void showSnackbarUnautorized(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(this.getResources().getString(co.questin.R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        getActivity().finish();
                        startActivity(new Intent(getContext(), SignIn.class));
                    }
                });
        alertDialog.show();
    }





    public void showSnackbarMessage(String message) {

        final Snackbar snackBar = Snackbar.make(getView(),message, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();

            }
        });
        snackBar.show();
    }

   // public void Dialog onCreateDialog(int id);
}
