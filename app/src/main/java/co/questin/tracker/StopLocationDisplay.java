package co.questin.tracker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.questin.R;
import co.questin.models.tracker.WayPointsArray;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;

public class StopLocationDisplay extends BaseAppCompactActivity  implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener {
    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;
    ArrayList<WayPointsArray> AllWayPoints;
    private static HashMap<Marker, WayPointsArray> mMarkersHashMap;
    static Activity activity;

    //Our Map
    private GoogleMap mMap;
    Marker currentMarker;
    //To store longitude and latitude from map
    private double longitude;
    private double latitude;

    //Google ApiClient
    private GoogleApiClient googleApiClient;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                // Logger.d(String.format("%f, %f", location.getLatitude(), location.getLongitude()));
                drawMarker();


                mLocationManager.removeUpdates(mLocationListener);
            } else {
                // Logger.d("Location is null");
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    private LocationManager mLocationManager;

    String Stopname,Stoplogo,Stoppick,Stopdrop,StopLatCompair,StopLongCompair;
    Double Stoplat,Stoplong,SourceStartLat,SourceStartLng,DestinationLat,DestinationLng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_location_display);
        activity = getActivity();
        Bundle b = getActivity().getIntent().getExtras();
        Stopname = b.getString("ROUTE_NAME");
        Stoplogo = b.getString("ROUTE_LOGO");
        Stoppick = b.getString("ROUTE_PICK");
        Stopdrop = b.getString("ROUTE_DROP");
        Stoplat = Double.valueOf(b.getString("ROUTE_LAT"));
        Stoplong = Double.valueOf(b.getString("ROUTE_LONG"));
        StopLatCompair =b.getString("ROUTE_LAT");
        StopLongCompair =b.getString("ROUTE_LONG");

        SourceStartLat = Double.valueOf(b.getString("START_LAT"));
        SourceStartLng = Double.valueOf(b.getString("START_LNG"));
        DestinationLat = Double.valueOf(b.getString("STOP_LAT"));
        DestinationLng = Double.valueOf(b.getString("STOP_LNG"));

        mMarkersHashMap=new HashMap<>();


        AllWayPoints = (ArrayList<WayPointsArray>)  b.getSerializable("AllPoints");


        Log.d("TAG", "questions: " + AllWayPoints.toString());


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        initMap();
        getCurrentLocation();
        //  DrawRoute(AllWayPoints);

    }

    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    private void initMap() {
        int googlePlayStatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (googlePlayStatus != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(googlePlayStatus, this, -1).show();
            finish();
        } else {
            if (mMap != null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
            }
        }
    }





    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
      /*  LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));*/
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        getCurrentLocation();
        drawMarker();



    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
      /*  //Clearing all the markers
        mMap.clear();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));*/
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map



    }

    @Override
    public void onClick(View v) {

    }





    private void getCurrentLocation() {
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Location location = null;
        if (!(isGPSEnabled || isNetworkEnabled)){

        }
        /*  Snackbar.make(mMapView, R.string.error_location_provider, Snackbar.LENGTH_INDEFINITE).show();*/
        else{
            if (isNetworkEnabled) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (isGPSEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
        if (location != null) {


            /* Stoplat = b.getString("ROUTE_LAT");
        Stoplong = b.getString("ROUTE_LOGO");*/





        }
    }

  /*  private void DrawRoute(ArrayList<WayPointsArray> allWayPoints) {

        for (int i = 0; i < allWayPoints.size(); i++) {
            if (i == allWayPoints.size() - 1) {
                String url = getDirectionsUrl(allWayPoints.get(i), destStop);
                DownloadTask downloadTask = new DownloadTask();
                downloadTask.execute(url);

                addStopMarker(mStopsList.get(i));


                continue;
            }
            if (i == 0) {
                String url = getDirectionsUrl(sourceStop, mStopsList.get(0));
                DownloadTask downloadTask = new DownloadTask();
                downloadTask.execute(url);

                addStopMarker(mStopsList.get(0));

            }
            String url = getDirectionsUrl(mStopsList.get(i), mStopsList.get(i + 1));
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);

            addStopMarker(mStopsList.get(i + 1));
        }
        String ltLngSrc = String.valueOf(sourceStop.getLat()) + sourceStop.getLng();
        String ltLngDes = String.valueOf(destStop.getLat()) + destStop.getLng();


        // condition here for geofencing
        if (ltLngSrc.equals(pickLocation)) {
            mGoogleMap.addMarker(new MarkerOptions()
                    .title(sourceStop.getPlanceName())
                    .snippet("Your pick")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pick_location))
                    .position(new LatLng(sourceStop.getLat(), sourceStop.getLng())));


            wayPointsPick.setLat("" + sourceStop.getLat());
            wayPointsPick.setLng("" + sourceStop.getLng());
            wayPointsPick.setField_place_name(sourceStop.getPlanceName());
            wayPointsDrop.setPick(true);

        } else {
            mGoogleMap.addMarker(new MarkerOptions()
                    .title(sourceStop.getPlanceName())
                    //  .snippet("Your pick")
                    .position(new LatLng(sourceStop.getLat(), sourceStop.getLng())));
        }


    //etaDatabase.child(myStop).addValueEventListener(etaEventListener);




    }*/






    private void drawMarker() {
        if (mMap != null) {
            mMap.clear();

            LatLng destStop = new LatLng(DestinationLat, DestinationLng);
            LatLng sourceStop = new LatLng(SourceStartLat, SourceStartLng);


            for (int i = 0; i < AllWayPoints.size(); i++) {

                 /* Double Lat1 = Double.valueOf(AllWayPoints.get(0).getLat());
                  Double Long1 = Double.valueOf(AllWayPoints.get(0).getLng());
                  Double Lat2 = Double.valueOf(AllWayPoints.get(1).getLat());
                  Double Long2 = Double.valueOf(AllWayPoints.get(1).getLng());
                  LatLng latLng1 = new LatLng(Lat1, Long1);
                  LatLng latLng2 = new LatLng(Lat2, Long2);
                  // Start downloading json data from Google Directions API

                  String url = getDirectionsUrl(latLng1, latLng2);

                  DownloadTask downloadTask = new DownloadTask();
                  downloadTask.execute(url);*/



                if (i == AllWayPoints.size() - 1) {
                    LatLng Last = new LatLng(Double.valueOf(AllWayPoints.get(i).getLat()), Double.valueOf( AllWayPoints.get(i).getLng()));

                    String url = getDirectionsUrl(Last, destStop);
                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url);




                    continue;
                }
                if (i == 0) {
                    LatLng Last = new LatLng(Double.valueOf(AllWayPoints.get(0).getLat()), Double.valueOf( AllWayPoints.get(0).getLng()));



                    String url = getDirectionsUrl(sourceStop, Last);
                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url);



                }

                LatLng Last = new LatLng(Double.valueOf(AllWayPoints.get(i).getLat()), Double.valueOf( AllWayPoints.get(i).getLng()));
                LatLng Last2 = new LatLng(Double.valueOf(AllWayPoints.get(i +1 ).getLat()), Double.valueOf( AllWayPoints.get(i +1).getLng()));

                String url = getDirectionsUrl(Last, Last2);
                DownloadTask downloadTask = new DownloadTask();
                downloadTask.execute(url);


            }







            if(AllWayPoints != null && AllWayPoints.size() > 0) {

                for (int i = 0; i < AllWayPoints.size(); i++) {


                    Double Lat = Double.valueOf(AllWayPoints.get(i).getLat());

                    Double Long = Double.valueOf(AllWayPoints.get(i).getLng());

                    LatLng latLng = new LatLng(Lat, Long);




                    if(StopLatCompair.contains(AllWayPoints.get(i).getLat()) && StopLongCompair.contains(AllWayPoints.get(i).getLng())){


                        MarkerOptions markerOption = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .position(latLng) //setting position
                                .draggable(true) //Making the marker draggable
                                .title(AllWayPoints.get(i).getField_place_name());

                        currentMarker = mMap.addMarker(markerOption);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));




                      /*  mMap.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .position(latLng) //setting position
                                .draggable(true) //Making the marker draggable
                                .title(AllWayPoints.get(i).getField_place_name())); //Adding a title
                        //Moving the camera
                      */


                    }else {


                        MarkerOptions markerOption = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                .position(latLng) //setting position
                                .draggable(true) //Making the marker draggable
                                .title(AllWayPoints.get(i).getField_place_name());

                        currentMarker = mMap.addMarker(markerOption);


                        /*mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
*/

                        //Adding a title
                        //Moving the camera
                        // mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                        //Animating the camera
                        // mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                        //  mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                    }

                    mMarkersHashMap.put(currentMarker,AllWayPoints.get(i));
                    mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());

                }


            }





           /* Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            latitude = Stoplat;
            longitude = Stoplong;

            Log.e("latitude", "latitude--" + latitude);

            try {
                Log.e("latitude", "inside latitude--" + longitude);
                addresses = geocoder.getFromLocation(latitude, longitude, 1);


                if (addresses != null && addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    Log.d("TAG", "completeAddress: " + address + "..." + city + "..." + state + "..." + country + "..." + postalCode + "..." + knownName);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
*/

          /*  mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = null;
                    try {

                        // Getting view from the layout file info_window_layout
                        v = getLayoutInflater().inflate(R.layout.googlemapinfo_window, null);

                        // Getting reference to the TextView to set latitude
                        TextView tittle = (TextView) v.findViewById(R.id.tittle);
                        TextView pick = (TextView) v.findViewById(R.id.Arivaltime);
                        TextView drop = (TextView) v.findViewById(R.id.DepatureTime);
                        ImageView location = (ImageView) v.findViewById(R.id.pinlocation);

                        tittle.setText(Stopname);
                        pick.setText(Stoppick);
                        drop.setText(Stopdrop);

                        if(Stoplogo != null && Stoplogo.length() > 0 ) {
                            Glide.with(StopLocationDisplay.this).load(Stoplogo)
                                    .placeholder(R.mipmap.stoplocation).dontAnimate()
                                    .fitCenter().into(location);

                        }else {
                            location.setImageResource(R.mipmap.stoplocation);
                        }

                    } catch (Exception ev) {
                        System.out.print(ev.getMessage());
                    }

                    return v;
                }
            });*/
        }

    }



    public static class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        public MarkerInfoWindowAdapter() {
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v =activity.getLayoutInflater().inflate(R.layout.googlemapinfo_window, null);



            WayPointsArray myMarker = mMarkersHashMap.get(marker);

            TextView tittle = v.findViewById(R.id.tittle);
            TextView pick = v.findViewById(R.id.Arivaltime);

            TextView drop = v.findViewById(R.id.DepatureTime);
            ImageView location = v.findViewById(R.id.pinlocation);
            // location.setImageResource(R.mipmap.stoplocation);
            if (myMarker!=null){
                tittle.setText(myMarker.getField_place_name());
                pick.setText(myMarker.getField_start_time());
                drop.setText(myMarker.getField_departure_time());
            }

            return v;
        }
    }



    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+"&key="+ Constants.MAPS_API_KEY;

        return url;
    }
    /** A method to download json data from url */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.BLUE);
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }






    @Override
    public void onBackPressed() {

        finish();
    }




}
