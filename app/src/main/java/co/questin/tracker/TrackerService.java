package co.questin.tracker;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import co.questin.R;
import co.questin.activities.SpalashScreen;
import co.questin.models.chat.SendMessageModel;
import co.questin.models.tracker.LocationModel;
import co.questin.models.tracker.StopsModel;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;



public class TrackerService extends Service {
    private SendMessageModel sendMessageModel;
    private StopsModel sourceStop;
    private StopsModel destStop;
    private ArrayList<String> stopsList;
    private HashMap<String, Integer> etaMap;
    private HashMap<String, StopsModel> stopsMap;
    private Gson gson;
    private Context mContext;
    String AdminId, routeName;
    private OkHttpClient okHttpClient;
    Handler delayhandler = new Handler();
    private LocationModel currentLocation;
    private DatabaseReference locationDatabase;
    private DatabaseReference locationstartDatabase;
    private static final String PACKAGE_NAME = "co.questin.tracker";
    private Timer timer;
    private TimerTask task;
    private static final String TAG = "TrackerActivity";
    private boolean isAlreadyRunning = false;
    /**
     * The name of the channel for notifications.
     */
    private static final String CHANNEL_ID = "channel_01";

    public static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

    public static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

    private final IBinder mBinder = new LocalBinder();
    private static final long MIN_TIME_BW_UPDATES = 1000 * 3; // 2 sec
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 1;

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 12345678;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;

    /**
     * The current location.
     */
    private Location mLocation;

    public TrackerService() {
    }

    @Override
    public void onCreate() {

        gson = new Gson();
        okHttpClient = new OkHttpClient();
        currentLocation = new LocationModel();
        stopsMap = new HashMap<>();
        etaMap = new HashMap<>();
        stopsList = new ArrayList<>();
        sendMessageModel = new SendMessageModel();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };
        createLocationRequest();

        final Handler handler = new Handler();
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            //UpdateNewLocation();
                            getLastLocation();
                        } catch (Exception e) {
                        }
                    }
                });
            }
        };


        timer.schedule(doAsynchronousTask, 0, MIN_TIME_BW_UPDATES); //execute in every 5 minute


        getLastLocation();

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }
    }



    private void UpdateNewLocation() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };





    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service started");
        boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
                false);



        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates();
            stopSelf();
        }
        // Tells the system to not try to recreate the service after it has been killed.
        return START_NOT_STICKY;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mChangingConfiguration = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()");
        stopForeground(true);
        mChangingConfiguration = false;
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()");
        stopForeground(true);
        mChangingConfiguration = false;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Last client unbound from service");

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && Utils.requestingLocationUpdates(this)) {
            Log.i(TAG, "Starting foreground service");
            /*
            // TODO(developer). If targeting O, use the following code.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                mNotificationManager.startServiceInForeground(new Intent(this,
                        LocationUpdatesService.class), NOTIFICATION_ID, getNotification());
            } else {
                startForeground(NOTIFICATION_ID, getNotification());
            }
             */
            startForeground(NOTIFICATION_ID, getNotification());
        }
        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        mServiceHandler.removeCallbacksAndMessages(null);
        if (mFusedLocationClient != null) {

            try {
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);


            } catch (IllegalArgumentException e) {
                // Check wether we are in debug mode

                e.printStackTrace();

            }
        }

        delayhandler.removeCallbacks(mUpdateTimeTask);
        isAlreadyRunning = false;
        timer.cancel();

    }


    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates");
        Utils.setRequestingLocationUpdates(this, true);
        startService(new Intent(getApplicationContext(), TrackerService.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, false);
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Log.i(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            Utils.setRequestingLocationUpdates(this, false);
            locationstartDatabase.setValue("false");
            stopSelf();
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, true);
            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, SpalashScreen.class);

        CharSequence text = Utils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SpalashScreen.class), 0);



        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                /* .addAction(R.mipmap.tick, getString(R.string.launch_activity),
                         activityPendingIntent)
                 .addAction(R.mipmap.cross, getString(R.string.remove_location_updates),
                         servicePendingIntent)*/
                // .setContentIntent(activityPendingIntent)
                .setContentText(text)
                .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.appicon)
                .setTicker(text)
                .setOnlyAlertOnce(true)
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLocation = task.getResult();
                                Log.w(TAG, "LocationWork" +mLocation);

                              /*  currentLocation.updateModel(mLocation);
                                locationDatabase.setValue(new LocationModel(mLocation));
                                LocalBroadcastManager.getInstance(mContext)
                                        .sendBroadcast(new Intent(Constants.ACTION_LOCATION_UPDATE)
                                                .putExtra(Constants.EXTRA_LOCATION, mLocation));

                                delayhandler.removeCallbacks(mUpdateTimeTask);


*/
                                delayhandler.removeCallbacks(mUpdateTimeTask);
                            } else {
                                Log.w(TAG, "Failed to get location.");
                                mUpdateTimeTask.run();
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }
    }





    private void onNewLocation(Location location) {
        Log.i(TAG, "New location: " + location);

        mLocation = location;
        currentLocation.updateModel(mLocation);
        locationDatabase.setValue(new LocationModel(mLocation));
        locationstartDatabase.setValue("true");
        // Notify anyone listening for broadcasts about the new location.
        Intent intent = new Intent(ACTION_BROADCAST);
        intent.putExtra(EXTRA_LOCATION, location);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        // Update notification content if running as a foreground service.
        if (serviceIsRunningInForeground(this)) {
            mNotificationManager.notify(NOTIFICATION_ID, getNotification());
        }
    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void requestLocationUpdatesOnId(String collageId, String routeTittle, String adminId, String route_id,String StartValue) {
        gson = new Gson();
        okHttpClient = new OkHttpClient();
        currentLocation = new LocationModel();
        stopsMap = new HashMap<>();
        etaMap = new HashMap<>();
        stopsList = new ArrayList<>();
        sendMessageModel = new SendMessageModel();


        locationDatabase = FirebaseDatabase.getInstance().getReference()
                .child(collageId +"/routes/"+ route_id + "/routes_name/" + routeTittle + "/location");

        locationstartDatabase = FirebaseDatabase.getInstance().getReference()
                .child(collageId +"/routes/"+ route_id + "/routes_name/" + routeTittle + "/TrackingStarted");



        AdminId=adminId;
        routeName =routeTittle;


    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public TrackerService getService() {
            return TrackerService.this;
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {   // Todo
            long NOTIFY_INTERVAL = 299;
            long   millis =System.currentTimeMillis();
            int seconds = (int) (millis / 1000);
            seconds = seconds % 300;




            Log.e(TAG, "Time:"+seconds);


            // This line is necessary for the next call
            delayhandler.postDelayed(this, NOTIFY_INTERVAL);

            if (seconds==NOTIFY_INTERVAL) {

                // Toast.makeText(TrackerService.this, "Fail Current Location Response", Toast.LENGTH_SHORT).show();


                NotifyToTheAdmin("Unable to find location of bus for route"+" " + routeName +" at" );

            }else {

            }

        }

    };


    private void NotifyToTheAdmin(String s) {
        Calendar  mcalender = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        String  formattedDate2 = sdf2.format(mcalender.getTime());


        sendMessage(sendMessageModel
                .setUids(AdminId)
                .setMessage(s+" "+ formattedDate2 +" " +" driven by"+" "+ SessionManager.getInstance(this).getUser().getUsername()));


        delayhandler.removeCallbacks(mUpdateTimeTask);

    }


    private void sendMessage(final SendMessageModel sendMessageModel){
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = RequestBody.create(Constants.JSON,
                        gson.toJson(sendMessageModel, SendMessageModel.class));
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.SEND_MSG, requestBody);



                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }



}








