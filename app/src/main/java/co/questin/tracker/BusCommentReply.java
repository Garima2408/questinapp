package co.questin.tracker;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.activities.UserDisplayProfile;
import co.questin.adapters.BusCommentReplyAdapter;
import co.questin.buttonanimation.LikeButton;
import co.questin.buttonanimation.OnAnimationEndListener;
import co.questin.buttonanimation.OnLikeListener;
import co.questin.models.ReplyOnCommentArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class BusCommentReply  extends BaseAppCompactActivity {
    String parent_id, ROUTE_id, parentname, Pimage, Pdate, parentcomment, comments, TransferId, noOfComments, noOfLikes, IsLiked, Content_url;
    static String PARENTID;
    static String Route_id;
    ImageView circleView, comment, options, inaprooptions, imagePost, imagepreview, share;
    private TextView blog_content, publisher_name, dateTime, commentNo, likeNo, tv_comment;
    static RecyclerView rv_reply_comment;
    public static ArrayList<ReplyOnCommentArray> commentsList;
    static BusCommentReplyAdapter mcommentAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static SubjectsCommentsList subjectscommentsList = null;
    //  private static SubjectsCommentsListRefresh subjectscommentsListRefresh = null;
    EditText editText_Commment;
    ImageButton Send;
    Bundle extras;
    static TextView ErrorText;
    static Activity activity;
    RelativeLayout previewlayout;
    private DeleteSubjectComments deletesubjectcomments = null;
    private InappropriateFeedComments inappropriatefeedcomments = null;
    private LikeFeedComments likefeedcomments = null;
    static String tittle, URLDoc;
    String BackScreen;
    private static boolean isLoading= true;
    private static int PAGE_SIZE = 0;
    String Extension;
    LikeButton like;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_comment_reply);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        activity = getActivity();
        Intent intent = getIntent();
        extras = intent.getExtras();

        commentsList = new ArrayList<ReplyOnCommentArray>();

        ROUTE_id = intent.getStringExtra("ROUTE_ID");
        parent_id = intent.getStringExtra("PARENT_ID");
        Route_id = intent.getStringExtra("ROUTE_ID");
        PARENTID = intent.getStringExtra("PARENT_ID");
        parentname = intent.getStringExtra("NAME");
        Pimage = intent.getStringExtra("IMAGE_URL");
        Pdate = intent.getStringExtra("DATE");
        parentcomment = intent.getStringExtra("BLOG_CONTENT");
        TransferId = intent.getStringExtra("USERSELECTED_ID");
        noOfComments = intent.getStringExtra("NOCOMMENT");
        noOfLikes = intent.getStringExtra("NOLIKED");
        IsLiked = intent.getStringExtra("IS_LIKED");
        Content_url = intent.getStringExtra("POSTCONENT");

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromTeacherAdapter")) {
                BackScreen = "CameFromTeacherAdapter";


            } else if (extras.getString("open").equals("CameFromStudentAdapter")) {
                BackScreen = "CameFromStudentAdapter";


            }else if(extras.getString("open").equals("CameFromParentAdapter")){
                BackScreen="CameFromParentAdapter";
            }
        }



        /*CameFromTeacherAdapter*/

       /* editText_Commment = findViewById(R.id.editText_Commment);
        Send =  findViewById(R.id.Send);*/
        blog_content = findViewById(R.id.blog_content);
        tv_comment = findViewById(R.id.tv_comment);
        dateTime = findViewById(R.id.dateTime);
        publisher_name = findViewById(R.id.publisher_name);
        circleView = findViewById(R.id.circleView);
        rv_reply_comment = findViewById(R.id.rv_reply_comment);
        layoutManager = new LinearLayoutManager(this);
        rv_reply_comment.setHasFixedSize(true);
        rv_reply_comment.setLayoutManager(layoutManager);
        ErrorText = findViewById(R.id.ErrorText);
        options = findViewById(R.id.options);
        like = findViewById(R.id.like);
        share = findViewById(R.id.share);
        comment = findViewById(R.id.comment);
        commentNo = findViewById(R.id.commentNo);
        likeNo = findViewById(R.id.likeNo);
        imagePost = findViewById(R.id.imagePost);
        imagepreview = findViewById(R.id.imagepreview);
        previewlayout = findViewById(R.id.previewlayout);
        inaprooptions = findViewById(R.id.inaprooptions);
        blog_content.setText(Html.fromHtml(parentcomment));
        publisher_name.setText(parentname);
        dateTime.setText(Pdate);
        commentNo.setText(noOfComments);
        likeNo.setText(noOfLikes);

        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
            tv_comment.setVisibility(View.VISIBLE);
        }


        mcommentAdapter = new BusCommentReplyAdapter(rv_reply_comment, commentsList ,activity);
        rv_reply_comment.setAdapter(mcommentAdapter);
        rv_reply_comment.setNestedScrollingEnabled(false);

        if (Pimage != null) {

            Glide.with(this).load(Pimage)
                    .placeholder(R.mipmap.place_holder).dontAnimate()
                    .fitCenter().into(circleView);

        } else {
            circleView.setImageResource(R.mipmap.place_holder);

        }



        /*CALLING LISTS OF REPLY COMMENTS*/

        subjectscommentsList = new SubjectsCommentsList();
        subjectscommentsList.execute(ROUTE_id);
        inItView();

        if (Content_url != null) {

            imagepreview.setVisibility(View.INVISIBLE);

            imagePost.setVisibility(View.VISIBLE);
            previewlayout.setVisibility(View.VISIBLE);
            Glide.clear(imagePost);
            Glide.with(this).load(Content_url)
                    .placeholder(R.mipmap.header_image).dontAnimate()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .fitCenter().into(imagePost);


        } else {
            previewlayout.setVisibility(View.GONE);


        }

        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
            if(TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getParentUid())){
                options.setVisibility(View.VISIBLE);
                inaprooptions.setVisibility(View.INVISIBLE);
            }else {
                options.setVisibility(View.INVISIBLE);
                inaprooptions.setVisibility(View.VISIBLE);
            }


        }else {

            if(TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())){
                options.setVisibility(View.VISIBLE);
                inaprooptions.setVisibility(View.INVISIBLE);
            }else {
                options.setVisibility(View.INVISIBLE);
                inaprooptions.setVisibility(View.VISIBLE);
            }
        }



     /*   if (TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())) {
            options.setVisibility(View.VISIBLE);
            inaprooptions.setVisibility(View.INVISIBLE);
        } else {
            options.setVisibility(View.INVISIBLE);
            inaprooptions.setVisibility(View.VISIBLE);
        }
*/

        if(IsLiked.contains("False")){
            like.setLiked(false);
        }else if(IsLiked.contains("True"))  {
            like.setLiked(true);
        }


        like.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);

                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();

                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(parent_id,"flag");
                        IsLiked ="True";
                        noOfLikes =NewLikeNo;
                        //lists.get(position).setIs_liked("True");

                        /* lists.get(position).setLikes(NewLikeNo);
                         */
                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(parent_id,"unflag");
                        IsLiked ="False";
                        noOfLikes =NewLikeNo;
                       /* lists.get(position).setIs_liked("False");
                        lists.get(position).setLikes(NewLikeNo);*/
                    }
                }
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);

                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();

                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(parent_id,"flag");
                        IsLiked ="True";
                        noOfLikes =NewLikeNo;
                        //lists.get(position).setIs_liked("True");

                        /* lists.get(position).setLikes(NewLikeNo);
                         */
                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(parent_id,"unflag");
                        IsLiked ="False";
                        noOfLikes =NewLikeNo;
                       /* lists.get(position).setIs_liked("False");
                        lists.get(position).setLikes(NewLikeNo);*/
                    }
                }
            }
        });


        like.setOnAnimationEndListener(new OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(LikeButton likeButton) {

            }
        });


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareBody = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");

                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody + parentcomment);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));


            }
        });


        publisher_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(BusCommentReply.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID", TransferId);
                startActivity(backIntent);


            }
        });


        circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(BusCommentReply.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID", TransferId);
                startActivity(backIntent);


            }
        });


        tv_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent backIntent = new Intent(BusCommentReply.this, BusAddReplyComment.class)
                        .putExtra("COURSE_ID", ROUTE_id)
                        .putExtra("PARENT_ID", parent_id)
                        .putExtra("open", "CameFromStudentCommentreply");
                startActivity(backIntent);


            }
        });


        inaprooptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.app.AlertDialog.THEME_HOLO_DARK)
                        .setTitle("Inappropriate")
                        .setMessage(R.string.Inappropriate)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                /*  DELETE COMMENTS ON POST*/
                                inappropriatefeedcomments = new InappropriateFeedComments();
                                inappropriatefeedcomments.execute(PARENTID);


                            }
                        })
                        .setNegativeButton("No", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();


            }
        });

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopupMenu();


            }

            private void showPopupMenu() {


                PopupMenu popup = new PopupMenu(getActivity(), options);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Editoption:


                                Intent backIntent = new Intent(BusCommentReply.this, BusUpdateComment.class)
                                        .putExtra("PARENT_ID", parent_id)
                                        .putExtra("NAME", parentname)
                                        .putExtra("BLOG_CONTENT", parentcomment)
                                        .putExtra("DATE", Pdate)
                                        .putExtra("IMAGE_URL", Pimage)
                                        .putExtra("USERSELECTED_ID", TransferId)
                                        .putExtra("POSTCONENT", Content_url)
                                        .putExtra("open", "BuaCommentReply");
                                startActivity(backIntent);
                                finish();

                                return true;
                            case R.id.deleteoption:

                                AlertDialog.Builder builder = new AlertDialog.Builder(BusCommentReply.this, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Delete My Feed")
                                        .setMessage(R.string.delete)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                /*  DELETE COMMENTS ON POST*/
                                                deletesubjectcomments = new DeleteSubjectComments();
                                                deletesubjectcomments.execute(PARENTID);

                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();


                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();

            }
        });


    }

    public static void RefreshWorked() {
        PAGE_SIZE=0;
        commentsList.clear();
        /* CALLING LISTS OF COMMENTS*/
        subjectscommentsList = new SubjectsCommentsList();
        subjectscommentsList.execute(Route_id);


    }


    private void inItView(){




        rv_reply_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();



                    subjectscommentsList = new SubjectsCommentsList();
                    subjectscommentsList.execute(Route_id);
                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }








    /*LISTS OF REPLY COMMENTS ON SUBJECTS*/

    private static class SubjectsCommentsList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEREPLYCOMMENTS +"?&rid=" + args[0]+"&pid=" + PARENTID+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

                mcommentAdapter.removeProgress();
                isLoading = false;


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            subjectscommentsList = null;
            try {
                if (responce != null) {
                    rv_reply_comment.setVisibility(View.VISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        ArrayList<ReplyOnCommentArray> arrayList = new ArrayList<>();

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                ReplyOnCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ReplyOnCommentArray.class);
                                arrayList.add(subjects);

                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                if (jsonObject.has("field_comment_attachments")) {
                                    JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                    if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0) {
                                        URLDoc = AttacmentsFeilds.getString("value");
                                        tittle = AttacmentsFeilds.getString("title");
                                        subjects.title = tittle;
                                        subjects.url = URLDoc;

                                    } else {

                                    }
                                }else {

                                }

                                commentsList.add(subjects);
                                mcommentAdapter.notifyDataSetChanged();
                                rv_reply_comment.setVisibility(View.VISIBLE);
                                mcommentAdapter.setInitialData(commentsList);
                                ErrorText.setVisibility(View.GONE);



                            }

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_reply_comment.setVisibility(View.VISIBLE);
                                mcommentAdapter.setInitialData(arrayList);

                            } else {
                                isLoading = false;
                                mcommentAdapter.removeProgress();

                                mcommentAdapter.addData(arrayList);


                            }
                        } else{
                            if (PAGE_SIZE==0){
                                rv_reply_comment.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                            }else
                                mcommentAdapter.removeProgress();

                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                } else {
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {


            }
        }

        @Override
        protected void onCancelled() {
            subjectscommentsList = null;


        }
    }









    private class DeleteSubjectComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETESUBJECTCOMMENTS + "/" + args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletesubjectcomments = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        /* showAlertDialog(msg);*/
                        Check();
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);


                    }
                } else {
                    hideLoading();
                    showAlertDialog(getString(R.string.error_responce));


                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            deletesubjectcomments = null;
            hideLoading();
            showAlertDialog(getString(R.string.error_responce));


        }
    }
    /*MARKED IN APPROPRIATE ON COMMENTS */


    private class InappropriateFeedComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name", "comments_inappropriate")
                    .add("entity_id", args[0])
                    .add("action", "flag")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MARKEDINAPPROPRIATECOMMENT, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            inappropriatefeedcomments = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        hideLoading();
                        /*  showAlertDialog(msg);*/
                        Check();
                        finish();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                } else {
                    hideLoading();
                    showAlertDialog(getString(R.string.error_responce));


                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            inappropriatefeedcomments = null;
            hideLoading();
            showAlertDialog(getString(R.string.error_responce));


        }
    }

    private class LikeFeedComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","comments_like")
                    .add("entity_id",args[0])
                    .add("action",args[1])
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LIKEDCOMMENTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            }catch (JSONException | IOException e) {

                e.printStackTrace();
                showAlertDialog(getString(R.string.error_responce));


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            likefeedcomments = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);



                    }
                }else {
                    showAlertDialog(getString(R.string.error_responce));


                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            likefeedcomments = null;
            showAlertDialog(getString(R.string.error_responce));



        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // TeacherCourceGroup.RefreshWorked();
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFrombusAdapter")) {

                RouteGroup.RefreshWorkedBusRoute();


            }
        }


    }

    @Override
    public void onBackPressed() {

        PAGE_SIZE=0;
        isLoading=false;
        ActivityCompat.finishAfterTransition(this);


    }


}
