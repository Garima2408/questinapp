package co.questin.tracker;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import co.questin.driver.TrackerActivity;
import co.questin.models.chat.SendMessageGroupModel;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.Constants;
import okhttp3.RequestBody;

public class GeofenceRegistrationService extends IntentService {
    public static final String MyPREFERENCES = "MyPrefs" ;
    private static final String TAG = "GeoIntentService";
    private Gson gson;
    public GeofenceRegistrationService() {
        super(TAG);
    }
    private SendMessageGroupModel sendMessage;
    String Route_id;
    Boolean RemoveData;
    private SharedPreferences preferences;
    private String Runfirst;
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.i(TAG, "onHandleIntent");
        gson = new Gson();
        sendMessage =new SendMessageGroupModel();
        Route_id =intent.getStringExtra("ROUTE_ID");
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            //String errorMessage = GeofenceErrorMessages.getErrorString(this,
            //      geofencingEvent.getErrorCode());
            Log.e(TAG, "Goefencing Error " + geofencingEvent.getErrorCode());
            return;
        }


        if (geofencingEvent.hasError()) {
            Log.d(TAG, "GeofencingEvent error " + geofencingEvent.getErrorCode());
        } else {
            int transaction = geofencingEvent.getGeofenceTransition();
            List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
            Geofence geofence = geofences.get(0);
            if (transaction == Geofence.GEOFENCE_TRANSITION_ENTER && geofence.getRequestId().equals(Constants.GEOFENCE_ID_STAN_UNI)) {

                Notifiy();
               // Toast.makeText(this, "You enter"  , Toast.LENGTH_SHORT).show();


               Log.d(TAG, "You enter");
            } else {


               Log.d(TAG, "You are outside ");
            }
        }
    }

    private void Notifiy() {


        NotifyToTheParent("has stopped","end");






    }

    private void NotifyToTheParent(String tracking_is_started, String trigerValue) {

        Calendar mcalender = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        String formattedDate2 = sdf2.format(mcalender.getTime());



        sendMessageToAll(sendMessage
                .setgid(Route_id)
                .settrigger(trigerValue));
    }

    private void sendMessageToAll(final SendMessageGroupModel sendMessage) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = RequestBody.create(Constants.JSON,
                        gson.toJson(sendMessage, SendMessageGroupModel.class));
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.SEND_MSG_TO_ALL, requestBody);
                    Log.d(TAG, "response" +response);

                    if (response != null) {


                        TrackerActivity.stopGeoFencing();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}





