package co.questin.tracker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import co.questin.Event.PickLocationEvent;
import co.questin.R;
import co.questin.models.tracker.WayPointResponse;
import co.questin.models.tracker.WayPointsArray;
import co.questin.models.tracker.WayPointsDrawArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class RouteMap extends BaseFragment {
    private static GoogleMap googleMap;
    private MapView mMapView;
    static Polyline polyline;

    ArrayList<LatLng> markerPoints;
    private ArrayList<WayPointsArray> wayPointsArrayList;
    JSONObject  jsonObjectSource;
    private ArrayList<WayPointsDrawArray> mMyMarkersArray = new ArrayList<WayPointsDrawArray>();
    private static HashMap<Marker, WayPointsDrawArray> mMarkersHashMap;
    String busroutecode, busno, routetittle, routediscription, Route_id, RouteTittle,routeName;
    public String field_place_name, field_start_time, field_departure_time, field_way_points, lat, lng, field_photo;
    static Activity activity;
    private ProgressBar spinner;
    View view;
    private double longitude;
    private double latitude;
    private FragmentActivity myContext;
    ArrayList<WayPointsArray> arrayList;
    private static ProgressBusRouteInfo busrouteinfoAuthTask = null;
    LocationManager mlocManager;
    LocationListener locListener;
    Location location; // location
    static LatLng myposition;
    Button Showmap;
    private static double Sendlongitude;
    private static double Sendlatitude;
    private LinearLayout ll_tracking;
     private  RelativeLayout update_pick;
    private String joinRouteData;
    int pickPos,dropPos;
    public RouteMap() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_route_map, container, false);
        activity = (Activity) view.getContext();
        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle = getArguments().getString("ROUTE_NAME");
        routeName = getArguments().getString("ROUTE_NAME");
        spinner = view.findViewById(R.id.progressBar);
        ll_tracking =view.findViewById(R.id.ll_tracking);
        mMarkersHashMap = new HashMap<Marker, WayPointsDrawArray>();
        markerPoints = new ArrayList<LatLng>();
        ll_tracking=view.findViewById(R.id.ll_tracking);
        update_pick=view.findViewById(R.id.update_pick);


//        update_pick.setOnClickListener(
//                new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                WayPointResponse wayPointResponse=new WayPointResponse();
//                wayPointResponse.setWayPointsArrays(wayPointsArrayList);
//                SelectPickupDialogFragment argumentFragment = new SelectPickupDialogFragment();//Get Fragment Instance
//                Bundle data = new Bundle();//Use bundle to pass data
//               // data.putString("ROUTE_ID",Route_id);
//
//                data.putString("wayPoint" ,new Gson().toJson(wayPointResponse, WayPointResponse.class));
//
//                argumentFragment.setArguments(data);//Finally set argument bundle to fragment
//
//                argumentFragment.show(getActivity().getSupportFragmentManager(),"SelectPick");
//            }
//        });

        mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        locListener = new LocationListener() {
            public void onLocationChanged(Location loc) {

                loc.getLatitude();
                loc.getLongitude();


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };


        getCurrentLocation();
        mMapView = view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);


        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity()
                        , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.


                } else {
                    googleMap.setMyLocationEnabled(true);


                    // For dropping a marker at a point on the Map
                    myposition = new LatLng(latitude, longitude);
                  //  googleMap.addMarker(new MarkerOptions().position(myposition).title("this is me").icon(BitmapDescriptorFactory.fromResource(R.drawable.locate_bus)));


                    // For zooming automatically to the location of the marker
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myposition).zoom(12).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    mMap = googleMap;
                    //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myposition, 16));






                  /*  mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {

                            if (markerPoints.size() > 1) {
                                markerPoints.clear();
                                googleMap.clear();
                            }

                            // Adding new item to the ArrayList
                            markerPoints.add(latLng);

                            // Creating MarkerOptions
                            MarkerOptions options = new MarkerOptions();

                            // Setting the position of the marker
                            options.position(latLng);

                            if (markerPoints.size() == 1) {
                                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                            } else if (markerPoints.size() == 2) {
                                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                            }

                            // Add new marker to the Google Map Android API V2
                            googleMap.addMarker(options);

                            // Checks, whether start and end locations are captured
                            if (markerPoints.size() >= 2) {
                                LatLng origin = (LatLng) markerPoints.get(0);
                                LatLng dest = (LatLng) markerPoints.get(1);

                                // Getting URL to the Google Directions API
                                String url = getDirectionsUrl(origin, dest);

                                DownloadTask downloadTask = new DownloadTask();

                                // Start downloading json data from Google Directions API
                                downloadTask.execute(url);
                            }

                        }
                    });
                }*/
                }
            }
        });
        setData();


        ll_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  Utils.init(getActivity());

                WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(getActivity());
                if (wayPointResponse!=null){

                    RideTrackDialogFragment argumentFragment = new RideTrackDialogFragment();//Get Fragment Instance
                    Bundle data = new Bundle();//Use bundle to pass data
                    data.putString("ROUTE_ID",Route_id);
                    data.putString("ROUTE_NAME", routeName);


                    argumentFragment.setArguments(data);//Finally set argument bundle to fragment

                    argumentFragment.show(getActivity().getSupportFragmentManager(),"ParentTrack");



                }else if (wayPointResponse==null){

                    Toast.makeText(getActivity(),"Please Select Your Route", Toast.LENGTH_SHORT).show();
                }*/

                RideTrackDialogFragment argumentFragment = new RideTrackDialogFragment();//Get Fragment Instance
                Bundle data = new Bundle();//Use bundle to pass data
                data.putString("ROUTE_ID",Route_id);
                data.putString("ROUTE_NAME", routeName);


                argumentFragment.setArguments(data);//Finally set argument bundle to fragment

               // argumentFragment.show(getActivity().getSupportFragmentManager(),"ParentTrack");


            }
        });


        return view;







    }

    @Override
    public void onResume() {
        super.onResume();



//                {
//                        "entity_type": "user",
//                "etid": 1,
//                "group_type": "node",
//                "gid": 256807,
//                "state": 1,
//                "membership type":"og_membership_type_bus_route",
//                "type":"og_membership_type_bus_route",
//                "field_name" : "field_bus_member",
//                "field_pickup_point": {
//            "lat": "23.406798107278433",
//                    "lng": "79.94529962539673"
//        },
//        "field_drop_point": {
//            "lat": "23.406798107278433",
//                    "lng": "79.94529962539673"
//        }
//
//}

        try {
            wayPointsArrayList=new ArrayList<>();
            JSONObject jsonObj = new JSONObject(RouteInfo.wayPointData);


            JSONObject collection_way_points=jsonObj.getJSONObject("field_collection_way_points");
            jsonObjectSource=collection_way_points.getJSONObject("source");
            JSONObject jsonObject=new JSONObject();

            jsonObject.put("lat",jsonObjectSource.get("lat"));
            jsonObject.put("lng",jsonObjectSource.get("lng"));
//            jSonPost.put("field_pickup_point",jsonObject);
//            jSonPost.put("field_drop_point",jsonObject);

            // studentselected.put(shareAttandanceJson);
            Log.e("event",jsonObj.getString("title"));
            WayPointsArray WaypointInfoSrc = new WayPointsArray();

            WaypointInfoSrc.field_departure_time=jsonObjectSource.getString("field_departure_time");
            WaypointInfoSrc.field_photo=jsonObjectSource.getString("field_photo");
            WaypointInfoSrc.field_place_name=jsonObjectSource.getString("field_place_name");
            WaypointInfoSrc.field_start_time=jsonObjectSource.getString("field_start_time");
            WaypointInfoSrc.lat=jsonObjectSource.get("lat").toString();
            WaypointInfoSrc.lng=jsonObjectSource.get("lng").toString();

            wayPointsArrayList.add(WaypointInfoSrc);
            JSONArray waypoint = collection_way_points.getJSONArray("waypoints");
            if(waypoint != null && waypoint.length() > 0 ) {

                for (int i = 0; i < waypoint.length(); i++) {
                    WayPointsArray WaypointInfo = new WayPointsArray();

                    JSONObject jsonObjectWayPoint = waypoint.getJSONObject(i);
                    if (jsonObjectWayPoint.has("field_way_points")) {
                        JSONObject locate = jsonObjectWayPoint.getJSONObject("field_way_points");
                        if (locate != null && locate.length() > 0) {
                            lat = locate.getString("lat");
                            lng = locate.getString("lng");

                            WaypointInfo.lat =lat;
                            WaypointInfo.lng =lng;



                        } else {

                        }
                    }


                    field_place_name = waypoint.getJSONObject(i).getString("field_place_name");
                    field_start_time = waypoint.getJSONObject(i).getString("field_start_time");
                    field_departure_time  = waypoint.getJSONObject(i).getString("field_departure_time");
                    field_photo = waypoint.getJSONObject(i).getString("field_photo");



                    WaypointInfo.field_place_name = field_place_name;
                    WaypointInfo.field_start_time = field_start_time;
                    WaypointInfo.field_departure_time = field_departure_time;
                    WaypointInfo.field_photo = field_photo;

                    wayPointsArrayList.add(WaypointInfo);

                    Log.d("TAG", "location: " + lat + lng +field_place_name +"..."+field_start_time);






                }


            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(JoinRoute event) throws JSONException {
//        joinRouteData=event.getIndex();
//
////       {
////        "entity_type": "user",
////                "etid": 1,
////                "group_type": "node",
////                "gid": 256807,
////                "state": 1,
////                "membership type":"og_membership_type_bus_route",
////                "type":"og_membership_type_bus_route",
////                "field_name" : "field_bus_member",
////                "field_pickup_point": {
////            "lat": "23.406798107278433",
////                    "lng": "79.94529962539673"
////        },
////        "field_drop_point": {
////            "lat": "23.406798107278433",
////                    "lng": "79.94529962539673"
////        }
////
////    }
//
//        JSONObject jsonObj = new JSONObject(joinRouteData);
//        JSONObject jSonPost=new JSONObject();
//        jSonPost.put("entity_type", "user");
//        jSonPost.put("etid", "1");
//        jSonPost.put("gid", Route_id);
//        jSonPost.put("state", 1);
//        jSonPost.put("membership type", "og_membership_type_bus_route");
//        jSonPost.put("type","og_membership_type_bus_route");
//        jSonPost.put( "field_name" , "field_bus_member");
//
//        JSONObject collection_way_points=jsonObj.getJSONObject("field_collection_way_points");
//         jsonObjectSource=collection_way_points.getJSONObject("source");
//        JSONObject jsonObject=new JSONObject();
//
//        jsonObject.put("lat",jsonObjectSource.get("lat"));
//        jsonObject.put("lng",jsonObjectSource.get("lng"));
//        jSonPost.put("field_pickup_point",jsonObject);
//        jSonPost.put("field_drop_point",jsonObject);
//
//        // studentselected.put(shareAttandanceJson);
//        Log.e("event",jsonObj.getString("title"));
//
//
//    }


    private class  EditBusRoute extends  AsyncTask<String ,JSONObject,JSONObject>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();








        }
        @Override
        protected JSONObject doInBackground(String... strings) {
//            {
//                "membership type":"og_membership_type_bus_route",
//                    "type":"og_membership_type_bus_route",
//                    "field_name" : "field_bus_member",
//                    "field_subscription_days" : 45,
//                    "field_pickup_point": {
//                "lat": "25.961996036257577",
//                        "lng": "79.94529962539673"
//            },
//                "field_drop_point": {
//                "lat": "25.961996036257577",
//                        "lng": "79.94529962539673"
//            }
//
//            }


            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .add("membership type", "og_membership_type_bus_route")
                    .add("field_name" , "field_bus_member")
                    .add("type","og_membership_type_bus_route")
                    .add("field_subscription_days","45")


                    .add("field_pickup_point[lat]",wayPointsArrayList.get(pickPos).getLat())
                    .add("field_pickup_point[lng]",wayPointsArrayList.get(pickPos).getLng())
                    .add("field_drop_point[lat]",wayPointsArrayList.get(dropPos).getLat())
                    .add("field_drop_point[lng]",wayPointsArrayList.get(dropPos).getLng())



                    .build();

            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_ROUTEJOINAXCCEPTREQUEST+"/"+strings[0] ,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);


            try {
                if (response != null) {
                    String errorCode = response.getString("status");
                    final String msg = response.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        JSONObject data = response.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                            String Membershipid = data.getString("membership_id");
                            String child_student=data.getString("child_student");
                            Utils.init(getActivity());

                             Utils.clearPickDropData();
                            WayPointResponse wayPointResponse=new WayPointResponse();
                            wayPointResponse.setChild_student(child_student);
                            wayPointResponse.setMembership_id(Membershipid);
                            ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                            wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                            wayPointsArrays.add(wayPointsArrayList.get(dropPos));
                            wayPointResponse.setWayPointsArrays(wayPointsArrays);
                            Utils.savePickAndDrop(getActivity(),wayPointResponse);
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            Log.d("TAG", "membership_id: " + Membershipid );

                        } else {
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.init(getActivity());

                        WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(getActivity());

                        Log.e("TAG", "membership_id: " + wayPointResponse.getMembership_id()+"size"+ wayPointResponse.getWayPointsArrays().size());


                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (JSONException e) {

            }
        }
    }

   /* private  class SelectBusRoute extends AsyncTask<String, JSONObject, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }

        @Override
        protected JSONObject doInBackground(String... args) {

//            jSonPost.put("entity_type", "user");
//
//            jSonPost.put("etid", "1");
//            jSonPost.put("gid", Route_id);
//            jSonPost.put("state", 1);
//            jSonPost.put("group_type", "node");
//
//
//            jSonPost.put("membership type", "og_membership_type_bus_route");
//            jSonPost.put("type","og_membership_type_bus_route");
//            jSonPost.put( "field_name" , "field_bus_member");
//            JSONObject jsonPickObj=new JSONObject();
//            jsonPickObj.put("lat",wayPointsArrayList.get(event.getPickPos()).getLat());
//            jsonPickObj.put("lng",wayPointsArrayList.get(event.getPickPos()).getLng());
//            jSonPost.put("field_pickup_point",jsonPickObj);
//
//            JSONObject jsonDropObj=new JSONObject();
//            jsonDropObj.put("lat",wayPointsArrayList.get(event.getDropPos()).getLat());
//            jsonDropObj.put("lng",wayPointsArrayList.get(event.getDropPos()).getLng());
//            jSonPost.put("field_drop_point",jsonDropObj);
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("etid", SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())
                    .add("gid", Route_id)
                    .add("state", "1")
                    .add("group_type", "node")
                    .add("membership type", "og_membership_type_bus_route")
                    .add("field_name" , "field_bus_member")
                     .add("type","og_membership_type_bus_route")
                                        .add("field_subscription_days","45")


                    .add("field_pickup_point[lat]",wayPointsArrayList.get(pickPos).getLat())
                    .add("field_pickup_point[lng]",wayPointsArrayList.get(pickPos).getLng())
                    .add("field_drop_point[lat]",wayPointsArrayList.get(dropPos).getLat())
                    .add("field_drop_point[lng]",wayPointsArrayList.get(dropPos).getLng())

//            "field_pickup_point": {
//                "lat": "23.406798107278433",
//                        "lng": "79.94529962539673"
//            },

                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ROUTEJOINAXCCEPTREQUEST ,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                       showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.init(getActivity());

                        ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                        wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                        wayPointsArrays.add(wayPointsArrays.get(dropPos));
                        Utils.savePickDrop(getActivity(),wayPointsArrays);
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();


                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (JSONException e) {

            }
        }
    }

    */





     private class SelectBusRoute extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

/*  "entity_type": "user",
    "etid": 1,
    "group_type": "node",
    "gid": 256807,
    "state": 1,
    "membership type":"og_membership_type_bus_route",
    "type":"og_membership_type_bus_route",
    "field_name" : "field_bus_member",
    "field_subscription_days" : 45,
    "field_pickup_point": {
    	"lat": "23.406798107278433",
    	"lng": "79.94529962539673"
    },
     "field_drop_point": {
    	"lat": "23.406798107278433",
    	"lng": "79.94529962539673"
    }
*/

 RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("etid", SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())
                    .add("gid", Route_id)
                    .add("state", "1")
                    .add("group_type", "node")
                    .add("membership type", "og_membership_type_bus_route")
                    .add("field_name" , "field_bus_member")
                     .add("type","og_membership_type_bus_route")
                                        .add("field_subscription_days","45")


                    .add("field_pickup_point[lat]",wayPointsArrayList.get(pickPos).getLat())
                    .add("field_pickup_point[lng]",wayPointsArrayList.get(pickPos).getLng())
                    .add("field_drop_point[lat]",wayPointsArrayList.get(dropPos).getLat())
                    .add("field_drop_point[lng]",wayPointsArrayList.get(dropPos).getLng())
                       .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ROUTEJOINAXCCEPTREQUEST,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
               /* TeacherRegistration.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });
*/
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        JSONObject data = responce.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                            String Membershipid = data.getString("membership_id");
                            String child_student=data.getString("child_student");
                            Utils.init(getActivity());


                            WayPointResponse wayPointResponse=new WayPointResponse();
                            wayPointResponse.setChild_student(child_student);
                            wayPointResponse.setMembership_id(Membershipid);
                            ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                            wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                            wayPointsArrays.add(wayPointsArrayList.get(dropPos));
                            wayPointResponse.setWayPointsArrays(wayPointsArrays);
                            Utils.savePickAndDrop(getActivity(),wayPointResponse);
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            Log.d("TAG", "membership_id: " + Membershipid );

                        } else {
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                       // Utils.init(getActivity());

                       // WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(getActivity());

                       // Log.e("TAG", "membership_id: " + wayPointResponse.getMembership_id()+"size"+ wayPointResponse.getWayPointsArrays().size());


                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (JSONException e) {

            }
        }

        @Override
        protected void onCancelled() {
       //     registrationteacherAuthTask = null;


        }
    }















    public void getCurrentLocation() {
        if (mlocManager != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            } else {
                mlocManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 0, 0, locListener);
                if (mlocManager != null) {
                    location = mlocManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
                } else {
                    mlocManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, 0, 0, locListener);
                    if (mlocManager != null) {
                        location = mlocManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    } else {
                        mlocManager.requestLocationUpdates(
                                LocationManager.PASSIVE_PROVIDER, 0, 0, locListener);
                        if (mlocManager != null) {
                            location = mlocManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }

                    }

                }
            }
        }
    }

    private void setData() {
        busrouteinfoAuthTask = new ProgressBusRouteInfo();
        busrouteinfoAuthTask.execute();

    }


    private  class ProgressBusRouteInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEINFO +"/"+Route_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        showAlertFragmentDialog(getActivity(), "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            busrouteinfoAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");
                        routetittle = data.getString("title");
                        routediscription = data.getString("body");
                        busroutecode = data.getString("field_route_number");
                        busno = data.getString("field_device_id");

                        JSONObject field_collection_way_points = data.getJSONObject("field_collection_way_points");


                        JSONArray waypoint = field_collection_way_points.getJSONArray("waypoints");
                        if(waypoint != null && waypoint.length() > 0 ) {

                            for (int i = 0; i < waypoint.length(); i++) {
                                WayPointsDrawArray WaypointInfo = new WayPointsDrawArray();

                                JSONObject jsonObject = waypoint.getJSONObject(i);
                                if (jsonObject.has("field_way_points")) {
                                    JSONObject locate = jsonObject.getJSONObject("field_way_points");
                                    if (locate != null && locate.length() > 0) {
                                        lat = locate.getString("lat");
                                        lng = locate.getString("lng");

                                        WaypointInfo.lat = Double.valueOf(lat);
                                        WaypointInfo.lng = Double.valueOf(lng);



                                    } else {

                                    }
                                }


                                field_place_name = waypoint.getJSONObject(i).getString("field_place_name");
                                field_start_time = waypoint.getJSONObject(i).getString("field_start_time");
                                field_departure_time  = waypoint.getJSONObject(i).getString("field_departure_time");
                                field_photo = waypoint.getJSONObject(i).getString("field_photo");



                                WaypointInfo.field_place_name = field_place_name;
                                WaypointInfo.field_start_time = field_start_time;
                                WaypointInfo.field_departure_time = field_departure_time;
                                WaypointInfo.field_photo = field_photo;

                                mMyMarkersArray.add(WaypointInfo);

                                Log.d("TAG", "location: " + lat + lng +field_place_name +"..."+field_start_time);





                            }


                        }

                        setUpMap();
                        plotMarkers(mMyMarkersArray);
                        plotPlollylines(mMyMarkersArray);



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        showAlertFragmentDialog(activity, "Information", msg);

                    }
                }

            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }


        @Override
        protected void onCancelled() {
            busrouteinfoAuthTask = null;
            spinner.setVisibility(View.INVISIBLE);


        }

        public  void showAlertFragmentDialog(Context context, String title, String message) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);


            builder.setMessage(message).setCancelable(false);

            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }

    }
    public static void showAlertFragmentDialog(Context context, String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(context.getResources().getString(R.string.app_name));

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void plotPlollylines(ArrayList<WayPointsDrawArray> mMypollylineArray) {
        PolylineOptions polylineOptions = new PolylineOptions();

        if (mMypollylineArray.size() > 0) {
            for (final WayPointsDrawArray myMarker : mMypollylineArray) {
                polylineOptions.color(Color.RED);

                polylineOptions.width(3);

                Double lat =myMarker.getLat();
                Double Longitude = myMarker.getLng();

                polylineOptions.add(new LatLng(lat, Longitude));

                // polyline=googleMap.addPolyline(polylineOptions);

                googleMap.addPolyline(polylineOptions);

               /* LatLng origin =myMarker.getLat();
                LatLng dest = myMarker.getLng();

                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(origin, dest);

                markerPoints.add(point);*/


            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PickLocationEvent event) {
        /* Do something */
          pickPos=event.getPickPos();
          dropPos=event.getDropPos();
         Log.e("test fire", wayPointsArrayList.get(event.getPickPos()).getField_place_name());

//        Request body:-
//                {
//                        "entity_type": "user",
//                "etid": 1,
//                "group_type": "node",
//                "gid": 256807,
//                "state": 1,
//                "membership type":"og_membership_type_bus_route",
//                "type":"og_membership_type_bus_route",
//                "field_name" : "field_bus_member",
//                "field_pickup_point": {
//            "lat": "23.406798107278433",
//                    "lng": "79.94529962539673"
//        },
//        "field_drop_point": {
//            "lat": "23.406798107278433",
//                    "lng": "79.94529962539673"
//        }
//
//}
        JSONObject jSonPost=new JSONObject();
        try {
            jSonPost.put("entity_type", "user");

            jSonPost.put("etid", "1");
            jSonPost.put("gid", Route_id);
            jSonPost.put("state", 1);
            jSonPost.put("group_type", "node");


            jSonPost.put("membership type", "og_membership_type_bus_route");
            jSonPost.put("type","og_membership_type_bus_route");
            jSonPost.put( "field_name" , "field_bus_member");
            JSONObject jsonPickObj=new JSONObject();
            jsonPickObj.put("lat",wayPointsArrayList.get(event.getPickPos()).getLat());
            jsonPickObj.put("lng",wayPointsArrayList.get(event.getPickPos()).getLng());
            jSonPost.put("field_pickup_point",jsonPickObj);

            JSONObject jsonDropObj=new JSONObject();
            jsonDropObj.put("lat",wayPointsArrayList.get(event.getDropPos()).getLat());
            jsonDropObj.put("lng",wayPointsArrayList.get(event.getDropPos()).getLng());
            jSonPost.put("field_drop_point",jsonDropObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utils.init(getActivity());

        WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(getActivity());
        if (wayPointResponse==null){
            SelectBusRoute selectBusRoute=new SelectBusRoute();
            selectBusRoute.execute(jSonPost.toString());
        }else if (wayPointResponse!=null){

            EditBusRoute editBusRoute=new EditBusRoute();
            editBusRoute.execute(wayPointResponse.getMembership_id());

        }

//        EditBusRoute editBusRoute=new EditBusRoute();
//        editBusRoute.execute();


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        for(int i=2;i<markerPoints.size();i++){
            LatLng point  = markerPoints.get(i);
            if(i==2)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor+"&"+waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }



    private static void plotMarkers(ArrayList<WayPointsDrawArray> markers) {
        if (markers.size() > 0) {
            for (final WayPointsDrawArray myMarker : markers) {

                // Create user marker with custom icon and other options
                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(myMarker.getLat(),myMarker.getLng()));

                // googleMap.addMarker(new MarkerOptions().position(myposition).title("this is me").icon(BitmapDescriptorFactory.fromResource(R.mipmap.stoplocation)));

                markerOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.stop));

                Marker currentMarker = googleMap.addMarker(markerOption);





                mMarkersHashMap.put(currentMarker, myMarker);
                googleMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());





            }
        }
    }








    private static void setUpMap() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (googleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            //   googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.googleMap)).getMap();


            if (googleMap != null) {

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {
                        marker.showInfoWindow();
                        return true;
                    }
                });
            } else

                Toast.makeText(activity, "Unable to create Maps", Toast.LENGTH_SHORT).show();
        }
    }




    public static class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        public MarkerInfoWindowAdapter() {
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v = activity.getLayoutInflater().inflate(R.layout.googlemapinfo_window, null);

            WayPointsDrawArray myMarker = mMarkersHashMap.get(marker);

            TextView tittle = v.findViewById(R.id.tittle);
            TextView pick = v.findViewById(R.id.Arivaltime);

            TextView drop = v.findViewById(R.id.DepatureTime);
            ImageView location = v.findViewById(R.id.pinlocation);
            location.setImageResource(R.mipmap.stoplocation);
            if(myMarker.field_place_name !=null){
                tittle.setText(myMarker.field_place_name);
            }else {
                tittle.setText("mylocation");
            }

            pick.setText(myMarker.getField_start_time());
            drop.setText(myMarker.getField_departure_time());
            return v;
        }
    }



}
