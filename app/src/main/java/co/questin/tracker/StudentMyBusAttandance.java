package co.questin.tracker;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import co.questin.R;
import co.questin.adapters.CalendarAdapter;
import co.questin.adapters.MonthlyAttandanceAdapter;
import co.questin.models.CalendarCollection;
import co.questin.models.MonthArrayAttandance;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.models.CalendarCollection.date_collection_arr;

public class StudentMyBusAttandance extends Fragment {
    String Route_id,RouteTittle,Adminuser,Join_Value,AdminCreater;
    public ArrayList<MonthArrayAttandance> mMonthList;
    private ProgressMonthlyDatesList progressmonthdateslist = null;
    public GregorianCalendar cal_month, cal_month_copy;
    private CalendarAdapter cal_adapter;
    LinearLayout MonthcalenderDisplay,PercentTotal;
    private TextView tv_month;
    RecyclerView rv_totalmonth;
    GridView gridview;
    FrameLayout top;
    private RecyclerView.LayoutManager layoutManager;
    ImageButton   previous,next;
    private MonthlyAttandanceAdapter monthadapter;
    Spinner routes;
    String RouteShiftId,ShiftId;
    //  PieChart pieChart;
    String total_perc;
    TextView ErrorText,TotalText,Text;
    static Activity activity;
    private static ProgressBar spinner;
    public StudentMyBusAttandance() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_student_my_bus_attandance, container, false);
        activity = (Activity) view.getContext();

        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle= getArguments().getString("ROUTE_NAME");
        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");
        routes =view.findViewById(R.id.routes);
        top =view.findViewById(R.id.top);
        spinner= view.findViewById(R.id.progressBar);
        gridview = view.findViewById(R.id.gv_calendar);
        MonthcalenderDisplay= view.findViewById(R.id.MonthcalenderDisplay);
        ErrorText = view.findViewById(R.id.ErrorText);
        rv_totalmonth =view.findViewById(R.id.rv_totalmonth);
        PercentTotal= view.findViewById(R.id.PercentTotal);
        TotalText =view.findViewById(R.id.TotalText);
        Text =view.findViewById(R.id.Text);
        layoutManager = new LinearLayoutManager(activity);
        rv_totalmonth.setHasFixedSize(true);
        rv_totalmonth.setLayoutManager(layoutManager);
        mMonthList=new ArrayList<>();

        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();

        cal_month.set(Calendar.DATE, cal_month.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date MonthFirstDay = cal_month.getTime();
        cal_month.set(Calendar.DATE, cal_month.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date MonthLastDay = cal_month.getTime();

        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        final String dateMonthStart= DATE_FORMAT.format(MonthFirstDay);
        final String dateMonthEnd= DATE_FORMAT.format(MonthLastDay);



        tv_month = view. findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));

        previous = view.findViewById(R.id.ib_prev);
        next = view. findViewById(R.id.Ib_next);



        previous.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            setPreviousMonth();
            refreshCalendar();
        }
    });

        next.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            setNextMonth();
            refreshCalendar();

        }
    });

        // Spinner Drop down elements
        List<String> Shift = new ArrayList<String>();
        Shift.add("morning");
        Shift.add("evening");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, Shift);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        routes.setAdapter(dataAdapter);
        routes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                ShiftId = parent.getItemAtPosition(position).toString();

                Log.d("TAG", "Shift: " + ShiftId);

                if (Adminuser.equals("TRUE") && Join_Value.equals("member"))  {

                    if(ShiftId.contains("morning")){

                        RouteShiftId ="morning";
                        progressmonthdateslist = new ProgressMonthlyDatesList();
                        progressmonthdateslist.execute(dateMonthStart,dateMonthEnd,RouteShiftId);


                    }else if(ShiftId.contains("evening")) {

                        RouteShiftId ="evening";
                        progressmonthdateslist = new ProgressMonthlyDatesList();
                        progressmonthdateslist.execute(dateMonthStart,dateMonthEnd,RouteShiftId);



                    }




                }else {
                    MonthcalenderDisplay.setVisibility(View.GONE);
                    rv_totalmonth.setVisibility(View.GONE);
                    PercentTotal.setVisibility(View.GONE);
                    Text.setVisibility(View.GONE);
                    ErrorText.setVisibility(View.VISIBLE);
                    ErrorText.setText("You Are Not Authorised");
                    top.setVisibility(View.GONE);


                }








            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        return view;
}




    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));

        cal_month.set(Calendar.DATE, cal_month.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date MonthFirstDay = cal_month.getTime();
        cal_month.set(Calendar.DATE, cal_month.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date MonthLastDay = cal_month.getTime();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateMonthStart= DATE_FORMAT.format(MonthFirstDay);
        String dateMonthEnd= DATE_FORMAT.format(MonthLastDay);



        mMonthList.clear();
        progressmonthdateslist = new ProgressMonthlyDatesList();
        progressmonthdateslist.execute(dateMonthStart,dateMonthEnd,ShiftId);



    }



private class ProgressMonthlyDatesList extends AsyncTask<String, JSONObject, JSONObject> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Utils.p_dialog(activity);

    }

    @Override
    protected JSONObject doInBackground(String... args) {
        JSONObject jsonObject = null;
        OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
        RequestBody body = new FormBody.Builder()
                .add("from_date",args[0])
                .add("to_date",args[1])
                .add("type",args[2])
                .build();


        try {

            String responseData = ApiCall.POSTHEADER(client, URLS.URL_ROUTEMYATTANDANCESTUDENT+"/"+Route_id+"/"+ SessionManager.getInstance(getActivity()).getUser().userprofile_id, body);
            jsonObject = new JSONObject(responseData);
            //  Log.d("TAG", "responseData: " + responseData);



        } catch (JSONException | IOException e) {
            e.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Utils.p_dialog_dismiss(activity);
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server.  Reload the page.");

                }
            });


        }
        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject responce) {
        progressmonthdateslist = null;
        try {
            if (responce != null) {
                Utils.p_dialog_dismiss(activity);
                String errorCode = responce.getString("status");
                final String msg = responce.getString("message");

                if (errorCode.equalsIgnoreCase("1")) {

                    JSONArray data = responce.getJSONArray("data");
                    if (data != null && data.length() > 0) {



                        rv_totalmonth.setVisibility(View.VISIBLE);
                        PercentTotal.setVisibility(View.VISIBLE);
                        Text.setVisibility(View.VISIBLE);
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject jsonObject = data.getJSONObject(i);


                            date_collection_arr = new ArrayList<CalendarCollection>();
                            String StudentName = data.getJSONObject(i).getString("student");
                            total_perc = data.getJSONObject(i).getString("total_perc");
                            String type = data.getJSONObject(i).getString("monthly_perc");

                            TotalText.setText(total_perc);

                            JSONArray jsonArrayMonths = new JSONArray(data.getJSONObject(i).getString("monthly_perc"));
                            if (jsonArrayMonths != null && jsonArrayMonths.length() > 0) {
                                for (int k = 0; k < jsonArrayMonths.length(); k++) {

                                    MonthArrayAttandance monthInfo = new Gson().fromJson(jsonArrayMonths.getJSONObject(k).toString(), MonthArrayAttandance.class);
                                    mMonthList.add(monthInfo);
                                    monthadapter = new MonthlyAttandanceAdapter(activity, mMonthList);
                                    rv_totalmonth.setAdapter(monthadapter);
                                    rv_totalmonth.setNestedScrollingEnabled(false);


                                }

                            }

                            try {
                            JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("dates"));



                                if (jsonArrayDates != null && jsonArrayDates.length() > 0) {

                                    for (int j = 0; j < jsonArrayDates.length(); j++) {
                                        String startdate = jsonArrayDates.getJSONObject(j).getString("date");
                                        String attendance = jsonArrayDates.getJSONObject(j).getString("attendance");

                                        String str = startdate;
                                        String[] splited = str.split("\\s+");

                                        String split_one = splited[0];
                                        String split_second = splited[1];

                                        Log.d("TAG", "split_one: " + split_one + attendance);
                                        CalendarCollection.date_collection_arr.add(new CalendarCollection(split_one, attendance));
                                        cal_adapter = new CalendarAdapter(activity, cal_month, date_collection_arr);
                                        gridview.setAdapter(cal_adapter);


                                        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                            public void onItemClick(AdapterView<?> parent, View v,
                                                                    int position, long id) {

                                          /*  ((CalendarAdapter) parent.getAdapter()).setSelected(v, position);
                                            String selectedGridDate = CalendarAdapter.day_string
                                                    .get(position);

                                            String[] separatedTime = selectedGridDate.split("-");
                                            String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                                            int gridvalue = Integer.parseInt(gridvalueString);

                                            if ((gridvalue > 10) && (position < 8)) {
                                                setPreviousMonth();
                                                refreshCalendar();
                                            } else if ((gridvalue < 7) && (position > 28)) {
                                                setNextMonth();
                                                refreshCalendar();
                                            }*/
                                            /*((CalendarAdapter) parent.getAdapter()).setSelected(v,position);


                                            ((CalendarAdapter) parent.getAdapter()).getPositionList(selectedGridDate, DetailDisplayMyattandance.this);*/
                                            }

                                        });

                                    }

                                } else {

                                }


                            } catch (JSONException e) {

                               // MonthcalenderDisplay.setVisibility(View.GONE);
                                rv_totalmonth.setVisibility(View.GONE);
                                PercentTotal.setVisibility(View.GONE);
                                Text.setVisibility(View.GONE);
                                cal_adapter = new CalendarAdapter(activity, cal_month, date_collection_arr);
                                gridview.setAdapter(cal_adapter);



                            }
                        }
                    }else {
                        ErrorText.setVisibility(View.VISIBLE);
                        ErrorText.setText("No Details Found");

                    }

                } else if (errorCode.equalsIgnoreCase("0")) {
                    Utils.p_dialog_dismiss(activity);


                }
            }
        } catch (JSONException e) {
            Utils.p_dialog_dismiss(activity);
            Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


        }
    }

    @Override
    protected void onCancelled() {
        progressmonthdateslist = null;
        Utils.p_dialog_dismiss(activity);
        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

    }

}
}
