package co.questin.tracker;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import co.questin.R;
import co.questin.adapters.AddBusAttendanceAdapter;
import co.questin.models.tracker.BusAttandanceArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.OkHttpClient;

public class BusAttandanceAdd extends BaseAppCompactActivity {
    String Route_id,RouteTittle,startfrom;
    public ArrayList<BusAttandanceArray> mAttendanceList;
    private AttendanceStudentList studentlist = null;
    private AddBusAttendanceAdapter addattendanceadapter;
    Spinner routes;
    RecyclerView list_of_Students;
    String ShiftId;
    private RecyclerView.LayoutManager layoutManager;
    private Calendar mcalender;
    TextView selectdate,eveningShift,morningShift;
    JSONArray studentselected;
    JSONObject sharefriendJson;
    TextView ErrorText;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int startDay, startMonth, startYear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_attandance_add);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getIntent().getExtras();
        Route_id = b.getString("ROUTE_ID");
        RouteTittle = b.getString("ROUTE_NAME");
        list_of_Students =findViewById(R.id.list_of_Students);
        eveningShift =findViewById(R.id.eveningShift);
        morningShift =findViewById(R.id.morningShift);

        ErrorText =findViewById(R.id.ErrorText);
        selectdate =findViewById(R.id.selectdate);
        layoutManager = new LinearLayoutManager(this);
        list_of_Students.setHasFixedSize(true);
        list_of_Students.setLayoutManager(layoutManager);
        routes =findViewById(R.id.routes);
        mAttendanceList=new ArrayList<>();
        mcalender = Calendar.getInstance();

        mYear = mcalender.get(Calendar.YEAR);
        mMonth = mcalender.get(Calendar.MONTH);
        mDay = mcalender.get(Calendar.DAY_OF_MONTH);


        //  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String  formattedDate = sdf.format(mcalender.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        String  formattedDate2 = sdf2.format(mcalender.getTime());
        selectdate.setText(formattedDate2);
        startfrom =formattedDate;

        // Spinner Drop down elements
        List<String> Shift = new ArrayList<String>();
        Shift.add("morning");
        Shift.add("evening");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Shift);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        routes.setAdapter(dataAdapter);
        routes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                ShiftId = parent.getItemAtPosition(position).toString();

                Log.d("TAG", "Shift: " + ShiftId);

                if(ShiftId.contains("morning")){
                    mAttendanceList.clear();
                    studentlist = new AttendanceStudentList();
                    studentlist.execute(Route_id);
                    morningShift.setTextColor(Color.parseColor("#4fa8ce"));
                    eveningShift.setTextColor(Color.parseColor("#747E80"));

                }else if(ShiftId.contains("evening")) {
                    mAttendanceList.clear();
                    studentlist = new AttendanceStudentList();
                    studentlist.execute(Route_id);
                    morningShift.setTextColor(Color.parseColor("#747E80"));
                    eveningShift.setTextColor(Color.parseColor("#4fa8ce"));



                }





            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        selectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  StartdateDialog();
            }
        });


    }



    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {



                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), android.app.AlertDialog.THEME_HOLO_LIGHT, listener, mYear, mMonth, mDay);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis() +1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {

        startfrom = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        selectdate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));
        mAttendanceList.clear();
        studentlist = new AttendanceStudentList();
        studentlist.execute(Route_id);


    }






    private class AttendanceStudentList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTESTUDENTLIST+"/"+args[0]+"/student?date="+startfrom+"&offset="+"0"+"&limit=50");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData +args[0] +startfrom);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                BusAttandanceAdd.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                BusAttandanceArray busInfo = new BusAttandanceArray();

                                String tnid = jsonArrayData.getJSONObject(i).getString("uid");
                                String title = jsonArrayData.getJSONObject(i).getString("name");
                                String field_logo = jsonArrayData.getJSONObject(i).getString("picture");
                                String Enrollment = jsonArrayData.getJSONObject(i).getString("enrollment_id");





                                JSONObject Attandance = jsonObject.getJSONObject("attendance");
                                if (Attandance != null && Attandance.length() > 0) {

                                    String morning = Attandance.getString("morning");
                                    String evening = Attandance.getString("evening");

                                    busInfo.MorningStatus = morning;
                                    busInfo.EveningStatus = evening;


                                }



                               /* JSONArray Attandance = jsonArrayData.getJSONObject(i).getJSONArray("attendance");

                                if (Attandance != null && Attandance.length() > 0) {

                                    for (int j = 0; j < Attandance.length(); j++) {

                                        String morning = Attandance.getJSONObject(j).getString("morning");
                                        String evening = Attandance.getJSONObject(j ).getString("evening");

                                        busInfo.MorningStatus = morning;
                                        busInfo.EveningStatus = evening;

                                    }

                                    }*/

                                busInfo.uid=tnid;
                                busInfo.name =title;
                                busInfo.picture =field_logo;
                                busInfo.enrollment_id =Enrollment;

                                mAttendanceList.add(busInfo);

                               /* BusAttandanceArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), BusAttandanceArray.class);
                                mAttendanceList.add(CourseInfo);
*/



                                addattendanceadapter = new AddBusAttendanceAdapter(BusAttandanceAdd.this, mAttendanceList,ShiftId,Route_id,startfrom);
                                list_of_Students.setAdapter(addattendanceadapter);

                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(BusAttandanceAdd.this);
                            builder.setMessage("No Student found")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentlist = null;
            hideLoading();


        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
