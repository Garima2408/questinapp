package co.questin.settings;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Switch;

import co.questin.R;

public class Settings extends AppCompatActivity {
    Switch appnotification,campusnotification,emailnotification,chatnotification,campusfeddnotification,groupnotification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        appnotification =findViewById(R.id.appnotification);
        campusnotification =findViewById(R.id.campusnotification);
        emailnotification =findViewById(R.id.emailnotification);
        chatnotification =findViewById(R.id.chatnotification);
        campusfeddnotification =findViewById(R.id.campusfeddnotification);
        groupnotification =findViewById(R.id.groupnotification);

        appnotification.setClickable(false);
        campusnotification.setClickable(false);
        emailnotification.setClickable(false);
        chatnotification.setClickable(false);
        campusfeddnotification.setClickable(false);
        groupnotification.setClickable(false);
        appnotification.setChecked(false);
        campusnotification.setChecked(false);
        emailnotification.setChecked(false);
        chatnotification.setChecked(false);
        groupnotification.setChecked(false);



    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
