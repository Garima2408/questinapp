package co.questin.settings;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.questin.R;
import co.questin.activities.MainActivity;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class EmailUs extends BaseAppCompactActivity {
    EditText edt_full_name,edt_email_address,edt_message;
    Button btn_enter;
    String email,fullname,msg;
    private EmailUsAuthTask EmailusAuthTask = null;
    String TITLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_email_us);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        edt_full_name = findViewById(co.questin.R.id.edt_full_name);
        edt_email_address = findViewById(co.questin.R.id.edt_email_address);
        edt_message = findViewById(co.questin.R.id.edt_message);
        btn_enter = findViewById(co.questin.R.id.btn_enter);
        Bundle b = getIntent().getExtras();
        TITLE = b.getString("TITLE");
        actionBar.setTitle(TITLE);


        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToSendEmail();
            }
        });

    }

    private void attemptToSendEmail() {

        edt_email_address.setError(null);
        edt_full_name.setError(null);
        edt_message.setError(null);


        // Store values at the time of the login attempt.
        email = edt_email_address.getText().toString().trim();
        fullname = edt_full_name.getText().toString().trim();
        msg = edt_message.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            focusView = edt_email_address;
            cancel = true;
            showSnackbarMessage(getString(co.questin.R.string.error_field_required));

            // Check for a valid email address.
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = edt_email_address;
            cancel = true;
            showSnackbarMessage(getString(co.questin.R.string.error_invalid_email));
            // Check for a valid password, if the user entered one.
        }else if (co.questin.utils.TextUtils.isNullOrEmpty(fullname)) {
            // check for First Name
            focusView = edt_full_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.questin.utils.TextUtils.isNullOrEmpty(msg)) {
            // check for First Name
            focusView = edt_message;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            EmailusAuthTask = new EmailUsAuthTask();
            EmailusAuthTask.execute();



        }
    }


    private class EmailUsAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("webform","1a42a905-064c-40b0-b414-8e3163bc683a")
                    .add("submission[Name]",fullname)
                    .add("submission[Email]",email)
                    .add("submission[Description]",msg)
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_EMAIL,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                EmailUs.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            EmailusAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(EmailUs.this);
                        builder.setTitle("Info");
                        builder.setMessage(msg)
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        Intent upanel = new Intent(EmailUs.this, MainActivity.class);
                                        upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(upanel);
                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            EmailusAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}



