package co.questin.settings;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.questin.R;
import co.questin.adapters.FAQExpandableAdapter;
import co.questin.models.FaqExpandableChildArray;
import co.questin.models.FaqExpandableparentArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.OkHttpClient;

public class FAQ extends BaseAppCompactActivity {
    ExpandableListView expandableListView;
    FAQExpandableAdapter expandableListAdapter;
    private ArrayList<FaqExpandableChildArray> goalList;

    private List<FaqExpandableparentArray> expandableList;

    FloatingActionButton fab;
    private ProgressFAQInfo faqinfoAuthTask = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        fab = findViewById(R.id.fab);
        expandableListView = findViewById(R.id.expandableListView);

        expandableList = new ArrayList<FaqExpandableparentArray>();
        ArrayList<FaqExpandableChildArray> goalList = null;


        faqinfoAuthTask = new ProgressFAQInfo();
        faqinfoAuthTask.execute();



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity (new Intent(FAQ.this, EmailUs.class));

            }
        });
    }



    private class ProgressFAQInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_FAQ);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);



            } catch (JSONException | IOException e) {

                e.printStackTrace();
                FAQ.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            faqinfoAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            ArrayList<FaqExpandableChildArray> goalList = new ArrayList<>();

                            FaqExpandableChildArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), FaqExpandableChildArray.class);
                            goalList.add(CourseInfo);
                            FaqExpandableparentArray habitParentBean = new FaqExpandableparentArray();
                            habitParentBean.parentTitle = jsonArrayData .getJSONObject(i).getString("question");
                            habitParentBean.childsTaskList.addAll(goalList);
                            expandableList.add(habitParentBean);


                            expandableListAdapter = new FAQExpandableAdapter(getActivity(), expandableList);
                            expandableListView.setIndicatorBounds(0, 20);
                            expandableListView.setAdapter(expandableListAdapter);
                            expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                @Override
                                public void onGroupExpand(int groupPosition) {
                                    int len = expandableListAdapter.getGroupCount();
                                    for (int i = 0; i < len; i++) {
                                        if (i != groupPosition) {
                                          //  expandableListAdapter.collapseGroup(i);
                                        }
                                    }
                                }
                            });
                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);

                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            faqinfoAuthTask = null;
            hideLoading();


        }
    }


    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
