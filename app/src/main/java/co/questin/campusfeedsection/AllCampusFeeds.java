package co.questin.campusfeedsection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.CampusFeedsAdapters;
import co.questin.adapters.CampusFeedsFilterAdapters;
import co.questin.alumni.AluminiProfile;
import co.questin.calendersection.CalenderMain;
import co.questin.chat.ChatTabbedActivity;
import co.questin.models.CampusFeedArray;
import co.questin.models.FilterArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.parent.ParentProfile;
import co.questin.studentprofile.ProfileDash;
import co.questin.teacher.TeacherProfile;
import co.questin.utils.Constants;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.fcm.MessagingService.college_Allfeeds;
import static co.questin.network.ApiCall.getActivity;

public class AllCampusFeeds extends AppCompatActivity {

    Dialog imageDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    static FloatingActionButton fab;
    ImageView campusfeed_toggle;
    ListView campusfeed_filter_List;
    ArrayList<FilterArray> listoffilter;
    private ProgressSearchOfFilterList progresssearchoffilterlist = null;
    static RecyclerView rv_campus_news;
    public static ArrayList<CampusFeedArray> mcampusfeedList;
    private static CampusFeedsAdapters campusfeedadapter;
    private static CollageNewFeedsDisplay collagenewfeedslist = null;
    private static CollageAllNewFeedsDisplay allcollagenewfeedslist = null;
    private RecyclerView.LayoutManager layoutManager;
    static String tittle,URLDoc,FilterTittle,FilterId;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static TextView ErrorText,badge_notificationcampus,campus_feeds;
    private boolean _hasLoadedOnce= false; // your boolean field
    private static ShimmerFrameLayout mShimmerViewContainer;
    static SwipeRefreshLayout swipeContainer;
    private ArrayList<String> filterlist;
    static ImageView ReloadProgress;
    static Activity activity;
    RelativeLayout relative_layout_item,campusfeed_togglelayout;
    boolean isPressed = false;
    CampusFeedsFilterAdapters adapter;

    public static final Integer[] images = { R.mipmap.allfeed,
            R.mipmap.studentfeed, R.mipmap.campusnews, R.mipmap.buy_sell, R.mipmap.housesharing  , R.mipmap.lost_found , R.mipmap.ridesharing , R.mipmap.admission };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_campus_feeds);
        activity = getActivity();
        preferences = Utils.getSharedPreference(AllCampusFeeds.this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_FALSE_CAMPUSFEED);
        FirstOneThis();
        fab = findViewById(R.id.fab);
        campusfeed_toggle =findViewById(R.id.campusfeed_toggle);
        campusfeed_filter_List = findViewById(R.id.campusfeed_filter_List);
        ReloadProgress = findViewById(R.id.ReloadProgress);
        badge_notificationcampus =findViewById(R.id.badge_notificationcampus);
        relative_layout_item =findViewById(R.id.relative_layout_item);
        campus_feeds =findViewById(R.id.campus_feeds);
        campusfeed_togglelayout =findViewById(R.id.campusfeed_togglelayout);
        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
        listoffilter = new ArrayList<FilterArray>();
        filterlist = new ArrayList<String>();
        ErrorText =findViewById(R.id.ErrorText);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));
        mcampusfeedList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        rv_campus_news = findViewById(R.id.rv_campus_news);
        campusfeedadapter = new CampusFeedsAdapters(rv_campus_news, mcampusfeedList ,this);
        rv_campus_news.setLayoutManager(layoutManager);
        rv_campus_news.setItemAnimator(new DefaultItemAnimator());
        rv_campus_news.setAdapter(campusfeedadapter);
        inItView();
        Setuponbottombar();
        GetTheListOfFilters();
        allcollagenewfeedslist = new CollageAllNewFeedsDisplay();
        allcollagenewfeedslist.execute();

        if(college_Allfeeds >0){
            badge_notificationcampus.setVisibility(View.VISIBLE);
            badge_notificationcampus.setText(String.valueOf(college_Allfeeds));

        }else if (college_Allfeeds ==0){
            badge_notificationcampus.setVisibility(View.GONE);
        }


        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_Allfeeds=0;
                badge_notificationcampus.setVisibility(View.GONE);
                Intent backIntent = new Intent(AllCampusFeeds.this, CampusFeedNotifications.class);
                startActivity(backIntent);

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(AllCampusFeeds.this, AddCampusFeeds.class)
                 .putExtra("open", "CameFromAllCampusFeed");
                startActivity(backIntent);




            }
        });

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RefreshWorked();
            }
        });


/*

                    }*/




        rv_campus_news.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                    campusfeed_filter_List.setVisibility(View.GONE);
                    campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
                }
                return false;
            }
        });
        campusfeed_togglelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPressed){
                    if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                        campusfeed_filter_List.setVisibility(View.GONE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);

                    } else {
                        campusfeed_filter_List.setVisibility(View.VISIBLE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.cross);

                    }


                }else if(!isPressed){

                    campusfeed_filter_List.setVisibility(View.VISIBLE);
                    campusfeed_toggle.setBackgroundResource(R.mipmap.cross);


                }

                isPressed = !isPressed; // reverse


            }


        });



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){
                            RefreshWorked();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 1000);
            }
        });

        rv_campus_news.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    AllCampusFeeds.fab.hide();
                    if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                        campusfeed_filter_List.setVisibility(View.GONE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);

                    }
                } else if (dy < 0) {
                    AllCampusFeeds.fab.show();
                    if (campusfeed_filter_List.getVisibility() == View.VISIBLE) {
                        campusfeed_filter_List.setVisibility(View.GONE);
                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);

                    }
                }
            }
        });

    }


    private void inItView(){


        rv_campus_news.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    campusfeedadapter.addProgress();


                    if (FilterId !=null){
                        if (FilterId.matches("1")){
                            allcollagenewfeedslist = new CollageAllNewFeedsDisplay();
                            allcollagenewfeedslist.execute();


                        }else {
                            collagenewfeedslist = new CollageNewFeedsDisplay();
                            collagenewfeedslist.execute(FilterId);
                            }
                    }else {
                        allcollagenewfeedslist = new CollageAllNewFeedsDisplay();
                        allcollagenewfeedslist.execute();

                    }


                }else
                    Log.i("loadinghua", "im else now");
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    public static void RefreshWorked() {

        PAGE_SIZE=0;
        isLoading=true;
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        if (FilterId !=null){
            if (FilterId.matches("1")){
                allcollagenewfeedslist = new CollageAllNewFeedsDisplay();
                allcollagenewfeedslist.execute();
            }else {
                collagenewfeedslist = new CollageNewFeedsDisplay();
                collagenewfeedslist.execute(FilterId);
            }
            }else {
            allcollagenewfeedslist = new CollageAllNewFeedsDisplay();
            allcollagenewfeedslist.execute();

        }


        }


    private void GetTheListOfFilters() {
        progresssearchoffilterlist = new ProgressSearchOfFilterList();
        progresssearchoffilterlist.execute();
    }


    private void FirstOneThis() {

        switch (Runfirst){
            case Constants.ROLE_RUNNING_TRUE :
                imageDialog = new Dialog(AllCampusFeeds.this);
                imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                imageDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                imageDialog.setContentView(R.layout.image_overlay_screen);
                imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);

                imageDialog.show();
                ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                showimage.setImageResource(R.mipmap.onboarding_campusfeed);

                showimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.getSharedPreference(AllCampusFeeds.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_FALSE_CAMPUSFEED).apply();
                        Utils.removeStringPreferences(AllCampusFeeds.this,"0");

                        imageDialog.dismiss();

                    }
                });



                break;
            case Constants.ROLE_RUNNING_FALSE :
                break;


        }

    }

    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent p = new Intent(AllCampusFeeds.this,CalenderMain.class);
                startActivity(p);
                finish();


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   Intent y = new Intent(AllCampusFeeds.this,AllCampusFeeds.class);
                startActivity(y);
              */


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(AllCampusFeeds.this,ChatTabbedActivity.class);
                startActivity(z);
                finish();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionManager.getInstance(getActivity()).getUserClgRole()!=null) {

                    if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("13")) {
                        Intent j = new Intent(AllCampusFeeds.this,ProfileDash.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                        Intent j = new Intent(AllCampusFeeds.this,TeacherProfile.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("15")) {
                        Intent j = new Intent(AllCampusFeeds.this,AluminiProfile.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                        Intent j = new Intent(AllCampusFeeds.this,ParentProfile.class);
                        startActivity(j);
                        finish();

                    }
                    else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("3")) {
                        Intent j = new Intent(AllCampusFeeds.this,TeacherProfile.class);
                        startActivity(j);
                        finish();

                    }



                }

            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");
        _hasLoadedOnce= false;
        super.onPause();
    }

    private static class CollageNewFeedsDisplay extends AsyncTask<String, JSONObject, JSONObject> {



        @Override
        protected void onPreExecute() {
            ErrorText.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SHORTCOMMENTS+"?"+"cid="+SessionManager.getInstance(activity).getCollage().getTnid()+"&"+"channel="+args[0]+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", ("msg"+responseData   +PAGE_SIZE+"offset"));

                } catch (JSONException | IOException e) {
                e.printStackTrace();

                try {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeContainer.setRefreshing(false);
                            ReloadProgress.setVisibility(View.VISIBLE);

                        }
                    });
                    campusfeedadapter.removeProgress();
                    isLoading=false;


                }catch (Exception o){

                }


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);
                    rv_campus_news.setVisibility(View.VISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");




                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<CampusFeedArray> arrayList = new ArrayList<>();
                        arrayList.clear();


                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                            arrayList.add(CourseInfo);
                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                            JSONArray jsoncategoryArray = jsonArrayData.getJSONObject(i).getJSONArray("category");
                            if (jsoncategoryArray != null && jsoncategoryArray.length() > 0) {

                                for (int j = 0; j < jsoncategoryArray.length(); j++) {
                                    String categoryid = jsoncategoryArray.getJSONObject(j).getString("id");
                                    String categoryName = jsoncategoryArray.getJSONObject(j).getString("name");
                                    CourseInfo.categoryId =categoryid;
                                    CourseInfo.categoryName =categoryName;
                                }
                            }




                            if (jsonObject.has("field_comment_attachments")){
                                JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                    URLDoc = AttacmentsFeilds.getString("value");
                                    tittle = AttacmentsFeilds.getString("title");
                                    CourseInfo.title =tittle;
                                    CourseInfo.url =URLDoc;

                                }else {

                                }
                            }
                        }



                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {

                                rv_campus_news.setVisibility(View.VISIBLE);
                                campusfeedadapter.setInitialData(arrayList);
                                // stop animating Shimmer and hide the layout
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            } else {

                                campusfeedadapter.removeProgress();
                                campusfeedadapter.addData(arrayList);
                                swipeContainer.setRefreshing(false);
                                rv_campus_news.setVisibility(View.VISIBLE);
                                // stop animating Shimmer and hide the layout
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                            }
                            isLoading = false;
                        }
                        else{
                            if (PAGE_SIZE==0){
                                // stop animating Shimmer and hide the layout
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                               // swipeContainer.setVisibility(View.GONE);
                                rv_campus_news.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                            }else
                                campusfeedadapter.removeProgress();

                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        //  Utils.showAlertFragmentDialog(activity,"Information", msg);
                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
            }
        }

        @Override
        protected void onCancelled() {
            collagenewfeedslist = null;
            swipeContainer.setRefreshing(false);


        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        PAGE_SIZE=0;
    }





    private class ProgressSearchOfFilterList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("vid","35 ")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_FILTERLIST,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AllCampusFeeds.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchoffilterlist = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                                data.remove(1);

                            } else {

                                data.remove(0);
                            }

                        }
                        FilterArray filterInfo2 =new FilterArray();
                        filterInfo2.id ="1";
                        filterInfo2.label="All feed";
                        listoffilter.add(filterInfo2);
                        filterlist.add(filterInfo2.getLabel());
                        for (int i = 0; i < data.length(); i++) {


                            FilterArray filterInfo = new Gson().fromJson(data.getJSONObject(i).toString(), FilterArray.class);
                            listoffilter.add(filterInfo);
                            filterlist.add(filterInfo.getLabel());



                            adapter = new CampusFeedsFilterAdapters(AllCampusFeeds.this, filterlist,images);
                            campusfeed_filter_List.setAdapter(adapter);
                            campusfeed_filter_List.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

                            // Capture ListView item click
                            campusfeed_filter_List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    FilterId = listoffilter.get(position).getId();
                                    FilterTittle = listoffilter.get(position).getLabel();

                                    if (FilterId.matches("1")){

                                        campusfeed_filter_List.setVisibility(View.GONE);
                                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
                                        mShimmerViewContainer.startShimmerAnimation();
                                        mShimmerViewContainer.setVisibility(View.VISIBLE);
                                        PAGE_SIZE=0;
                                        isLoading=true;
                                        allcollagenewfeedslist = new CollageAllNewFeedsDisplay();
                                        allcollagenewfeedslist.execute();
                                        FilterId ="1";

                                    }else {

                                        campusfeed_filter_List.setVisibility(View.GONE);
                                        campusfeed_toggle.setBackgroundResource(R.mipmap.categoryicon);
                                        mShimmerViewContainer.startShimmerAnimation();
                                        mShimmerViewContainer.setVisibility(View.VISIBLE);
                                        PAGE_SIZE=0;
                                        isLoading=true;
                                        collagenewfeedslist = new CollageNewFeedsDisplay();
                                        collagenewfeedslist.execute(FilterId);

                                    }




                                }
                            });

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {



                    }
                }
            } catch (JSONException e) {

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchoffilterlist = null;


        }
    }



    private static class CollageAllNewFeedsDisplay extends AsyncTask<String, JSONObject, JSONObject> {


        @Override
        protected void onPreExecute() {
            ErrorText.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_CAMPUSFEED+"?"+"cid="+SessionManager.getInstance(activity).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", ("msgAll"+responseData   +PAGE_SIZE+"offset"));

            } catch (JSONException | IOException e) {

                e.printStackTrace();

                try {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeContainer.setRefreshing(false);
                            ReloadProgress.setVisibility(View.VISIBLE);

                        }
                    });
                    campusfeedadapter.removeProgress();
                    isLoading=false;


                }catch (Exception o){

                }


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);
                    rv_campus_news.setVisibility(View.VISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");


                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<CampusFeedArray> arrayList = new ArrayList<>();
                        arrayList.clear();


                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                            arrayList.add(CourseInfo);
                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                            JSONArray jsoncategoryArray = jsonArrayData.getJSONObject(i).getJSONArray("category");
                            if (jsoncategoryArray != null && jsoncategoryArray.length() > 0) {

                                for (int j = 0; j < jsoncategoryArray.length(); j++) {
                                    String categoryid = jsoncategoryArray.getJSONObject(j).getString("id");
                                    String categoryName = jsoncategoryArray.getJSONObject(j).getString("name");
                                    CourseInfo.categoryId =categoryid;
                                    CourseInfo.categoryName =categoryName;
                                }
                            }




                            if (jsonObject.has("field_comment_attachments")){
                                JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                    URLDoc = AttacmentsFeilds.getString("value");
                                    tittle = AttacmentsFeilds.getString("title");
                                    CourseInfo.title =tittle;
                                    CourseInfo.url =URLDoc;

                                }else {

                                }
                            }
                        }



                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {

                                rv_campus_news.setVisibility(View.VISIBLE);
                                campusfeedadapter.setInitialData(arrayList);
                                // stop animating Shimmer and hide the layout
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            } else {
                                rv_campus_news.setVisibility(View.VISIBLE);
                                campusfeedadapter.removeProgress();
                                campusfeedadapter.addData(arrayList);
                                swipeContainer.setRefreshing(false);
                                // stop animating Shimmer and hide the layout
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                            }
                            isLoading = false;
                        }
                        else{
                            if (PAGE_SIZE==0){
                                // stop animating Shimmer and hide the layout
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                               // swipeContainer.setVisibility(View.GONE);
                                rv_campus_news.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                            }else
                                campusfeedadapter.removeProgress();

                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        //  Utils.showAlertFragmentDialog(activity,"Information", msg);
                        // stop animating Shimmer and hide the layout
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);
                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
            }
        }

        @Override
        protected void onCancelled() {
            collagenewfeedslist = null;
            swipeContainer.setRefreshing(false);


        }
    }






    @Override
    public void onBackPressed() {
        Utils.getSharedPreference(this).edit()
                .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_FALSE_CAMPUSFEED).apply();
        Utils.removeStringPreferences(AllCampusFeeds.this,"0");

        super.onBackPressed();
    }
}
