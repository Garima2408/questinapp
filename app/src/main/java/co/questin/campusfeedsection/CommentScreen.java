package co.questin.campusfeedsection;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.soundcloud.android.crop.Crop;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CommentScreen extends BaseAppCompactActivity implements View.OnClickListener {
    ImageView Camera,Smilely,AddGallary,AddAttachments,imagepreview,cancelPreview,profilepic;
    ImageButton button_send;

    TextView use_name;
    String post_id,comments,fullPath;
    RelativeLayout previewlayout;
    Bitmap thumbnail = null;
    String Fileimagename;
    static String strFile = null;
    private static final int REQUEST_PATH = 3;
    private static final int REQUEST_CODE = 2;
    View rootView;
    Bundle extras;
    EditText emojiconEditText;
    private File selectedFile;
    private CourseAddComments addcommentinsubject = null;
    /*new cam/gallary*/
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private String mImageFileLocation = "";
    private int SELECT_FILE = 1;
    public static final int RequestPermissionCode = 1;
    Boolean CallingCamera,CallingGallary,CallingAttachment;
    Uri imageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_screen);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        rootView = findViewById(R.id.root_view);

        Intent intent = getIntent();
        extras = intent.getExtras();
        String action = intent.getAction();
        post_id = intent.getStringExtra("POST_ID");



        emojiconEditText = findViewById(R.id.emojicon_edit_text);
        profilepic = findViewById(R.id.profilepic);
        use_name = findViewById(R.id.use_name);


        button_send =findViewById(R.id.button_send);
        Camera  =findViewById(R.id.Camera);
        Smilely  =findViewById(R.id.Smilely);
        AddGallary  =findViewById(R.id.AddGallary);
        AddAttachments  =findViewById(R.id.AddAttachments);
        imagepreview  =findViewById(R.id.imagepreview);
        cancelPreview  =findViewById(R.id.cancelPreview);
        previewlayout  =findViewById(R.id.previewlayout);



        button_send.setOnClickListener(this);
        Camera.setOnClickListener(this);
        Smilely.setOnClickListener(this);
        AddGallary.setOnClickListener(this);
        AddAttachments.setOnClickListener(this);
        cancelPreview.setOnClickListener(this);

        displayUserData();
    }


    private void displayUserData() {
        if (getUser() != null) {
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().FirstName)) {
                use_name.setText(getUser().FirstName+" "+getUser().LastName);
            }
            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().LastName)) {
            }

            if (!co.questin.utils.TextUtils.isNullOrEmpty(getUser().photo)) {

                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(profilepic);

            }
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.button_send:
                hideKeyBoard(v);
                AttempToAddComments();
                break;

            case R.id.Camera:
                hideKeyBoard(v);
                CallingCamera =true;
                CallingGallary =false;
                CallingAttachment =false;

                if(checkPermission()){
                    CallCamera();
                }

                break;

            case R.id.Smilely:

                break;


            case R.id.AddGallary:
                hideKeyBoard(v);
                imagepreview.setImageDrawable(null);
                CallingGallary =true;
                CallingAttachment =false;
                CallingCamera =false;

                if(checkPermission()){
                    galleryIntent();
                }

                break;


            case R.id.AddAttachments:
                hideKeyBoard(v);
                CallingAttachment =true;
                CallingCamera =false;
                CallingGallary =false;


                if(checkPermission()){
                    AddValueFromTheBrows();
                }
                break;

            case R.id.cancelPreview:
                hideKeyBoard(v);
                previewlayout.setVisibility(View.GONE);
                strFile ="";
                Fileimagename="";


                break;



        }
    }

    private void AddValueFromTheBrows() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] mimeTypes = {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                "text/plain",
                "application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, REQUEST_CODE);


    }


    private void CallCamera() {
        imagepreview.setImageDrawable(null);
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(CommentScreen.this, "co.questin.fileprovider",photoFile);


        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void galleryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }



    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {
            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }
        if (requestCode == REQUEST_CODE &&resultCode == RESULT_OK) {


            Uri FileURI = data.getData();
            String uriString = FileURI.toString();
            selectedFile = new File(uriString);
            try {
                fullPath = Utils.getPath(this,FileURI);
            }catch (NumberFormatException e){

                System.out.println("not a number");

            } catch (Exception e){

                System.out.println(e);
            }


            String FileName = Utils.getDataColumn(this,FileURI,null,null);

            Log.d("TAG", "onActivityResult: " + FileName + FileName );

            String path = selectedFile.toString();

            String filepath = path;

            String displayName = null;

            if (uriString.startsWith("content://")) {

                Cursor cursor = null;

                try {

                    cursor = getContentResolver().query(FileURI, null, null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {

                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

                        Fileimagename = cursor.getString(nameIndex);

                        Log.d("TAG", "onActivityResult: " + Fileimagename + filepath + fullPath);

                        /*Log.d("TAG", "onActivityResult2: " + Fileimagename);*/



                            if (fullPath != null) {



                                if (filepath.matches("(.*)providers(.*)")) {

                                    String fullfilePath = fullPath + "/" + Fileimagename;

                                    File file = new File(fullfilePath);
                                    int fulllsize = (int) file.length()/1024;


                                    if (fulllsize/1024<8) {
                                        convertFileToString(fullfilePath, Fileimagename);
                                    }else{
                                        Toast.makeText(this, "file size is greater than 8mb it can not be upload", Toast.LENGTH_SHORT).show();

                                    }

                                    Log.d("TAG", "onActivityResult3: " + fullfilePath);
                                } else if (filepath.matches("(.*)externalstorage(.*)")) {

                                    fullPath = fullPath;

                                    File file = new File(fullPath);
                                    int fulllsize = (int) file.length()/1024;

                                    if (fulllsize/1024<8)

                                    {

                                        convertFileToString(fullPath, Fileimagename);
                                        Log.d("TAG", "onActivityResult4: " + fullPath);
                                    }
                                    else{
                                        Toast.makeText(this, "file size is greater than 8mb it can not be upload", Toast.LENGTH_SHORT).show();

                                    }
                                }


                            } else {
                                Toast.makeText(this, "File not found !! Get it from internal/external storage", Toast.LENGTH_SHORT).show();
                            }


                    }


                }catch (Exception e )
                {
                    e.printStackTrace();
                }

            }




        }



    }
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setImageBitmap(thumbnail);

            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public String convertFileToString(String pathOnSdCard, String curFileName){

        File file = new File(pathOnSdCard);

        try {

            byte[] data = FileUtils.readFileToByteArray(file);//Convert any file, image or video into byte array


            // byte[] data = FileUtils.readFileToByteArray(file); //Convert any file, image or video into byte array

            strFile = Base64.encodeToString(data, Base64.NO_WRAP);

            //Convert byte array into string

            System.out.println("file in bitmap first method " + strFile);

            Fileimagename =curFileName;

            Log.d("TAG", "onActivityResult2: " + strFile);

            System.out.println("filename in bitmap first method " + Fileimagename);

            // CreateFileMsg(strFile,curFileName);

            String Extension = Fileimagename.substring(Fileimagename.lastIndexOf("."));

            imagepreview.setImageDrawable(null);
            imagepreview.setBackgroundResource(R.mipmap.file);
            previewlayout.setVisibility(View.VISIBLE);



        } catch (Exception e) {

            e.printStackTrace();

        }

        return strFile;

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();

            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }

        imagepreview.setImageBitmap(BitmapFactory.decodeFile(Fileimagename));//convert file to bitmap
        strFile = encodeImageTobase64(BitmapFactory.decodeFile(Fileimagename));//convert file to base64
        // Fileimagename ="GallaryImage.jpg";
        System.out.println("file in bitmap first method from cam " + strFile +"...");
        previewlayout.setVisibility(View.VISIBLE);
    }



    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    private void AttempToAddComments() {

        emojiconEditText.setError(null);
        if(strFile ==null){
            strFile ="";
        }
        if(Fileimagename ==null){
            Fileimagename ="";
        }
        // Store values at the time of the login attempt.
        comments = emojiconEditText.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(comments)) {
            focusView = emojiconEditText;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD COMMENTS ON SUBJECTS*/
            addcommentinsubject = new CourseAddComments();
            addcommentinsubject.execute();

        }
    }

    /*
    ADD COMMENTS ON SUBJECTS*/

    private class CourseAddComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("pid", post_id)
                    .add("nid", SessionManager.getInstance(getActivity()).getCollage().getTnid())
                    .add("subject", "this comments is added by question userer")
                    .add("comment_body", comments)
                    .add("field_comment_attachments[file]",strFile)
                    .add("field_comment_attachments[filename]",Fileimagename)
                    .add("field_comment_attachments[description]","SubjectAttach")

                    .build();


            try {
                String responseData = ApiCall.POSTHEADERCREATE(client, URLS.URL_CAMPUSFEEDADD, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CommentScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            addcommentinsubject = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        // showAlertDialog(msg);
                        emojiconEditText.setText("");
                        Utils.hideKeyBoard(CommentScreen.this, emojiconEditText);


                        strFile="";
                        Fileimagename="";
                        Check();
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));

            }
        }

        @Override
        protected void onCancelled() {
            addcommentinsubject = null;
            hideLoading();


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CommentFromMyPostReply")) {

                ReplyOnMyFeeds.CallAgainList();


            } else if (extras.getString("open").equals("CommentFromReply")) {

                ReplyOnCampusFeeds.CallAgainList();

            }
        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }


    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    }else if (CallingAttachment==true){
                        AddValueFromTheBrows();


                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}




