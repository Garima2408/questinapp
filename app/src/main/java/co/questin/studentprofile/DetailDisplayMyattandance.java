package co.questin.studentprofile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import co.questin.Event.CampusFeed;
import co.questin.R;
import co.questin.adapters.CalendarAdapter;
import co.questin.adapters.MonthlyAttandanceAdapter;
import co.questin.models.CalendarCollection;
import co.questin.models.MonthArrayAttandance;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.models.CalendarCollection.date_collection_arr;

public class DetailDisplayMyattandance extends BaseAppCompactActivity {
    String Tittle,Exampercent,Class_id;
    public ArrayList<MonthArrayAttandance> mMonthList;
    private ProgressMonthlyDatesList progressmonthdateslist = null;
    public GregorianCalendar cal_month, cal_month_copy;
    private CalendarAdapter cal_adapter;
    LinearLayout MonthcalenderDisplay,PercentTotal;
    private TextView tv_month;
    RecyclerView rv_totalmonth;
    GridView gridview;
    private RecyclerView.LayoutManager layoutManager;
    private MonthlyAttandanceAdapter monthadapter;
  //  PieChart pieChart;
    String total_perc;
    TextView ErrorText,TotalText,Text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_display_myattandance);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getActivity().getIntent().getExtras();
        Tittle = b.getString("TITTLE");
        Exampercent = b.getString("PERCENT");
        Class_id = b.getString("CLASS_ID");
        Log.d("TAG", "Upcommingdata: " + Tittle + Exampercent + Class_id);
        gridview = findViewById(R.id.gv_calendar);
        MonthcalenderDisplay= findViewById(R.id.MonthcalenderDisplay);
        ErrorText = findViewById(R.id.ErrorText);
        rv_totalmonth =findViewById(R.id.rv_totalmonth);
        PercentTotal= findViewById(R.id.PercentTotal);
        TotalText =findViewById(R.id.TotalText);
        Text =findViewById(R.id.Text);
        layoutManager = new LinearLayoutManager(this);
        rv_totalmonth.setHasFixedSize(true);
        rv_totalmonth.setLayoutManager(layoutManager);
        mMonthList=new ArrayList<>();

       /* CalendarCollection.date_collection_arr=new ArrayList<CalendarCollection>();
        CalendarCollection.date_collection_arr.add(new CalendarCollection("2018-02-01","p"));
        CalendarCollection.date_collection_arr.add(new CalendarCollection("2018-02-04","A"));
        CalendarCollection.date_collection_arr.add(new CalendarCollection("2018-02-06","p"));
        CalendarCollection.date_collection_arr.add(new CalendarCollection("2018-02-02","A"));
        CalendarCollection.date_collection_arr.add(new CalendarCollection("2018-02-11","A"));
*/



        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        cal_month.set(Calendar.DATE, cal_month.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date MonthFirstDay = cal_month.getTime();
        cal_month.set(Calendar.DATE, cal_month.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date MonthLastDay = cal_month.getTime();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM");
        final String dateMonthStart= DATE_FORMAT.format(MonthFirstDay);



        /*  cal_adapter = new CalendarAdapter(this, cal_month, date_collection_arr);*/
        tv_month = findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));


        progressmonthdateslist = new ProgressMonthlyDatesList();
        progressmonthdateslist.execute(dateMonthStart);


        ImageButton previous = findViewById(R.id.ib_prev);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        ImageButton next = findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();

            }
        });




    }








    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));



        cal_month.set(Calendar.DATE, cal_month.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date MonthFirstDay = cal_month.getTime();
        cal_month.set(Calendar.DATE, cal_month.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date MonthLastDay = cal_month.getTime();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM");
        String dateMonthStart= DATE_FORMAT.format(MonthFirstDay);



        mMonthList=new ArrayList<>();
        progressmonthdateslist = new ProgressMonthlyDatesList();
        progressmonthdateslist.execute(dateMonthStart);

    }



    private class ProgressMonthlyDatesList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(DetailDisplayMyattandance.this);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()

                    .build();


            try {

                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MYCALENDERDISPLAY+"/"+Class_id+"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id+"?month="+args[0], body);
                jsonObject = new JSONObject(responseData);
                //  Log.d("TAG", "responseData: " + responseData);



            } catch (JSONException | IOException e) {
                e.printStackTrace();
                DetailDisplayMyattandance.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.p_dialog_dismiss(DetailDisplayMyattandance.this);
                        Utils.showAlertDialog(DetailDisplayMyattandance.this, "Error", "There seems to be some problem with the Server.  Reload the page.");

                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progressmonthdateslist = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(DetailDisplayMyattandance.this);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if (data != null && data.length() > 0) {
                            MonthcalenderDisplay.setVisibility(View.VISIBLE);
                            rv_totalmonth.setVisibility(View.VISIBLE);
                            PercentTotal.setVisibility(View.VISIBLE);
                            Text.setVisibility(View.VISIBLE);
                            for (int i = 0; i < data.length(); i++) {


                                date_collection_arr = new ArrayList<CalendarCollection>();
                                String StudentName = data.getJSONObject(i).getString("student");
                                total_perc = data.getJSONObject(i).getString("total_perc");
                                String type = data.getJSONObject(i).getString("monthly_perc");

                                TotalText.setText(total_perc);
                                try {
                                JSONArray jsonArrayMonths = new JSONArray(data.getJSONObject(i).getString("monthly_perc"));
                                if (jsonArrayMonths != null && jsonArrayMonths.length() > 0) {
                                    for (int k = 0; k < jsonArrayMonths.length(); k++) {

                                        MonthArrayAttandance monthInfo = new Gson().fromJson(jsonArrayMonths.getJSONObject(k).toString(), MonthArrayAttandance.class);
                                        mMonthList.add(monthInfo);
                                        monthadapter = new MonthlyAttandanceAdapter(DetailDisplayMyattandance.this, mMonthList);
                                        rv_totalmonth.setAdapter(monthadapter);
                                        rv_totalmonth.setNestedScrollingEnabled(false);


                                        }
                                        }



                                JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("dates"));
                                if (jsonArrayDates != null && jsonArrayDates.length() > 0) {
                                    for (int j = 0; j < jsonArrayDates.length(); j++) {
                                        String startdate = jsonArrayDates.getJSONObject(j).getString("date");
                                        String attendance = jsonArrayDates.getJSONObject(j).getString("attendance");

                                        String str = startdate;
                                        String[] splited = str.split("\\s+");

                                        String split_one = splited[0];
                                        String split_second = splited[1];

                                        Log.d("TAG", "split_one: " + split_one + attendance);
                                        CalendarCollection.date_collection_arr.add(new CalendarCollection(split_one, attendance));
                                        cal_adapter = new CalendarAdapter(DetailDisplayMyattandance.this, cal_month, date_collection_arr);
                                        gridview.setAdapter(cal_adapter);


                                        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                            public void onItemClick(AdapterView<?> parent, View v,
                                                                    int position, long id) {

                                          /*  ((CalendarAdapter) parent.getAdapter()).setSelected(v, position);
                                            String selectedGridDate = CalendarAdapter.day_string
                                                    .get(position);

                                            String[] separatedTime = selectedGridDate.split("-");
                                            String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                                            int gridvalue = Integer.parseInt(gridvalueString);

                                            if ((gridvalue > 10) && (position < 8)) {
                                                setPreviousMonth();
                                                refreshCalendar();
                                            } else if ((gridvalue < 7) && (position > 28)) {
                                                setNextMonth();
                                                refreshCalendar();
                                            }*/
                                            /*((CalendarAdapter) parent.getAdapter()).setSelected(v,position);


                                            ((CalendarAdapter) parent.getAdapter()).getPositionList(selectedGridDate, DetailDisplayMyattandance.this);*/
                                            }

                                        });

                                    }

                                } else {

                                }


                                } catch (JSONException e) {

                                    // MonthcalenderDisplay.setVisibility(View.GONE);
                                    rv_totalmonth.setVisibility(View.GONE);
                                    PercentTotal.setVisibility(View.GONE);
                                    Text.setVisibility(View.GONE);
                                    cal_adapter = new CalendarAdapter(DetailDisplayMyattandance.this, cal_month, date_collection_arr);
                                    gridview.setAdapter(cal_adapter);

                                    }
                            }
                        }else {
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Details Found");

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(DetailDisplayMyattandance.this);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(DetailDisplayMyattandance.this);
                Utils.showAlertDialog(DetailDisplayMyattandance.this, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            progressmonthdateslist = null;
            Utils.p_dialog_dismiss(DetailDisplayMyattandance.this);
            Utils.showAlertDialog(DetailDisplayMyattandance.this, "Error", "There seems to be some problem with the Server. Try again later.");

        }

    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new CampusFeed(6));
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                EventBus.getDefault().post(new CampusFeed(6));
                finish();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }




}

