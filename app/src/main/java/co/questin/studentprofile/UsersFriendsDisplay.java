package co.questin.studentprofile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.UsesFriendsDisplayAdapter;
import co.questin.models.AllFriends;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.OkHttpClient;

public class UsersFriendsDisplay extends BaseAppCompactActivity {
    RecyclerView rv_myfriendslist;
    public ArrayList<AllFriends> mFriendsList;
    private UsesFriendsDisplayAdapter mMyfriendadapter;
    private RecyclerView.LayoutManager layoutManager;
    private MyFriendsListDisplay myfriendslist = null;
    private ProgressBar spinner;
  public static   String CollageMail;
    public String MainRole,CollageId,  CollageName,CollageRole,CollageBranch,CollageEnrollment,Collagedepartment,CollageStudentName;
    TextView ErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_friends_display);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Intent intent = getIntent();
        CollageMail = intent.getStringExtra("EMAIL");


        mFriendsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        rv_myfriendslist =findViewById(R.id.rv_myfriendslist);
        spinner= findViewById(R.id.progressBar);
        ErrorText =findViewById(R.id.ErrorText);
        rv_myfriendslist.setHasFixedSize(true);
        rv_myfriendslist.setLayoutManager(layoutManager);


        myfriendslist = new MyFriendsListDisplay();
        myfriendslist.execute();


    }
    private class MyFriendsListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYFRIENDS+"?"+"email="+CollageMail+"&offset="+0+"&limit=200");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                  runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                         hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });



            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            myfriendslist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                AllFriends FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);


                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                if (jsonObject.has("college")) {
                                    JSONObject CampusData = jsonObject.getJSONObject("college");

                                    if (CampusData != null && CampusData.length() > 0 ) {
                                        String MainRole = CampusData.getString("role");

                                        if (MainRole.matches("13")) {
                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageBranch = CampusData.getString("field_branch");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId = CollageId;
                                            FriendInfo.CollageName = CollageName;
                                            FriendInfo.CollageRole = CollageRole;
                                            FriendInfo.CollageBranch = CollageBranch;
                                            FriendInfo.MainRole = MainRole;


                                        } else if (MainRole.matches("14")) {


                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            Collagedepartment = CampusData.getString("field_department");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId = CollageId;
                                            FriendInfo.CollageName = CollageName;
                                            FriendInfo.CollageRole = CollageRole;
                                            FriendInfo.Collagedepartment = Collagedepartment;
                                            FriendInfo.MainRole = MainRole;


                                        } else if (MainRole.matches("15")) {


                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageBranch = CampusData.getString("field_branch");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId = CollageId;
                                            FriendInfo.CollageName = CollageName;
                                            FriendInfo.CollageRole = CollageRole;
                                            FriendInfo.CollageBranch = CollageBranch;
                                            FriendInfo.MainRole = MainRole;


                                        } else if (MainRole.matches("16")) {

                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageStudentName = CampusData.getString("field_student_name");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId = CollageId;
                                            FriendInfo.CollageName = CollageName;
                                            FriendInfo.CollageRole = CollageRole;
                                            FriendInfo.CollageStudentName = CollageStudentName;
                                            FriendInfo.MainRole = MainRole;


                                        } else if (MainRole.matches("3")) {


                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            Collagedepartment = CampusData.getString("field_department");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId = CollageId;
                                            FriendInfo.CollageName = CollageName;
                                            FriendInfo.CollageRole = CollageRole;
                                            FriendInfo.Collagedepartment = Collagedepartment;
                                            FriendInfo.MainRole = MainRole;


                                        }


                                    }


                                }

                                mFriendsList.add(FriendInfo);
                                mMyfriendadapter = new UsesFriendsDisplayAdapter(UsersFriendsDisplay.this, mFriendsList);
                                rv_myfriendslist.setAdapter(mMyfriendadapter);



                            }
                        }else {
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("you have no friends");

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {

                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));

                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            myfriendslist = null;
            hideLoading();


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        finish();
    }
}


