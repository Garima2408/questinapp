package co.questin.studentprofile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.MyGroupAdapters;
import co.questin.clubandsociety.GroupNSocieties;
import co.questin.models.MyGroupItem;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.OkHttpClient;

public class MyGroups extends BaseAppCompactActivity {
    public  ArrayList<MyGroupItem> mgroups;

    private RecyclerView.LayoutManager layoutManager;
    private MyGroupAdapters groupadapter;
    private MyGroupsListDisplay mygrouplistdisplay = null;

    RecyclerView mRecyclerView;
    TextView text_view,text_view2;
    RelativeLayout relative;
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.questin.R.layout.activity_my_groups);

        mgroups=new ArrayList<>();

        mRecyclerView = findViewById(co.questin.R.id.rv_myGroups);
        mRecyclerView.setHasFixedSize(true);
        groupadapter=new MyGroupAdapters(this,mgroups);

        text_view = findViewById(R.id.text_view);
        text_view2 = findViewById(R.id.text_view2);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        relative = findViewById(R.id.relative);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);

        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(groupadapter);

        mygrouplistdisplay = new MyGroupsListDisplay();
        mygrouplistdisplay.execute();

        ArrayList<MyGroupItem> mgroups = null;

        relative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MyGroups.this, GroupNSocieties.class);
                startActivity(intent);


            }
        });



    }


    private class MyGroupsListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYGROUP+"?"+"uid="+ SessionManager.getInstance(getActivity()).getUser().userprofile_id+"&"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                MyGroups.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            mygrouplistdisplay = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                MyGroupItem GoalInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyGroupItem.class);
                                mgroups.add(GoalInfo);
                                groupadapter = new MyGroupAdapters(ApiCall.getContext(), mgroups);
                                mRecyclerView.setAdapter(groupadapter);
                                text_view.setVisibility(View.GONE);
                                text_view2.setVisibility(View.GONE);

                            }

                        }else{


                            text_view.setVisibility(View.VISIBLE);
                            text_view2.setVisibility(View.VISIBLE);
                            materialDesignFAM.setVisibility(View.VISIBLE);

                        }




                        } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            mygrouplistdisplay = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}









