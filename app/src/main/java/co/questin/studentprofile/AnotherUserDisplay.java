package co.questin.studentprofile;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.AnoUserProfileCampusFeedsAdapters;
import co.questin.chat.ChatActivity;
import co.questin.chat.MyServerChatActivity;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.models.CampusFeedArray;
import co.questin.models.chat.ConversationModel;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class AnotherUserDisplay extends BaseAppCompactActivity implements View.OnClickListener {
    ImageView mainheader,imageProfile,backone;
    LinearLayout thredHeader,FriendDisplayLayout;
    static String DisplayUserUid,DisplayClgId,college_email,rid,DetailClgId,DetailUserId;
    TextView tv_username,tv_useremail,PostNumber,friendsNumber,SendRequest,SendMessage,tv_userbatch,tv_usercollagename;
    private ProgressUserDisplayProfile userdisplayAuthTask = null;
    String  username,picture,is_friendMain,is_friendpending,email,password,urlsofphoto,CollagePostcount,urlsofWallpic, MainRole,CollageId,CollageName,CollageImage,lati,longi,CollageBatch,CollageBranch,Collagedepartment,CollageRole,CollageLogo,CollageEmail,CollageDesignation,CollageEnrollment,CollageStudentName;
    private QuestinSQLiteHelper questinSQLiteHelper;
    private SendInviteToFriends sendinvitetofriends = null;
    static RecyclerView rv_mycampus_news;
    public static ArrayList<CampusFeedArray> mcampusfeedList;
    private static AnoUserProfileCampusFeedsAdapters campusfeedadapter;
    private static MyCollageNewFeedsDisplay mycollagenewfeedslist = null;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    static String tittle,URLDoc;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private static ProgressBar spinner;
    private  DeleteFriend deleteFriend=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another_user_display);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        Bundle b = getIntent().getExtras();
        DisplayUserUid = b.getString("USERPROFILE_ID");
        DisplayClgId = b.getString("COLLAGE_ID");
        NestedScrollView nsv =  findViewById(R.id.nestedScroll);
        mainheader = findViewById(R.id.mainheader);
        imageProfile = findViewById(R.id.imageProfile);
        tv_usercollagename = findViewById(R.id.tv_usercollagename);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        PostNumber = findViewById(R.id.PostNumber);
        friendsNumber = findViewById(R.id.friendsNumber);
        SendRequest = findViewById(R.id.SendRequest);
        SendMessage = findViewById(R.id.SendMessage);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        thredHeader = findViewById(R.id.thredHeader);
        backone = findViewById(R.id.backone);
        spinner = findViewById(R.id.progressBar);
        FriendDisplayLayout = findViewById(R.id.FriendDisplayLayout);
        mcampusfeedList = new ArrayList<>();
        activity = getActivity();
        layoutManager = new LinearLayoutManager(this);
        rv_mycampus_news = findViewById(R.id.rv_mycampus_news);
        rv_mycampus_news.setHasFixedSize(true);
        rv_mycampus_news.setLayoutManager(layoutManager);



        campusfeedadapter = new AnoUserProfileCampusFeedsAdapters(rv_mycampus_news, mcampusfeedList, activity);
        rv_mycampus_news.setAdapter(campusfeedadapter);
        rv_mycampus_news.setNestedScrollingEnabled(false);


        SendMessage.setOnClickListener(this);
      //  SendRequest.setOnClickListener(this);
        backone.setOnClickListener(this);
        FriendDisplayLayout.setOnClickListener(this);

        userdisplayAuthTask = new ProgressUserDisplayProfile();
        userdisplayAuthTask.execute(DisplayClgId,DisplayUserUid);

    }






    public static void RefreshWorkedFromMycampusAdapter() {
        PAGE_SIZE=0;
        isLoading=false;
        spinner.setVisibility(View.VISIBLE);
        mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
        mycollagenewfeedslist.execute(DisplayUserUid);



    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.SendRequest:
                hideKeyBoard(v);
                if (is_friendpending!=null){
                    if(is_friendpending.matches("1")){

                    }else {
                        SendFriendRequest();

                    }
                }



                break;

            case R.id.backone:
                PAGE_SIZE=0;
                isLoading=false;
                ActivityCompat.finishAfterTransition(this);


                break;

            case R.id.SendMessage:
                hideKeyBoard(v);

                if(is_friendMain.matches("1")){
                    ConversationModel conversationModel = new ConversationModel()
                            .setSenderId(DisplayUserUid)
                            .setName(username)
                            .setDpUrl(picture);

                    Intent backIntent = new Intent(AnotherUserDisplay.this, ChatActivity.class)
                            .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                            .putExtra(Constants.CONVERSATION_MODEL, conversationModel);
                    startActivity(backIntent);
                    AnotherUserDisplay.this.finish();

                }else if(is_friendMain.matches("")){

                    ConversationModel conversationModel = new ConversationModel()
                            .setSenderId(DisplayUserUid)
                            .setName(username)
                            .setDpUrl(picture);

                    Intent backIntent = new Intent(AnotherUserDisplay.this, MyServerChatActivity.class)
                            .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                            .putExtra(Constants.CONVERSATION_MODEL, conversationModel)

                            .putExtra("USER_ID", DisplayUserUid)
                            .putExtra("USER_NAME",username)
                            .putExtra("PICTURE", picture);
                    startActivity(backIntent);
                    AnotherUserDisplay.this.finish();


                }



                break;



            case R.id.FriendDisplayLayout:
                hideKeyBoard(v);

                Intent backIntent = new Intent(AnotherUserDisplay.this, UsersFriendsDisplay.class)
                        .putExtra("EMAIL",college_email);

                startActivity(backIntent);
                break;
        }
    }

    private void SendFriendRequest() {

        sendinvitetofriends = new SendInviteToFriends();
        sendinvitetofriends.execute(DisplayUserUid);

    }

    public void DeleteFriendRequest(){
        deleteFriend = new DeleteFriend();
        deleteFriend.execute();
    }


    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        ActivityCompat.finishAfterTransition(this);
    }

    private class ProgressUserDisplayProfile extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_USERPROFILEDISPLAY+"?"+"cid="+ args[0]+"&"+"uid="+args[1]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AnotherUserDisplay.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            userdisplayAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                username = jsonArrayData.getJSONObject(i).getString("first_name") + " " + jsonArrayData.getJSONObject(i).getString("last_name");
                                String wall_picture = jsonArrayData.getJSONObject(i).getString("wall_picture");
                                picture = jsonArrayData.getJSONObject(i).getString("picture");
                                String type = jsonArrayData.getJSONObject(i).getString("type");
                                // String course = jsonArrayData.getJSONObject(i).getString("course");
                                String batch = jsonArrayData.getJSONObject(i).getString("batch");
                                String branch = jsonArrayData.getJSONObject(i).getString("branch");
                                String enrollment_number = jsonArrayData.getJSONObject(i).getString("enrollment_number");
                                college_email = jsonArrayData.getJSONObject(i).getString("college_email");
                                String department = jsonArrayData.getJSONObject(i).getString("department");
                                String faculty_designation = jsonArrayData.getJSONObject(i).getString("faculty_designation");
                                String parents_of = jsonArrayData.getJSONObject(i).getString("parents_of");
                                is_friendMain = jsonArrayData.getJSONObject(i).getString("is_friend");
                                is_friendpending = jsonArrayData.getJSONObject(i).getString("is_friend_request_pending");


                                String friends_count = jsonArrayData.getJSONObject(i).getString("friends_count");
                                String post_count = jsonArrayData.getJSONObject(i).getString("post_count");
                                DetailUserId = jsonArrayData.getJSONObject(i).getString("uid");
                                String college_title = jsonArrayData.getJSONObject(i).getString("college_title");
                                DetailClgId   = jsonArrayData.getJSONObject(i).getString("group_id");
                                rid = jsonArrayData.getJSONObject(i).getString("rid");




                                tv_username.setText(username);
                                tv_useremail.setText(type +" "+batch);
                                tv_userbatch.setText(branch);
                                tv_usercollagename.setText(college_title);


                                if(post_count.matches("")){
                                    PostNumber.setText("0");

                                }else {
                                    PostNumber.setText(post_count);

                                }
                                if(friends_count.matches("")){
                                    friendsNumber.setText("0");

                                }else {
                                    friendsNumber.setText(friends_count);

                                }


                                if(DetailClgId.matches(SessionManager.getInstance(getActivity()).getCollage().getTnid())){

                                    mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
                                    mycollagenewfeedslist.execute(DisplayUserUid);
                                    inItView();


                                }

                                if(is_friendpending.matches("1")){
                                    SendRequest.setText("Request Sent");
                                    SendRequest.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ic_user_requestsend, 0, 0, 0);
                                   // SendRequest.setOnClickListener(null);
                                    rv_mycampus_news.setVisibility(View.GONE);
                                    spinner.setVisibility(View.GONE);
                                    thredHeader.setVisibility(View.VISIBLE);

                                }else {

                                    SendRequest.setText("Add Friend");
                                    rv_mycampus_news.setVisibility(View.GONE);
                                    spinner.setVisibility(View.GONE);
                                    thredHeader.setVisibility(View.VISIBLE);
                                    SendRequest.setOnClickListener(AnotherUserDisplay.this);

                                }

                                if(is_friendMain.matches("1")){
                                    SendRequest.setText("Friend");
                                    SendRequest.setCompoundDrawablesWithIntrinsicBounds( R.mipmap.ic_user_checked, 0, 0, 0);
                                    thredHeader.setVisibility(View.VISIBLE);
                                    SendRequest.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showAlertFragmentDialog(activity,"Unfriend","remove friend");

                                           /* SendRequest.setText("Add Friend");
                                            thredHeader.setVisibility(View.VISIBLE);*/
                                        }
                                    });


                                }else if (is_friendMain.matches("")){

                                    if(DetailUserId.equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())){
                                        thredHeader.setVisibility(View.GONE);
                                        mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
                                        mycollagenewfeedslist.execute(DisplayUserUid);
                                        inItView();

                                    }else if (DetailUserId.equals(SessionManager.getInstance(getActivity()).getUser().getParentUid())){



                                        thredHeader.setVisibility(View.GONE);
                                        mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
                                        mycollagenewfeedslist.execute(DisplayUserUid);
                                        inItView();
                                       /* SendRequest.setText("Add Friend");
                                        rv_mycampus_news.setVisibility(View.GONE);
                                        spinner.setVisibility(View.GONE);

                                        thredHeader.setVisibility(View.VISIBLE);*/
                                    }
                                }

                                try{
                                    if(picture != null && picture.length() > 0 ) {

                                        Glide.with(AnotherUserDisplay.this).load(picture)
                                                .placeholder(R.mipmap.place_holder).dontAnimate()
                                                .fitCenter().into(imageProfile);

                                    }else {
                                        imageProfile.setImageResource(R.mipmap.place_holder);

                                    }

                                    if(wall_picture != null && wall_picture.length() > 0 ) {

                                        Glide.with(AnotherUserDisplay.this).load(wall_picture)
                                                .placeholder(R.mipmap.header_main).dontAnimate()
                                                .fitCenter().into(mainheader);

                                    }else {
                                        mainheader.setImageResource(R.mipmap.header_main);

                                    }


                                }catch (Exception e){

                                }




                            }
                        }else {
                            try{
                                AlertDialog.Builder builder = new AlertDialog.Builder(AnotherUserDisplay.this);
                                builder.setMessage("User is not member of any college")
                                        .setCancelable(false)
                                        .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                finish();
                                                dialog.dismiss();

                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }catch (Exception e){

                            }

                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {



                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));
                hideLoading();

            }
        }





        @Override
        protected void onCancelled() {
            showAlertDialog(getString(R.string.error_responce));
            userdisplayAuthTask = null;
            hideLoading();


        }
    }



    private class DeleteFriend extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;


            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("rid", rid)
                    .add("reason", "delete this friend" )
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_REMOVE_MYFRIEND , body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();


                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetofriends = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        //  CallTheMainListAgain();
                        startActivity(getIntent());
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetofriends = null;
            hideLoading();
            showAlertDialog(getString(R.string.error_responce));

        }
    }





     /*
    SEND FRIEND REQUEST*/

    private class SendInviteToFriends extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()

                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_SENDINFITE+"/"+args[0]+"/"+"relationship/friends", body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                hideLoading();
                showAlertDialog(getString(R.string.error_responce));




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetofriends = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        startActivity(getIntent());
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetofriends = null;
            hideLoading();
            showAlertDialog(getString(R.string.error_responce));


        }
    }



    private void inItView(){

        rv_mycampus_news.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager)layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;
                    campusfeedadapter.addProgress();
                    mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
                    mycollagenewfeedslist.execute(DisplayUserUid);

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }






    //College Feed Display

    private static class MyCollageNewFeedsDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYCAMPUSFEED+"?"+"cid="+ SessionManager.getInstance(activity).getCollage().getTnid()+"&uid="+args[0]+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

                campusfeedadapter.removeProgress();
                isLoading=false;
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            mycollagenewfeedslist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    rv_mycampus_news.setVisibility(View.VISIBLE);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        ArrayList<CampusFeedArray> arrayList = new ArrayList<>();


                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                            arrayList.add(CourseInfo);

                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                            if (jsonObject.has("field_comment_attachments")){
                                JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                    URLDoc = AttacmentsFeilds.getString("value");
                                    tittle = AttacmentsFeilds.getString("title");
                                    Log.d("TAG", "titleurl: " + tittle +URLDoc);
                                    CourseInfo.title =tittle;
                                    CourseInfo.url =URLDoc;


                                }
                            }

                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_mycampus_news.setVisibility(View.VISIBLE);
                                campusfeedadapter.setInitialData(arrayList);



                            } else {
                                isLoading = false;
                                campusfeedadapter.removeProgress();
                                spinner.setVisibility(View.INVISIBLE);

                                campusfeedadapter.addData(arrayList);



                            }
                        }

                        else{
                            if (PAGE_SIZE==0){
                                spinner.setVisibility(View.GONE);
                                rv_mycampus_news.setVisibility(View.GONE);



                            }else
                                campusfeedadapter.removeProgress();


                        }



                       /* if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {

                                rv_mycampus_news.setVisibility(View.VISIBLE);
                                campusfeedadapter.setInitialData(arrayList);

                            } else {
                                isLoading = false;
                                campusfeedadapter.removeProgress();
                                spinner.setVisibility(View.INVISIBLE);
                                campusfeedadapter.addData(arrayList);

                            }
                            isLoading =false;

                        }else{

                            if (PAGE_SIZE==0){

                                spinner.setVisibility(View.GONE);
                                rv_mycampus_news.setVisibility(View.GONE);

                            }else
                                campusfeedadapter.removeProgress();

                        }
*/


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        Utils.showAlertDialog(activity,"Error",msg );


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            mycollagenewfeedslist = null;
            spinner.setVisibility(View.GONE);
            Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


        }
    }

    public void showAlertFragmentDialog(Context context, String title, String message) {
        AlertDialog.Builder builder  = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message).setCancelable(false);
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {


                DeleteFriendRequest();
                // CallTheMainListAgain();


            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int arg1) {

                // finish used for destroyed DialogInterface
                dialogInterface.dismiss();

            }
        });


        builder.setCancelable(true);

        builder.show();



    }



}

