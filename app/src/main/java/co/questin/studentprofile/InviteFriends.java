package co.questin.studentprofile;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.InviteFirendsAdapter;
import co.questin.adapters.SearchedInviteFirendsAdapter;
import co.questin.models.AllUser;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.PaginationScrollListener;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class InviteFriends extends Fragment {
    static EditText searchEmail;
    String SearchedName;
    public static String MainRole,CollageId, CollageName,CollageRole,CollageBranch,CollageEnrollment,Collagedepartment,CollageStudentName;

    static RecyclerView rv_myfriends_INVITElist;
    static RecyclerView SearchedInviteList;
    public static ArrayList<AllUser> mFriendsInviteList;
    public ArrayList<AllUser> mFriendsInviteSearchList;
    private static InviteFirendsAdapter mMyfriendinviteadapter;
    private SearchedInviteFirendsAdapter searchedadapter;
    private RecyclerView.LayoutManager layoutManager, mLayoutManager;
    private static MyFriendsInviteListDisplay myfriendsinvitelist = null;
    private SendInviteListDisplay sendinvitelist = null;
    static Activity activity;
    Activity activityhide;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    public InviteFriends() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_invite_friends, container, false);
        activity = (Activity) view.getContext();
        activityhide = (Activity) view.getContext();
        searchEmail = view.findViewById(R.id.searchEmail);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mFriendsInviteList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        mLayoutManager = new LinearLayoutManager(getContext());
        rv_myfriends_INVITElist = view.findViewById(R.id.rv_myfriends_INVITElist);
        rv_myfriends_INVITElist.setHasFixedSize(true);
        rv_myfriends_INVITElist.setLayoutManager(layoutManager);
        SearchedInviteList = view.findViewById(R.id.SearchedInviteList);
        SearchedInviteList.setHasFixedSize(true);
        SearchedInviteList.setLayoutManager(mLayoutManager);
        spinner= view.findViewById(R.id.progressBar);

        mMyfriendinviteadapter = new InviteFirendsAdapter(rv_myfriends_INVITElist,activity, mFriendsInviteList );
        rv_myfriends_INVITElist.setAdapter(mMyfriendinviteadapter);

        myfriendsinvitelist = new MyFriendsInviteListDisplay();
        myfriendsinvitelist.execute();

        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    addnewfriends();


                    return true;
                }
                return false;
            }
        });




        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        RefreshWorked();


                    }
                }, 1000);
            }
        });

        inItView();


        return view;
    }
  /*  public static void CalledFromInviteAdapter() {
        mFriendsInviteList.clear();
        searchEmail.setText("");
        myfriendsinvitelist = new MyFriendsInviteListDisplay();
        myfriendsinvitelist.execute();



    }*/


    private void addnewfriends() {
        rv_myfriends_INVITElist.setVisibility(View.GONE);
        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            //showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD COMMENTS ON SUBJECTS*/
            sendinvitelist = new SendInviteListDisplay();
            sendinvitelist.execute();
            rv_myfriends_INVITElist.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);
            activityhide.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        }
    }




    private void inItView(){


        swipeContainer.setRefreshing(true);

        rv_myfriends_INVITElist.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=10;
                    mMyfriendinviteadapter .addProgress();

                    myfriendsinvitelist = new MyFriendsInviteListDisplay();
                    myfriendsinvitelist.execute();

                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    public static void RefreshWorked() {

        PAGE_SIZE=0;
        isLoading=false;
        swipeContainer.setRefreshing(true);
        myfriendsinvitelist = new MyFriendsInviteListDisplay();
        myfriendsinvitelist.execute();
        SearchedInviteList.setVisibility(View.GONE);
        searchEmail.setText("");


    }

    private class SendInviteListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Utils.p_dialog(activity);
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SEARCHBYEMAIL+SearchedName+"&offset="+PAGE_SIZE+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });



            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitelist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        mFriendsInviteSearchList  = new ArrayList<>();
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {


                                AllUser FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllUser.class);

                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                if (jsonObject.has("college")) {
                                    JSONObject CampusData = jsonObject.getJSONObject("college");

                                    if (CampusData != null && CampusData.length() > 0 ){
                                        MainRole = CampusData.getString("role");

                                        if (MainRole.matches("13")) {
                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageBranch = CampusData.getString("field_branch");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.CollageBranch =CollageBranch;
                                            FriendInfo.MainRole =MainRole;


                                        } else if (MainRole.matches("14")) {

                                            CollageId = CampusData.getString("tnid");

                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            Collagedepartment = CampusData.getString("field_department");


                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.Collagedepartment =Collagedepartment;
                                            FriendInfo.MainRole =MainRole;


                                        } else if (MainRole.matches("15")) {


                                            CollageId = CampusData.getString("tnid");
                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageBranch = CampusData.getString("field_branch");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.CollageBranch =CollageBranch;
                                            FriendInfo.MainRole =MainRole;



                                        } else if (MainRole.matches("16")) {

                                            CollageId = CampusData.getString("tnid");
                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageStudentName = CampusData.getString("field_student_name");
                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.CollageStudentName =CollageStudentName;
                                            FriendInfo.MainRole =MainRole;


                                        }
                                        else if (MainRole.matches("3")) {


                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            Collagedepartment = CampusData.getString("field_department");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.Collagedepartment =Collagedepartment;
                                            FriendInfo.MainRole =MainRole;


                                        }
                                    }else {

                                    }
                                }else {

                                }



                                mFriendsInviteSearchList.add(FriendInfo);
                                searchedadapter = new SearchedInviteFirendsAdapter(activity, mFriendsInviteSearchList);
                                SearchedInviteList.setAdapter(searchedadapter);
                                SearchedInviteList.setVisibility(View.VISIBLE);
                            }
                        }else{
                            try{
                                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                builder.setMessage(getString(co.questin.R.string.nouser))
                                        .setCancelable(false)
                                        .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {


                                                dialog.dismiss();

                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();

                            }catch (Exception e){

                            }


                        }







                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertFragmentDialog(activity, "Error", msg);
                        spinner.setVisibility(View.INVISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            sendinvitelist = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }








    private static class MyFriendsInviteListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ALLUSERS+"?offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);

                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);

                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

                mMyfriendinviteadapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            myfriendsinvitelist = null;
            try {
                if (responce != null) {
                    rv_myfriends_INVITElist.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);

                    //    spinner.setVisibility(View.INVISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<AllUser> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {


                            AllUser FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllUser.class);

                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                            if (jsonObject.has("college")) {
                                JSONObject CampusData = jsonObject.getJSONObject("college");

                                if (CampusData != null && CampusData.length() > 0 ){
                                    String MainRole = CampusData.getString("role");

                                    if (MainRole.matches("13")) {
                                        CollageName = CampusData.getString("title");
                                        CollageRole = CampusData.getString("field_i_am_a");
                                        CollageBranch = CampusData.getString("field_branch");
                                        CollageId = CampusData.getString("tnid");

                                        FriendInfo.CollageId =CollageId;
                                        FriendInfo.CollageName =CollageName;
                                        FriendInfo.CollageRole =CollageRole;
                                        FriendInfo.CollageBranch =CollageBranch;
                                        FriendInfo.MainRole =MainRole;


                                    } else if (MainRole.matches("14")) {


                                        CollageName = CampusData.getString("title");
                                        CollageRole = CampusData.getString("field_i_am_a");
                                        Collagedepartment = CampusData.getString("field_department");
                                        CollageId = CampusData.getString("tnid");

                                        FriendInfo.CollageId =CollageId;

                                        FriendInfo.CollageName =CollageName;
                                        FriendInfo.CollageRole =CollageRole;
                                        FriendInfo.Collagedepartment =Collagedepartment;
                                        FriendInfo.MainRole =MainRole;


                                    } else if (MainRole.matches("15")) {


                                        CollageName = CampusData.getString("title");
                                        CollageRole = CampusData.getString("field_i_am_a");
                                        CollageBranch = CampusData.getString("field_branch");
                                        CollageId = CampusData.getString("tnid");

                                        FriendInfo.CollageId =CollageId;
                                        FriendInfo.CollageName =CollageName;
                                        FriendInfo.CollageRole =CollageRole;
                                        FriendInfo.CollageBranch =CollageBranch;
                                        FriendInfo.MainRole =MainRole;



                                    } else if (MainRole.matches("16")) {

                                        CollageName = CampusData.getString("title");
                                        CollageRole = CampusData.getString("field_i_am_a");
                                        CollageStudentName = CampusData.getString("field_student_name");
                                        CollageId = CampusData.getString("tnid");

                                        FriendInfo.CollageId =CollageId;
                                        FriendInfo.CollageName =CollageName;
                                        FriendInfo.CollageRole =CollageRole;
                                        FriendInfo.CollageStudentName =CollageStudentName;
                                        FriendInfo.MainRole =MainRole;


                                    } else if (MainRole.matches("3")) {


                                        CollageName = CampusData.getString("title");
                                        CollageRole = CampusData.getString("field_i_am_a");
                                        Collagedepartment = CampusData.getString("field_department");
                                        CollageId = CampusData.getString("tnid");

                                        FriendInfo.CollageId =CollageId;
                                        FriendInfo.CollageName =CollageName;
                                        FriendInfo.CollageRole =CollageRole;
                                        FriendInfo.Collagedepartment =Collagedepartment;
                                        FriendInfo.MainRole =MainRole;


                                    }



                                }else {

                                }



                            }else {

                            }

                            arrayList.add(FriendInfo);


                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_myfriends_INVITElist.setVisibility(View.VISIBLE);
                                mMyfriendinviteadapter.setInitialData(arrayList);

                            } else {
                                isLoading = false;
                                mMyfriendinviteadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                mMyfriendinviteadapter.addData(arrayList);


                            }
                        }else{
                            mMyfriendinviteadapter.removeProgress();
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertFragmentDialog(activity, "Error", msg);



                    }
                }
            } catch (JSONException e) {

            }
        }

        @Override
        protected void onCancelled() {
            myfriendsinvitelist = null;




        }
    }



    @Override
    public void onDestroy() {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        isLoading=false;
        PAGE_SIZE=0;
        super.onDestroy();
    }
}
