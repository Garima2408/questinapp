package co.questin.calendersection;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import co.questin.R;

public class CalenderDash extends AppCompatActivity {
    TextView upcoming,completed,testexam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_dash);
        upcoming = findViewById(R.id.upcoming);
        completed = findViewById(R.id.completed);
        testexam = findViewById(R.id.testexam);

        upcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(CalenderDash.this,CalenderDues.class);
                startActivity(i);

            }
        });

        completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CalenderDash.this,CompletedDos.class);
                startActivity(i);
            }
        });

        testexam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }
}
