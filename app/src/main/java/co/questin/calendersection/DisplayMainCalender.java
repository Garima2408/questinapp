package co.questin.calendersection;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import co.questin.R;
import co.questin.adapters.DisplayMainCalenderAdapter;
import co.questin.models.CalendarCollection;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.questin.models.CalendarCollection.date_collection_arr;

public class DisplayMainCalender extends BaseAppCompactActivity {
    String Tittle,Exampercent,Class_id,split_second;
    private ProgressMonthlyDatesList progressmonthdateslist = null;
    public GregorianCalendar cal_month, cal_month_copy;
    private DisplayMainCalenderAdapter cal_adapter;
    private TextView tv_month;
    GridView gridview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_main_calender);
        gridview = findViewById(R.id.gv_calendar);


        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
      /*  cal_adapter = new CalendarAdapter(this, cal_month, date_collection_arr);*/
        tv_month = findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        date_collection_arr = new ArrayList<CalendarCollection>();
        progressmonthdateslist = new ProgressMonthlyDatesList();
        progressmonthdateslist.execute();

        ImageButton previous = findViewById(R.id.ib_prev);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        ImageButton next = findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();

            }
        });





    }




    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }



    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }



    private class ProgressMonthlyDatesList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(DisplayMainCalender.this);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();


            try {

                String responseData = ApiCall.POSTHEADER(client, URLS.URL_CALENDERMONTLYDATES+"?"+"month=2018-02&type=month", body);
                jsonObject = new JSONObject(responseData);
                //  Log.d("TAG", "responseData: " + responseData);



            } catch (JSONException | IOException e) {
                e.printStackTrace();
                DisplayMainCalender.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.p_dialog_dismiss(DisplayMainCalender.this);
                        Utils.showAlertDialog(DisplayMainCalender.this, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progressmonthdateslist = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(DisplayMainCalender.this);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i = 0; i < data.length(); i++) {





                            String Eventtittle = data.getJSONObject(i).getString("title");
                            String nid = data.getJSONObject(i).getString("nid");
                            String type = data.getJSONObject(i).getString("type");
                            String body = data.getJSONObject(i).getString("body");


                            Log.d("TAG", "type: " + type);
                            if (type.contains("classes")) {
                                JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                for (int j = 0; j < jsonArrayDates.length(); j++) {
                                    String startdate = jsonArrayDates.getJSONObject(j).getString("startDate");
                                    String EventTimeStart = jsonArrayDates.getJSONObject(j).getString("startTime");
                                    String endDate = jsonArrayDates.getJSONObject(j).getString("endDate");
                                    String EventtimeEnd = jsonArrayDates.getJSONObject(j).getString("endTime");

                                    SimpleDateFormat sdf=new SimpleDateFormat("MMM-dd-yyyy");
                                    Date dateclass=sdf.parse(startdate);
                                    sdf=new SimpleDateFormat("yyyy-MM-dd");
                                    System.out.println(sdf.format(dateclass));
                                    split_second =sdf.format(dateclass);

                                    Log.d("TAG", "split_one: " + split_second + Eventtittle);
                                    CalendarCollection.date_collection_arr.add(new CalendarCollection(split_second,Eventtittle));
                                    cal_adapter = new DisplayMainCalenderAdapter(DisplayMainCalender.this, cal_month, date_collection_arr);
                                    gridview.setAdapter(cal_adapter);


                                    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                        public void onItemClick(AdapterView<?> parent, View v,
                                                                int position, long id) {

                                            ((DisplayMainCalenderAdapter) parent.getAdapter()).setSelected(v,position);
                                            String selectedGridDate = DisplayMainCalenderAdapter.day_string
                                                    .get(position);

                                            String[] separatedTime = selectedGridDate.split("-");
                                            String gridvalueString = separatedTime[2].replaceFirst("^0*","");
                                            int gridvalue = Integer.parseInt(gridvalueString);

                                            if ((gridvalue > 10) && (position < 8)) {
                                                setPreviousMonth();
                                                refreshCalendar();
                                            } else if ((gridvalue < 7) && (position > 28)) {
                                                setNextMonth();
                                                refreshCalendar();
                                            }
                                            ((DisplayMainCalenderAdapter) parent.getAdapter()).setSelected(v,position);


                                            ((DisplayMainCalenderAdapter) parent.getAdapter()).getPositionList(selectedGridDate, DisplayMainCalender.this);
                                        }

                                    });




                            }


                            } else if (type.contains("assignment")) {
                                JSONArray jsonArrayAssignments = new JSONArray(data.getJSONObject(i).getString("field_date_of_birth"));

                                for (int j = 0; j < jsonArrayAssignments.length(); j++) {


                                }
                            } else if (type.contains("exam")) {
                                JSONArray jsonArrayAssignments = new JSONArray(data.getJSONObject(i).getString("field_exam_time"));

                                for (int j = 0; j < jsonArrayAssignments.length(); j++) {


                                }
                            }


                            Log.d("TAG", "type: " + type);
                            if (type.contains("classes")) {
                                JSONArray jsonArrayDates = new JSONArray(data.getJSONObject(i).getString("field_class_time"));

                                for (int j = 0; j < jsonArrayDates.length(); j++) {
                                    String startdate = jsonArrayDates.getJSONObject(j).getString("startDate");
                                    String EventTimeStart = jsonArrayDates.getJSONObject(j).getString("startTime");
                                    String endDate = jsonArrayDates.getJSONObject(j).getString("endDate");
                                    String EventtimeEnd = jsonArrayDates.getJSONObject(j).getString("endTime");


                                }

                            }

                        }









                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(DisplayMainCalender.this);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(DisplayMainCalender.this);
                Utils.showAlertDialog(DisplayMainCalender.this, "Error", "There seems to be some problem with the Server. Try again later.");


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            progressmonthdateslist = null;
            Utils.p_dialog_dismiss(DisplayMainCalender.this);
            Utils.showAlertDialog(DisplayMainCalender.this, "Error", "There seems to be some problem with the Server. Try again later.");

        }

    }



}

