package co.questin.calendersection;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class UpdateCalenderDues extends BaseAppCompactActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener, View.OnClickListener {
    TextView TV_StartDate,TV_StartTime,TV_location;
    Switch AllDayOn;
    EditText edittittle,editDetails;
    String Classes_Id, tittle,Details, timeEnd, StartDate, TimeStart,EndDate, startDate,  startfrom,format, weekmon, weektues, weekwedns, weekthur, weekfri, weeksat, weeksun, classrepeat;

    boolean isPressed = false;

    private int CalendarHour, CalendarMinute;
    TimePickerDialog timepickerdialog;
    private Calendar mcalender;
    private DuesCreateCalenderTask myduecreatecalenderAuthTask = null;
    private int startDay, startMonth, startYear;
    Double Lat,Long;
    Double SelectedLat, SelectedLong;
    String time24format,time24formarend,
            DetailclassDate,DetailClassStart,DetailClassEndDate,DetailClassEnd,Detailweeknos,ClassEndDate;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;
    private int year, month, day, week;
    //To store longitude and latitude from map
    private double longitude;
    private double latitude;
    JSONObject ClassDetailsJson;
    MenuItem shareItem;
    private DeleteEventsFromCalender deleteeventfromcalender = null;
    private ProgressDetailsMySection mysectiondetailsAuthTask = null;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    // Used for selecting the current place.
    private static final int PLACE_PICKER_REQUEST = 1000;
    RelativeLayout MapLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_calender_dues);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        Bundle b = getActivity().getIntent().getExtras();
        Classes_Id = b.getString("CLASSES_ID");
        edittittle = findViewById(R.id.edittittle);
        editDetails = findViewById(R.id.Details);
        TV_StartDate = findViewById(R.id.StartDate);

        TV_StartTime = findViewById(R.id.StartTime);

        TV_location = findViewById(R.id.location);

        mcalender = Calendar.getInstance();
        CalendarHour = mcalender.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = mcalender.get(Calendar.MINUTE);
        TV_StartDate.setOnClickListener(this);

        TV_StartTime.setOnClickListener(this);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String  formattedDate = sdf.format(c.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(mcalender.getTime());


        StartDate = formattedDate2;
        EndDate = formattedDate2;



        TV_StartDate.setText(formattedDate);


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(mcalender.getTime());

        TV_StartTime.setText(time);

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(time);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        String time24format = displayFormat.format(date);

        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));





        TimeStart =time24format;
        timeEnd =time24format;



        edittittle.setEnabled(false);
        editDetails.setEnabled(false);
        TV_StartDate.setEnabled(false);
        TV_StartTime.setEnabled(false);

        TV_location.setEnabled(false);
        GetAllServicesDetails();
        TV_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
                    SelectedLat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
                    SelectedLong = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());

                }else {
                    SelectedLat = latitude;
                    SelectedLong = longitude;


                }


                LatLngBounds latLngBounds = new LatLngBounds(new LatLng(SelectedLat, SelectedLong),
                        new LatLng(SelectedLat, SelectedLong));
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                builder.setLatLngBounds(latLngBounds);

                try {
                    startActivityForResult(builder.build(UpdateCalenderDues.this), PLACE_PICKER_REQUEST);
                } catch (Exception e) {
                    Log.e("TAG", e.getStackTrace().toString());
                }



            }
        });
    }
    private void GetAllServicesDetails() {

        mysectiondetailsAuthTask = new ProgressDetailsMySection();
        mysectiondetailsAuthTask.execute();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.StartDate:
                hideKeyBoard(v);

                StartdateDialog();
                break;




            case R.id.StartTime:
                hideKeyBoard(v);


                timepickerdialog = new TimePickerDialog(UpdateCalenderDues.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }


                                TV_StartTime.setText(hourOfDay + ":" + minute + format);
                                String Slow =(hourOfDay + ":" + minute + format);



                                DateFormat f1 = new SimpleDateFormat("h:mma", Locale.US);

                                try {
                                    Date d = f1.parse(Slow);
                                    DateFormat f2 = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                    String FF = f2.format(d).toLowerCase();
                                    Log.d("TAG", "f2 " + FF);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }




                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String Time = null;
                                try {
                                    Time = outFormatx.format(inFormatx.parse(Slow));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatS : " + Time);

                                TimeStart=Time;


                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();
                break;




        }
    }

    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }




    private void showDateStart(int year, int month, int day) {
        Log.d("TAG", "Start Date: " + day + "-" + month + "-" + year);
        StartDate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        TV_StartDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));

    }




    private class ProgressDetailsMySection extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYCLASSESDETAILS+"/"+Classes_Id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateCalenderDues.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            mysectiondetailsAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");

                        tittle =data.getString("title");
                        Details=data.getString("body");

                        JSONArray ClassdateArrays = data.getJSONArray("field_class_time");
                        JSONArray ar = data.getJSONArray("field_class_time");




                        if(ClassdateArrays != null && ClassdateArrays.length() > 0 ) {


                            for (int j = 0; j < ClassdateArrays.length(); j++) {
                                DetailclassDate = ClassdateArrays.getJSONObject(0).getString("startDate");
                                DetailClassStart = ClassdateArrays.getJSONObject(j).getString("startTime");
                                //  DetailClassEndDate  = ClassdateArrays.getJSONObject(j).getString("endDate");
                                DetailClassEnd = ClassdateArrays.getJSONObject(j).getString("endTime");
                                Detailweeknos = ClassdateArrays.getJSONObject(j).getString("Week");

                                Log.d("TAG", "recource: " + DetailclassDate + DetailClassStart);

                                JSONObject lastObj = ClassdateArrays.getJSONObject(ClassdateArrays.length()-1);
                                ClassEndDate = lastObj.getString("endDate");
                                DetailClassEndDate =ClassEndDate;

                                Log.d("TAG", "ClassEndDatedddd: " + ClassEndDate +" "+ DetailClassEndDate);


                            }


                        }

                        JSONObject Local = data.getJSONObject("field_dept_location");
                        if (Local!=null && Local.length() >0){
                            MapLayout.setVisibility(View.VISIBLE);
                            Lat = Double.valueOf(Local.getString("lat"));
                            Long = Double.valueOf(Local.getString("lng"));
                            Log.d("TAG", "recource3: " + Lat + Long);

                        }else {
                            if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
                                Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
                                Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());

                            }else {
                                Lat = latitude;
                                Long = longitude;


                            }
                        }

                        edittittle.setText(tittle);
                        editDetails.setText(Details);
                        TV_StartDate.setText(DetailclassDate);

                        DateFormat inputFormat = new SimpleDateFormat("MMM-dd-yyyy");
                        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");



                        Date datemain;
                        try {
                            datemain = inputFormat.parse(DetailclassDate);
                            String outputDateStr = outputFormat.format(datemain);

                            StartDate =outputDateStr;

                            System.out.println(StartDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }



                        TV_StartTime.setText(DetailClassStart);

                        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss",Locale.US);
                        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a",Locale.US);
                        Date date;
                        try {
                            date = parseFormat.parse(DetailClassStart);
                            time24format = displayFormat.format(date);
                            System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
                            TimeStart =time24format;


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        onSearch(Lat,Long, SessionManager.getInstance(getActivity()).getCollage().getTitle());




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            mysectiondetailsAuthTask = null;
            hideLoading();


        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.calender_edit_menu, menu);
        shareItem = menu.findItem(R.id.Submit);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:

                AddEventinCalenderProcess();

                return true;

            case R.id.Editoption:
                shareItem.setVisible(true);
                edittittle.setEnabled(true);
                editDetails.setEnabled(true);
                TV_StartDate.setEnabled(true);
                TV_StartTime.setEnabled(true);
                TV_location.setEnabled(true);

                return true;


            case R.id.deleteoption:

                deleteeventfromcalender = new DeleteEventsFromCalender();
                deleteeventfromcalender.execute(Classes_Id);

                return true;

            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void AddEventinCalenderProcess() {

        edittittle.setError(null);
        editDetails.setError(null);



        // Store values at the time of the login attempt.
        tittle = edittittle.getText().toString().trim();
        Details = editDetails.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(tittle)) {
            focusView = edittittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        else if (co.questin.utils.TextUtils.isNullOrEmpty(Details)) {
            // check for First Name
            focusView = editDetails;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ClassDetailsJson = new JSONObject();

            try {
                ClassDetailsJson.put("start_date", StartDate);
                ClassDetailsJson.put("end_date", StartDate);
                ClassDetailsJson.put("start_time", TimeStart);
                ClassDetailsJson.put("end_time", TimeStart);
                ClassDetailsJson.put("week", "");
                ClassDetailsJson.put("repeat", "0");


                Log.d("TAG", "classdetails: " + ClassDetailsJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            myduecreatecalenderAuthTask = new DuesCreateCalenderTask();
            myduecreatecalenderAuthTask.execute();

        }
    }

    private class DuesCreateCalenderTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();




            RequestBody body = new FormBody.Builder()
                    .add("title",tittle)
                    .add("body",Details)
                    .add("field_dept_location[lat]", String.valueOf(Lat))
                    .add("field_dept_location[lng]", String.valueOf(Long))
                    .add("field_class_time",ClassDetailsJson.toString())
                    .build();
            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_TEACHERCREATECLASS+"/"+Classes_Id,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateCalenderDues.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            myduecreatecalenderAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                       // showAlertDialog(msg);
                        Intent i = new Intent(UpdateCalenderDues.this,CalenderMain.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();


            }
        }

        @Override
        protected void onCancelled() {
            myduecreatecalenderAuthTask = null;
            hideLoading();



        }
    }





    @Override
    public void onBackPressed() {
        Intent i = new Intent(UpdateCalenderDues.this,CalenderMain.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    public void onSearch(Double lat, Double aLong, String placename) {
        mMap.clear();
        LatLng latLng = new LatLng(lat, aLong);
        MapLayout.setVisibility(View.VISIBLE);
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
                .title(placename)); //Adding a title
        //Moving the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //Animating the camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(20));


    }


    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);

        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.setOnMapLongClickListener(this);
        getCurrentLocation();

    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

  /*DELETE  CALENDER STUFF*/

    private class DeleteEventsFromCalender extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETESUBJECTS+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                              e.printStackTrace();
                UpdateCalenderDues.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deleteeventfromcalender = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                      //  showAlertDialog(msg);
                        Intent i = new Intent(UpdateCalenderDues.this,CalenderMain.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);

                        finish();




                    }
                }else {
                    hideLoading();
                    showAlertDialog(getString(R.string.error_something_wrong));

                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_something_wrong));

            }
        }

        @Override
        protected void onCancelled() {
            deleteeventfromcalender = null;
            hideLoading();


        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                TV_location.setText(placename);
                Lat= place.getLatLng().latitude;
                Long = place.getLatLng().longitude;
                onSearch(place.getLatLng().latitude, place.getLatLng().longitude,placename);


            }
        }
    }

}
