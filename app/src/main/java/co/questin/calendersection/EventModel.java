package co.questin.calendersection;

import java.io.Serializable;

/**
 * Created by HCL on 02-10-2016.
 */
public class EventModel implements Serializable {

    private String strDate;
    private String strDateEnd;
    private String strStartTime;
    private String strEndTime;
    private String strName;
    private String strDetails;
    private String strLocation;
    private int id;
    private String Displayid;
    private String calenderMain;
    private String calenderType;

    public String getCalenderType() {
        return calenderType;
    }

    public void setCalenderType(String calenderType) {
        this.calenderType = calenderType;
    }

    public String getCalenderMain() {
        return calenderMain;
    }

    public void setCalenderMain(String calenderMain) {
        this.calenderMain = calenderMain;
    }



    private int image = -1;



    public EventModel() {
        super();

    }

    public EventModel(String startdate) {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EventModel(int id,String strDate, String strStartTime, String strEndTime, String strName, String calenderMain,String Displayid,String calenderType ) {
        this.id = id;
        this.strDate = strDate;
        this.strStartTime = strStartTime;
        this.strEndTime = strEndTime;
        this.strName = strName;
        this.calenderMain =calenderMain;
        this.Displayid =Displayid;
        this.calenderType =calenderType;

    }



    /*database constructer*/


    public EventModel(String strDate,String strDateEnd, String strStartTime, String strEndTime, String strName, String strDetails,String Displayid) {
       
        this.strDate = strDate;
        this.strDateEnd = strDateEnd;
        this.strStartTime = strStartTime;
        this.strEndTime = strEndTime;
        this.strName = strName;
        this.strDetails = strDetails;
        this.Displayid = Displayid;


    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getStrStartTime() {
        return strStartTime;
    }

    public void setStrStartTime(String strStartTime) {
        this.strStartTime = strStartTime;
    }

    public String getStrEndTime() {
        return strEndTime;
    }

    public void setStrEndTime(String strEndTime) {
        this.strEndTime = strEndTime;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getStrDetails() {
        return strDetails;
    }

    public void setStrDetails(String strDetails) {
        this.strDetails = strDetails;
    }

    public String getStrLocation() {
        return strLocation;
    }

    public void setStrLocation(String strLocation) {
        this.strLocation = strLocation;
    }

    public String getStrDateEnd() {
        return strDateEnd;
    }

    public void setStrDateEnd(String strDateEnd) {
        this.strDateEnd = strDateEnd;
    }


    public String getDisplayid() {
        return Displayid;
    }

    public void setDisplayid(String displayid) {
        Displayid = displayid;
    }


}
