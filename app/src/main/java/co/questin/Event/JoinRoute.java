package co.questin.Event;

/**
 * Created by HP on 5/28/2018.
 */

public class JoinRoute {
    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public JoinRoute(int index) {
        this.index = index;
    }
}
