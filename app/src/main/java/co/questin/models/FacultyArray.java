package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 07-09-2017.
 */

public class FacultyArray implements Serializable {
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public FacultyArray(int type) {
        this.type = type;

    }
    public String mid;
    public String first_name;
    public String last_name;
    public String uid;
    public String department;
    public String picture;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
