package co.questin.models.fcm;

import co.questin.models.chat.GroupChatModel;
import co.questin.utils.Constants;

/**
 * Created by farheen on 23/10/17
 */

public class FcmGroupChatModel {

    private int type;
    private String senderId;
    private String groupId;
    private int category;
    private String text;
    private String link;
    private String time;
    private String ISChat;
    private String MyName;

    public String getMyName() {
        return MyName;
    }

    public void setMyName(String myName) {
        MyName = myName;
    }

    public String getISChat() {
        return ISChat;
    }

    public void setISChat(String ISChat) {
        this.ISChat = ISChat;
    }

    public FcmGroupChatModel(){

    }

    public FcmGroupChatModel(GroupChatModel chatModel, String senderId,String MyName ,String groupId, String link ,String ISChat){
        this.type = Constants.FCM_TYPE_GROUPCHAT;
        this.senderId = senderId;
        this.groupId = groupId;
        this.category = chatModel.getCategory();
        this.text = chatModel.getText();
        this.link = link;
        this.time = chatModel.getTime();
        this.ISChat = ISChat;
        this.MyName =MyName;
    }

    public FcmGroupChatModel(int type, String senderId,String groupId, int category, String text, String link, String time) {
        this.type = type;
        this.senderId = senderId;
        this.groupId = groupId;
        this.category = category;
        this.text = text;
        this.link = link;
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public FcmGroupChatModel setType(int type) {
        this.type = type;
        return this;
    }

    public String getSenderId() {
        return senderId;
    }

    public FcmGroupChatModel setSenderId(String senderId) {
        this.senderId = senderId;
        return this;
    }


    public String getGroupId() {
        return groupId;
    }

    public FcmGroupChatModel setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }


    public int getCategory() {
        return category;
    }

    public FcmGroupChatModel setCategory(int category) {
        this.category = category;
        return this;
    }

    public String getText() {
        return text;
    }

    public FcmGroupChatModel setText(String text) {
        this.text = text;
        return this;
    }

    public String getLink() {
        return link;
    }

    public FcmGroupChatModel setLink(String link) {
        this.link = link;
        return this;
    }

    public String getTime() {
        return time;
    }

    public FcmGroupChatModel setTime(String time) {
        this.time = time;
        return this;
    }
}
