package co.questin.models.fcm;

import co.questin.models.chat.ChatModel;
import co.questin.utils.Constants;

/**
 * Created by farheen on 23/10/17
 */

public class FcmChatModel {

    private int type;
    private String senderId;
    private int category;
    private String text;
    private String link;
    private String time;
    private String ISChat;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getISChat() {
        return ISChat;
    }

    public void setISChat(String ISChat) {
        this.ISChat = ISChat;
    }

    public FcmChatModel(){

    }

    public FcmChatModel(ChatModel chatModel,String userName, String senderId, String link,String ISChat){
        this.type = Constants.FCM_TYPE_CHAT;
        this.senderId = senderId;
        this.category = chatModel.getCategory();
        this.text = chatModel.getText();
        this.link = link;
        this.time = chatModel.getTime();
        this.ISChat = ISChat;
        this.userName=userName;
    }

    public FcmChatModel(int type, String senderId, int category, String text, String link, String time) {
        this.type = type;
        this.senderId = senderId;
        this.category = category;
        this.text = text;
        this.link = link;
        this.time = time;
    }



    public int getType() {
        return type;
    }

    public FcmChatModel setType(int type) {
        this.type = type;
        return this;
    }

    public String getSenderId() {
        return senderId;
    }

    public FcmChatModel setSenderId(String senderId) {
        this.senderId = senderId;
        return this;
    }

    public int getCategory() {
        return category;
    }

    public FcmChatModel setCategory(int category) {
        this.category = category;
        return this;
    }

    public String getText() {
        return text;
    }

    public FcmChatModel setText(String text) {
        this.text = text;
        return this;
    }

    public String getLink() {
        return link;
    }

    public FcmChatModel setLink(String link) {
        this.link = link;
        return this;
    }

    public String getTime() {
        return time;
    }

    public FcmChatModel setTime(String time) {
        this.time = time;
        return this;
    }
}
