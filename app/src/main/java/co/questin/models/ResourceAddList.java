package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 18-12-2017.
 */

public class ResourceAddList implements Serializable {
    public String title;
    public String value;

    public ResourceAddList() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public ResourceAddList(String title, String value) {
        super();
        this.title = title;
        this.value = value;
    }
}
