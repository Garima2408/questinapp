package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 09-10-2017.
 */

public class AttendanceClassArray  implements Serializable {
    public String tnid;
    public String title;

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
