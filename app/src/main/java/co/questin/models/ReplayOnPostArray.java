package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 16-10-2017.
 */

public class ReplayOnPostArray implements Serializable {
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;
    public String id;
    public String  uid;
    public String date;
    public String  post;
    public String userName;
    public String  picture;
    public String  comment;
    public String  subject;
    public String pid;
    public  String replies_count;
    public  String likes;
    public  String is_liked;
    public  String title;
    public  String url;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ReplayOnPostArray(int type) {
        this.type = type;

    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReplies_count() {
        return replies_count;
    }

    public void setReplies_count(String replies_count) {
        this.replies_count = replies_count;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(String is_liked) {
        this.is_liked = is_liked;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


}
