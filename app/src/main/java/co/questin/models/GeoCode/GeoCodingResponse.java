package co.questin.models.GeoCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajay on 2/13/17.
 */

public class GeoCodingResponse {

    public List<Result> results = new ArrayList<>();


    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
