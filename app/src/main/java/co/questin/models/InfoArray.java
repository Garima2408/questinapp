package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 31-10-2017.
 */

public class InfoArray implements Serializable{
   public String value;
    public String title;
    public String pos;
    public String resource;

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
