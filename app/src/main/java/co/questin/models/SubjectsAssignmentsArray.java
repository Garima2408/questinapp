package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 22-09-2017.
 */

public class SubjectsAssignmentsArray implements Serializable {
    public String tnid;
    public String date;
    public String title;
    public String teacher;
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public SubjectsAssignmentsArray(int type) {
        this.type = type;

    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
