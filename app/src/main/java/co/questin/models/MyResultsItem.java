package co.questin.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nmn on 3/7/17.
 */

public class MyResultsItem implements Serializable {

    public  String className ;
    public  String classType ;
    public  String percentage ;
    public boolean isSelected = false;
    public List<ResultChlidListArray> childsTaskList = new ArrayList<ResultChlidListArray>();
}

