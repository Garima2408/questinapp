package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 17-08-2017.
 */

public class CollageEventArray implements Serializable {


    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;
    public String title;
    public String  tnid;
    public String  logo;
    // public String  location;
    public String  period;


    public CollageEventArray(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

 /*   public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }*/

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }









}
