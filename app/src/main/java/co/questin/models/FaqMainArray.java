package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 12-12-2017.
 */

public class FaqMainArray implements Serializable {
    public String question;
    public String answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
