package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 28-12-2017.
 */

public class ShowAttandanceDates implements Serializable {
   public String date;
    public String attendance;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }
}
