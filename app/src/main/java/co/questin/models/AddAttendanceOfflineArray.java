package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 04-10-2017.
 */

public class AddAttendanceOfflineArray implements Serializable{
    private int id;
    public String uid;
    public String name;
    public String picture;
    public String attendance;
    public String markedValue;
    public String Course_id;
    public String ClasssId;
    public String Date;
    public String type;

    public AddAttendanceOfflineArray(int id,String uid, String attendance, String Course_id, String ClasssId, String Date, String type) {
        this.id = id;
        this.uid = uid;
        this.attendance = attendance;
        this.Course_id = Course_id;
        this.ClasssId = ClasssId;
        this.Date = Date;
        this.type = type;



    }

    public AddAttendanceOfflineArray() {

    }


    public String getCourse_id() {
        return Course_id;
    }

    public void setCourse_id(String course_id) {
        Course_id = course_id;
    }

    public String getClasssId() {
        return ClasssId;
    }

    public void setClasssId(String classsId) {
        ClasssId = classsId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMarkedValue() {
        return markedValue;
    }

    public void setMarkedValue(String markedValue) {
        this.markedValue = markedValue;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }


}
