package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 17-08-2017.
 */

public class MediaArray implements Serializable {
    public String tnid;
    public String media_type;
    public String gallery;
    public String  video;
    public String title;
    public String field_forum_images;
    public String field_ffile;
    public String field_media_type;

    private int type= 0;
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;



    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public MediaArray(int type) {
        this.type = type;

    }


    public String getField_forum_images() {
        return field_forum_images;
    }

    public void setField_forum_images(String field_forum_images) {
        this.field_forum_images = field_forum_images;
    }

    public String getField_ffile() {
        return field_ffile;
    }

    public void setField_ffile(String field_ffile) {
        this.field_ffile = field_ffile;
    }

    public String getField_media_type() {
        return field_media_type;
    }

    public void setField_media_type(String field_media_type) {
        this.field_media_type = field_media_type;
    }



    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getGallery() {
        return gallery;
    }

    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
