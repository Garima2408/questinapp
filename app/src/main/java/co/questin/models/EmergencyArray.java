package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 29-01-2018.
 */

public class EmergencyArray implements Serializable {
    public String tnid;
    public String description;
    public String title;

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
