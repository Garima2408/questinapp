package co.questin.models.routedetailResponse;

/**
 * Created by HP on 6/12/2018.
 */

public class PickDropLatLng {


    private String lat;

    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
