package co.questin.models.tracker;

/**
 * Created by farheen on 9/10/17
 */

public class RouteModel {
    private String title;
    private String id;

    public RouteModel() {

    }

    public RouteModel(String title, String id) {
        this.title = title;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
