package co.questin.models.tracker;

import java.io.Serializable;

public class WayPointsArray implements Serializable {

    public String field_place_name;
    public String field_start_time;
    public String field_departure_time;
    public String field_way_points;
    public String lat;
    public String lng;
    public String field_photo;

    public String pickuplat;
    public String pickuplng;

    public String  droplat;
    public String  droplng;


    public String getPickuplat() {
        return pickuplat;
    }

    public void setPickuplat(String pickuplat) {
        this.pickuplat = pickuplat;
    }

    public String getPickuplng() {
        return pickuplng;
    }

    public void setPickuplng(String pickuplng) {
        this.pickuplng = pickuplng;
    }

    public String getDroplat() {
        return droplat;
    }

    public void setDroplat(String droplat) {
        this.droplat = droplat;
    }

    public String getDroplng() {
        return droplng;
    }

    public void setDroplng(String droplng) {
        this.droplng = droplng;
    }

    private boolean isPick;
    private boolean isDrop;

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    public boolean isDrop() {
        return isDrop;
    }

    public void setDrop(boolean drop) {
        isDrop = drop;
    }

    public String getField_place_name() {
        return field_place_name;
    }

    public void setField_place_name(String field_place_name) {
        this.field_place_name = field_place_name;
    }

    public String getField_start_time() {
        return field_start_time;
    }

    public void setField_start_time(String field_start_time) {
        this.field_start_time = field_start_time;
    }

    public String getField_departure_time() {
        return field_departure_time;
    }

    public void setField_departure_time(String field_departure_time) {
        this.field_departure_time = field_departure_time;
    }

    public String getField_way_points() {
        return field_way_points;
    }

    public void setField_way_points(String field_way_points) {
        this.field_way_points = field_way_points;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getField_photo() {
        return field_photo;
    }

    public void setField_photo(String field_photo) {
        this.field_photo = field_photo;
    }
}
