package co.questin.models.tracker;

import java.io.Serializable;

public class RoutelistArray implements Serializable{
    public String tnid;
    public  String title;
    public  String is_faculty;
    public  String is_student;
    public  String is_member;
    public  String is_driver;
    public  String node_uid;
    public  String is_creator;

    private String sponsered;

    public String getSponsered() {
        return sponsered;
    }

    public void setSponsered(String sponsered) {
        this.sponsered = sponsered;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public String getIs_faculty() {
        return is_faculty;
    }

    public void setIs_faculty(String is_faculty) {
        this.is_faculty = is_faculty;
    }

    public String getIs_student() {
        return is_student;
    }

    public void setIs_student(String is_student) {
        this.is_student = is_student;
    }

    public String getIs_member() {
        return is_member;
    }

    public void setIs_member(String is_member) {
        this.is_member = is_member;
    }

    public String getIs_driver() {
        return is_driver;
    }

    public void setIs_driver(String is_driver) {
        this.is_driver = is_driver;
    }

    public String getNode_uid() {
        return node_uid;
    }

    public void setNode_uid(String node_uid) {
        this.node_uid = node_uid;
    }

    public String getIs_creator() {
        return is_creator;
    }

    public void setIs_creator(String is_creator) {
        this.is_creator = is_creator;
    }









}
