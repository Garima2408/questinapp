package co.questin.models.chat;

/**
 * Created by farheen on 17/10/17
 */

public class SendGroupMessageModel {

    private String gid;
    private String message;

    public SendGroupMessageModel() {

    }

    public SendGroupMessageModel(String gid, String message) {
        this.gid = gid;
        this.message = message;
    }

    public String getUids() {
        return gid;
    }

    public SendGroupMessageModel setUids(String uids) {
        this.gid = uids;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public SendGroupMessageModel setMessage(String message) {
        this.message = message;
        return this;
    }
}
