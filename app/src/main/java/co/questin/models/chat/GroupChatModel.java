package co.questin.models.chat;

import android.os.Parcel;
import android.os.Parcelable;


public class GroupChatModel implements Parcelable{
    private int isMine;
    private int category;
    private String text;
    private String link;
    private String time;
    private int status;
    private boolean isDownloading = false;
    private String senderId;

    public GroupChatModel() {

    }

    public GroupChatModel(int isMine, int category, String text, String link, String time, int status, boolean isDownloading) {
        this.isMine = isMine;
        this.category = category;
        this.text = text;
        this.link = link;
        this.time = time;
        this.status = status;
        this.isDownloading = isDownloading;
    }

    protected GroupChatModel(Parcel in) {
        isMine = in.readInt();
        category = in.readInt();
        text = in.readString();
        link = in.readString();
        time = in.readString();
        status = in.readInt();
        senderId=in.readString();
        isDownloading = in.readByte() != 0;
    }

    public static final Creator<GroupChatModel> CREATOR = new Creator<GroupChatModel>() {
        @Override
        public GroupChatModel createFromParcel(Parcel in) {
            return new GroupChatModel(in);
        }

        @Override
        public GroupChatModel[] newArray(int size) {
            return new GroupChatModel[size];
        }
    };

    public String getSenderId() {
        return senderId;
    }

    public GroupChatModel setSenderId(String senderId) {
        this.senderId = senderId;
        return this;

    }

    public boolean isDownloading() {
        return isDownloading;
    }

    public GroupChatModel setDownloading(boolean downloading) {
        isDownloading = downloading;
        return this;
    }

    public int getIsMine() {
        return isMine;
    }

    public GroupChatModel setIsMine(int isMine) {
        this.isMine = isMine;
        return this;
    }

    public int getCategory() {
        return category;
    }

    public GroupChatModel setCategory(int category) {
        this.category = category;
        return this;
    }

    public String getText() {
        return text;
    }

    public GroupChatModel setText(String text) {
        this.text = text;
        return this;
    }

    public String getLink() {
        return link;
    }

    public GroupChatModel setLink(String link) {
        this.link = link;
        return this;
    }

    public String getTime() {
        return time;
    }

    public GroupChatModel setTime(String time) {
        this.time = time;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public GroupChatModel setStatus(int status) {
        this.status = status;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(isMine);
        parcel.writeInt(category);
        parcel.writeString(text);
        parcel.writeString(link);
        parcel.writeString(time);
        parcel.writeInt(status);
        parcel.writeString(senderId);
        parcel.writeByte((byte) (isDownloading ? 1 : 0));
    }
}
