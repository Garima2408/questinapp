package co.questin.models;

import java.io.Serializable;

/**
 * Created by Dell on 22-12-2017.
 */

public class TeacherViewStudentArray implements Serializable {
    public  String student;
    public  String perc;
    public  String picture;
    public String Enrollment;

    public String getEnrollment() {
        return Enrollment;
    }

    public void setEnrollment(String enrollment) {
        Enrollment = enrollment;
    }

    public String date0,attendance0,date1,attendance1,date2,attendance2,date3,attendance3,date4,attendance4,date5,attendance5,date6,attendance6,date7,attendance7;

    public String getDate0() {
        return date0;
    }

    public void setDate0(String date0) {
        this.date0 = date0;
    }

    public String getAttendance0() {
        return attendance0;
    }

    public void setAttendance0(String attendance0) {
        this.attendance0 = attendance0;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getAttendance1() {
        return attendance1;
    }

    public void setAttendance1(String attendance1) {
        this.attendance1 = attendance1;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String getAttendance2() {
        return attendance2;
    }

    public void setAttendance2(String attendance2) {
        this.attendance2 = attendance2;
    }

    public String getDate3() {
        return date3;
    }

    public void setDate3(String date3) {
        this.date3 = date3;
    }

    public String getAttendance3() {
        return attendance3;
    }

    public void setAttendance3(String attendance3) {
        this.attendance3 = attendance3;
    }

    public String getDate4() {
        return date4;
    }

    public void setDate4(String date4) {
        this.date4 = date4;
    }

    public String getAttendance4() {
        return attendance4;
    }

    public void setAttendance4(String attendance4) {
        this.attendance4 = attendance4;
    }

    public String getDate5() {
        return date5;
    }

    public void setDate5(String date5) {
        this.date5 = date5;
    }

    public String getAttendance5() {
        return attendance5;
    }

    public void setAttendance5(String attendance5) {
        this.attendance5 = attendance5;
    }

    public String getDate6() {
        return date6;
    }

    public void setDate6(String date6) {
        this.date6 = date6;
    }

    public String getAttendance6() {
        return attendance6;
    }

    public void setAttendance6(String attendance6) {
        this.attendance6 = attendance6;
    }

    public String getDate7() {
        return date7;
    }

    public void setDate7(String date7) {
        this.date7 = date7;
    }

    public String getAttendance7() {
        return attendance7;
    }

    public void setAttendance7(String attendance7) {
        this.attendance7 = attendance7;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getPerc() {
        return perc;
    }

    public void setPerc(String perc) {
        this.perc = perc;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
