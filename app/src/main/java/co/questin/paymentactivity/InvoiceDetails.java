package co.questin.paymentactivity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import co.questin.R;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;

public class InvoiceDetails extends BaseAppCompactActivity {
    ImageView CollageLogo;
    TextView tv_college_name,textreceiptID,paidDate,textTotal,textViewFullName,Enrollment,Transaction_id;

    String orderid,userid,enrolmentid,duedate,totaldue,transaction,sportsfee,educationfee,examfee,hostelfee,othersfee,latefee;
    private LinearLayout llScroll;
    private Bitmap bitmap;
    private TextView Sports_fee,Education_Fee,Exam_fee,Hostel_Fee,Others,Late_Fee;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_details);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        orderid = intent.getStringExtra("Order_ID");
        userid = intent.getStringExtra("Uid");
        enrolmentid = intent.getStringExtra("Enrolment_ID");
        duedate = intent.getStringExtra("DUE_Date");
        totaldue = intent.getStringExtra("Total_FEE");
        transaction = intent.getStringExtra("Transaction_id");
        sportsfee = intent.getStringExtra("Sports_fee");
        educationfee = intent.getStringExtra("Education_Fee");
        examfee = intent.getStringExtra("Exam_fee");
        hostelfee = intent.getStringExtra("Hostel_Fee");
        othersfee = intent.getStringExtra("Others");
        latefee = intent.getStringExtra("Late_Fee");


        llScroll = findViewById(R.id.llScroll);
        CollageLogo =findViewById(R.id.CollageLogo);
        tv_college_name =findViewById(R.id.tv_college_name);
        textreceiptID =findViewById(R.id.textreceiptID);
        paidDate =findViewById(R.id.paidDate);
        textTotal =findViewById(R.id.textTotal);
        textViewFullName =findViewById(R.id.textViewFullName);
        Enrollment =findViewById(R.id.Enrollment);
        Sports_fee = findViewById(R.id.Sports_fee);
        Education_Fee = findViewById(R.id.Education_Fee);
        Exam_fee = findViewById(R.id.Exam_fee);
        Hostel_Fee = findViewById(R.id.Hostel_Fee);
        Others = findViewById(R.id.Others);
        Late_Fee =findViewById(R.id.Late_Fee);
        Transaction_id =findViewById(R.id.Transaction_id);

        if (SessionManager.getInstance(getActivity()).getCollage().getTitle()!=null){
            String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
            SpannableString spString = new SpannableString(Topheading);
            AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
            spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_college_name.setText(spString);

        }

        if (SessionManager.getInstance(this).getCollage().getField_groups_logo() !=null) {
            Glide.with(this).load(SessionManager.getInstance(this).getCollage().getField_groups_logo())
                    .placeholder(R.mipmap.appicon).dontAnimate()
                    .fitCenter().into(CollageLogo);
            }

        textreceiptID.setText(orderid);
        paidDate.setText(duedate);
        textTotal.setText(totaldue);
        textViewFullName.setText(SessionManager.getInstance(getActivity()).getUser().getFirstName()+" "+SessionManager.getInstance(getActivity()).getUser().getLastName());
        Transaction_id.setText(transaction);
        Enrollment.setText(enrolmentid);
        Sports_fee.setText(sportsfee);
        Education_Fee.setText(educationfee);
        Exam_fee.setText(examfee);
        Hostel_Fee.setText(hostelfee);
        Others.setText(othersfee);
        Late_Fee.setText(latefee);


    }

    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recepit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;

            case R.id.Generate_recepit:
                Log.d("size"," "+llScroll.getWidth() +"  "+llScroll.getWidth());
                bitmap = loadBitmapFromView(llScroll, llScroll.getWidth(), llScroll.getHeight());
                createPdf();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }
    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    private void createPdf(){
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        //  Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels ;
        float width = displaymetrics.widthPixels ;

        int convertHighet = (int) hight, convertWidth = (int) width;

//        Resources mResources = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0 , null);
        document.finishPage(page);

        // write the document content
        String targetPdf = "/sdcard/pdffromScroll.pdf";
        File filePath;
        filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();
        Toast.makeText(this, "PDF of Scroll is created!!!", Toast.LENGTH_SHORT).show();

        openGeneratedPDF();

    }

    private void openGeneratedPDF(){
        File file = new File("/sdcard/pdffromScroll.pdf");
        if (file.exists())
        {
            Intent intent=new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try
            {
                startActivity(intent);
            }
            catch(ActivityNotFoundException e)
            {
                Toast.makeText(InvoiceDetails.this, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }

}



