package co.questin.paymentactivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.MyDuesAdapter;
import co.questin.models.MyduesArray;
import co.questin.models.chat.SendMessageModel;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class DetailsStudentDues extends BaseAppCompactActivity {
    RecyclerView Rv_MY_Dues;
    private MyDuesDisplayList myduesdisplaydisplay = null;
    public ArrayList<MyduesArray> mduesList;
    private MyDuesAdapter duesadapter;
    private RecyclerView.LayoutManager layoutManager;
    String Sports_fee ,Education_Fee , Exam_fee ,Hostel_Fee ,Others,StudentId,TpyeSelected;
    Button button_message;
    Dialog AttachmentDialog;
    private SendMessageModel sendMessageModel;
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_student_dues);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getIntent().getExtras();
        StudentId = b.getString("STUDENTID");
        mduesList=new ArrayList<>();
        gson = new Gson();
        sendMessageModel = new SendMessageModel();
        layoutManager = new LinearLayoutManager(this);
        Rv_MY_Dues = findViewById(co.questin.R.id.Rv_MY_Dues);
        button_message =findViewById(R.id.button_message);
        Rv_MY_Dues.setHasFixedSize(true);
        Rv_MY_Dues.setLayoutManager(layoutManager);
        mduesList.clear();

        myduesdisplaydisplay = new MyDuesDisplayList();
        myduesdisplaydisplay.execute(StudentId);

        button_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AttachmentDialogOpen();
            }
        });
    }


    private void AttachmentDialogOpen() {

        AttachmentDialog = new Dialog(this);
        AttachmentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        AttachmentDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.WHITE));
        AttachmentDialog.setContentView(R.layout.template_dialog);

        AttachmentDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        AttachmentDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        AttachmentDialog.show();
        final String[] TemplateType = { "Dear Parent, you are requested to pay the school fees before [DATE]",
                " After the lapse of this given date: Penalties and fine will be imposed",
                "We have not received payment for [MONTH/YEAR], dated [DATE]. Please contact us on [MOBILE NUMBER] immediately",};
        Spinner templateSpinnner = AttachmentDialog.findViewById(R.id.templateSpinnner);
        final EditText templateText = AttachmentDialog.findViewById(R.id.templateText);


        ArrayAdapter classadapter = new ArrayAdapter(DetailsStudentDues.this,android.R.layout.simple_spinner_item,TemplateType);
        classadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        templateSpinnner.setAdapter(classadapter);


        templateSpinnner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                  TpyeSelected = TemplateType[position];

                templateText.setText(TpyeSelected);

                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        templateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sendMessage(sendMessageModel
                        .setUids(StudentId)
                        .setMessage(TpyeSelected));



            }
        });

        }
    private void sendMessage(final SendMessageModel sendMessageModel) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = RequestBody.create(Constants.JSON,
                        gson.toJson(sendMessageModel, SendMessageModel.class));
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.SEND_MSG, requestBody);




                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private class MyDuesDisplayList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MYCOURCEINCOMPLETEDUESLIST+"/"+SessionManager.getInstance(getActivity()).getCollage().getTnid()+"/"+args[0],body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                DetailsStudentDues.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            myduesdisplaydisplay = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            MyduesArray mdueee = new MyduesArray();

                            String id = jsonArrayData.getJSONObject(i).getString("id");
                            String uid = jsonArrayData.getJSONObject(i).getString("uid");
                            String enrollmentId = jsonArrayData.getJSONObject(i).getString("enrollmentId");
                            String due_date = jsonArrayData.getJSONObject(i).getString("due_date");
                            String fee = jsonArrayData.getJSONObject(i).getString("fee");
                            String late_fee = jsonArrayData.getJSONObject(i).getString("late_fee");
                            String transaction_id = jsonArrayData.getJSONObject(i).getString("transaction_id");
                            String transaction_status = jsonArrayData.getJSONObject(i).getString("transaction_status");
                            String transaction_date = jsonArrayData.getJSONObject(i).getString("transaction_date");
                            String total_fee = jsonArrayData.getJSONObject(i).getString("total_fee");
                            JSONObject data = jsonArrayData.getJSONObject(i).getJSONObject("fee_component");

                            if (data != null && data.length() > 0) {
                                Sports_fee = data.getString("Sports fee");
                                Education_Fee = data.getString("Education Fee");
                                Exam_fee = data.getString("Exam fee");
                                Hostel_Fee = data.getString("Hostel Fee");
                                Others = data.getString("Others");

                                Log.d("TAG", "fees: " + Sports_fee +Education_Fee + Exam_fee +Hostel_Fee +Others);
                            } else {

                            }

                            if (transaction_status !=null &&transaction_status.length() >0){
                                if (transaction_status.matches("completed")){

                                }else if(transaction_status.matches("due")){
                                    mdueee.setId(id);
                                    mdueee.setUid(uid);
                                    mdueee.setEnrollmentId(enrollmentId);
                                    mdueee.setFee(fee);
                                    mdueee.setLate_fee(late_fee);
                                    mdueee.setDue_date(due_date);
                                    mdueee.setTotal_fee(total_fee);
                                    mdueee.setTransaction_id(transaction_id);
                                    mdueee.setTransaction_status(transaction_status);
                                    mdueee.setTransaction_date(transaction_date);
                                    mdueee.setSports_fee(Sports_fee);
                                    mdueee.setEducation_Fee(Education_Fee);
                                    mdueee.setExam_fee(Exam_fee);
                                    mdueee.setHostel_Fee(Hostel_Fee);
                                    mdueee.setOthers(Others);
                                    mduesList.add(mdueee);
                                }


                            }else {
                                mdueee.setId(id);
                                mdueee.setUid(uid);
                                mdueee.setEnrollmentId(enrollmentId);
                                mdueee.setFee(fee);
                                mdueee.setLate_fee(late_fee);
                                mdueee.setDue_date(due_date);
                                mdueee.setTotal_fee(total_fee);
                                mdueee.setTransaction_id(transaction_id);
                                mdueee.setTransaction_status(transaction_status);
                                mdueee.setTransaction_date(transaction_date);
                                mdueee.setSports_fee(Sports_fee);
                                mdueee.setEducation_Fee(Education_Fee);
                                mdueee.setExam_fee(Exam_fee);
                                mdueee.setHostel_Fee(Hostel_Fee);
                                mdueee.setOthers(Others);
                                mduesList.add(mdueee);
                            }




                            duesadapter = new MyDuesAdapter(DetailsStudentDues.this, mduesList);
                            Rv_MY_Dues.setAdapter(duesadapter);

                        }
                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            myduesdisplaydisplay = null;
            hideLoading();


        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
