package co.questin.paymentactivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.MyPaidDuesAdapter;
import co.questin.models.MyduesArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class PaidPaymentList extends BaseAppCompactActivity {
    RecyclerView Rv_MY_Dues;
    private MyDuesDisplayList myduesdisplaydisplay = null;
    public ArrayList<MyduesArray> mduesList;
    private MyPaidDuesAdapter duesadapter;
    private RecyclerView.LayoutManager layoutManager;
    String Sports_fee ,Education_Fee , Exam_fee ,Hostel_Fee ,Others;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid_payment_list);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        mduesList =new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        Rv_MY_Dues = findViewById(co.questin.R.id.Rv_MY_Dues);
        Rv_MY_Dues.setHasFixedSize(true);
        Rv_MY_Dues.setLayoutManager(layoutManager);
        myduesdisplaydisplay = new MyDuesDisplayList();
        myduesdisplaydisplay.execute();


    }





    private class MyDuesDisplayList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MYCOURCEINCOMPLETEDUESLIST+"/"+SessionManager.getInstance(getActivity()).getCollage().getTnid()+"/"+SessionManager.getInstance(getActivity()).getUser().getUserprofile_id(),body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                PaidPaymentList.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            myduesdisplaydisplay = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                MyduesArray mdueee = new MyduesArray();

                                String id = jsonArrayData.getJSONObject(i).getString("id");
                                String uid = jsonArrayData.getJSONObject(i).getString("uid");
                                String enrollmentId = jsonArrayData.getJSONObject(i).getString("enrollmentId");
                                String due_date = jsonArrayData.getJSONObject(i).getString("due_date");
                                String fee = jsonArrayData.getJSONObject(i).getString("fee");
                                String late_fee = jsonArrayData.getJSONObject(i).getString("late_fee");
                                String transaction_id = jsonArrayData.getJSONObject(i).getString("transaction_id");
                                String transaction_status = jsonArrayData.getJSONObject(i).getString("transaction_status");
                                String transaction_date = jsonArrayData.getJSONObject(i).getString("transaction_date");
                                String total_fee = jsonArrayData.getJSONObject(i).getString("total_fee");
                                JSONObject data = jsonArrayData.getJSONObject(i).getJSONObject("fee_component");

                                if (data != null && data.length() > 0) {
                                    Sports_fee = data.getString("Sports fee");
                                    Education_Fee = data.getString("Education Fee");
                                    Exam_fee = data.getString("Exam fee");
                                    Hostel_Fee = data.getString("Hostel Fee");
                                    Others = data.getString("Others");

                                    Log.d("TAG", "fees: " + Sports_fee + Education_Fee + Exam_fee + Hostel_Fee + Others);
                                } else {

                                }
                                if (transaction_status != null) {
                                    if (transaction_status.matches("completed")) {
                                        mdueee.setId(id);
                                        mdueee.setUid(uid);
                                        mdueee.setEnrollmentId(enrollmentId);
                                        mdueee.setFee(fee);
                                        mdueee.setLate_fee(late_fee);
                                        mdueee.setDue_date(due_date);
                                        mdueee.setTotal_fee(total_fee);
                                        mdueee.setTransaction_id(transaction_id);
                                        mdueee.setTransaction_status(transaction_status);
                                        mdueee.setTransaction_date(transaction_date);
                                        mdueee.setSports_fee(Sports_fee);
                                        mdueee.setEducation_Fee(Education_Fee);
                                        mdueee.setExam_fee(Exam_fee);
                                        mdueee.setHostel_Fee(Hostel_Fee);
                                        mdueee.setOthers(Others);
                                        mduesList.add(mdueee);
                                    } else if (transaction_status.matches("due")) {

                                    }


                                } else {

                                }

                                duesadapter = new MyPaidDuesAdapter(PaidPaymentList.this, mduesList);
                                Rv_MY_Dues.setAdapter(duesadapter);

                            }


                        }else {
                            showAlertDialogFinish(PaidPaymentList.this,"No Paid Dues");
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            myduesdisplaydisplay = null;
            hideLoading();


        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
