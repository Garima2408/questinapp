package co.questin.paymentactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import co.questin.R;
import co.questin.studentprofile.MyDues;
import co.questin.utils.SessionManager;

public class DuesDash extends AppCompatActivity {
ImageView onlinepay,duedetails,paiddetails,feesummary,logo,mainheader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dues_dash);
        onlinepay = findViewById(R.id.onlinepay);
        duedetails = findViewById(R.id.duedetails);
        mainheader = findViewById(R.id.mainheader);
        logo =findViewById(R.id.logo);

        if (SessionManager.getInstance(this).getCollage().getField_group_image() !=null) {
            Log.e("TAG", "getField_group_image: " +SessionManager.getInstance(this).getCollage().getField_group_image());
            Glide.with(this).load(SessionManager.getInstance(this).getCollage().getField_group_image())
                    .placeholder(R.mipmap.header_image).dontAnimate()
                    .fitCenter().into(mainheader);



        }


        if (SessionManager.getInstance(this).getCollage().getField_groups_logo() !=null) {
            Log.e("TAG", "logo2: " +SessionManager.getInstance(this).getCollage().getField_groups_logo());
            Glide.with(this).load(SessionManager.getInstance(this).getCollage().getField_groups_logo())
                    .placeholder(R.mipmap.appicon).dontAnimate()
                    .fitCenter().into(logo);



        }


        onlinepay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent(DuesDash.this, MyDues.class));

            }
        });


        duedetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent(DuesDash.this, PaidPaymentList.class));

            }
        });


/*

        paiddetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent(DuesDash.this, MyDues.class));

            }
        });

        feesummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent(DuesDash.this, MyDues.class));

            }
        });
*/

    }
}
