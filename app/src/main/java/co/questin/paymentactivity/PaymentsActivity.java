package co.questin.paymentactivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class PaymentsActivity extends BaseAppCompactActivity {

    String orderId,mid,customerid,Feeid,Totalfee;
    String GeneratedChecksum ,GeneratedChecksum2;
    String  URL ="https://questin.co/sites/all/libraries/paytm_checksum/generateChecksum.php";
    String CALLBACK_URL ="https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";
    private ProcessToGenerateChecksum generatechecksum = null;
    private ProcessToGenrateOrderId generateorderid = null;
    private PaytmPGServiceexample Service = null;
    private PaytmStatusTransa statusapi = null;
    private PaytmStatusTransactionToServer Transactionstatustoserver = null;
    String Txn;
    String  PAYTMSTATUS ="https://securegw-stage.paytm.in/merchant-status/getTxnStatus?JsonData=";
    JSONArray studentselected;
    JSONObject shareAttandanceJson;
    private static ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
        Intent intent = getIntent();
        Feeid = intent.getStringExtra("DUE_ID");
        Totalfee = intent.getStringExtra("DUE_FEE");
        spinner = findViewById(R.id.progressBar);
        generateorderid = new ProcessToGenrateOrderId();
        generateorderid.execute(Feeid);

        mid ="Questi04900730080518";
        customerid ="Questin111";


        }

    private class ProcessToGenrateOrderId extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_GENERATEMYORDERID+"/"+SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()+"/"+Feeid,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                PaymentsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            generateorderid = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        JSONObject data = responce.getJSONObject("data");
                        if (data != null && data.length() > 0) {
                            orderId = data.getString("ORDER_ID");
                            customerid = "CUST_ID"+data.getString("CUST_ID");
                            Log.d("TAG", "orderId: " + orderId +".."+customerid +".."+Totalfee);
                        }

                        if (orderId !=null &&orderId.length() >0){
                            generatechecksum = new ProcessToGenerateChecksum();
                            generatechecksum.execute();

                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            generateorderid = null;
            hideLoading();


        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }


    private class ProcessToGenerateChecksum extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("MID","Questi04900730080518")
                    .add("ORDER_ID", orderId)
                    .add("CUST_ID", customerid)
                    .add("INDUSTRY_TYPE_ID", "Retail")
                    .add("CHANNEL_ID", "WAP")
                    .add("TXN_AMOUNT", Totalfee)
                    .add("WEBSITE", "APPSTAGING")
                    .add( "MOBILE_NO" , "9669249016")
                    .add( "EMAIL" , "garimashukla2408@gmail.com")
                    .add( "CALLBACK_URL" , CALLBACK_URL+orderId)
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URL, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                PaymentsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            generatechecksum = null;
            try {
                if (responce != null) {
                    hideLoading();

                    JSONObject jsonObject = new JSONObject(responce.toString());

                    GeneratedChecksum = jsonObject.has("CHECKSUMHASH") ? jsonObject.getString("CHECKSUMHASH") : "";

                    // Toast.makeText(PaymentsActivity.this, GeneratedChecksum, Toast.LENGTH_SHORT).show();
                    Log.d("chekhash",GeneratedChecksum);

                    if (GeneratedChecksum !=null && GeneratedChecksum.length()>0){
                        onStartTransaction();

                    }
                    }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            generatechecksum = null;
            hideLoading();


        }
    }



    public void onStartTransaction() {

        PaytmPGService Service = PaytmPGService.getStagingService();


        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put( "EMAIL" , "garimashukla2408@gmail.com");
        paramMap.put("MID","Questi04900730080518");
        paramMap.put("CALLBACK_URL",CALLBACK_URL+orderId);
        paramMap.put("TXN_AMOUNT", Totalfee);
        paramMap.put("ORDER_ID", orderId);
        paramMap.put("WEBSITE", "APPSTAGING");
        paramMap.put( "MOBILE_NO" , "9669249016");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("CHECKSUMHASH" , GeneratedChecksum);
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("CUST_ID", customerid);

        PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);

        Service.initialize(Order, null);

        Service.startPaymentTransaction(this, true, true,
                new PaytmPaymentTransactionCallback() {

                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        Log.d("LOG", "UI Error Occur.");

                        Toast.makeText(getApplicationContext(), " UI Error Occur. ", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction : " + inResponse);
                        //  Toast.makeText(getApplicationContext(), "Payment Transaction response "+inResponse.toString(), Toast.LENGTH_LONG).show();

/*Payment Transaction : Bundle[{STATUS=TXN_SUCCESS,
CHECKSUMHASH=+5kXzrna8E8YnKzjJxzghRugFJe9mIWF2TZVzmnI+i23IGQMhMazUSfN0cLO1yN+pviCBif59SdiITD8uD1Uj6jtOUkNojia/1gkyC941Zk=,
 BANKNAME=WALLET, ORDERID=33, TXNAMOUNT=35.00, TXNDATE=2018-10-31 11:51:35.0, MID=Questi04900730080518,
  TXNID=20181031111212800110168306200023970, RESPCODE=01, PAYMENTMODE=PPI, BANKTXNID=5754484,
  CURRENCY=INR, GATEWAYNAME=WALLET, RESPMSG=Txn Success}]*/

                        String STATUS = inResponse.getString("STATUS");
                        String RESPCODE = inResponse.getString("RESPCODE");

                        if (RESPCODE.matches("01")){
                            CallTheTransactionStatusapi();
                        }



                    }

                    @Override
                    public void networkNotAvailable() {
                        Log.d("LOG", "UI Error Occur.");

                        Toast.makeText(getApplicationContext(), " UI Error Occur. ", Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        Log.d("LOG", "UI Error Occur.");

                        Toast.makeText(getApplicationContext(), " Severside Error "+ inErrorMessage, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {

                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        // TODO Auto-generated method stub
                        finish();

                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                        Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                        finish();
                    }

                });
    }

    private void CallTheTransactionStatusapi() {

        Service = new PaytmPGServiceexample();
        Service.execute();

        //  PaytmPGService Service


    }

    private class PaytmPGServiceexample extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("MID","Questi04900730080518")
                    .add("ORDER_ID", orderId)

                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URL, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                PaymentsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                    }
                });

            }
            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            Service = null;
            try {
                if (responce != null) {
                    hideLoading();

                    JSONObject jsonObject = new JSONObject(responce.toString());

                    GeneratedChecksum2 = jsonObject.has("CHECKSUMHASH") ? jsonObject.getString("CHECKSUMHASH") : "";

                    // Toast.makeText(PaymentsActivity.this, GeneratedChecksum2, Toast.LENGTH_SHORT).show();
                    Log.d("chekhash",GeneratedChecksum2);


                    if (GeneratedChecksum2 !=null &&GeneratedChecksum2.length() >0){
                        onStartTransactionStatusApi();

                    }




                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            Service = null;
            hideLoading();


        }
    }

    private void onStartTransactionStatusApi() {
        studentselected = new JSONArray();

        shareAttandanceJson = new JSONObject();
        try {
            shareAttandanceJson.put("MID", "Questi04900730080518");
            shareAttandanceJson.put("ORDERID", orderId);
            shareAttandanceJson.put("CHECKSUMHASH", GeneratedChecksum2);

            studentselected.put(shareAttandanceJson);


            Log.d("TAG", "studentselected: " + studentselected);
            Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        statusapi = new PaytmStatusTransa();
        statusapi.execute(String.valueOf(shareAttandanceJson));

    }


    private class PaytmStatusTransa extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, PAYTMSTATUS+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {

                e.printStackTrace();
                PaymentsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            statusapi = null;

            if (responce != null) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(responce.toString());
                    Txn = jsonObject.has("TXNID") ? jsonObject.getString("TXNID") : "";


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*responseData: {"TXNID":"20180601111212800110168087300019353","BANKTXNID":"","ORDERID":"2","TXNAMOUNT":"1.00","STATUS":"TXN_SUCCESS","TXNTYPE":"SALE","GATEWAYNAME":"WALLET","RESPCODE":"01","RESPMSG":"Txn Successful.","BANKNAME":"WALLET","MID":"Questi04900730080518","PAYMENTMODE":"PPI","REFUNDAMT":"0.00","TXNDATE":"2018-06-01 13:57:15.0"}
                 */

                /*{"TXNID":"20181031111212800110168311800020826","BANKTXNID":"5754158","ORDERID":"32","TXNAMOUNT":"10.00","STATUS":"TXN_SUCCESS","TXNTYPE":"SALE","GATEWAYNAME":"WALLET","RESPCODE":"01","RESPMSG":"Txn Success","BANKNAME":"WALLET","MID":"Questi04900730080518","PAYMENTMODE":"PPI","REFUNDAMT":"0.00","TXNDATE":"2018-10-31 11:22:29.0"}*/

                if (Txn!=null &&Txn.length()>0){
                    Transactionstatustoserver = new PaytmStatusTransactionToServer();
                    Transactionstatustoserver.execute(Feeid,Txn);

                }





            }

        }

        @Override
        protected void onCancelled() {
            statusapi = null;



        }
    }


    private class PaytmStatusTransactionToServer extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("status","completed")
                    .add("log", "Mobile: Order completed. Payment was processed successfully through mobile.")
                    .add("field_transaction_id", args[1])
                    .build();

            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_STATUSSENDTOSERVER+"/"+orderId ,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {

                e.printStackTrace();
                PaymentsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.questin.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            Transactionstatustoserver = null;

            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Intent i =new Intent(PaymentsActivity.this, PaymentDetails.class);
                        startActivity(i);
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }
        @Override
        protected void onCancelled() {
            Transactionstatustoserver = null;



        }
    }





}
