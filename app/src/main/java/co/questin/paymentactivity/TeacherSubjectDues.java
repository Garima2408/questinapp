package co.questin.paymentactivity;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.R;
import co.questin.adapters.TeacherStudentDuesAdapter;
import co.questin.models.CoursemoduleArray;
import co.questin.models.StudentListsArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.SessionManager;
import okhttp3.OkHttpClient;

public class TeacherSubjectDues extends BaseAppCompactActivity {

    private RecyclerView.LayoutManager layoutManager;
    private TeacherStudentDuesAdapter studentadapter;
    Spinner SubjectsList;
    FrameLayout top;
    View view1;
    public  ArrayList<CoursemoduleArray> mCouseList;
    private ArrayList<String> courselist;
    private  MyCoursesListDisplay mycourseslistdisplay = null;
    private StudentListDisplay studentlist = null;
    public ArrayList<StudentListsArray> mStudentList;
    RecyclerView rv_student_inClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher_subject_dues);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        SubjectsList =findViewById(R.id.SubjectsList);
        layoutManager = new LinearLayoutManager(this);
        rv_student_inClass = findViewById(R.id.rv_student_inClass);
        rv_student_inClass.setHasFixedSize(true);
        rv_student_inClass.setLayoutManager(layoutManager);

        view1 =findViewById(R.id.view1);
        top =findViewById(R.id.top);
        mCouseList=new ArrayList<>();
        courselist=new ArrayList<>();
        mStudentList=new ArrayList<>();

        mycourseslistdisplay = new MyCoursesListDisplay();
        mycourseslistdisplay.execute();

    }

    private class MyCoursesListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYCOURSES+"?"+"uid"+"="+SessionManager.getInstance(getActivity()).getUser().userprofile_id+"&"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&offset="+"0"+"&limit=120");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherSubjectDues.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            mycourseslistdisplay = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                mCouseList.add(CourseInfo);
                                courselist.add(CourseInfo.getTitle());

                                SubjectsList.setAdapter(new ArrayAdapter<String>(TeacherSubjectDues.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        courselist));
                                SubjectsList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                        String  SubjectId = mCouseList.get(position).getTnid();
                                        String SubjectTittle = mCouseList.get(position).getTitle();
                                        mStudentList.clear();
                                        studentlist = new StudentListDisplay();
                                        studentlist.execute(SubjectId);



                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            }



                        }else{
                            top.setVisibility(View.GONE);
                            showAlertDialogFinish(TeacherSubjectDues.this,getString(co.questin.R.string.No_course));


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            mycourseslistdisplay = null;
            hideLoading();


        }
    }


    private class StudentListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_STUDENTATTENDANCELIST+"/"+args[0]+"/"+"og");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherSubjectDues.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                StudentListsArray StudentInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), StudentListsArray.class);
                                mStudentList.add(StudentInfo);
                                studentadapter = new TeacherStudentDuesAdapter(TeacherSubjectDues.this, mStudentList, "");
                                rv_student_inClass.setAdapter(studentadapter);
                                view1.setVisibility(View.VISIBLE);
                            }

                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherSubjectDues.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(R.string.No_Student_subject))
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentlist = null;
            hideLoading();


        }
    }


}