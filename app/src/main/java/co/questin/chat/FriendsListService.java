package co.questin.chat;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.questin.Event.ServerMemberUpdate;
import co.questin.models.chat.NewChatModel;
import co.questin.models.userFriendsResponse.Datum;
import co.questin.models.userFriendsResponse.UserFriendsResponseModel;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;

/**
 * Created by HP on 4/12/2018.
 */

public class FriendsListService extends IntentService {

    private Gson gson;
    private List<NewChatModel> frrindList;
    GetFriendsServerList getFriendsServerList = null;

    public FriendsListService() {
        super("FriendsListService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //use for clear snappy db
        DB snappyDB = null;
        try {
            snappyDB = DBFactory.open(this);
            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {

                snappyDB.destroy();

            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        getFriendsServerList = new GetFriendsServerList();
        getFriendsServerList.execute();
    }



    private class GetFriendsServerList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {


            JSONObject jsonObject = null;

            try {
                final String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(), URLS.URL_MYFRIENDS + "?" + "email=" + SessionManager.getInstance(FriendsListService.this).getUser().getEmail());

                jsonObject = new JSONObject(response);
                Log.d("TAG", "responseData: " + response);

            } catch (JSONException | IOException e) {




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            try {
                if (responce != null) {

                    String errorCode = responce.getString("status");
                    // final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        gson = new Gson();
                        UserFriendsResponseModel userFriendsResponseModel =
                                gson.fromJson(String.valueOf(responce), UserFriendsResponseModel.class);
                        // saveFriendsLIstData(userFriendsResponseModel);
                        Log.e("response services", userFriendsResponseModel.toString() + " size " + userFriendsResponseModel.getData().size());

                        Log.e("offline save", "data");

                        try {
                            DB snappyDB = null;
                            snappyDB = DBFactory.open(getApplicationContext());
                            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {
                                UserFriendsResponseModel datum = null;
                                datum = snappyDB.getObject(Constants.CART_FRIENDS_LIST, UserFriendsResponseModel.class);
                                Log.e("offline user size", "" + datum.getData().size());

                                datum=new UserFriendsResponseModel();
                                List<Datum> data = new ArrayList<>();

//                                List<String> ListIds = new ArrayList<>();
//                                for (int i = 0; i < datum.getData().size(); i++) {
//                                    ListIds.add(datum.getData().get(i).getUid());
//                                }

                                for (int j = 0; j < userFriendsResponseModel.getData().size(); j++) {
//                                    if (!ListIds.contains(userFriendsResponseModel.getData().get(j).getUid())) {
//
//
//                                    }
                                    Datum datum1 = new Datum();
                                    datum1.setUid(userFriendsResponseModel.getData().get(j).getUid());
                                    datum1.setFieldFirstname(userFriendsResponseModel.getData().get(j).getFieldFirstname());
                                    datum1.setFieldLastname(userFriendsResponseModel.getData().get(j).getFieldLastname());
                                    datum1.setPicture(userFriendsResponseModel.getData().get(j).getPicture());
                                    data.add(datum1);
//
                                }
                                try {
                                    datum.getData().addAll(data);
                                    snappyDB.put(Constants.CART_FRIENDS_LIST, datum);
                                }catch (Exception e){

                                }


                                //snappyDB.close();


                            } else {

                                try {
                                    UserFriendsResponseModel datum = new UserFriendsResponseModel();
                                    datum.setData(userFriendsResponseModel.getData());
                                    snappyDB.put(Constants.CART_FRIENDS_LIST, datum);
                                    snappyDB.close();
                                }catch  (Exception e){

                                }


                            }

                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }catch (NullPointerException n)
                        {
                            n.printStackTrace();
                        }
                        try {
                            EventBus.getDefault().post(new ServerMemberUpdate(1));
                            ConversationsActivity.serverMemberUpdate=true;
                            stopSelf();
                        }catch (Exception e){

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {


                    }
                }
            } catch (JSONException e) {

            }
        }

        @Override
        protected void onCancelled() {


        }




        private void saveFriendsLIstData(UserFriendsResponseModel userFriendsResponseModel) {


        }
    }
}

/*

                    }*/

