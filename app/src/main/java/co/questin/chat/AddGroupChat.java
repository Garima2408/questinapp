package co.questin.chat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.questin.R;
import co.questin.database.QuestinSQLiteHelper;
import co.questin.models.chat.GroupConversationModel;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import co.questin.utils.Constants;
import co.questin.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class AddGroupChat extends BaseAppCompactActivity {
    String groupname, GroupId, uid, GroupMemberId,groupImage,admin;
    ImageView circleView;
    public String is_admin ="TRUE";
    EditText Group_Name;
    FloatingActionButton fab;
    FriendMembersAdapter friendmemberAdapter;
    RecyclerView SelectedName_Lists;
    RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<String> selectedstudent;
    private ArrayList<String> UIDlists;
    private QuestinSQLiteHelper questinSQLiteHelper;
    private GroupConversationListAdapter conversationListAdapter;
    private static final String TAG = "ConversationsActivity";
    private int RC_NEW_GROUPCHAT = 2;
    private int RC_NEW_CHAT = 1;
    Bitmap thumbnail = null;
    public static final int RequestPermissionCode = 1;
    Boolean CallingCamera,CallingGallary;
    String strFile = null;
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private int SELECT_FILE = 1;
    Uri imageUri;
    private String mImageFileLocation;
    String Fileimagename;
    RequestBody body;

    private static final int EXTERNAL_STORAGE_PERMISSION_REQUEST = 23;
    private CreateChatGroup groupchatelist = null;
    private CreateChatGroupMember groupmemberchatelist = null;
    private ProgressUpdateUserProfile updateProfileAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Bundle b = getIntent().getExtras();


        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        selectedstudent = b.getStringArrayList("selectedItems");
        conversationListAdapter = new GroupConversationListAdapter(new ArrayList<GroupConversationModel>(), this);

        admin = SessionManager.getInstance(getActivity()).getUser().getUserprofile_id();
        UIDlists = new ArrayList<>(); /*all friend list*/


        Log.d("TAG", "resultArr: " + selectedstudent);
        String[] outputStrArr = new String[selectedstudent.size()];

        for (int x = 0; x < selectedstudent.size(); x++) {
            outputStrArr[x] = selectedstudent.get(x);
            Log.d("TAG", " outputprint[x]: " + outputStrArr[x]);


            String[] parts =  outputStrArr[x] .split(",");
            uid = parts[0];
            String name = parts[1] + " " + parts[2];
            String picture = parts[3];
            GroupMemberId = uid;
            UIDlists.add(uid);

        }


        Log.d("TAG", "GroupMemberId: " + GroupMemberId);
        Log.d("TAG", "UIDlists: " + UIDlists.toString());


        SelectedName_Lists = findViewById(R.id.SelectedName_Lists);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        SelectedName_Lists.setLayoutManager(mLayoutManager);
        SelectedName_Lists.setHasFixedSize(true);
        friendmemberAdapter = new FriendMembersAdapter(getApplicationContext(), R.layout.list_friend_member, selectedstudent);
        SelectedName_Lists.setAdapter(friendmemberAdapter);
        Group_Name = findViewById(R.id.Group_Name);
        groupname = Group_Name.getText().toString().trim();
        circleView = findViewById(R.id.circleView);
        fab = findViewById(R.id.fab);

        circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showImageDialog();

            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                addnewGroupName();



            }
        });


    }
    private void showImageDialog() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(AddGroupChat.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    circleView.setImageDrawable(null);
                    CallingCamera =true;
                    CallingGallary =false;
                    if(checkPermission()){
                        CallCamera();
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    circleView.setImageDrawable(null);
                    CallingGallary =true;
                    CallingCamera =false;
                    if(checkPermission()){
                        galleryIntent();
                    }
                } else if (items[item].equals("Cancel")) {

                    Glide.with(AddGroupChat.this).load(getUser().photo)
                            .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                            .fitCenter().into(circleView);

                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void CallCamera() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(AddGroupChat.this, "co.questin.fileprovider",photoFile);
        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }


    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }


    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {

            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)


                onSelectFromGalleryResult(data);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, data);

        }
        else if (resultCode == RESULT_CANCELED) {

            circleView.setImageBitmap(decodeImageBase64(getUser().photo));
        }
    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();
            }catch (Exception e)
            {
                e.printStackTrace();
            }


        }

    }



    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,1000);
                strFile = encodeImageTobase64(thumbnail);
                System.out.println("camera " + strFile);
                circleView.setImageBitmap(thumbnail);

              /*  if (strFile !=null){
                    updateProfileAuthTask = new ProgressUpdateUserProfile();
                    updateProfileAuthTask.execute(strFile);


                }else {
                    showSnackbarMessage("There seems to be some problem.please select again");

                }
*/
            } catch (IOException e) {
                e.printStackTrace();
            }




        } else if (resultCode == Crop.RESULT_ERROR) {
            //  Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void  addnewGroupName() {


        Group_Name.setError(null);
        // Store values at the time of the login attempt.
        groupname = Group_Name.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(groupname)) {
            focusView = Group_Name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));


        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD GROP MEMBER*/
            groupchatelist = new CreateChatGroup();
            groupchatelist.execute();

        }
    }


    private class CreateChatGroup extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .add("type", "chat_group")
                    .add("title", groupname)
                    .add("og_group_ref", "109193")
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_CREATEGROUP, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AddGroupChat.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupchatelist = null;
            try {
                if (responce != null) {
                    hideLoading();


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject data = responce.getJSONObject("data");

                        GroupId = data.getString("nid");
                        String body = data.getString("uri");

                        if (strFile!=null   &&  Fileimagename !=null ){
                            updateProfileAuthTask = new ProgressUpdateUserProfile();
                            updateProfileAuthTask.execute(strFile);
                        }else{
                        }

                        AddToDataBase();

                        if (GroupId != null) {


                            CallToAddMemberInGroup();


                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            groupchatelist = null;
            hideLoading();


        }
    }

    private void CallToAddMemberInGroup() {
        for (int i = 0; i < UIDlists.size(); i++) {
            System.out.println(UIDlists.get(i));
            groupmemberchatelist = new CreateChatGroupMember();
            groupmemberchatelist.execute(UIDlists.get(i));

        }
    }





    /*2195 sourabh sir,2148 nilash,2102akash*/


    private class CreateChatGroupMember extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()

                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_CREATEGROUPMEMBER +"/"+ GroupId +"/"+args[0], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);
                Log.d("TAG", "args[0]: " + args[0]);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AddGroupChat.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberchatelist = null;
            try {
                if (responce != null) {
                    hideLoading();


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        // showAlertDialog(msg);


                        hideLoading();
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            groupmemberchatelist = null;
            hideLoading();


        }
    }

    private void AddToDataBase() {

        GroupConversationModel conversationModel = new GroupConversationModel()
                .setGroupId(GroupId)
                .setSenderId(SessionManager.getInstance(this).getUser().getUserprofile_id())
                .setName(groupname)
                .setDpUrl(groupImage);

        QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(this);
        conversationListAdapter.addAllData(questinSQLiteHelper.getGroupConversationsList());

        Intent backIntent = new Intent(this, GroupChatActivity.class)
                .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesGroupConversationExist(conversationModel.getGroupId()))
                .putExtra(Constants.CONVERSATION_MODEL, conversationModel)
                .putExtra("GroupID", GroupId)
                .putExtra("GroupName", groupname)
                .putExtra("GroupImage",groupImage)
                .putExtra("ADMIN",admin)
                .putExtra("is_ADMIN",is_admin)
                .putExtra("open", "activityaddGroupdata");

        startActivity(backIntent);

        finish();

    }







    public void addGroupProfile(){
        updateProfileAuthTask = new ProgressUpdateUserProfile();
        updateProfileAuthTask.execute();

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private class ProgressUpdateUserProfile extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            if (strFile!=null   &&  Fileimagename !=null ){
                body = new FormBody.Builder()
                        .add("file", strFile)
                        .add("filename",Fileimagename)
                        .build();


            }

            try {
                String responseData = ApiCall.POSTHEADER(client,URLS.URL_UPDATEGROUPPHOTO+"/"+GroupId,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AddGroupChat.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            updateProfileAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject jsonArrayData = responce.getJSONObject("data");

                        groupImage=jsonArrayData.getString("icon_url");
                        // showToast(msg);

                        AddToDataBase();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            updateProfileAuthTask = null;
            hideLoading();


        }
    }


    @Override
    public void onBackPressed() {
        finish();

    }
    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }
}


