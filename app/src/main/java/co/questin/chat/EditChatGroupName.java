package co.questin.chat;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.questin.R;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseAppCompactActivity;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class EditChatGroupName extends BaseAppCompactActivity {

    Button cancel_action, ok;
    EditText firstname;
    String groupId,groupName,is_admin;
    public boolean isCreated;
    RequestBody body;
    ProgressUpdateGroupName progressUpdateGroupName = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_chat_group_name);


        cancel_action = findViewById(R.id.cancel_action);
        ok = findViewById(R.id.ok);
        firstname = findViewById(R.id.firstname);


        Bundle b = getIntent().getExtras();
        groupId = b.getString("GROUP_ID");
        groupName = b.getString("GROUP_NAME");
        isCreated = b.getBoolean("isCreated");
        is_admin=b.getString("is_ADMIN");


        firstname.setText(groupName);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressUpdateGroupName = new ProgressUpdateGroupName();

                progressUpdateGroupName.execute();
              /*  Intent i = new Intent(EditChatGroupName.this,ChatGroupMemberProfile.class);
                startActivity(i);*/

            }
        });

        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }


    private class ProgressUpdateGroupName extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("title", firstname.getText().toString()).add("nid", groupId)
                    .build();


            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_UPDATEGROUPNAME + "/" + groupId, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                EditChatGroupName.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // firstname.setText(groupName);
                        ChatGroupMemberProfile.groupName=firstname.getText().toString();
                        finish();
                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {

            hideLoading();


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }
}