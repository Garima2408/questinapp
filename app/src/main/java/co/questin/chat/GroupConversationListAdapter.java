package co.questin.chat;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.questin.R;
import co.questin.models.chat.GroupConversationModel;
import co.questin.utils.Constants;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by farheen on 13/9/17
 */

public class GroupConversationListAdapter extends RecyclerView.Adapter<GroupConversationListAdapter.ConversationListHolder>{

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<GroupConversationModel> data;

    public GroupConversationListAdapter(ArrayList<GroupConversationModel> data, Context context){
        mContext = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ConversationListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConversationListHolder(inflater.inflate(R.layout.conversation_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(final ConversationListHolder holder, int position) {
        GroupConversationModel currentData = data.get(position);
        Glide.with(mContext).load(currentData.getDpUrl()).placeholder(R.mipmap.placeholder_dp)
                .fitCenter().into(holder.imageDp);
        holder.textName.setText(currentData.getName());
        holder.textLastMsg.setText(currentData.getLastMsgText());
        holder.textLastMsgTime.setText(currentData.getLastMsgTime());

        if(currentData.getUnreadCount() > 0){
            holder.textUnreadMsgs.setVisibility(View.VISIBLE);
            holder.textUnreadMsgs.setText(String.valueOf(currentData.getUnreadCount()));
        }
        else {
            holder.textUnreadMsgs.setVisibility(View.GONE);
        }

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chatIntent = new Intent(mContext, GroupChatActivity.class)
                        .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, false)
                        .putExtra(Constants.CONVERSATION_MODEL, data.get(holder.getAdapterPosition()));
                mContext.startActivity(chatIntent);
            }
        });
    }

    public void addData(GroupConversationModel conversationModel){
        data.add(conversationModel);
        notifyItemInserted(data.size() - 1);
    }

    public void addAllData(ArrayList<GroupConversationModel> list){
        data.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ConversationListHolder extends RecyclerView.ViewHolder{

       CircleImageView imageDp;
        TextView textName;
        TextView textLastMsg;
        TextView textLastMsgTime;
        TextView textUnreadMsgs;

        View rootView;

        ConversationListHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            imageDp =itemView.findViewById(R.id.image_dp);
            textName =itemView.findViewById(R.id.text_name);
            textLastMsg =itemView.findViewById(R.id.text_last_message);
            textLastMsgTime =itemView.findViewById(R.id.text_last_message_time);
            textUnreadMsgs =itemView.findViewById(R.id.text_unread_messages);



        }
    }
}
