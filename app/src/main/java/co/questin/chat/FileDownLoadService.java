package co.questin.chat;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import co.questin.database.QuestinSQLiteHelper;
import co.questin.models.chat.ChatModel;
import co.questin.utils.Constants;

/**
 * Created by HP on 4/6/2018.
 */

public class FileDownLoadService extends IntentService {

    private static final int BUFFER_SIZE = 4096;


    public FileDownLoadService(){
        super("ImageDownloadThread");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        assert intent != null;
        final ChatModel chatModel = intent.getParcelableExtra(Constants.CHAT_MODEL);
        final String senderId = intent.getStringExtra(Constants.SENDER_ID);

        try {
            downloadFile(senderId,chatModel,this);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void downloadFile(String senderId, ChatModel chatModel, FileDownLoadService activity) throws IOException {
        final QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(activity);

        URL url = new URL(chatModel.getLink());
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();



            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + chatModel.getText());

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Questin";

            File dir = new File(path);
            if(!dir.exists())
                dir.mkdirs();
            File file = new File(dir, chatModel.getText());
            //String saveFilePath = "" + File.separator + fileName;
            //String saveFilePath=file;
            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(file);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }


            outputStream.close();

            inputStream.close();
            chatModel.setLink(file.getAbsolutePath());

            questinSQLiteHelper.updateChatMessageLink(chatModel, senderId);
                                    LocalBroadcastManager.getInstance(activity)
                                            .sendBroadcast(new Intent(Constants.ACTION_IMAGE_DOWNLOADED)
                                                    .putExtra(Constants.CHAT_MODEL, chatModel)
                                                    .putExtra(Constants.SENDER_ID, senderId)
                                            );
            System.out.println("File downloaded");
        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();
    }
}
