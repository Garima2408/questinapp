package co.questin.chat;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.questin.Event.ConversationFire;
import co.questin.R;
import co.questin.models.chat.GroupConversationModel;
import co.questin.models.chat.MyChatGroupArray;
import co.questin.network.ApiCall;
import co.questin.network.OkHttpClientObject;
import co.questin.network.URLS;
import co.questin.utils.BaseFragment;
import co.questin.utils.SessionManager;
import co.questin.utils.Utils;
import okhttp3.OkHttpClient;

public class MyChatsGroups extends BaseFragment {
    static RecyclerView MyGroupsName_list;
    FloatingActionButton btnNewChat;
    public static ArrayList<MyChatGroupArray> mchatmemberList;
    public ArrayList<GroupConversationModel> databasedataList;
    private static MyChatGroupsAdapters chatmemberadapter;
    private RecyclerView.LayoutManager layoutManager;
    private static ChatGroupMemberDisplay groupmemberlist = null;
    public static String groupId,title;
    private int RC_NEW_GROUP_CHAT = 1;
    static Activity activity;
    static LinearLayout notChat;
    private static ProgressBar spinner;

    public MyChatsGroups() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_my_chats_groups, container, false);
        activity = (Activity)view.getContext();
        mchatmemberList=new ArrayList<>();
        databasedataList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(activity);
        MyGroupsName_list = view.findViewById(R.id.MyGroupsName_list);
        spinner= view.findViewById(R.id.progressBar);
        btnNewChat =view.findViewById(R.id.btn_new_chat);
        notChat =view.findViewById(R.id.notChat);
        MyGroupsName_list.setHasFixedSize(true);
        MyGroupsName_list.setLayoutManager(layoutManager);
        RefreshChatListWhileAdding();
        btnNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newChatIntent = new Intent(activity, AddGroupChatList.class);
                startActivityForResult(newChatIntent, RC_NEW_GROUP_CHAT);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //  RefreshChatListWhileAdding();
    }

    public static void RefreshChatListWhileAdding() {
        mchatmemberList.clear();

        if (SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {

            groupmemberlist = new ChatGroupMemberDisplay();
            groupmemberlist.execute(SessionManager.getInstance(activity).getUser().getParentUid());


        }else {
            groupmemberlist = new ChatGroupMemberDisplay();
            groupmemberlist.execute(SessionManager.getInstance(activity).getUser().getUserprofile_id());

        }




    }

    private static class ChatGroupMemberDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYGROUPSMEMBER+"?"+"uid="+args[0]+"&limit=100&offset=0");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData );

            } catch ( Exception e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberlist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        mchatmemberList.clear();


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if (jsonArrayData !=null && jsonArrayData.length()>0) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                MyChatGroupArray memberInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyChatGroupArray.class);
                                mchatmemberList.add(memberInfo);
                                chatmemberadapter = new MyChatGroupsAdapters(activity, mchatmemberList);
                                MyGroupsName_list.setAdapter(chatmemberadapter);
                                notChat.setVisibility(View.GONE);
                                MyGroupsName_list.setVisibility(View.VISIBLE);

                            }
                        }else {
                            notChat.setVisibility(View.VISIBLE);
                            MyGroupsName_list.setVisibility(View.GONE);
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        //   showAlertFragmentDialog(activity,"Information", msg);

                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            groupmemberlist = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ConversationFire event) {

        chatmemberadapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
